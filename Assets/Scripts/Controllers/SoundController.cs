﻿/*
 * @Author: CuongNH
 * @Description: Xu ly cac hieu ung am thanh cua game
 * */

using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour {

    public static SoundController sound;

    public AudioClip[] soundClips;
	[Range(0,1)]
	public float click_Volume;

	public AudioClip swap_Sound;
	[Range(0,1)]
	public float swap_Volume;

	public AudioClip eat_Sound;
	[Range(0,1)]
	public float eat_Volume;

    public AudioClip[] eatItemSoundClips;
	[Range(0,1)]
	public float boostitem_Volume;
	[Range(0,1)]
	public float boom_Volume;
	[Range(0,1)]
	public float special_Volume;

	public AudioClip wrong_Sound;
	[Range(0,1)]
	public float wrong_Volume;

	public AudioClip win_Sound;
	[Range(0,1)]
	public float win_Volume;

	public AudioClip ice_Sound;
	[Range(0,1)]
	public float ice_Volume;

	public AudioClip winStar_Sound;
	[Range(0,1)]
	public float winStar_Volume;

    //public AudioClip[] createItemSoundClips;

    //public AudioClip[] levelSoundClips;

    //public AudioClip[] popupSoundClips;

    public AudioSource audioSource;

    private int countMatchSwap = 0;
    private int numberMatchSwap = 4;


	public AudioClip cowSound;
	[Range(0,1)]
	public float cow_Volume;

	public AudioClip blendedSound;
	[Range(0,1)]
	public float blended_Volume;

	public AudioClip orderSound;
	[Range(0,1)]
	public float order_Volume;

	public AudioClip rocketSound;
	[Range(0,1)]
	public float rocket_Volume;

	public AudioClip starSound;
	[Range(0,1)]
	public float star_Volume;

	public AudioClip dropSound;
	[Range(0,1)]
	public float drop_Volume;

	public AudioClip glassSound;
	[Range(0,1)]
	public float glass_Volume;

	public AudioClip effStarSound;
	[Range(0,1)]
	public float effStar_Volume;

	public AudioClip rewardedSound;
	[Range(0,1)]
	public float rewarded_Volume;

	public AudioClip iceCreamSound;
	[Range(0,1)]
	public float iceCream_Volume;

    // Use when object have awaked
    void Awake()
    {
        if (sound == null)
        {
            sound = this;

            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            DestroyImmediate(this.gameObject);
        }
    }

	public void rocket_Sound()
	{
		audioSource.PlayOneShot(rocketSound, rocket_Volume);
	}

	public void Swap_Move()
	{
		audioSource.PlayOneShot(swap_Sound, swap_Volume);
	}

    // Turn on sound
    public void SoundOn()
    {
        audioSource.mute = false;
    }

    // Turn off sound
    public void SoundOff()
    {
        audioSource.mute = true;
    }

    // Sound when click on buttons
    public void Click()
    {
		audioSource.PlayOneShot(soundClips[0], click_Volume);
    }

    bool isPlayMatchSwap = false;
    // Sound when match move
    public void MatchSwap(float time = GameConstants.TIME_CLEAR_TILE)
    {
        if (!isPlayMatchSwap)
        {
            isPlayMatchSwap = true;

			audioSource.PlayOneShot(eat_Sound, eat_Volume);

            countMatchSwap++;
            if (countMatchSwap == numberMatchSwap)
            {
                countMatchSwap = 0;
            }

            LeanTween.delayedCall(time, () =>
                {
                    isPlayMatchSwap = false;
                });
        }
    }

	public void OrderSound()
	{
		audioSource.PlayOneShot (orderSound, order_Volume);
	}

	public void BlendedSound()
	{
		audioSource.PlayOneShot (blendedSound, blended_Volume);
	}

	public void CowSound()
	{
		audioSource.PlayOneShot (cowSound, cow_Volume);
	}

	public void EatSoundPlay()
	{
		audioSource.PlayOneShot (eat_Sound, eat_Volume);
	}

	public void IceSoundPlay()
	{
		audioSource.PlayOneShot (ice_Sound, ice_Volume);
	}

	public void WinStarSoundPlay()
	{
		audioSource.PlayOneShot (winStar_Sound, winStar_Volume);
	}

    // Sound when wrong move
    public void WrongSwap()
    {
		audioSource.PlayOneShot(wrong_Sound, wrong_Volume);
    }

	public void StarSoundPlay()
	{
		audioSource.PlayOneShot(starSound, star_Volume);
	}

	public void dropSoundPlay()
	{
		audioSource.PlayOneShot(dropSound, drop_Volume);
	}

	public void glassSoundPlay()
	{
		audioSource.PlayOneShot(glassSound, glass_Volume);
	}

	public void effStarPlay()
	{
		audioSource.PlayOneShot(effStarSound, effStar_Volume);
	}

	public void rewardedPlay()
	{
		audioSource.PlayOneShot(rewardedSound, rewarded_Volume);
	}

	public void iceCreamPlay()
	{
		audioSource.PlayOneShot(iceCreamSound, iceCream_Volume);
	}

    #region
    // Sound when spider combo
    public void Hammer()
    {
		audioSource.PlayOneShot(eatItemSoundClips[0], boostitem_Volume);
    }

    bool isPlayExplodeBoom = false;
    // Sound when boom exploded
    public void ExplodeBoom(float time = GameConstants.TIME_CLEAR_TILE)
    {
        if (!isPlayExplodeBoom)
        {
            isPlayExplodeBoom = true;

			audioSource.PlayOneShot(eatItemSoundClips[1], boom_Volume);

            countMatchSwap++;
            if (countMatchSwap == numberMatchSwap)
            {
                countMatchSwap = 0;
            }

            LeanTween.delayedCall(time, () =>
            {
                isPlayExplodeBoom = false;
            });
        }
    }

    bool isPlayExplodeThunder = false;
    // Sound when thunder exploded
    public void ExplodeThunder(float time = GameConstants.TIME_CLEAR_TILE)
    {
        if (!isPlayExplodeThunder)
        {
            isPlayExplodeThunder = true;

			audioSource.PlayOneShot(eatItemSoundClips[2], special_Volume);

            countMatchSwap++;
            if (countMatchSwap == numberMatchSwap)
            {
                countMatchSwap = 0;
            }

            LeanTween.delayedCall(time, () =>
            {
                isPlayExplodeThunder = false;
            });
        }
    }
    #endregion
}

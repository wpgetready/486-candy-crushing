﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CoinController : MonoBehaviour 
{
	public GameObject Shopsss;
	public GameObject Coinsss;
	GameObject objectTarget;
	public GameObject CoinBg;
	public GameObject Bg2;
	public PopupGame popupGame;
	public AllPanel allPanel;

	public void MoveDown(int chooseObject)
	{
		SoundController.sound.Click ();
		if (chooseObject == 1) 
		{
			objectTarget = Shopsss;
		} 
		else 
		{
			objectTarget = Coinsss;
		}
		objectTarget.SetActive (true);
		objectTarget.transform.position = new Vector3 (0,11f);
		iTween.MoveTo(objectTarget, iTween.Hash("position", new Vector3(0,0,0), "time", 0.25f, "easetype", iTween.EaseType.easeOutBack));	
	}

	public void MoveUp()
	{
		SoundController.sound.Click ();
		iTween.MoveTo(Bg2, iTween.Hash("position", new Vector3(0,0,0), "time", 0.25f, "easetype", iTween.EaseType.easeOutBack));	
		iTween.MoveTo(objectTarget, iTween.Hash("position", new Vector3(0,11f,0), "time", 0.25f, "easetype", iTween.EaseType.easeOutBack, "oncomplete", "HideOnjectTarget", "oncompletetarget", gameObject));
	}

	public void MoveDown()
	{
		SoundController.sound.Click ();


		if (popupGame != null) 
		{
			if (Bg2.activeInHierarchy == false) 
			{
				if (CoinBg != null) 
				{
					CoinBg.SetActive (false);
				}
				iTween.MoveTo (objectTarget, iTween.Hash ("position", new Vector3 (0, -11f, 0), "time", 0.25f, "easetype", iTween.EaseType.easeOutBack, "oncomplete", "HideOnjectTarget", "oncompletetarget", gameObject));
			}

			if (popupGame.dialogBuyMove.activeInHierarchy == true) {
				popupGame._Coins ();
				iTween.MoveTo (popupGame.dialogBuyMove, iTween.Hash ("position", new Vector3 (0, 0, 0), "time", 0.25f, "easetype", iTween.EaseType.easeOutBack));
				iTween.MoveTo (objectTarget, iTween.Hash ("position", new Vector3 (0, 11f, 0), "time", 0.25f, "easetype", iTween.EaseType.easeOutBack, "oncomplete", "HideOnjectTarget", "oncompletetarget", gameObject));
			}
		}
		else 
		{
			if(CoinBg != null)
			{
				CoinBg.SetActive(false);
			}
			iTween.MoveTo(Bg2, iTween.Hash("position", new Vector3(0,0,0), "time", 0.25f, "easetype", iTween.EaseType.easeOutBack));	
			iTween.MoveTo(objectTarget, iTween.Hash("position", new Vector3(0,-11f,0), "time", 0.25f, "easetype", iTween.EaseType.easeOutBack, "oncomplete", "HideOnjectTarget", "oncompletetarget", gameObject));
			if(allPanel.popupStart.activeInHierarchy == true)
			{
				allPanel.popupStart.SetActive (false);
			}
		}
	}

	void HideOnjectTarget()
	{
		objectTarget.SetActive (false);
	}
}

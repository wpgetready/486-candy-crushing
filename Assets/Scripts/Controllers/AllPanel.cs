﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Facebook.Unity;
using Facebook.MiniJSON;
using SimpleJSON;
using System.Collections.Generic;
using System;

public class AllPanel : MonoBehaviour 
{
	public GameObject getCoinFree, shop, dailyReward, Setting, loginfb, bg2, invitefb, hideInvitefb;
	public Text title;
	GameObject CurrentObjActive;
	[HideInInspector]
	public bool isShow;
	Vector3 abc;
	public GameObject friendItem;
	public Transform friendListBoard;
	public GameObject invitePanel;
	List<GameObject> friendList;
	List<string> friendIDsFromFB;
	string[] friendName;
	int count;
	List<Toggle> friendToggle;
	public Toggle selectAll;
	string[] listToSend;
	public GameObject sidePPos;
	Vector3 posBG;
	public GameObject firstPanel;
	public GameObject[] allButtongBg;
	GameObject switchButtonBg;
	[HideInInspector]
	public string savedName;
	public SizeScroll sizeScroll;
	public GameObject getCoin;
	public GameObject bg;
	public GameObject _start;
	[HideInInspector]
	public Vector3 posSidePanel;
	[HideInInspector]
	public Vector3 pos;
	public GameObject popupStart;
	public GameObject combo;
	public SpinsBT spinsbt;
	public Spins spins;
	public BuyCoinsController buyCoinsController;
	public AllButton allButton;

	void Start()
	{
//		if(popupStart.activeInHierarchy == true)
//		{
//			popupStart.SetActive (false);
//		}

		double _scale = Math.Round((1-(Camera.main.aspect-(double)9/(double)16)*1.2f),2);
		//Debug.Log ("_scale"+_scale);
		Vector3 _number = Vector3.one * (float)_scale;
		//forground.transform.localScale = _number;
		getCoin.transform.localScale = _number;
		bg.transform.localScale = _number;
		_start.transform.localScale = _number;

		transform.localScale = _number;
		//Debug.Log ("localPosition"+sidePanel.GetComponent<RectTransform>().anchoredPosition.x);
		GetComponent<RectTransform>().anchoredPosition = new Vector2 (GetComponent<RectTransform>().anchoredPosition.x*(float)_scale, 
			GetComponent<RectTransform>().anchoredPosition.y*(float)_scale);
		posSidePanel = transform.position;
		sidePPos.GetComponent<RectTransform>().anchoredPosition = new Vector2 (sidePPos.GetComponent<RectTransform>().anchoredPosition.x*(float)_scale, 
			sidePPos.GetComponent<RectTransform>().anchoredPosition.y*(float)_scale);

		sizeScroll.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector2 (sizeScroll.transform.parent.GetComponent<RectTransform>().anchoredPosition.x, 
			sizeScroll.transform.parent.GetComponent<RectTransform>().anchoredPosition.y+(1-(float)_scale)*100*4.5f);

		posBG = bg2.transform.localPosition;
		friendName = new string[26];
		friendList = new List<GameObject>();
		friendToggle = new List<Toggle>();
		abc = GetComponent<RectTransform> ().localPosition;
		//Debug.Log (abc);
		isShow = false;
		getCoinFree.SetActive (false);
		shop.SetActive (false);
		dailyReward.SetActive (false);
		Setting.SetActive (false);
		CurrentObjActive = getCoinFree;
		SetupInvite ();
	}

	public void OnClickButton(string name)
	{
		combo.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, 140f);
		savedName = name;
		SoundController.sound.Click ();
		CurrentObjActive.SetActive (false);
		if (name == "getcoin") 
		{
			CurrentObjActive = getCoinFree;
			title.text = "Free Coins";
			SButtonBG (1);
		} 
		else 
		{
			if(name == "shop")
			{
				CurrentObjActive = shop;
				title.text = "Shop";
				SButtonBG (2);
			}
			else
			{
				if(name == "reward")
				{
					CurrentObjActive = dailyReward;
					title.text = "Daily Reward";
					SButtonBG (0);
				}
				else
				{
					CurrentObjActive = Setting;
					title.text = "Setting";
					SButtonBG (3);
					int number = PlayerPrefs.GetInt("loginfb");
					if (number == 1) 
					{
						if (name == "invite") {
							InviteFriends (true);
						}
					}
				}
			}
		}
		CurrentObjActive.SetActive (true);
		if(CurrentObjActive == Setting)
		{
			int number = PlayerPrefs.GetInt("loginfb");
			if(number==1)
			{
				if (name != "invite") 
				{
					loginfb.SetActive (false);
					invitefb.SetActive (true);
				}
				else 
				{
					loginfb.SetActive (false);
					invitefb.SetActive (false);
					hideInvitefb.SetActive (true);
				}
			}
			else
			{
				loginfb.SetActive(true);
				invitefb.SetActive(false);
			}
		}
		if(isShow == false)
		{
			firstPanel.SetActive (true);
			firstPanel.GetComponent<Image> ().enabled = true;
			iTween.MoveTo(gameObject, iTween.Hash("position", sidePPos.transform.position, "time", 0.25f, "easetype", iTween.EaseType.easeInBack));
			//iTween.MoveTo(bg2, iTween.Hash("position", new Vector3(0,0,0), "time", 0.25f, "easetype", iTween.EaseType.easeOutBack, "oncomplete", "HideOnjectTarget", "oncompletetarget", gameObject));	
			isShow = true;
		}
	}

//	public void CheckCoins()
//	{
//		int coins = PlayerPrefs.GetInt ("coins");
//		if (coins >= PopupStartController.priceItem) 
//		{
//			dialogBuyMoveBT.gameObject.SetActive (true);
//			getFreeCoinsBT.gameObject.SetActive (false);
//			buyCoinsBT.gameObject.SetActive (false);
//		} 
//		else 
//		{
//			dialogBuyMoveBT.gameObject.SetActive (false);
//			getFreeCoinsBT.gameObject.SetActive (true);
//			buyCoinsBT.gameObject.SetActive (true);
//		}
//	}

	public void SButtonBG(int i)
	{
		SoundController.sound.Click ();
		if(switchButtonBg != null)
		{
			switchButtonBg.SetActive (false);
		}
		allButtongBg [i].SetActive (true);
		switchButtonBg = allButtongBg [i];
	}

	public void CloseButton(bool panel = false)
	{
		SoundController.sound.Click ();
		//firstPanel.SetActive (panel);
		firstPanel.GetComponent<Image>().enabled = panel;
		iTween.MoveTo(gameObject, iTween.Hash("position", posSidePanel, "time", 0.25f, "easetype", iTween.EaseType.easeOutBack));
		switchButtonBg.SetActive (false);
		CurrentObjActive.SetActive (false);
		switchButtonBg.SetActive (false);
		if(popupStart.activeInHierarchy == true)
		{
			popupStart.SetActive (false);
		}
		isShow = false;
	}

	public void CloseButtonbg2()
	{
		//iTween.MoveTo(bg2, iTween.Hash("position", new Vector3(0,0,0), "time", 0.25f, "easetype", iTween.EaseType.easeOutBack, "oncomplete", "HideOnjectTarget", "oncompletetarget", gameObject));	
		iTween.MoveTo(gameObject, iTween.Hash("position", posBG, "time", 0.25f, "easetype", iTween.EaseType.easeOutBack));
		isShow = false;
	}

	public void CloseButtonOther(GameObject obj)
	{
		iTween.MoveTo(obj, iTween.Hash("position", new Vector3(0,11f,0), "time", 0.25f, "easetype", iTween.EaseType.easeOutBack, "oncomplete", "HideObjectTarget", "oncompletetarget", gameObject));
	}

	void HideObjectTarget()
	{
		invitePanel.transform.position = new Vector3 (0,11,0);
		foreach(GameObject item in friendList)
		{
			Destroy(item);
		}
		SetupInvite ();
		//invitePanel.SetActive (false);
	}

	public void SetupInvite()
	{
		count = 0;
		FB.API("/me/invitable_friends?fields=id,first_name,picture&limit=26", HttpMethod.GET, this.ListFriends);
	}

	public void InviteFriends(bool value)
	{
		SoundController.sound.Click ();
		invitePanel.SetActive (value);
	}

	private void ListFriends(IGraphResult result)
	{
		if (string.IsNullOrEmpty(result.Error) && result.Texture != null)
		{
			//Debug.Log(result.RawResult);
			StartCoroutine( FriendsHndlr(result.RawResult, result));
		}
	}

	IEnumerator FriendsHndlr (string FBresult, IGraphResult _result)
	{            
		var dict = Json.Deserialize(FBresult) as Dictionary<string,object>;
		var friendList = new List<object>();
		friendList = (List<object>)(dict["data"]);
		var ST = JSON.Parse(FBresult);
		var N = JSONNode.Parse (ST["data"].ToString());
		int _friendCount = friendList.Count;
		Debug.Log("Found friends on FB, _friendCount ... " +_friendCount);

		friendIDsFromFB = new List<string>();
		for (int i=0; i<_friendCount; i++) 
		{
			string friendToken = getDataValueForKey( (Dictionary<string,object>)(friendList[i]), "id");

			//string friendFBID = getDataValueForKey( (Dictionary<string,object>)(friendList[i]), "id");

			friendName[i] = getDataValueForKey( (Dictionary<string,object>)(friendList[i]), "first_name");
			//var infolist = new List<object>();
			//infolist = ((Dictionary<string,object>)(friendList[i]))[""];
			string urlImg = N[i]["picture"]["data"]["url"];//Debug.Log("urlImg.Texture:   "+urlImg);
			WWW sprFB = new WWW (urlImg);
			yield return sprFB;
			Texture2D _sprFB = sprFB.texture;
			//Debug.Log("_result.Texture:   "+_result.Texture);

			LoadFriendImgFromID(_sprFB, i);

			friendIDsFromFB.Add(friendToken);
			//Debug.Log("friendFBID:   "+friendToken);
		}
	}

	private void LoadFriendImgFromID (/*string userID,*/Texture2D imageFB, int i)
	{
		GameObject obj = Instantiate(friendItem)as GameObject;
		obj.transform.SetParent(friendListBoard);
		obj.transform.localScale = new Vector3 (1,1,1);
		obj.transform.localPosition = Vector3.zero;
		obj.transform.GetChild (2).GetComponent<RawImage> ().texture = imageFB;
		obj.transform.GetChild (3).GetComponent<Text> ().text = friendName[i];
		friendToggle.Add(obj.transform.GetChild (4).GetComponent<Toggle> ());
		count++;
		friendList.Add (obj);
		//Debug.Log ("friendtoggle count: " + friendToggle.Count);
	}

	private string getDataValueForKey(Dictionary<string, object> dict, string key) {
		object objectForKey;
		if (dict.TryGetValue(key, out objectForKey)) {
			return (string)objectForKey;
		} else {
			return "";
		}
	}

	public void SellectAll()
	{
		SoundController.sound.Click ();
		if (selectAll.isOn == true) 
		{
			foreach (Toggle item in friendToggle) 
			{
				item.isOn = true;
			}
		} 
		else 
		{
			foreach (Toggle item in friendToggle) 
			{
				item.isOn = false;
			}
		}
	}

	public void SendButton()
	{
		SoundController.sound.Click ();
		List<string> listSend = new List<string>();
		listToSend = new string[friendIDsFromFB.Count];
		listToSend = friendIDsFromFB.ToArray ();
		for(int i = 0; i<listToSend.Length; i++)
		{
			if(friendToggle[i].isOn == true)
			{
				listSend.Add(listToSend[i].ToString());
				//Debug.Log ("send to:..."+listToSend[i].ToString());
			}

		}
		RequestChallenge (listSend);
	}

	private static int numberSend;

	void RequestChallenge (List<string> listSend)
	{
		List<string> recipient = new List<string>();
		string title, message, data = string.Empty;
		title = "Match 3 Game!";
		message = "Best game is here. Check it out!";
		recipient = new List<string> ();
		recipient = listSend;
		numberSend = listSend.Count;
		        FB.AppRequest(
		            message,
		            recipient,
		            null,
		            null,
		            null,
		            data,
		            title,
		            AppRequestCallback
		            );
	}

	private static void AppRequestCallback (IAppRequestResult result)
	{
		// Error checking
		Debug.Log ("AppRequestCallback");
		if (result.Error != null) {
			Debug.LogError (result.Error);
			return;
		}
		Debug.Log (result.RawResult);
		
		// Check response for success - show user a success popup if so
		object obj;
		if (result.ResultDictionary.TryGetValue ("cancelled", out obj)) {
			Debug.Log ("Request cancelled");
		} else if (result.ResultDictionary.TryGetValue ("did_complete", out obj)) 
		{
			Debug.Log ("Request sent" + obj.ToString());
			if(obj.ToString() == "True")
			{
				AdsControl.instance.addCoins.AddReward (true,10*numberSend);
				Debug.Log ("Request sent");
			}
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class UnityAdsController : MonoBehaviour
{
	
	[SerializeField] private string androidGameId;// = "18658"; //ID for testing
	[SerializeField] private string iosGameId;// = "18660"; //ID for testing
	
	[SerializeField] bool enableTestMode;

	public string zoneId;
	string gameId;

	void Start()
	{
		InitUnityAdv ();
	}

	public void InitUnityAdv ()
	{
		gameId = null;
		
		#if UNITY_IOS // If build platform is set to iOS...
		gameId = iosGameId;
		#elif UNITY_ANDROID // Else if build platform is set to Android...
		gameId = androidGameId;
		#endif
		
//		if (string.IsNullOrEmpty(gameId)) { // Make sure the Game ID is set.
//			Debug.LogError("Failed to initialize Unity Ads. Game ID is null or empty.");
//		} else if (!Advertisement.isSupported) {
//			Debug.LogWarning("Unable to initialize Unity Ads. Platform not supported.");
//		} else if (Advertisement.isInitialized) {
//			Debug.Log("Unity Ads is already initialized.");
//		} else {
//			Debug.Log(string.Format("Initialize Unity Ads using Game ID {0} with Test Mode {1}.",
//			                        gameId, enableTestMode ? "enabled" : "disabled"));
		Advertisement.Initialize(gameId, enableTestMode);
//		}
	}

	public bool IsLoaded()
	{
		if (Advertisement.IsReady ("rewardedVideo") == true) 
		{
			return true;
		} 
		return false;
	}

	public void ShowAds ()
	{
		ShowOptions options = new ShowOptions();
		options.resultCallback = HandleShowResult;
		if(Advertisement.IsReady(zoneId) == true)
		{
			AdsControl.instance.OnHideUnity (false);
			Advertisement.Show (zoneId, options);
			#if UNITY_IOS
			SoundController.sound.audioSource.mute = true;
			MusicController.music.audioSource.mute = true;
			#endif
		}
	}
	
	private void HandleShowResult (ShowResult result)
	{
		#if UNITY_IOS
		SoundController.sound.audioSource.mute = false;
		MusicController.music.audioSource.mute = false;
		#endif
		switch (result)
		{
		case ShowResult.Finished:
			Debug.Log ("Video completed. User rewarded " + AddCoins.rewardAds + " credits.");
			AdsControl.instance.addCoins.AddReward(true, AddCoins.rewardAds);
			InitUnityAdv();
			break;
		case ShowResult.Skipped:
			Debug.LogWarning ("Video was skipped.");
			break;
		case ShowResult.Failed:
			Debug.LogError ("Video failed to show.");
			if(Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork || Application.internetReachability ==NetworkReachability.ReachableViaCarrierDataNetwork)
			{
				Debug.Log("vungle not load");
				InitUnityAdv();
			}
			break;
		}
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpinsBT : MonoBehaviour
{
	public Text starsCount;
	int stars;
	public GameObject spins;
	public AllPanel allpanel;

	IEnumerator Start()
	{
		stars = PlayerPrefs.GetInt ("totalStars", 0);
		starsCount.text = "" + stars;

		while (true)
		{
			yield return new WaitForSeconds (7);
			LeanTween.rotate(spins.GetComponent<RectTransform>(), -(360*3), 3).setEase(LeanTweenType.easeOutCubic);
		}
	}

	public bool SetStarsCount()
	{
		if(Spins.stateSpins == 1)
		{
			stars -= 15;
			starsCount.text = "" + stars;
			PlayerPrefs.SetInt ("totalStars", stars);
			PlayerPrefs.Save ();

			allpanel.spins.starsText.text = "" + stars;
			return true;
		}
		else
		{
			if (Spins.stateSpins == 2)
			{
				int coins = PlayerPrefs.GetInt ("coins");
				if (coins > 15) {
					coins -= 15;
					PlayerPrefs.SetInt ("coins", coins);
					PlayerPrefs.Save ();
					allpanel.spins.CoinsText.text = "" + coins;
					allpanel.buyCoinsController.CoinNumbers.text = "" + coins;
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		return false;
	}

	public void ShowSpins()
	{
		allpanel.spins.ShowSpins ();
	}
}

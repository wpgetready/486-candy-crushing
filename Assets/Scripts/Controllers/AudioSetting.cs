﻿/*
 * @Author: CuongNH
 * @Description: Xu ly viec setting on/off cho music/sound
 * */

using UnityEngine;
using System.Collections;

public class AudioSetting : MonoBehaviour {

    //public static AudioSetting audio;

    public GameObject musicOnBtn;
    public GameObject musicOffBtn;
    public GameObject soundOnBtn;
    public GameObject soundOffBtn;

    private bool musicOn = true;
    private bool soundOn = true;

    private const string MUSIC = "MUSIC";
    private const string SOUND = "SOUND";
    private const string ON_STATE = "ON";
    private const string OFF_STATE = "OFF";

    // Use when object have awaked
    //void Awake()
    //{
    //    if (audio == null)
    //    {
    //        DontDestroyOnLoad(gameObject);

    //        audio = this;
    //    }
    //    else if (audio != this)
    //    {
    //        Destroy(gameObject);
    //    }
    //}

    // Use this for initialization
    void Start()
    {
        // Load audio setting
		if(!PlayerPrefs.GetString(MUSIC).Equals(OFF_STATE))
            PlayerPrefs.SetString(MUSIC, ON_STATE);
		if(!PlayerPrefs.GetString(SOUND).Equals(OFF_STATE))
            PlayerPrefs.SetString(SOUND, ON_STATE);
        //PlayerPrefs.DeleteAll();
		//Debug.Log(PlayerPrefs.GetString(MUSIC));
        print(PlayerPrefs.GetString("MUSIC") + ":" + PlayerPrefs.GetString("SOUND"));
        LoadAudioSetting();
    }

    // Turn on/off music btn
    public void HandleMusicBtn()
    {
        SoundController.sound.Click();
        //print(MusicController.music.audioSource.mute);
        if (musicOn)
        {
            MusicOff();

            PlayerPrefs.SetString(MUSIC, OFF_STATE);
        }
        else
        {
            MusicOn();

            PlayerPrefs.SetString(MUSIC, ON_STATE);
        }
    }

    // Turn off music
    void MusicOff()
    {
        musicOn = false;

        MusicController.music.MusicOff();

        musicOnBtn.SetActive(false);
        musicOffBtn.SetActive(true);
    }

    // Turn on music
    void MusicOn()
    {
        musicOn = true;

        MusicController.music.MusicOn();

        musicOffBtn.SetActive(false);
        musicOnBtn.SetActive(true);
    }

    // Turn on/off sound btn
    public void HandleSoundBtn()
    {
        SoundController.sound.Click();

        if (soundOn)
        {
			StartCoroutine (waitClick());
           

            PlayerPrefs.SetString(SOUND, OFF_STATE);
        }
        else
        {
            SoundOn();

            PlayerPrefs.SetString(SOUND, ON_STATE);
        }
    }

	IEnumerator waitClick()
	{
		yield return new WaitForSeconds (0.2f);
		SoundOff();
	}
    // Turn off sound
    void SoundOff()
    {
        soundOn = false;

        SoundController.sound.SoundOff();

        soundOnBtn.SetActive(false);
        soundOffBtn.SetActive(true);
    }

    // Turn on sound
    void SoundOn()
    {
        soundOn = true;

        SoundController.sound.SoundOn();

        soundOffBtn.SetActive(false);
        soundOnBtn.SetActive(true);
    }

    // Load audio setting from PlayerPref
    void LoadAudioSetting()
    {
        string musicState = PlayerPrefs.GetString(MUSIC);
        //Debug.Log("musicState: " + musicState);
        if (OFF_STATE.Equals(musicState))
        {
            MusicOff();
        }
        else
        {
            MusicOn();
        }

        string soundState = PlayerPrefs.GetString(SOUND);
        //Debug.Log("soundState: " + soundState);
        if (OFF_STATE.Equals(soundState))
        {
            SoundOff();
        }
        else
        {
            SoundOn();
        }
    }
}

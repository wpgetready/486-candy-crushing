﻿/*
 * @Author: CuongNH
 * @Description: Hien thi man hinh loading trong khi cho load vao cac man choi
 * */

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadingHandler : MonoBehaviour {

    public static LoadingHandler loading;
    public GameObject fade;
    public UnityEngine.UI.Image processbar;

    void Awake()
    {
        if (loading == null)
        {
            loading = this;
        }
        else if (loading != this)
        {
            Destroy(gameObject);
        }
    }

	// Handle loading
    public IEnumerator HandleLoading(string sceneName = GameConstants.GAMEPLAY_SCENE_NAME, float waitTime = 0.2f)
    {
        Debug.Log("HandleLoading....");

        processbar.transform.parent.transform.parent.transform.gameObject.SetActive(true);

        Animator fadeOut = fade.GetComponent<Animator>();
        if (fadeOut != null)
        {
            fadeOut.SetBool("isFadeOut", false);
            fadeOut.SetBool("isFadeOut", true);
        }

        processbar.fillAmount = 0f; // 0.28f;

		AsyncOperation async = SceneManager.LoadSceneAsync (sceneName);

		while (async.progress < 0.89)
        {
			if(processbar != null)
			{
				processbar.fillAmount = async.progress;
	            yield return null;
			}
        }

		if(processbar != null)
		{
        	processbar.fillAmount = 1f;
		}
          Debug.Log("Done");

        yield return null;
    }
}

﻿/*
 * @Author: CuongNH
 * @Description: Load scene loading to prepare load game
 * */

using UnityEngine;
using System.Collections;

public class LoadingScene : MonoBehaviour {

    public string sceneLoading = GameConstants.HOME_SCENE_NAME;
    public float delayTime = 1f;

	void Start ()
	{
		if (UnityEngine.SceneManagement.SceneManager.GetActiveScene ().buildIndex != 1) 
		{
			StartCoroutine (LoadingHandler.loading.HandleLoading (sceneLoading, delayTime));
		}
	}

//    void FinishLoading()
//    {
//        Application.LoadLevel(sceneLoading);
//    }
}

﻿using UnityEngine;
using System.Collections;

public class sugarColor : MonoBehaviour 
{
	public SpriteRenderer[] piece_sugar;

	void Start()
	{
		for(int i = 0; i < piece_sugar.Length; i++)
		{
			piece_sugar [i].enabled = false;
		}
	}

	public bool SetandShow(string _color)
	{
		for(int i = 0; i < piece_sugar.Length; i++)
		{
			if (piece_sugar [i].gameObject.name == _color) 
			{
				if (piece_sugar [i].enabled == true) 
				{
					return false;
				} 
				else 
				{
					piece_sugar [i].enabled = true;
					return true;
				}
			}
		}
		return false;
	}
}

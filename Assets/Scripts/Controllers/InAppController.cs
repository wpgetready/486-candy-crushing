﻿using UnityEngine;
using System.Collections;
using OnePF;
using System.Collections.Generic;
using UnityEngine.UI;

public class InAppController : MonoBehaviour
{
	public Text CoinNumbers;
	public GameObject RemovesAdsUnit;

	public void ButtonBuy(string sku_name)
	{
		OpenIAB.purchaseProduct(sku_name);
	}
		
	const string SKU = "sku";
	string _label = "";
	bool _isInitialized = false;
	Inventory _inventory = null;

	private void Awake()
	{
		// Listen to all events for illustration purposes
		OpenIABEventManager.billingSupportedEvent += billingSupportedEvent;
		OpenIABEventManager.billingNotSupportedEvent += billingNotSupportedEvent;
		OpenIABEventManager.queryInventorySucceededEvent += queryInventorySucceededEvent;
		OpenIABEventManager.queryInventoryFailedEvent += queryInventoryFailedEvent;
		OpenIABEventManager.purchaseSucceededEvent += purchaseSucceededEvent;
		OpenIABEventManager.purchaseFailedEvent += purchaseFailedEvent;
		OpenIABEventManager.consumePurchaseSucceededEvent += consumePurchaseSucceededEvent;
		OpenIABEventManager.consumePurchaseFailedEvent += consumePurchaseFailedEvent;

		if(AdsControl.isNoAds == "true")
		{
			RemovesAdsUnit.SetActive (false);
		}
	}
	private void OnDestroy()
	{
		// Remove all event handlers
		OpenIABEventManager.billingSupportedEvent -= billingSupportedEvent;
		OpenIABEventManager.billingNotSupportedEvent -= billingNotSupportedEvent;
		OpenIABEventManager.queryInventorySucceededEvent -= queryInventorySucceededEvent;
		OpenIABEventManager.queryInventoryFailedEvent -= queryInventoryFailedEvent;
		OpenIABEventManager.purchaseSucceededEvent -= purchaseSucceededEvent;
		OpenIABEventManager.purchaseFailedEvent -= purchaseFailedEvent;
		OpenIABEventManager.consumePurchaseSucceededEvent -= consumePurchaseSucceededEvent;
		OpenIABEventManager.consumePurchaseFailedEvent -= consumePurchaseFailedEvent;
	}
	private void Start()
	{
		// Map skus for different stores       
		#if UNITY_ANDROID
		OpenIAB.mapSku("com.hudd.removeads", OpenIAB_Android.STORE_GOOGLE, "com.hudd.removeads");//android.test.purchased
		OpenIAB.mapSku("com.hudd.jewels.0.99", OpenIAB_Android.STORE_GOOGLE, "com.hudd.jewels.0.99");
		OpenIAB.mapSku("com.hudd.jewels.1.99", OpenIAB_Android.STORE_GOOGLE, "com.hudd.jewels.1.99");
		OpenIAB.mapSku("com.hudd.jewels.4.99", OpenIAB_Android.STORE_GOOGLE, "com.hudd.jewels.4.99");
		OpenIAB.mapSku("com.hudd.jewels.9.99", OpenIAB_Android.STORE_GOOGLE, "com.hudd.jewels.9.99");
		OpenIAB.mapSku("com.hudd.jewels.19.99", OpenIAB_Android.STORE_GOOGLE, "com.hudd.jewels.19.99");
		OpenIAB.mapSku("com.hudd.jewels.29.99", OpenIAB_Android.STORE_GOOGLE, "com.hudd.jewels.29.99");
		OpenIAB.mapSku("com.hudd.jewels.49.99", OpenIAB_Android.STORE_GOOGLE, "com.hudd.jewels.49.99");
		OpenIAB.mapSku("com.hudd.jewels.99.99", OpenIAB_Android.STORE_GOOGLE, "com.hudd.jewels.99.99");
		#endif
		#if UNITY_IOS
		OpenIAB.mapSku("com.hudd.removeads", OpenIAB_iOS.STORE, "com.hudd.removeads");
		OpenIAB.mapSku("com.hudd.jewels.0.99", OpenIAB_iOS.STORE, "com.hudd.jewels.0.99");
		OpenIAB.mapSku("com.hudd.jewels.1.99", OpenIAB_iOS.STORE, "com.hudd.jewels.1.99");
		OpenIAB.mapSku("com.hudd.jewels.4.99", OpenIAB_iOS.STORE, "com.hudd.jewels.4.99");
		OpenIAB.mapSku("com.hudd.jewels.9.99", OpenIAB_iOS.STORE, "com.hudd.jewels.9.99");
		OpenIAB.mapSku("com.hudd.jewels.19.99", OpenIAB_iOS.STORE, "com.hudd.jewels.19.99");
		OpenIAB.mapSku("com.hudd.jewels.29.99", OpenIAB_iOS.STORE, "com.hudd.jewels.29.99");
		OpenIAB.mapSku("com.hudd.jewels.49.99", OpenIAB_iOS.STORE, "com.hudd.jewels.49.99");
		OpenIAB.mapSku("com.hudd.jewels.99.99", OpenIAB_iOS.STORE, "com.hudd.jewels.99.99");
		#endif
		//OpenIAB.mapSku(SKU, OpenIAB_iOS.STORE, "sku");
		//OpenIAB.mapSku(SKU, OpenIAB_WP8.STORE, "ammo");

		//Khoi tao inapp
		// Application public key
		if (!_isInitialized)
		{

			var options = new Options();

			var publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiJ2Uh3abImB1dIqXX2pk044QbArxEW5ulaoRSdPq6jzJpTlOSAGbM4SKeuMnjz3hxE5d+TNNdVwaV/r6VBJ5HRqeaFRv5ukwcdkeo7pxn7WvWBqVv78k0IBpBQufJP/Nw8BoVNy6eRolZYUK80Dswnyi8GoU89w0vesjsXO4LKGHEEA5IOTQL0wf+xmE9CAPI1hFiuqwNuM1N0CNlF3Eta4uomuAESC8iT0X3pe1KuuPDBgNNTVFHVzPoDHSsQQ3ryze8dXgbHK2/k7Izzi0rnTD0NvDSUdtmlV/+UL3oSfQ1gib9SkzchnFzxo/n1tlIL6eEx9jvnhpO8THAOtCSwIDAQAB";
			options.checkInventoryTimeoutMs = Options.INVENTORY_CHECK_TIMEOUT_MS * 2;
			options.discoveryTimeoutMs = Options.DISCOVER_TIMEOUT_MS * 2;
			options.checkInventory = false;
			options.verifyMode = OptionsVerifyMode.VERIFY_SKIP;
			#if UNITY_ANDROID
			options.prefferedStoreNames = new string[] { OpenIAB_Android.STORE_GOOGLE };
			options.availableStoreNames = new string[] { OpenIAB_Android.STORE_GOOGLE };
			options.storeKeys = new Dictionary<string, string> { { OpenIAB_Android.STORE_GOOGLE, publicKey } };
			#endif
			options.storeSearchStrategy = SearchStrategy.INSTALLER_THEN_BEST_FIT;
			// Transmit options and start the service

			OpenIAB.init(options);
		}
	}
	private void billingSupportedEvent()
	{
		_isInitialized = true;
		Debug.Log("billingSupportedEvent");
	}
	private void billingNotSupportedEvent(string error)
	{
		Debug.Log("billingNotSupportedEvent: " + error);
	}
	private void queryInventorySucceededEvent(Inventory inventory)
	{
		Debug.Log("queryInventorySucceededEvent: " + inventory);
		if (inventory != null)
		{
			_label = inventory.ToString();

			List<Purchase> prods = inventory.GetAllPurchases();
			foreach (Purchase p in prods) OpenIAB.consumeProduct(p);
		}
	}
	private void queryInventoryFailedEvent(string error)
	{
		Debug.Log("queryInventoryFailedEvent: " + error);
		_label = error;
	}
	private void purchaseSucceededEvent(Purchase purchase)
	{
		switch (purchase.Sku)
		{
		case "com.hudd.removeads":
			PlayerPrefs.SetString ("isNoAds", "true");
			PlayerPrefs.Save ();
			AdsControl.isNoAds = "true";
			RemovesAdsUnit.SetActive (false);
			OpenIAB.consumeProduct(purchase);
			break;
		case "com.hudd.jewels.0.99":
			AdsControl.instance.addCoins.AddReward (true,20);
			OpenIAB.consumeProduct(purchase);
			break;
		case "com.hudd.jewels.1.99":
			AdsControl.instance.addCoins.AddReward (true,40);
			OpenIAB.consumeProduct(purchase);
			break;
		case "com.hudd.jewels.4.99":
			AdsControl.instance.addCoins.AddReward (true,100);
			OpenIAB.consumeProduct(purchase);
			break;
		case "com.hudd.jewels.9.99":
			AdsControl.instance.addCoins.AddReward (true,200);
			OpenIAB.consumeProduct(purchase);
			break;
		case "com.hudd.jewels.19.99":
			AdsControl.instance.addCoins.AddReward (true,400);
			OpenIAB.consumeProduct(purchase);
			break;
		case "com.hudd.jewels.29.99":
			AdsControl.instance.addCoins.AddReward (true,600);
			OpenIAB.consumeProduct(purchase);
			break;
		case "com.hudd.jewels.49.99":
			AdsControl.instance.addCoins.AddReward (true,1000);
			OpenIAB.consumeProduct(purchase);
			break;
		case "com.hudd.jewels.99.99":
			AdsControl.instance.addCoins.AddReward (true,2000);
			OpenIAB.consumeProduct(purchase);
			break;
		default:
			break;
		}
	}
	private void purchaseFailedEvent(int errorCode, string errorMessage)
	{
		Debug.Log("purchaseFailedEvent: " + errorMessage);
		_label = "Purchase Failed: " + errorMessage;
	}
	private void consumePurchaseSucceededEvent(Purchase purchase)
	{
		Debug.Log("consumePurchaseSucceededEvent: " + purchase);
		_label = "CONSUMED: " + purchase.ToString();
	}
	private void consumePurchaseFailedEvent(string error)
	{
		Debug.Log("consumePurchaseFailedEvent: " + error);
		_label = "Consume Failed: " + error;
	}

	public static void ShowInapp()
	{
//		GameObject.FindWithTag("Inapp").GetComponent<DialogInapp>().ShowDialog();
	}
}

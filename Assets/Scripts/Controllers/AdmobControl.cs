﻿using System;
using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;
using System.Collections.Generic;
using UnityEngine.iOS;
public class AdmobControl : MonoBehaviour 
{
	public string bannerAdmobID_Android;
	public string InadsAdmobID_Android;
	public string bannerAdmobID_IOS;
	public string InadsAdmobID_IOS;
	string bannerAdmobID, InadsAdmobID;
	public InterstitialAd interstitial;

	public bool isTest;
	BannerView bannerView;


	void Awake()
	{
		if (isTest == true) {
			bannerAdmobID = "ca-app-pub-3940256099942544/6300978111";
			InadsAdmobID = "ca-app-pub-3940256099942544/1033173712";
		} 
		else 
		{
			#if UNITY_ANDROID
			bannerAdmobID = bannerAdmobID_Android;
			InadsAdmobID = InadsAdmobID_Android;
			#elif UNITY_IOS
			bannerAdmobID = bannerAdmobID_IOS;
			InadsAdmobID = InadsAdmobID_IOS;
			#endif
		}
	}

	public void RequestBannerAdmob()
	{
		// Create a 320x50 banner at the top of the screen.
		bannerView = new BannerView(bannerAdmobID, AdSize.Banner, AdPosition.Bottom);
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();
		// Load the banner with the request.
		bannerView.LoadAd(request);
		bannerView.Hide ();
	}
	
	public bool isHasBanner()
	{
		if(bannerView !=  null)
		{
			return true;
		}
		return false;
	}

	public void ShowBannerAdmob()
	{
		if(bannerView !=  null)
		{
			bannerView.Show ();
		}
	}

	public void HideBannerAdmob()
	{
		if(bannerView !=  null)
		{
			bannerView.Hide ();
		}
	}

	public void ClearBannerAdmob()
	{
		if(bannerView != null)
		{
			bannerView.Hide ();
			bannerView.Destroy ();
		}
	}

	public void RequestInterstitial()
	{
		// Initialize an InterstitialAd.
		interstitial = new InterstitialAd(InadsAdmobID);
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();
		this.interstitial.OnAdClosed += this.HandleInterstitialClosed;
		interstitial.LoadAd(request);
	}

	public void ShowInadsAdmob()
	{
		if (interstitial != null && interstitial.IsLoaded ()) 
		{
			interstitial.Show ();
			#if UNITY_IOS
			SoundController.sound.audioSource.mute = true;
			MusicController.music.audioSource.mute = true;
			#endif
		}
	}

	public void ClearInadsAdmob()
	{
		if (interstitial != null) 
		{
			interstitial.Destroy ();
		}
	}

	public void HandleInterstitialClosed(object sender, EventArgs args)
	{
		#if UNITY_IOS
		SoundController.sound.audioSource.mute = false;
		MusicController.music.audioSource.mute = false;
		#endif
	}
}

﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class DailyRewardedController : MonoBehaviour 
{
	public Sprite[] iReward;
	public Image[] smallIreward;
	public Text[] textReward;
	public Sprite doneReward;
	public Button[] allBT;
	int[] Itype = new int[5];
	int[] IQuantity =  new int[5];
	int type;
	int quantity;
	int count;
	int count2;
	int dateCount;
	string newDates;
	public static bool isOverDay;
	bool isSetReward;
	public GameObject[] rewarded;
	public Image[] bg;
	public Sprite cReward;
	public Sprite disableImage;
	public Image[] lockImage;
	public Sprite normalImage;
	public Text timeText;
	public GameObject[] starsEffect;
	TimeSpan countTime;
	DateTime oldDate;
	private static int setReward;
	public NotifyRwd notifyRwd;
	public GameObject[] rewardNumber;
	bool isSetPanel;

	void Awake()
	{
		setReward = PlayerPrefs.GetInt ("setreward");
		dateCount = PlayerPrefs.GetInt ("datecount");
	}

	void Start()
	{
		if (PlayerPrefs.GetString ("isFTSetReward", "first") == "first")
		{
			PlayerPrefs.SetString ("isFTSetReward", "count");
			PlayerPrefs.Save ();
		}
		else 
		{
			if (dateCount == 0 && PlayerPrefs.GetString ("setrd", "false") == "false") {
				SetReward ();
				PlayerPrefs.SetString ("setrd", "true");
				PlayerPrefs.Save ();
			}
			else 
			{
				PlayerPrefs.SetString ("setrd", "false");
				PlayerPrefs.Save ();
			}
			SetPanel ();
		}
	}

	void Update()
	{
		if(setReward == 1)
		{
			isSetReward = true;
		}
		else
		{
			isSetReward = false;
		}

		dayCheck ();

		if(isSetReward == true && isOverDay == true && isSetPanel == false)
		{
			SetPanel();
		}
	}

	private void dayCheck()
	{
		string stringDate = PlayerPrefs.GetString("PlayDate");
		//Debug.Log (stringDate);
		oldDate = new DateTime ();

		if (stringDate == "" || stringDate == null) 
		{
			oldDate =  DateTime.Now;
		} 
		else 
		{
			oldDate =  Convert.ToDateTime(stringDate);
		}
		
		DateTime newDate = DateTime.Now;
		newDates = Convert.ToString (newDate);
		
		TimeSpan difference = newDate.Subtract(oldDate);

		if (difference.Days >= 1 || stringDate == "") 
		{
			if(stringDate == "")
			{
				stringDate = "-1";
			}
			isOverDay = true;
			if(dateCount == 0 && isSetReward == false)
			{
				Debug.Log ("setReward");
				SetReward ();
				isSetReward = true;
				PlayerPrefs.SetInt("setreward", 1);
				PlayerPrefs.Save();
				setReward = 1;
			}
			starsEffect [dateCount].SetActive (true);
			allBT[dateCount].gameObject.GetComponent<Image>().sprite = cReward;
			allBT [dateCount].interactable = true;
			lockImage [dateCount].gameObject.SetActive (false);
			bg[dateCount].color = new Color(1f,1f,1f,1f);
			int value = PlayerPrefs.GetInt ("day" + dateCount.ToString());

			if(value != 10)
			{
				type = (int)value/10;
			}
			else
			{
				type = 0;
			}

			quantity = value%10;
			if(quantity == 0)
			{
				quantity = 10;
			}

			if (type == 0) {
				rewardNumber [dateCount].SetActive (true);
				rewardNumber [dateCount].GetComponent<Text> ().text = "" + quantity;
			}
			else 
			{
				rewardNumber [dateCount].SetActive (false);
			}
		}
		if (difference.Hours < 24 && stringDate != "-1") 
		{
			timeText.text = "Time: " + string.Format ("{0:00}h {1:00}m {2:00}s", 23 - difference.Hours, 59 - difference.Minutes, 60 - difference.Seconds);
		} 
		else
		{
			timeText.text = "Time: " + string.Format ("{0:00}h {1:00}m {2:00}s", 0, 0, 0);
		}
	}

	public Text _coins;

	public void GetReward(int numberbutton)
	{
		if (isOverDay == true && dateCount == numberbutton) 
		{
			//SoundController.sound.Click ();
			SoundController.sound.rewardedPlay();
			PlayerPrefs.SetString ("PlayDate", newDates);
			int mytype = Itype [numberbutton];
			int myquantity = IQuantity [numberbutton];
			if (mytype == 0) {
				int coins = PlayerPrefs.GetInt("coins");
				coins += myquantity;
				PlayerPrefs.SetInt("coins", coins);
				PlayerPrefs.Save ();
				_coins.text = coins.ToString ();
			}

			if (mytype == 1) {
				int item1count = PlayerPrefs.GetInt ("numberitem1");
				item1count += myquantity;
				PlayerPrefs.SetInt ("numberitem1", item1count);
			}

			if (mytype == 2) {
				int item1count = PlayerPrefs.GetInt ("numberitem2");
				item1count += myquantity;
				PlayerPrefs.SetInt ("numberitem2", item1count);
			}

			if (mytype == 3) {
				int item1count = PlayerPrefs.GetInt ("numberitem3");
				item1count += myquantity;
				PlayerPrefs.SetInt ("numberitem3", item1count);
			}

			smallIreward[dateCount].color = new Color(0.5f,0.5f,0.5f,1f);
			rewarded [dateCount].SetActive(true);// = doneReward;
			bg[dateCount].color = new Color(0.5f,0.5f,0.5f,1f);
			allBT[dateCount].gameObject.GetComponent<Image>().sprite = normalImage;
			textReward[dateCount].text = "Received";
			starsEffect [dateCount].SetActive (false);
			dateCount++;
			Debug.Log ("dateCount"+dateCount);
			if (dateCount == 5)
			{
				dateCount = 0;
				isSetPanel = false;
				isSetReward = false;
			}
			PlayerPrefs.SetInt ("datecount", dateCount);
			PlayerPrefs.Save ();
			allBT[numberbutton].interactable = false;
			setReward = 0;
			notifyRwd.notifyObj.SetActive (false);
			notifyRwd.circle.SetActive (false);
			isOverDay = false;
		}
	}

	void SetPanel()
	{
		isSetPanel = true;
		int[] value = new int[5];
		for(int i = 0; i<5; i++)
		{
			value[i] = PlayerPrefs.GetInt ("day" + i.ToString());
			//Debug.Log (""+value[i]);
			if(value[i] != 10)
			{
				type = (int)value[i]/10;
			}
			else
			{
				type = 0;
			}

			quantity = value[i]%10;
			if(quantity == 0)
			{
				quantity = 10;
			}
			//Debug.Log (""+type);
			Debug.Log (""+quantity);
			smallIreward[i].sprite = iReward[type];

			if(i>=dateCount)
			{
				if(i == dateCount)
				{
					if(isOverDay == true)
					{
						textReward[dateCount].text = "Today's Bonus";
						if(type == 0)
						{
							bg[i].color = new Color(1f,1f,1f,1f);
							rewardNumber [i].SetActive (true);
							rewardNumber [i].GetComponent<Text> ().text = ""+quantity;
						}
					}
					else
					{
						textReward[i].text = "Day " + (i+1).ToString();
						allBT[i].interactable = false;
						allBT[i].gameObject.GetComponent<Image>().sprite = disableImage;
						lockImage [i].gameObject.SetActive (true);
					}
				}
				else
				{
					textReward[i].text = "Day " + (i+1).ToString();
					allBT[i].interactable = false;
					allBT[i].gameObject.GetComponent<Image>().sprite = disableImage;
					lockImage [i].gameObject.SetActive (true);
				}
				Itype[i] = type;
				IQuantity[i] = quantity;
			}
			else
			{
				allBT[i].interactable = false;
				smallIreward[i].color = new Color(0.5f,0.5f,0.5f,1f);
				rewarded [i].SetActive(true);// = doneReward;
				bg[i].color = new Color(0.5f,0.5f,0.5f,1f);
				if (type == 0)
				{
					rewardNumber [i].SetActive (true);
					rewardNumber [i].GetComponent<Text> ().text = "" + quantity;
				}
				else 
				{
					rewardNumber [i].SetActive (false);
				}
			}
		}
	}

	void SetReward()
	{
		for(int i= 0; i<5;i++)
		{
			rewarded [i].SetActive (false);
			smallIreward[i].color = new Color(1f,1f,1f,1f);

			int R = UnityEngine.Random.Range(0,4);
			int typeReward;
			if(R < 3)
			{
				typeReward = 0; 
			}
			else
			{
				typeReward = UnityEngine.Random.Range(1,4);
			}
			//type 0  = coins, 1 = rowbreak, 2 = columnbreak, 3 =  break;

			int quantity = 0;

			if(typeReward == 0)
			{
				quantity = UnityEngine.Random.Range(5,11);
			}
			else
			{
				quantity = 1;
			}
			string value = typeReward.ToString() + quantity.ToString();
			Debug.Log (""+value);
			int saveNumber = 0;
			int.TryParse(value,out saveNumber);
			PlayerPrefs.SetInt("day" + i.ToString(), saveNumber);
			PlayerPrefs.Save();
		}
	}
}

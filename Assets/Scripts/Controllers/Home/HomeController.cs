﻿/*
 * @Author: CuongNH
 * @Description: Xu ly cac su kien tren man Home
 * */

using UnityEngine;
using System.Collections;
using System;

public class HomeController : MonoBehaviour {

    public readonly static Vector2 BASE_SCREEN_SIZE = new Vector2(480f, 854f);
    public static float ratioScale = 1f;

	public GameObject loading;

	public GameObject bg, bg3;

    void Start()
    {
        // Set ratio scale
        //ratioScale = (((float)Screen.height / Screen.width) / (BASE_SCREEN_SIZE.y / BASE_SCREEN_SIZE.x));

		if(bg != null)
		{
			double _scale = Math.Round((Camera.main.aspect-(double)9/(double)16)*1.2f,2);
			//Debug.Log ("_scale"+_scale);
			Vector3 _number = new Vector3(1+(float)_scale,1+(float)_scale,1+(float)_scale);;

			bg.transform.localScale = _number;

			bg3.transform.localScale = _number;
		}
    }

    // Handle Play Button
    public void HandlePlay()
    {
		SoundController.sound.Click ();
		loading.SetActive (true);
		StartCoroutine(LoadingHandler.loading.HandleLoading(GameConstants.LEVEL_SELECT_SCENE_NAME));
    }
}

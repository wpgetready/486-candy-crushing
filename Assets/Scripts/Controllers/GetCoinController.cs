﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using UnityEngine.Advertisements;
using Facebook.MiniJSON;

public class GetCoinController : MonoBehaviour 
{
	public GameObject rate, vunglebt, adcolonybt, loginfbbt, unitybt;
	public Text textCoins;
	public Text ownCoins;
	public Text buyCoins;
	public GameObject inviteButton;
	public AllPanel allPanel;
	public Text[] watchVideo_Coins;
	public Text rate_Coins;

	void Awake()
	{
		rate.SetActive (true);
		int number = PlayerPrefs.GetInt ("loginfb");
	}

	void OnEnable()
	{
		if(watchVideo_Coins.Length > 0)
		{
			foreach(Text obj in watchVideo_Coins)
			{
				obj.text = ""+AddCoins.rewardAds;
			}
		}

		if(rate_Coins != null)
		{
			rate_Coins.text = ""+AddCoins.rateOrLogin;
		}

		int coins = PlayerPrefs.GetInt ("coins");
		int count = PlayerPrefs.GetInt ("rate");
		int number = PlayerPrefs.GetInt ("loginfb");

		int value = PlayerPrefs.GetInt ("coins");
		textCoins.text = value.ToString ();
		if(count == 1)
		{
			rate.SetActive(false);
		}

		if(number == 1)
		{
			if(loginfbbt != null)
			{
				loginfbbt.SetActive(false);
			}
		}

		if (Vungle.isAdvertAvailable () == true) 
		{
			vunglebt.SetActive (true);
		} 
		else 
		{
			vunglebt.SetActive (false);

		}
		
		if(AdsControl.instance != null)
		{
			if (AdColony.IsV4VCAvailable (AdsControl.instance.adcolonyController.zoneID)) 
			{
				adcolonybt.SetActive (true);
			} 
			else 
			{
				adcolonybt.SetActive(false);
			}
		}

		if (AdsControl.instance.unityAdsController.IsLoaded () == true) 
		{
			unitybt.SetActive (true);
		} 
		else 
		{
			unitybt.SetActive (false);
		}
	}

	public void RateButton()
	{
		SoundController.sound.Click ();
		string linkApp;
		#if UNITY_ANDROID
		linkApp = "https://play.google.com/store/apps/details?id=" + Application.identifier;
		#elif UNITY_IOS
		linkApp = "https://itunes.apple.com/us/app/candy-fruit-juice/id1228059324?ls=1&mt=8";
		#endif

		Application.OpenURL (linkApp);
		PlayerPrefs.SetInt ("rate", 1);
		PlayerPrefs.Save ();
		rate.SetActive (false);
		AdsControl.instance.addCoins.AddReward (true, AddCoins.rateOrLogin);
	}

	public void ShowVungle()
	{
		SoundController.sound.Click ();
		if(Vungle.isAdvertAvailable() == true)
		{
			Vungle.playAd ();
			#if UNITY_IOS
			SoundController.sound.audioSource.mute = true;
			MusicController.music.audioSource.mute = true;
			#endif
		}
	}

	public void ShowAdcolony()
	{
		SoundController.sound.Click ();
		AdsControl.instance.adcolonyController.PlayV4VCAd (AdsControl.instance.adcolonyController.zoneID, false, false);
	}

	public void ShowAdUnity()
	{
		SoundController.sound.Click ();
		AdsControl.instance.unityAdsController.ShowAds ();
	}

	public void LoginFacebook()
	{
		SoundController.sound.Click ();
		List<string> perms = new List<string>(){"public_profile", "email", "user_friends"};
		FB.LogInWithReadPermissions(perms, AuthCallback);
	}

	private void AuthCallback (ILoginResult result) 
	{
		if (FB.IsLoggedIn) {
			// AccessToken class will have session details
			var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
			// Print current access token's User ID
			//Debug.Log(aToken.UserId);
			// Print current access token's granted permissions
//			foreach (string perm in aToken.Permissions) {
//				Debug.Log(perm);
//			}

			string fbname = "";
			string queryString = "/me?fields=first_name";
			FB.API(queryString, HttpMethod.GET, delegate(IGraphResult gresult)
				{
					if (gresult.Error != null)
					{
						Debug.Log("error");
						return;
					}
					else
					{
						if (gresult.RawResult ==  null)
						{
							Debug.Log("null");
							return;
						}
						else
						{
							//Debug.Log(gresult.RawResult);
							var dict = Json.Deserialize(gresult.RawResult) as IDictionary;
							//Debug.Log(dict.va);
							fbname = dict ["first_name"].ToString();
							//Debug.Log ("fbname..."+fbname);
							PlayerPrefs.SetString("userName", fbname);
							PlayerPrefs.Save();
						}
					}
				}

			);

			string userID = aToken.UserId;

			PlayerPrefs.SetString("userID", userID);
			PlayerPrefs.SetInt("loginfb", 1);
			PlayerPrefs.Save();

			if(inviteButton != null)
			{
				inviteButton.SetActive(true);
			}
			loginfbbt.SetActive(false);
			AdsControl.instance.addCoins.AddReward (true, AddCoins.rateOrLogin);
			allPanel.SetupInvite ();
			allPanel.OnClickButton ("invite");
		} 
		else 
		{
			Debug.Log("User cancelled login");
		}
	}
}

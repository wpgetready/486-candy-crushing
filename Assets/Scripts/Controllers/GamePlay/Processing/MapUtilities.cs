﻿/*
 * @Author: CuongNH
 * @Description: Mot so phuong thuc khac duoc su dung trong viec phuc vu dieu khien map cua game play
 * */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class MapUtilities {

    /// <summary>
    /// Convert x,y counter mark to real position with direction
    /// Return a list to process
    /// </summary>
    public static List<int> GetIDList(Grid.Direction direction)
    {
        List<int> result = new List<int>();
        // NOTE: Normally, map controller iterated like left gravity
        switch (direction)
        {
            case Grid.Direction.BOTTOM:
            case Grid.Direction.BOTTOM_LEFT:
                {
                    for (int y = 0; y < GameConstants.MAP_HEIGHT; y++)
                    {
                        for (int x = 0; x < GameConstants.MAP_WIDTH; x++)
                        {
                            result.Add(x * GameConstants.MAP_HEIGHT + y);
                        }
                    }
                }
                break;
            case Grid.Direction.BOTTOM_RIGHT:
                {
                    for (int y = 0; y < GameConstants.MAP_HEIGHT; y++)
                    {
                        for (int x = GameConstants.MAP_WIDTH - 1; x >= 0; x--)
                        {
                            result.Add(x * GameConstants.MAP_HEIGHT + y);
                        }
                    }
                }
                break;
            case Grid.Direction.TOP:
            case Grid.Direction.TOP_LEFT:
                {
                    for (int y = GameConstants.MAP_HEIGHT - 1; y >= 0; y--)
                    {
                        for (int x = 0; x < GameConstants.MAP_WIDTH; x++)
                        {
                            result.Add(x * GameConstants.MAP_HEIGHT + y);
                        }
                    }
                }
                break;
            case Grid.Direction.TOP_RIGHT:
                {
                    for (int y = GameConstants.MAP_HEIGHT - 1; y >= 0; y--)
                    {
                        for (int x = GameConstants.MAP_WIDTH - 1; x >= 0; x--)
                        {
                            result.Add(x * GameConstants.MAP_HEIGHT + y);
                        }
                    }
                }
                break;
            case Grid.Direction.LEFT:
                {
                    for (int x = 0; x < GameConstants.MAP_WIDTH; x++)
                    {
                        for (int y = 0; y < GameConstants.MAP_HEIGHT; y++)
                        {
                            result.Add(x * GameConstants.MAP_HEIGHT + y);
                        }
                    }
                }
                break;
            case Grid.Direction.RIGHT:
                {
                    for (int x = GameConstants.MAP_WIDTH - 1; x >= 0; x--)
                    {
                        for (int y = 0; y < GameConstants.MAP_HEIGHT; y++)
                        {
                            result.Add(x * GameConstants.MAP_HEIGHT + y);
                        }
                    }
                }
                break;
            case Grid.Direction.CENTER:
                {
                    // This part is COMPLETED
                    // CENTER gravity do not make tiles shrink
                }
                break;
            default:
                {
                    for (int x = 0; x < GameConstants.MAP_WIDTH; x++)
                    {
                        for (int y = 0; y < GameConstants.MAP_HEIGHT; y++)
                        {
                            result.Add(x * GameConstants.MAP_HEIGHT + y);
                        }
                    }
                }
                break;
        }

        return result;
    }

    /// <summary>
    /// Convert x,y counter mark to real position with direction and size of map
    /// Return a list to process
    /// </summary>
    public static List<int> GetIndexList(int width, int height, Grid.Direction direction)
    {
        List<int> list = new List<int>();

        if (width > GameConstants.MAP_WIDTH || width <= 0)
        {
            width = GameConstants.MAP_WIDTH;
        }

        if (height > GameConstants.MAP_HEIGHT || height <= 0)
        {
            height = GameConstants.MAP_HEIGHT;
        }

        switch (direction)
        {
            case Grid.Direction.BOTTOM:
            case Grid.Direction.BOTTOM_LEFT:
                {
                    for (int y = 0; y < height; y++)
                    {
                        for (int x = 0; x < width; x++)
                        {
                            list.Add(y * width + x);
                        }
                    }

                    //for (int x = 0; x < width; x++)
                    //{
                    //    for (int y = 0; y < height; y++)
                    //    {
                    //        list.Add(x * height + y);
                    //    }
                    //}

                    break;
                }
            default:
                {
                    for (int y = 0; y < height; y++)
                    {
                        for (int x = 0; x < width; x++)
                        {
                            list.Add(y * width + x);
                        }
                    }
                    break;
                }
        }

        return list;
    }

    /// <summary>
    /// Get list index with from Started Index
    /// </summary>
    /// <param name="gridList"></param>
    /// <param name="startedIndexList"></param>
    /// <param name="direction"></param>
    /// <returns></returns>
    public static List<int> GetIndexListFromStartedIndex(List<Grid> gridList, 
        List<int> startedIndexList, Grid.Direction direction)
    {
        List<int> list = new List<int>();
        if (gridList != null && gridList.Count > 0 && startedIndexList != null && startedIndexList.Count > 0)
        {
            Grid.Direction opposedDir = Grid.GetOpposedDir(direction);
            for (int i = 0; i < startedIndexList.Count; i ++)
            {
                Grid grid = gridList[startedIndexList[i]];

                int count = 0;

                do
                {
                    if (grid.gridData.gridType != GridData.GridType.NULL_GRID)
                    {
                        list.Add(grid.gridPosition.Row * Contance.width + grid.gridPosition.Column);
                    }

                    if (grid.gridData.gridStaticType == GridData.GridStaticType.PORTAL_GRID
                        && grid.portal != null && grid.portal.portalType == Portal.PortalType.OUTLET
                        && grid.portal.portalMatched != null && grid.portal.portalMatched.grid != null)
                    {
                        grid = grid.portal.portalMatched.grid;
                    }
                    else
                    {
                        grid = grid.GetGridInDir(opposedDir);
                    }
                } while (grid != null && !grid.isBottom);
            }
        }
        else
        {
            list = GetIndexList(Contance.width, Contance.height, direction);
        }

        return list;
    }

    /// <summary>
    /// Get all started index with Grid Direction
    /// </summary>
    /// <param name="gridList"></param>
    /// <param name="direction"></param>
    /// <returns></returns>
    public static List<int> GetStartedIndexList(List<Grid> gridList, Grid.Direction direction)
    {
        List<int> list = new List<int>();

        if (gridList == null || gridList.Count < 1)
        {
            return null;
        }

        bool check = false;
        for (int i = 0; i < gridList.Count; i++)
        {
            Grid grid = gridList[i];
            if (grid == null || grid.gridData.gridType == GridData.GridType.NULL_GRID 
                || (grid.gridData.gridStaticType == GridData.GridStaticType.PORTAL_GRID 
                && grid.portal != null && grid.portal.portalType == Portal.PortalType.INTLET))
            {
                continue;
            }

            Grid gridInDir = grid.GetGridInDir(direction);
            if (gridInDir != null && gridInDir.gridData.gridType != GridData.GridType.NULL_GRID)
            {
                continue;
            }
            else
            {
                // TODO: recheck to add
                check = false;
                if (gridInDir == null)
                {
                    check = true;
                }
                else
                {
                    while (gridInDir != null)
                    {
                        gridInDir = gridInDir.GetGridInDir(direction);
                        if (gridInDir == null)
                        {
                            check = true;
                            break;
                        }
                        else if (gridInDir.gridData.gridStaticType == GridData.GridStaticType.SPAWNER_GRID)
                        {
                            check = true;
                            break;
                        }
                        else if (gridInDir.gridData.gridType != GridData.GridType.NULL_GRID)
                        {
                            check = false;
                            break;
                        }
                    }
                }

                if (check)
                {
                    list.Add(i);
                }
            }
        }

        return list;
    }

    /// <summary>
    /// Get all starter ID of a group (row/collumn) based on direction
    /// </summary>
    public static List<int> GetGroupStartIDList(Grid.Direction direction)
    {
        List<int> result = new List<int>();
        // NOTE: Normally, map controller iterated like left gravity
        switch (direction)
        {
            case Grid.Direction.BOTTOM:
                {
                    for (int x = 0; x < GameConstants.MAP_WIDTH; x++)
                    {
                        result.Add(x * GameConstants.MAP_HEIGHT + 0);
                    }
                }
                break;
            case Grid.Direction.TOP:
                {
                    for (int x = 0; x < GameConstants.MAP_WIDTH; x++)
                    {
                        result.Add(x * GameConstants.MAP_HEIGHT + GameConstants.MAP_HEIGHT - 1);
                    }
                }
                break;
            case Grid.Direction.LEFT:
                {
                    for (int y = 0; y < GameConstants.MAP_HEIGHT; y++)
                    {
                        result.Add(0 * GameConstants.MAP_HEIGHT + y);
                    }
                }
                break;
            case Grid.Direction.RIGHT:
                {
                    for (int y = 0; y < GameConstants.MAP_HEIGHT; y++)
                    {
                        result.Add((GameConstants.MAP_WIDTH - 1) * GameConstants.MAP_HEIGHT + y);
                    }
                }
                break;
            case Grid.Direction.CENTER:
                {
                    // Every single grid is a starter if gravity is CENTER
                    for (int i = 0; i < GameConstants.MAP_HEIGHT * GameConstants.MAP_WIDTH; i++)
                    {
                        result.Add(i);
                    }
                }
                break;
            case Grid.Direction.BOTTOM_LEFT:
                {
                    for (int x = 0; x < GameConstants.MAP_WIDTH; x++)
                    {
                        result.Add(x * GameConstants.MAP_HEIGHT + 0);
                    }
                    for (int y = 0; y < GameConstants.MAP_HEIGHT; y++)
                    {
                        result.Add(0 * GameConstants.MAP_HEIGHT + y);
                    }
                }
                break;
            case Grid.Direction.BOTTOM_RIGHT:
                {
                    for (int x = 0; x < GameConstants.MAP_WIDTH; x++)
                    {
                        result.Add(x * GameConstants.MAP_HEIGHT + 0);
                    }
                    for (int y = 0; y < GameConstants.MAP_HEIGHT; y++)
                    {
                        result.Add((GameConstants.MAP_WIDTH - 1) * GameConstants.MAP_HEIGHT + y);
                    }
                }
                break;
            case Grid.Direction.TOP_LEFT:
                {
                    for (int x = 0; x < GameConstants.MAP_WIDTH; x++)
                    {
                        result.Add(x * GameConstants.MAP_HEIGHT + GameConstants.MAP_HEIGHT - 1);
                    }
                    for (int y = 0; y < GameConstants.MAP_HEIGHT; y++)
                    {
                        result.Add(0 * GameConstants.MAP_HEIGHT + y);
                    }
                }
                break;
            case Grid.Direction.TOP_RIGHT:
                {
                    for (int x = 0; x < GameConstants.MAP_WIDTH; x++)
                    {
                        result.Add(x * GameConstants.MAP_HEIGHT + GameConstants.MAP_HEIGHT - 1);
                    }
                    for (int y = 0; y < GameConstants.MAP_HEIGHT; y++)
                    {
                        result.Add((GameConstants.MAP_WIDTH - 1) * GameConstants.MAP_HEIGHT + y);
                    }
                }
                break;
            default:
                {
                    for (int x = 0; x < GameConstants.MAP_WIDTH; x++)
                    {
                        result.Add(x * GameConstants.MAP_HEIGHT + 0);
                    }
                }
                break;
        }

        return result;
    }

    /// <summary>
    /// Currently useless, skip this part ASAP
    /// </summary>
    public static List<Grid.Direction> GetPossibleMoveDirections(Grid.Direction dir)
    {
        List<Grid.Direction> dirList = new List<Grid.Direction>();

        switch (dir)
        {
            case Grid.Direction.TOP:
                {
                    dirList.Add(Grid.Direction.LEFT);
                    dirList.Add(Grid.Direction.RIGHT);
                }
                break;
            case Grid.Direction.BOTTOM:
                {
                    dirList.Add(Grid.Direction.LEFT);
                    dirList.Add(Grid.Direction.RIGHT);
                }
                break;
            case Grid.Direction.LEFT:
                {
                    dirList.Add(Grid.Direction.TOP);
                    dirList.Add(Grid.Direction.BOTTOM);
                }
                break;
            case Grid.Direction.RIGHT:
                {
                    dirList.Add(Grid.Direction.TOP);
                    dirList.Add(Grid.Direction.BOTTOM);
                }
                break;
        }


        return dirList;
    }

    /// <summary>
    /// Tao hieu ung roi qua portal
    /// </summary>
    /// <param name="tile"></param>
    /// <param name="position"></param>
    /// <param name="time"></param>
    public static void FakeDropTileInPortal(Tile tile, Vector3 position, float time)
    {
        LeanTween.moveLocal(tile.gameObject, Vector3.zero, time)
            .setEase(LeanTweenType.easeOutSine).setOnComplete(() =>
            {
                tile.gameObject.SetActive(false);
            });
    }

    public static Tile GetRandomTile(List<Grid> containsList)
    {
        Grid grid = null;
        Tile tile = null;

        if (containsList == null || containsList.Count == 0)
        {
            return null;
        }
        int count = 0;
        int rand = 0;
        while (true)
        {
            count++;
            if (count > GameConstants.MAX_RESET)
            {
                break;
            }
            rand = Random.Range(0, containsList.Count);
            grid = containsList[rand];

            if (grid != null && grid.gridData.gridType != GridData.GridType.NULL_GRID)
            {
                tile = grid.GetTile();
                if (tile != null && tile.tileData.tileType == TileData.TileType.match_object)
                {
                    return tile;
                }
                else
                {
                    tile = null;
                }
            }


        }
        return tile;
    }

    /// <summary>
    /// Get random tile from list of Grid
    /// </summary>
    /// <param name="containsList"></param>
    /// <param name="exceptList"></param>
    /// <returns></returns>
    public static Tile GetRandomTileFromList(List<Grid> containsList, TileData.TileColor tileColor 
        = TileData.TileColor.blue, List<Grid> exceptList = null, bool sameColor = true)
    {
        Grid grid = null;
        Tile tile = null;

        if (containsList == null || containsList.Count == 0)
        {
            return null;
        }

        int count = 0;
        int rand = 0;
        while(true)
        {
            count++;
            if (count > GameConstants.MAX_RESET)
            {
                break;
            }

            rand = Random.Range(0, containsList.Count);
            grid = containsList[rand];

            if (exceptList != null && exceptList.Count > 0 && exceptList.Contains(grid))
            {
                continue;
            }
            else
            {
                if (grid != null && grid.gridData.gridType != GridData.GridType.NULL_GRID)
                {
                    tile = grid.GetTile();
                    if (tile != null && tile.isHit && tile.tileData.tileType == TileData.TileType.match_object 
                        && (tile.tileData.tileColor == tileColor || !sameColor) && !tile.isMoving 
                        && !tile.isOnClearingProcess && !tile.isTransforming)
                    {
                        return tile;
                    }
                    else
                    {
                        tile = null;
                    }
                }
            }
        }

        return tile;
    }

    /// <summary>
    /// Get random grid from list of Grid
    /// </summary>
    /// <param name="containsList"></param>
    /// <param name="exceptList"></param>
    /// <returns></returns>
    public static Grid GetRandomGridFromList(List<Grid> containsList, List<Grid> exceptList = null)
    {
        Grid grid = null;
        Tile tile = null;

        if (containsList == null || containsList.Count == 0)
        {
            return null;
        }

        int count = 0;
        int rand = 0;
        while (true)
        {
            count++;
            if (count > GameConstants.MAX_RESET)
            {
                break;
            }

            rand = Random.Range(0, containsList.Count);
            grid = containsList[rand];

            if (exceptList != null && exceptList.Count > 0 && exceptList.Contains(grid))
            {
                continue;
            }
            else
            {
                if (grid != null && grid.gridData.gridType != GridData.GridType.NULL_GRID)
                {
                    tile = grid.GetTile();
                    if (tile == null)
                    {
                        return grid;
                    }

                    if (tile != null && tile.isHit && tile.isColored && tile.isSwappable && !tile.isSpecialActivable 
                        && !tile.isMoving && !tile.isOnClearingProcess && !tile.isTransforming)
                    {
                        return grid;
                    }
                    else
                    {
                        grid = null;
                    }
                }
            }
        }

        return grid;
    }
}

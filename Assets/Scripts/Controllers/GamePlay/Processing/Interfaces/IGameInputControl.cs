﻿using UnityEngine;
using System.Collections;

/// Control inputting, process input 
public interface IGameInputControl {

    void UpdateInput(Vector2 pos, GameObject obj, GameEnum.InputState inState);

    void DisposeInput();

}

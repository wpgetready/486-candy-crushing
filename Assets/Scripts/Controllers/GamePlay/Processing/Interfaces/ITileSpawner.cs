﻿/*
 * Spawning new Tile, shrink the map, move tile around, do every ****ing thing Tile related.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface ITileSpawner {

    float SpawnTiles(Grid.Direction direction = Grid.Direction.CENTER);

    float ShrinkTiles(Grid.Direction direction = Grid.Direction.CENTER, bool diagonalDrop = false);

    void SpawnInitialTiles();

    void RandomSwapTiles(List<Grid> swappableGrids);

    void ResetBoard(List<Grid> swappableGrids);
}

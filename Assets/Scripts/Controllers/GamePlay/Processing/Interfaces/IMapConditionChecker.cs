﻿/*
 * Check map status, no more move, have move available.
 */

using UnityEngine;
using System.Collections;

public interface IMapConditionChecker
{

    bool CheckAvailableMove();

    bool CheckHitInGrid(Grid grid);

    bool CheckSwappable(Grid grid1, Grid grid2);

    bool CheckSwapExecutable(Grid grid1, Grid grid2);

    bool CheckMatchSwap(Tile testIile, Grid targetGrid, Grid.Direction comeDir);
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface IGameInputProcessor {

    bool ProcessInput(List<Grid> grids, GameEnum.ProcessType processType = GameEnum.ProcessType.NONE);

}

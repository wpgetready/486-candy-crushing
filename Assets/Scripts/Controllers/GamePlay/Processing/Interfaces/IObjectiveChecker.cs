﻿using UnityEngine;
using System.Collections;

public interface IObjectiveChecker {

    /// <summary>
    /// Is this objective completed
    /// </summary>
    bool IsCompleted();

    /// <summary>
    /// Do things, get profit
    /// </summary>
    bool ProcessChecker(bool fakeSwap);
}

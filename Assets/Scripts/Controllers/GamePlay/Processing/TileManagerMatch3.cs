﻿/*
 * @Author: CuongNH
 * @Description: Manage tiles in processing
 * */

using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class TileManagerMatch3 {

    /// <summary>
    /// Total number of gem color
    /// </summary>
    public int maxGemColor;

    /// <summary>
    /// Tiles waiting to be spawned
    /// </summary>
    public List<TileData> queuedTileList;

    /// <summary>
    /// Tiles waiting to be processed
    /// </summary>
    public List<Tile> waitingForProcessTiles;

    /// <summary>
    /// Tiles waiting to be killed after (and only after) processing phase
    /// </summary>
    public List<Tile> waitingForKillTiles;

    /// <summary>
    /// Tile waiting for executing, e.g. magic converted special tiles
    /// </summary>
    public List<Tile> cascadingTiles;

    /// Collections for groups of Tiles
    public List<Tile> specialEffectTiles;
    //public List<Tile> objectiveGems;

    // tile types that spawn randomly
    public List<TileData.TileType> randomTileType;

    /// <summary>
    /// List cac spawners o layer 18
    /// </summary>
    public List<P_Object_18> spawnerList;

    /// <summary>
    /// List cac table spawn chua cac ty le cac loai tile duoc roi ra
    /// </summary>
    public List<P_SpawnTable> spawnTableList;

    /// <summary>
    /// List chua cac tile trong queue chung
    /// </summary>
    public List<P_ForcedSpawnQueue> forcedSpawnQueueList;

    /// <summary>
    /// Constructor
    /// </summary>
    public TileManagerMatch3()
    {
        maxGemColor = GameConstants.MAX_COLOR;

        queuedTileList = new List<TileData>();

        waitingForProcessTiles = new List<Tile>();
        waitingForKillTiles = new List<Tile>();
        specialEffectTiles = new List<Tile>();
        //objectiveGems = new List<Tile>();

        cascadingTiles = new List<Tile>();
        randomTileType = new List<TileData.TileType>();
    }

    /// <summary>
    /// Init data for spawners and spawntable
    /// </summary>
    public void InitData()
    { 
        if (Contance.LIST_OBJECT_18 != null && Contance.LIST_OBJECT_18.Count > 0)
        {
            spawnerList = new List<P_Object_18>();
            spawnerList.AddRange(Contance.LIST_OBJECT_18);
        }

        if (Contance.LIST_SPAWNTABLE != null && Contance.LIST_SPAWNTABLE.Count > 0)
        {
            spawnTableList = new List<P_SpawnTable>();
            spawnTableList.AddRange(Contance.LIST_SPAWNTABLE);

            //Debug.Log("spawnTableList Size: " + spawnTableList.Count);
        }

        if (Contance.LIST_FORCEDSPAWNQUEUE != null && Contance.LIST_FORCEDSPAWNQUEUE.Count > 0)
        {
            forcedSpawnQueueList = new List<P_ForcedSpawnQueue>();
            forcedSpawnQueueList.AddRange(Contance.LIST_FORCEDSPAWNQUEUE);
        }
    }

    /// <summary>
    /// Get TileData cho tile se duoc roi ra tai Grid Spawner
    /// </summary>
    /// <param name="grid"></param>
    /// <returns></returns>
    public TileData GetTileData(Grid grid)
    {
        TileData tileData = new TileData();

        #region Get data in spawners
        if (spawnerList != null && spawnerList.Count > 0)
        {
            P_Object_18 pObject = new P_Object_18();
            if (CheckInLayer18(grid, ref pObject))
            {
                //Debug.Log("In layer18");
                if (pObject.listForcedSpawn != null && pObject.listForcedSpawn.Count > 0)
                {
                    P_ForcedSpawnQueue pQueue = pObject.listForcedSpawn[0];
                    pObject.listForcedSpawn.Remove(pQueue);
                    tileData = GetTileDataByPQueue(pQueue, tileData);

                    return tileData;
                }

                if (forcedSpawnQueueList != null && forcedSpawnQueueList.Count > 0)
                {
                    P_ForcedSpawnQueue pQueue = forcedSpawnQueueList[0];
                    forcedSpawnQueueList.Remove(pQueue);
                    tileData = GetTileDataByPQueue(pQueue, tileData);

                    return tileData;
                }

                if (pObject.listSpawnTable != null && pObject.listSpawnTable.Count > 0)
                {
                    int rand = Random.Range(0, pObject.listSpawnTable.Count);
                    if (pObject.listColor != null && pObject.listColor.Count > 0)
                    {
                        tileData = GetTileDataBySpawnTable(pObject.listSpawnTable[rand], tileData, pObject.listColor);
                    }
                    else
                    {
                        tileData = GetTileDataBySpawnTable(pObject.listSpawnTable[rand], tileData, Contance.LIST_COLOR);
                    }

                    return tileData;
                }

                if (pObject.listColor != null && pObject.listColor.Count > 0)
                {
                    //Debug.Log("Spawner has color table");

                    tileData.tileType = TileData.TileType.match_object;
                    tileData.tileColor = GetTileColorFromSpawnTable(pObject.listColor);
                }
                else
                {
                    if (spawnTableList != null && spawnTableList.Count > 0)
                    {
                        //Debug.Log("Has spawnTableList");
                        int rand = Random.Range(0, spawnTableList.Count);

                        tileData = GetTileDataBySpawnTable(spawnTableList[rand], tileData, Contance.LIST_COLOR);

                        return tileData;
                    }
                    else
                    {
                        tileData.tileType = TileData.TileType.match_object;
                        tileData.tileColor = GetTileColorFromSpawnTable(Contance.LIST_COLOR);
                    }
                }

                return tileData;
            }
        }
        #endregion

        #region Get data from forced spawn queue
        if (forcedSpawnQueueList != null && forcedSpawnQueueList.Count > 0)
        {
            P_ForcedSpawnQueue pQueue = forcedSpawnQueueList[0];
            forcedSpawnQueueList.Remove(pQueue);
            tileData = GetTileDataByPQueue(pQueue, tileData);

            return tileData;
        }
        #endregion

        #region Get data from spawners
        if (spawnTableList != null && spawnTableList.Count > 0)
        {
            int rand = Random.Range(0, spawnTableList.Count);

            tileData = GetTileDataBySpawnTable(spawnTableList[rand], tileData, Contance.LIST_COLOR);

            return tileData;
        }
        #endregion

        #region Get random match_object
        tileData.tileType = TileData.TileType.match_object;
        tileData.tileColor = GetTileColorFromSpawnTable(Contance.LIST_COLOR);
        #endregion

        return tileData;
    }

    /// <summary>
    /// Check grid has in layer18
    /// </summary>
    /// <param name="grid"></param>
    /// <returns></returns>
    private bool CheckInLayer18(Grid grid, ref P_Object_18 _pObject)
    {
        bool check = false;

        if (grid == null || grid.gridData.gridType == GridData.GridType.NULL_GRID)
        {
            return false;
        }

        for (int i = 0; i < Contance.LIST_OBJECT_18.Count; i++)
        {
            P_Object_18 pObject = Contance.LIST_OBJECT_18[i];
            if (grid.gridPosition.Row == pObject.row && grid.gridPosition.Column == pObject.column)
            {
                _pObject = pObject;
                return true;
            }
        }

        return check;
    }

    /// <summary>
    /// Get tile data by element in queue
    /// </summary>
    /// <param name="pQueue"></param>
    /// <param name="tileData"></param>
    /// <returns></returns>
    private TileData GetTileDataByPQueue(P_ForcedSpawnQueue pQueue, TileData tileData)
    {
        if (tileData == null || pQueue == null)
        {
            return null;
        }

        tileData.tileType = GetTileType(pQueue.Type_ForcedSpawnQueue);
        tileData.tileColor = GetTileColor(pQueue.Value_ForcedSpawnQueue);

        return tileData;
    }

    /// <summary>
    /// Get tile data by Spawn Table
    /// </summary>
    /// <param name="spawnTable"></param>
    /// <param name="tileData"></param>
    /// <returns></returns>
    private TileData GetTileDataBySpawnTable(P_SpawnTable spawnTable, TileData tileData, List<P_Color> colorList)
    {
        if (tileData == null || spawnTable == null)
        {
            return null;
        }

        int rand = Random.Range(0, 100);
        //Debug.Log("rand: " + rand);
        if (rand < spawnTable.Value_SpawnTable * 100)
        {
            //Debug.Log("spawnTable.Value_SpawnTable * 100: " + spawnTable.Value_SpawnTable * 100);
            tileData.tileType = GetTileType(spawnTable.Type_SpawnTable);
            tileData.data = (spawnTable.Data > 0) ? spawnTable.Data : 1;

            return tileData;
        }
        else
        {
            tileData.tileType = TileData.TileType.match_object;
            tileData.tileColor = GetTileColorFromSpawnTable(colorList);

            return tileData;
        }

        //return tileData;
    }

    /// <summary>
    /// Get tile type by string
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    private TileData.TileType GetTileType(string type)
    {
        TileData.TileType tileType = TileData.TileType.match_object;

        if (string.IsNullOrEmpty(type))
        {
            return tileType;
        }

        type = type.Trim();

        switch (type)
        {
            case GameConstants.match_object_tile_type:
                {
                    tileType = TileData.TileType.match_object;
                    break;
                }
            case GameConstants.muff_tile_type:
                {
                    tileType = TileData.TileType.muffin;
                    break;
                }
            case GameConstants.muffin_tile_type:
                {
                    tileType = TileData.TileType.muffin;
                    break;
                }
            case GameConstants.muffs_tile_type:
                {
                    tileType = TileData.TileType.muffin;
                    break;
                }
            case GameConstants.muffins_tile_type:
                {
                    tileType = TileData.TileType.muffin;
                    break;
                }
            case GameConstants.bee_tile_type:
                {
                    tileType = TileData.TileType.bee;
                    break;
                }
            case GameConstants.coconut_1_tile_type:
                {
                    tileType = TileData.TileType.coconut_1;
                    break;
                }
            case GameConstants.coconut_2_tile_type:
                {
                    tileType = TileData.TileType.coconut_2;
                    break;
                }
            case GameConstants.ice_cube_tile_type:
                {
                    tileType = TileData.TileType.ice_cube;
                    break;
                }
            default:
                break;
        }

        return tileType;
    }

    /// <summary>
    /// Get tile color by string
    /// </summary>
    /// <param name="color"></param>
    /// <returns></returns>
    private TileData.TileColor GetTileColor(string color)
    {
        TileData.TileColor tileColor = TileData.TileColor.blue;

        if (string.IsNullOrEmpty(color))
        {
            return tileColor;
        }

        color = color.Trim();

        switch (color)
        {
            case GameConstants.blue_tile_color:
                {
                    tileColor = TileData.TileColor.blue;
                    break;
                }
            case GameConstants.green_tile_color:
                {
                    tileColor = TileData.TileColor.green;
                    break;
                }
            case GameConstants.orange_tile_color:
                {
                    tileColor = TileData.TileColor.orange;
                    break;
                }
            case GameConstants.purple_tile_color:
                {
                    tileColor = TileData.TileColor.purple;
                    break;
                }
            case GameConstants.red_tile_color:
                {
                    tileColor = TileData.TileColor.red;
                    break;
                }
            case GameConstants.yellow_tile_color:
                {
                    tileColor = TileData.TileColor.yellow;
                    break;
                }
            default:
                tileColor = GetTileColorFromSpawnTable(Contance.LIST_COLOR);
                break;
        }

        return tileColor;
    }

    /// <summary>
    /// Get color from Spawn Table
    /// </summary>
    /// <param name="colorList"></param>
    /// <returns></returns>
    private TileData.TileColor GetTileColorFromSpawnTable(List<P_Color> colorList)
    {
        TileData.TileColor tileColor = TileData.TileColor.blue;

        if (colorList == null || colorList.Count < 1)
        {
            return tileColor;
        }

        float total = 0;
        for (int i = 0; i < colorList.Count; i++)
        {
            total += colorList[i].value * 100;
        }

        int rand = Random.Range(0, (int)total);

        //Debug.Log("rand: " + rand);
        //Debug.Log("total: " + total);

        for (int i = 0; i < colorList.Count; i++)
        {
            float rangeVal = 0f;
            for (int j = 0; j <= i; j++)
            {
                rangeVal += colorList[j].value * 100;
            }

            //Debug.Log("rangeVal: " + rangeVal);
            if (rand < rangeVal)
            {
                //Debug.Log("colorList[i].color: " + colorList[i].color);
                tileColor = GetTileColor(colorList[i].color);
                
                return tileColor;
            }
        }

        return tileColor;
    }

    #region Reference
    public void TileSpawn(Tile tile, TileData data = null, bool skipQueued = false)
    {
        //TileData nextTileData;

        //if (data != null) nextTileData = data;
        //else
        //{
        //    // Check if there is any special gem waiting to be spawned
        //    if (queuedGemList.Count > 0 && !skipQueued)
        //    {
        //        nextTileData = queuedGemList[0];
        //        if (nextTileData.tileColor == TileData.TileColor.RANDOM)
        //        {
        //            nextTileData.tileColor = (TileData.TileColor)(Random.Range(1, MaxGemColor + 1));
        //        }
        //        queuedGemList.RemoveAt(0); // Delete used data
        //    }
        //    else
        //    {
        //        nextTileData = new TileData();
        //        nextTileData.tileColor = (TileData.TileColor)(Random.Range(1, MaxGemColor + 1));

        //        if (randomTileType.Count > 0 && Random.Range(0, 100f) < GameConstants.RATE_SPAWN_SPECIAL_TYPE && !skipQueued)
        //            nextTileData.tileType = randomTileType[Random.Range(0, randomTileType.Count)];
        //        else
        //            nextTileData.tileType = TileData.TileType.NORMAL_GEM;

        //        if (nextTileData.tileType != TileData.TileType.ICE_BOMB_GEM)
        //            nextTileData.tileHealth = 1;
        //        else
        //            nextTileData.tileHealth = 6;
        //    }
        //}
        //// Commit
        //tile.SetTileProperties(nextTileData.tileType, nextTileData.tileColor, nextTileData.tileHealth);
        //tile.OnCreated();

        //this.OnTileSpawned(tile);
    }

    public void OnTileSpawned(Tile tile)
    {
        // Save to list
        //if (GameConstants.TILE_GROUP_OBJECTIVE.Contains(tile.tileData.tileType))
        //{
        //    objectiveGems.Add(tile);
        //}
        //else if (GameConstants.TILE_GROUP_SPECIAL_EFFECT.Contains(tile.tileData.tileType))
        //{
        //    specialEffectGems.Add(tile);
        //}
    }

    public void OnTileCleared(Tile tile)
    {
        // Save Counter for target, archievement,....
        // Remove from list
        //if (GameConstants.TILE_GROUP_OBJECTIVE.Contains(tile.tileData.tileType))
        //{
        //    objectiveGems.Remove(tile);
        //}
        //else if (GameConstants.TILE_GROUP_SPECIAL_EFFECT.Contains(tile.tileData.tileType))
        //{
        //    specialEffectGems.Remove(tile);
        //}

        if (cascadingTiles.Contains(tile))
        {
            cascadingTiles.Remove(tile);
        }

        if (waitingForProcessTiles.Contains(tile))
        {
            waitingForProcessTiles.Remove(tile);
        }

        if (waitingForKillTiles.Contains(tile))
        {
            waitingForKillTiles.Remove(tile);
        }
    }

    public void SetMaxGemColor(int num)
    {
        maxGemColor = num;
    }

    public void AddQueuedTile(TileData data)
    {
        queuedTileList.Add(data);
    }

    public void AddProcessTile(Tile tile)
    {
        if (!waitingForProcessTiles.Contains(tile) && !waitingForKillTiles.Contains(tile))
            waitingForProcessTiles.Add(tile);
    }

    public void AddDelayedKillTile(Tile tile)
    {
        if (!waitingForKillTiles.Contains(tile))
        {
            waitingForKillTiles.Add(tile);
        }

        if (waitingForProcessTiles.Contains(tile))
        {
            waitingForProcessTiles.Remove(tile);
        }
    }

    public bool IsOnProcessTile(Tile tile)
    {
        return (waitingForProcessTiles.Contains(tile));
    }

    public bool IsOnDelayedKillTile(Tile tile)
    {
        return (waitingForKillTiles.Contains(tile));
    }
    #endregion
}

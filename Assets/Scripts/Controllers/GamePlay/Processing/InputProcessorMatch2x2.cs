﻿/*
 * @Author: CuongNH
 * @Description: Xu ly cac vien trong match 2x2
 * */

using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class InputProcessorMatch2x2 : IGameInputProcessor {

	MapController mapController;

    //cache
    List<Grid> matchedList, additionalList, boundCheckTempList, findMatchTempList;

    public InputProcessorMatch2x2(MapController mapControl)
    {
        this.mapController = mapControl;

        this.matchedList = new List<Grid>(GameConstants.CACHE_NUMBER_GRID_CHECKING);
        this.additionalList = new List<Grid>(GameConstants.CACHE_NUMBER_GRID_CHECKING);
        this.boundCheckTempList = new List<Grid>(GameConstants.CACHE_NUMBER_GRID_CHECKING);
        this.findMatchTempList = new List<Grid>(GameConstants.CACHE_NUMBER_GRID_CHECKING);
    }

    /// <summary>
    /// Run process at a list of grids by a type
    /// </summary>
    /// <param name="grids">List of grids need to be processed</param>
    /// <param name="processType">Type of processing</param>
    public bool ProcessInput(List<Grid> grids, GameEnum.ProcessType processType = GameEnum.ProcessType.NONE)
    {
        switch (processType)
        {
            case GameEnum.ProcessType.SWAP:
                {
                    return ProcessSwap(grids[0], grids[1]);
                }
                //break;
            case GameEnum.ProcessType.CASCADE:
                {
                    return ProcessCascade(grids);
                }
                //break;
            default:
                break;
        }

        return false;
    }

    /// <summary>
    /// Run matching process at swapped grid, Match-3 Jewel game usually have 2 but in other kind, 
    /// input could be a little different
    /// </summary>
    bool ProcessSwap(Grid grid1, Grid grid2)
    {
        // DEBUG Message system
        //GameMessageManager.Instance.SendMessageToPopup("swap", grid1.PositionX 
        //    + "\t" + grid1.PositionY + "\t" +grid2.PositionX + "\t" + grid2.PositionY);


        //// Mark swapping process
        //mapController.isSwapping = true;
        //LeanTween.delayedCall(GameConstants.SPEED_SWAP_TILE + GameConstants.TIME_SWAP_DELAY, () =>
        //{
        //    mapController.isSwapping = false;
        //});

        //// Pre Wait
        //mapController.StartProcessingPhase();
        //GameplayController.instance.executeFlag = true;

        //// Execute Swap
        //Tile tile1 = grid1.GetTile();
        //Tile tile2 = grid2.GetTile();

        bool result = false;

        //// Single swap only
        //if (GameConstants.ENABLE_SWAP_WITH_EMPTY_GRID && (tile1 == null || tile2 == null))
        //{
        //    if (tile1 != null)
        //    {
        //        result |= ActiveMatchProcessAt(grid1, true, grid1.GetDirOfGrid(grid2));
        //    }
        //    if (tile2 != null)
        //    {
        //        result |= ActiveMatchProcessAt(grid2, true, grid2.GetDirOfGrid(grid1));
        //    }

        //    return result;
        //}

        //// 2 tile swap, this is real part

        //// Special swap
        //if (tile1.isSpecialActivable && tile2.isSpecialActivable)
        //{
        //    ActiveSpecialSwap(grid1, grid2);

        //    return true;
        //}

        //// Speical Normal Swap, isColored condition must be checked already in "InpuControl"
        //if (tile1.tileData.tileType == TileData.TileType.MAGIC_GEM 
        //    || tile2.tileData.tileType == TileData.TileType.MAGIC_GEM)
        //{
        //    if (tile1.tileData.tileType == TileData.TileType.MAGIC_GEM)
        //    {
        //        ActiveMagicSwap(grid1, grid2);
        //    }
        //    else
        //    {
        //        ActiveMagicSwap(grid2, grid1);
        //    }

        //    return true;
        //}

        //// Normal swap
        //result |= ActiveMatchProcessAt(grid1, true, grid1.GetDirOfGrid(grid2));
        //result |= ActiveMatchProcessAt(grid2, true, grid2.GetDirOfGrid(grid1));

        return result;
    }

    bool ProcessCascade(List<Grid> needCheckGrids)
    {
        bool result = false;
        for (int i = 0; i < needCheckGrids.Count; i++)
        {
            if (needCheckGrids[i].isGemMatchEnabled)
            {
                result |= ActiveMatchProcessAt(needCheckGrids[i]);
            }
        }

        return result;
    }

    bool ActiveMatchProcessAt(Grid grid, bool swapGem = false, 
        Grid.Direction swapDirection = Grid.Direction.UNKNOWN)
    {
        // Null check or anything is already done above, this 2 tiles is guaranteed to be ok
        //Tile tile = grid.GetTile();
        //if (tile == null || (tile.isMoving && !swapGem) || 
        //    tile.isOnClearingProcess || tile.isTransforming)
        //{
        //    return false;
        //}

        //// Find all Linked 
        //matchedList.Clear();
        //int dirCount = 0;
        //int lengthCount = 0;
        //Grid.Direction matchDirection;
        //FindMatchInfoAt(grid, matchedList, out lengthCount, out dirCount, out matchDirection);
        
        //if (lengthCount >= 2) // match found
        //{
        //    matchedList.Add(grid);

        //    Grid centerGrid = grid; // set to middle
        //    additionalList.Clear();

        //    if (!swapGem)
        //    {
        //        // Check all hit and find what's the highest gem
        //        for (int i = 0; i < matchedList.Count; i++)
        //        {
        //            if (matchedList[i] == grid) continue; // reduce 1 call =))

        //            int dCount = 0;
        //            int lCount = 0;
        //            Grid.Direction mDirection;
        //            boundCheckTempList.Clear();
        //            FindMatchInfoAt(matchedList[i], boundCheckTempList, 
        //                out lCount, out dCount, out mDirection);

        //            if (lCount > lengthCount) // Longer stack
        //            {
        //                lengthCount = lCount;
        //                matchDirection = mDirection;
        //                if (!matchedList[i].GetTile().isSpecialActivable)
        //                {
        //                    centerGrid = matchedList[i];
        //                    additionalList.Clear();
        //                    additionalList.AddRange(boundCheckTempList);
        //                }
        //            }

        //            if (dCount > dirCount && lengthCount <= 3) // length 5 is more powerful than 2 dir 
        //            {
        //                dirCount = dCount;
        //                matchDirection = mDirection;
        //                if (!matchedList[i].GetTile().isSpecialActivable)
        //                {
        //                    centerGrid = matchedList[i];
        //                    additionalList.Clear();
        //                    additionalList.AddRange(boundCheckTempList);
        //                }
        //            }
        //        }
        //    }

        //    matchedList.AddRange(additionalList);

        //    if (centerGrid != null)
        //    {
        //        // Transform center grid
        //        Tile centerTile = centerGrid.GetTile();

        //        bool centerUpgrade = mapController.UpgradeTile(centerTile, 
        //            lengthCount, dirCount, swapDirection, matchDirection);

        //        // Put all other to process list
        //        for (int i = 0; i < matchedList.Count; i++)
        //        {
        //            if (matchedList[i] == centerGrid)
        //            {
        //                if (!centerUpgrade)
        //                {
        //                    mapController.ExecuteTile(matchedList[i].GetTile(),matchedList[i]);
        //                }
        //                else
        //                {
        //                    // Keep the tile, only active grid
        //                    matchedList[i].OnTileCleared();
        //                    matchedList[i].AffectNearbyGrid();
        //                }
        //            }
        //            else
        //            {
        //                if (centerUpgrade)
        //                {
        //                    matchedList[i].GetTile().combinedTarget = centerTile;
        //                }

        //                matchedList[i].GetTile().SetValue(centerTile.GetValue());
        //                mapController.ExecuteTile(matchedList[i].GetTile(), matchedList[i]);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        // Kill them all
        //        for (int i = 0; i < matchedList.Count; i++)
        //        {
        //            mapController.ExecuteTile(matchedList[i].GetTile(), matchedList[i]);
        //        }
        //    }

        //    return true;
        //} // end (match > 2) IF

        // 
        return false;
    }

    void ActiveSpecialSwap(Grid grid1, Grid grid2)
    {
        // Special - Special, Magic - Magic, Magic - Special
        // NUll check or anything is already done above, this 2 tiles is guaranteed to be special
        //Tile tile1 = grid1.GetTile();
        //Tile tile2 = grid2.GetTile();

        //mapController.ActiveSpecialCombineEffect(tile1, tile2);
    }

    void ActiveMagicSwap(Grid magicGrid, Grid normalGrid)
    {
        // NUll check or anything is already done above, this 2 tiles is guaranteed to be special
        //Tile magicTile = magicGrid.GetTile();
        //Tile normalTile = normalGrid.GetTile();

        //mapController.ActiveMagicEffect(magicTile, normalTile.tileData.tileColor);
    }

    /// <summary>
    /// Get match info, return length count, dir count and match direction
    /// </summary>
    void FindMatchInfoAt(Grid grid, List<Grid> matchedList, out int lengthCount, 
        out int dirCount, out Grid.Direction matchDirection)
    {
        Tile tile = grid.GetTile();

        int dirMatchCount = 0;
        int lengthMatchCount = 0;
        Grid.Direction mDirection = Grid.Direction.UNKNOWN;

        for (int i = 0; i < GameConstants.DIRECTIONS_CHECK_MATCHABLE3.Count; i++) 
        {
            Grid.Direction dir = GameConstants.DIRECTIONS_CHECK_MATCHABLE3[i];

            Grid.Direction opposedDir = Grid.GetOpposedDir(dir);
            findMatchTempList.Clear();
            int length = 0;
            // Find at dir
            Grid finder = grid.GetGridInDir(dir);
            while (finder != null)
            {
                if (finder.isGemContainer && finder.isGemMatchEnabled)
                {
                    Tile finderTile = finder.GetTile();
                    if (finderTile == null) break;
                    if (finderTile.isMoving || finderTile.isOnClearingProcess) break;

                    if (Tile.CheckSameColor(tile, finderTile))
                    {
                        findMatchTempList.Add(finder);
                        length++;
                    }
                    else
                    {
                        break; // WHILE End
                    }
                }
                else { break; }
                finder = finder.GetGridInDir(dir);
            }

            // Find at opposed dir
            finder = grid.GetGridInDir(opposedDir);
            while (finder != null)
            {
                if (finder.isGemContainer && finder.isGemMatchEnabled)
                {
                    Tile finderTile = finder.GetTile();
                    if (finderTile == null) break;
                    if (finderTile.isMoving || finderTile.isOnClearingProcess) break;

                    if (Tile.CheckSameColor(tile, finderTile))
                    {
                        findMatchTempList.Add(finder);
                        length++;
                    }
                    else
                    {
                        break; // WHILE End
                    }
                }
                else { break; }

                finder = finder.GetGridInDir(opposedDir);
            }

            // summarize 
            if (length >= 2)
            {
                dirMatchCount++;
                if (length > lengthMatchCount) lengthMatchCount = length;

                if (matchedList != null)
                {
                    matchedList.AddRange(findMatchTempList);
                }
                mDirection = dir;
            }
        }

        lengthCount = lengthMatchCount;
        dirCount = dirMatchCount;
        matchDirection = mDirection;
    }
}

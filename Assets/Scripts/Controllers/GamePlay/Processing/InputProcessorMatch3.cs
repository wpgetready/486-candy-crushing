﻿/*
 * @Author: CuongNH
 * @Description: Xu ly cac vien trong match 3
 * */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputProcessorMatch3 : IGameInputProcessor
{

    MapController mapController;

    //cache
    List<Grid> matchedList, additionalList, boundCheckTempList, findMatchTempList, addMagnetList, match2x2List;

	bool isSet, isSet_3, isSet_5;

    public InputProcessorMatch3(MapController mapControl)
    {
        this.mapController = mapControl;

        this.matchedList = new List<Grid>(GameConstants.CACHE_NUMBER_GRID_CHECKING);
        this.additionalList = new List<Grid>(GameConstants.CACHE_NUMBER_GRID_CHECKING);
        this.boundCheckTempList = new List<Grid>(GameConstants.CACHE_NUMBER_GRID_CHECKING);
        this.findMatchTempList = new List<Grid>(GameConstants.CACHE_NUMBER_GRID_CHECKING);
        this.addMagnetList = new List<Grid>(GameConstants.CACHE_NUMBER_GRID_CHECKING);
        this.match2x2List = new List<Grid>(GameConstants.CACHE_NUMBER_GRID_CHECKING);
    }

    /// <summary>
    /// Run process at a list of grids by a type
    /// </summary>
    /// <param name="grids">List of grids need to be processed</param>
    /// <param name="processType">Type of processing</param>
    public bool ProcessInput(List<Grid> grids, GameEnum.ProcessType processType = GameEnum.ProcessType.NONE)
    {
        //Debug.Log("ProcessInput:::::::: 0");

        switch (processType)
        {
            case GameEnum.ProcessType.SWAP:
                {
                    return ProcessSwap(grids[0], grids[1]);
                }
                //break;
            case GameEnum.ProcessType.CASCADE:
                {
                    return ProcessCascade(grids);
                }
                //break;
            default:
                break;
        }

        return false;
    }

    /// <summary>
    /// Run matching process at swapped grid, Match-3 Jewel game usually have 2 but in other kind, 
    /// input could be a little different
    /// </summary>
    bool ProcessSwap(Grid grid1, Grid grid2)
    {
        //Debug.Log("ProcessSwap:::::::: 0");

        // DEBUG Message system
        //GameMessageManager.Instance.SendMessageToPopup("swap", grid1.PositionX 
        //    + "\t" + grid1.PositionY + "\t" +grid2.PositionX + "\t" + grid2.PositionY);

        // Mark swapping process
        mapController.isSwapping = true;
        LeanTween.delayedCall(GameConstants.SPEED_SWAP_TILE + GameConstants.TIME_SWAP_DELAY, () =>
        {
            mapController.isSwapping = false;
        });

        // Pre Wait
        

        #region Excute swap
        Tile tile1 = grid1.GetTile();
        Tile tile2 = grid2.GetTile();

        bool result = false;

        // Special swap
        // TODO: swap booster items
        if (((tile1.tileData.tileType == TileData.TileType.rainbow || tile1.tileData.tileType 
            == TileData.TileType.magnet) && (tile2.isColored && tile2.isHit))
            || ((tile2.tileData.tileType == TileData.TileType.rainbow || tile2.tileData.tileType 
            == TileData.TileType.magnet) && (tile1.isColored && tile1.isHit)))
        {
            mapController.ActiveBoosterSwapped(tile1, tile2);

            return true;
        }

        // Swap 2 booster items
        if (tile1.isSpecialActivable && tile2.isSpecialActivable)
        {
            mapController.ActiveBoosterSwapped(tile1, tile2);

            return true;
        }

        // Normal swap
        result |= ActiveMatchProcessAt(grid1, true, grid1.GetDirOfGrid(grid2));
        result |= ActiveMatchProcessAt(grid2, true, grid2.GetDirOfGrid(grid1));

        #endregion

		mapController.StartProcessingPhase();
		GamePlayController.gamePlayController.executeFlag = true;

        return result;
    }

    /// <summary>
    /// Process with list of grid
    /// </summary>
    /// <param name="needCheckGrids"></param>
    /// <returns></returns>
    bool ProcessCascade(List<Grid> needCheckGrids)
    {
        //Debug.Log("InputProcessorMatch3 : ProcessCascade");

        bool result = false;
        for (int i = 0; i < needCheckGrids.Count; i++)
        {
            if (needCheckGrids[i] != null && needCheckGrids[i].gridData.gridType != GridData.GridType.NULL_GRID)
            {
                //Debug.Log("iiiiiiiiiiiiiiiii: " + i);
                result |= ActiveMatchProcessAt(needCheckGrids[i]);
            }
        }

        return result;
    }

    /// <summary>
    /// Handle active matched at grid
    /// </summary>
    /// <param name="grid"></param>
    /// <param name="swapGem"></param>
    /// <param name="swapDirection"></param>
    /// <returns></returns>
    bool ActiveMatchProcessAt(Grid grid, bool swapGem = false, 
        Grid.Direction swapDirection = Grid.Direction.UNKNOWN)
    {
        //Debug.Log("ActiveMatchProcessAt:::::::: 0");

        // Null check or anything is already done above, this 2 tiles is guaranteed to be ok
        Tile tile = grid.GetTile();
        if (tile == null || (tile.isMoving && !swapGem) || tile.isOnClearingProcess || tile.isTransforming)
        {
            //Debug.Log("Debug 1");

            return false;
        }

        // Find all Linked 
        matchedList.Clear();
        int dirCount = 0;
        int lengthCount = 0;
        bool hitMatch2x2;
        Grid.Direction matchDirection;
        FindMatchInfoAt(grid, matchedList, out lengthCount, out dirCount, out matchDirection, out hitMatch2x2);

        //Debug.Log("lengthCount::::::: " + lengthCount);
        //Debug.Log("dirCount::::::: " + dirCount);

        #region Handle matched found
        Grid centerGrid = grid; // set to middle
        if (hitMatch2x2)
        {
            matchedList.Add(grid);
			GamePlayController.gamePlayController.mapController.tileSpawner.isDone = true;

			if(GamePlayController.gamePlayController.mapController.tileSpawner.waterMelon_O_tile.Count > 0)
			{
				foreach(Tile _item in GamePlayController.gamePlayController.mapController.tileSpawner.waterMelon_O_tile)
				{
					//Debug.Log("isMatch");
					_item.isMatch = true;
				}
			}

            bool centerUpgrade = mapController.UpgradeTile(centerGrid.GetTile(), lengthCount, 
                dirCount, swapDirection, matchDirection, matchedList, true);
            // Kill all
            for (int i = 0; i < matchedList.Count; i++)
            {
                
                if (centerGrid == matchedList[i])
                {
                    //mapController.ExecuteTile(matchedList[i].GetTile(), matchedList[i]);

                    //matchedList[i].OnTileCleared();
                    //matchedList[i].AffectNearbyGrid();
                }
                else
                {
                    if (centerUpgrade)
                    {
						matchedList[i].GetTile().combinedTarget = centerGrid.GetTile();
                    }

                    mapController.ExecuteTile(matchedList[i].GetTile(), matchedList[i]);
                }
            }

            mapController.ExecuteTile(centerGrid.GetTile(), centerGrid);
			mapController.countStar.CountStar(75);





            return true;
        }
        else if (lengthCount >= (GameConstants.MATCH3_MIN - 1)) // match found
        {
            //Debug.Log("ActiveMatchProcessAt:::::::: 1");
			GamePlayController.gamePlayController.mapController.tileSpawner.isDone = true;

			if(GamePlayController.gamePlayController.mapController.tileSpawner.waterMelon_O_tile.Count > 0)
			{
				foreach(Tile _item in GamePlayController.gamePlayController.mapController.tileSpawner.waterMelon_O_tile)
				{
					_item.isMatch = true;
				}
			}

			if(Contance.NUMBER_LEVEL == 1 && isSet == false)
			{
				mapController.touchInputHandler.touchInputMask = LayerMask.GetMask("Grid");
				mapController.gridList [2].gameObject.layer = 15;
				mapController.gridList [3].gameObject.layer = 15;
				mapController.gridList [4].gameObject.layer = 15;
				mapController.gridList [9].gameObject.layer = 15;
				mapController.gridList [10].gameObject.layer = 15;
				mapController.gridList [11].gameObject.layer = 15;
				GamePlayController.gamePlayController.introduce.SetActive(false);
				GamePlayController.gamePlayController.isTurnHelp = true;
				isSet = true;
			}

			if(Contance.NUMBER_LEVEL == 4 && isSet_3 == false)
			{
				GamePlayController.gamePlayController.introduce.GetComponent<Lv1Introduce>()._play[1].SetActive(true);
				GamePlayController.gamePlayController.introduce.GetComponent<Lv1Introduce>().content.text = "Keep it up! Drop his meter to zero before you're out of moves!";
				GamePlayController.gamePlayController.introduce.GetComponent<Lv1Introduce>().content.transform.parent.localPosition = Vector3.zero;
				GamePlayController.gamePlayController.introduce.GetComponent<Lv1Introduce>().panel [3].SetActive (true);
				GamePlayController.gamePlayController.introduce.GetComponent<Lv1Introduce>().square.SetActive(false);
				isSet_3 = true;
			}

            matchedList.Add(grid);

            additionalList.Clear();

            bool checkHitMatch2x2Extra = false;
            if (!swapGem)
            {
                // Check all hit and find what's the highest gem
                for (int i = 0; i < matchedList.Count; i++)
                {
                    if (matchedList[i] == grid)
                    {
                        continue; // reduce 1 call =))
                    }

                    int dCount = 0;
                    int lCount = 0;
                    Grid.Direction mDirection;
                    boundCheckTempList.Clear();
                    bool hitMatch2x2Extra = false;
                    FindMatchInfoAt(matchedList[i], boundCheckTempList, 
                        out lCount, out dCount, out mDirection, out hitMatch2x2Extra);

                    if (hitMatch2x2Extra && !checkHitMatch2x2Extra && matchedList.Count < (GameConstants.MATCH3_MIN + 1))
                    {
                        if (!matchedList[i].GetTile().isSpecialActivable)
                        {
                            checkHitMatch2x2Extra = true;

                            centerGrid = matchedList[i];
                            additionalList.Clear();
							additionalList.AddRange(boundCheckTempList);//Debug.Log("2");
                        }
                    }
                    else
                    {
                        if (lCount > lengthCount) // Longer stack
                        {
                            lengthCount = lCount;
                            matchDirection = mDirection;
                            if (!matchedList[i].GetTile().isSpecialActivable)
                            {
                                centerGrid = matchedList[i];
                                additionalList.Clear();
								additionalList.AddRange(boundCheckTempList);//Debug.Log("3");
                            }
                        }

                        if (dCount > dirCount && lengthCount <= GameConstants.MATCH3_MIN) // length 5 is more powerful than 2 dir 
                        {
                            dirCount = dCount;
                            matchDirection = mDirection;
                            if (!matchedList[i].GetTile().isSpecialActivable)
                            {
                                centerGrid = matchedList[i];
                                additionalList.Clear();
                                additionalList.AddRange(boundCheckTempList);
								//Debug.Log("4");
                            }
                        }
                    }
                }
            }

            matchedList.AddRange(additionalList);

            if (centerGrid != null)
            {
                //Debug.Log("ActiveMatchProcessAt:::::::: 2");
                //Debug.Log("matchedList.Count::::::::: " + matchedList.Count);

                // Transform center grid
                Tile centerTile = centerGrid.GetTile();

                bool centerUpgrade = mapController.UpgradeTile(centerTile,
                    lengthCount, dirCount, swapDirection, matchDirection, matchedList, checkHitMatch2x2Extra);

                if (checkHitMatch2x2Extra)
                {
                    // Kill all
                    for (int i = 0; i < matchedList.Count; i++)
                    {

                        if (centerGrid == matchedList[i])
                        {
                            //mapController.ExecuteTile(matchedList[i].GetTile(), matchedList[i]);

                            //matchedList[i].OnTileCleared();
                            //matchedList[i].AffectNearbyGrid();
                        }
                        else
                        {
                            if (centerUpgrade)
                            {
								matchedList[i].GetTile().combinedTarget = centerGrid.GetTile();//Debug.Log("5");
                            }

							mapController.ExecuteTile(matchedList[i].GetTile(), matchedList[i]);
                        }
                    }

                    mapController.ExecuteTile(centerGrid.GetTile(), centerGrid);
					mapController.countStar.CountStar(85);
                }
                else
                {
					Grid _grid = null;
                    // Put all other to process list
                    for (int i = 0; i < matchedList.Count; i++)
                    {
                        if (matchedList[i] == centerGrid)
                        {
                            if (!centerUpgrade)
                            {
								mapController.ExecuteTile(matchedList[i].GetTile(), matchedList[i]);//Debug.Log("6");
								mapController.countStar.CountStar(65);
                            }
                            else
                            {
                                matchedList[i].OnTileCleared();
								mapController.countStar.CountStar(75);
                                if (i == (matchedList.Count - 1))
                                {
									matchedList[i].AffectNearbyGrid(matchedList[i - 1].GetTile());//Debug.Log("7");
                                }
                                else
                                {
									matchedList[i].AffectNearbyGrid(matchedList[i + 1].GetTile());//Debug.Log("8");
                                }
                            }
                        }
                        else
                        {
                            if (centerUpgrade)
                            {
								matchedList[i].GetTile().combinedTarget = centerTile;
                            }

                            // TODO:
                            //matchedList[i].GetTile().SetValue(centerTile.GetValue());
                            mapController.ExecuteTile(matchedList[i].GetTile(), matchedList[i]);
                        }
                    }
                }
            }
            else
            {
                // Kill them all

                for (int i = 0; i < matchedList.Count; i++)
                {
					mapController.ExecuteTile(matchedList[i].GetTile(), matchedList[i]);//Debug.Log("10");
					mapController.countStar.CountStar(255);
                }
            }

			//GamePlayController.gamePlayController.mapController.tileSpawner.waterMelon_O_tile
            return true;
        } // end (match > 2) 
        #endregion

        return false;
    }

    void ActiveSpecialSwap(Grid grid1, Grid grid2)
    {
        // Special - Special, Magic - Magic, Magic - Special
        // NUll check or anything is already done above, this 2 tiles is guaranteed to be special
        //Tile tile1 = grid1.GetTile();
        //Tile tile2 = grid2.GetTile();

        //mapController.ActiveSpecialCombineEffect(tile1, tile2);
    }

    void ActiveMagicSwap(Grid magicGrid, Grid normalGrid)
    {
        // NUll check or anything is already done above, this 2 tiles is guaranteed to be special
        //Tile magicTile = magicGrid.GetTile();
        //Tile normalTile = normalGrid.GetTile();

        //mapController.ActiveMagicEffect(magicTile, normalTile.tileData.tileColor);
    }

    /// <summary>
    /// Get match info, return length count, dir count and match direction
    /// </summary>
    void FindMatchInfoAt(Grid grid, List<Grid> matchedList, out int lengthCount, 
        out int dirCount, out Grid.Direction matchDirection, out bool hitMatch2x2)
    {
        //Debug.Log("FindMatchInfoAt:::::::: 0");

        int dirMatchCount = 0;
        int lengthMatchCount = 0;
        int length = 0;

        Tile tile = grid.GetTile();
        if (tile == null || !tile.isHit)
        {
            //Debug.Log("Debug 1.1");
            lengthCount = 0;
            dirCount = 0;
            matchDirection = Grid.Direction.UNKNOWN;
            hitMatch2x2 = false;

            return;
        }

        bool hasDir = false;
        bool hasOpposedDir = false;

        Grid.Direction mDirection = Grid.Direction.UNKNOWN;

        addMagnetList.Clear();

        #region Check Match 3
        for (int i = 0; i < GameConstants.DIRECTIONS_CHECK_MATCHABLE3.Count; i++) 
        {
            hasDir = false;
            hasOpposedDir = false;

            Grid.Direction dir = GameConstants.DIRECTIONS_CHECK_MATCHABLE3[i];

            Grid.Direction opposedDir = Grid.GetOpposedDir(dir);
            findMatchTempList.Clear();
            length = 0;
            // Find at dir
            Grid finder = grid.GetGridInDir(dir);
            while (finder != null && finder.gridData.gridType == GridData.GridType.NORMAL_GRID)
            {
                Tile finderTile = finder.GetTile();
                if (finderTile == null || !finderTile.isHit)
                {
                    //Debug.Log("Debug 2");
                    break;
                }

                if (finderTile.isMoving || finderTile.isOnClearingProcess/* || finderTile.isTransforming*/)
                {
                    //Debug.Log("Debug 3");
                    break;
                }

                if (Tile.CheckSameHit(tile, finderTile))
                {
                    //Debug.Log("Debug 4");
                    findMatchTempList.Add(finder);
                    length++;
                    hasDir = true;
                }
                else
                {
                    //Debug.Log("Debug 4.1");
                    break; // WHILE End
                }

                finder = finder.GetGridInDir(dir);
            }

            // Find at opposed dir
            finder = grid.GetGridInDir(opposedDir);
            while (finder != null && finder.gridData.gridType == GridData.GridType.NORMAL_GRID)
            {
                Tile finderTile = finder.GetTile();
                if (finderTile == null || !finderTile.isHit)
                {
                    //Debug.Log("Debug 5");
                    break;
                }

                if (finderTile.isMoving || finderTile.isOnClearingProcess/* || finderTile.isTransforming*/)
                {
                    //Debug.Log("Debug 6");
                    break;
                }

                if (Tile.CheckSameHit(tile, finderTile))
                {
                    //Debug.Log("Debug 7");
                    findMatchTempList.Add(finder);
                    length++;
                    hasOpposedDir = true;
                }
                else
                {
                    //Debug.Log("Debug 7.1");
                    break; // WHILE End
                }

                finder = finder.GetGridInDir(opposedDir);
            }

            // Check to create magnet or rainbow
            if (length >= (GameConstants.MATCH3_MIN + 1))
            {
                if (addMagnetList.Count > 0)
				{
                    matchedList.AddRange(addMagnetList);

                    dirMatchCount++;
                }
            }
            else if (addMagnetList.Count >= (GameConstants.MATCH3_MIN + 1))
            {
                if (length > 0)
                {
                    matchedList.AddRange(addMagnetList);
                    matchedList.AddRange(findMatchTempList);

                    dirMatchCount++;
                }
            }

            // summarize 
            if (length >= (GameConstants.MATCH3_MIN - 1))
            {
                dirMatchCount++;

                if (length > lengthMatchCount)
                {
                    lengthMatchCount = length;
                }

                if (matchedList != null)
                {
                    matchedList.AddRange(findMatchTempList);
                }

                addMagnetList.AddRange(findMatchTempList);

                mDirection = dir;
            }
            else if (length >= 1)
            {
                addMagnetList.AddRange(findMatchTempList);
            }

            //// Check to create x_breaker or bomb
            if (hasDir && hasOpposedDir && lengthMatchCount < (GameConstants.MATCH3_MIN + 1))
            {
                dirMatchCount++;
            }
        }

        if (matchedList.Count < (GameConstants.MATCH3_MIN + 1))
        {
            dirMatchCount = 1;
        }

        lengthCount = lengthMatchCount;
        dirCount = dirMatchCount;
        matchDirection = mDirection;

        #endregion

        #region Check Match2x2
        if (lengthCount < (GameConstants.MATCH3_MIN - 1))
        {
            matchedList.Clear();
        }

        int countMatch2x2 = 0;
        for (int i = 0; i < GameConstants.CORNER_ARRAY.Length; i++)
        {
            match2x2List.Clear();
            countMatch2x2 = 0;
            List<Grid.Direction> corner = GameConstants.CORNER_ARRAY[i];
            for (int ii = 0; ii < corner.Count; ii++)
            {
                Grid.Direction dir = corner[ii];
                Grid finder = grid.GetGridInDir(dir);
                if (finder == null || finder.gridData.gridType == GridData.GridType.NULL_GRID)
                {
                    break;
                }

                Tile finderTile = finder.GetTile();
                if (finderTile == null || !finderTile.isHit)
                {
                    break;
                }

                if (finderTile.isMoving || finderTile.isOnClearingProcess/* || finderTile.isTransforming*/)
                {
                    break;
                }

                if (Tile.CheckSameHit(tile, finderTile))
                {
                    countMatch2x2++;
                    if (!matchedList.Contains(finder))
                    {
                        match2x2List.Add(finder);
                    }
                }
                else
                {
                    break;
                }
            }

            if (countMatch2x2 >= (GameConstants.MATCH2X2_MIN - 1))
            {
                break;
            }
        }

        bool matched2x2 = false;
        if (countMatch2x2 >= (GameConstants.MATCH2X2_MIN - 1))
        {
            if (matchedList.Count < (GameConstants.MATCH3_MIN))
            {
                matched2x2 = true;
            }

            matchedList.AddRange(match2x2List);
        }
        hitMatch2x2 = matched2x2;
        #endregion
    }
}

﻿/*
 * @Author: CuongNH
 * @Description: Xu ly viec nhap cac vien match 3
 * */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputControlMatch3 : IGameInputControl
{
    // Controller Components
    MapController mapController;
    IGameInputProcessor inputProcessor;
    //IGameInputProcessor inputProcessorMatch2x2;

    // Grid selected
    Grid selectedGrid;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="mapControl"></param>
    public InputControlMatch3(MapController mapControl)
    {
        mapController = mapControl;
        inputProcessor = mapControl.gameInputProcessor;
        //inputProcessorMatch2x2 = mapControl.gameInputProcessorMatch2x2;
        selectedGrid = null;
    }

    /// <summary>
    /// Dispose input
    /// </summary>
    public void DisposeInput()
    {
        selectedGrid = null;
        //mapController.HideGridSelector();
    }

    /// <summary>
    /// Update input
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="obj"></param>
    /// <param name="inState"></param>
    public void UpdateInput(Vector2 pos, GameObject obj, GameEnum.InputState inState)
    {
        //Debug.Log("UpdateInput");

        if (inState == GameEnum.InputState.END)
        {
            //Debug.Log("UpdateInput : GameEnum.InputState.END");

            Grid g = obj.GetComponent<Grid>();
            if (g != null)
            {

            }
        }
        else if (inState == GameEnum.InputState.BEGIN)
        {
			
            //Debug.Log("UpdateInput : GameEnum.InputState.BEGIN");

            Grid g = obj.GetComponent<Grid>();
            if (g != null)
            {
                //Debug.Log(g.gridPosition.Column + " : " + g.gridPosition.Row);
                if (selectedGrid != null)
                {
                    if (mapController.isSwapping)
                    {
                        return;
                    }

                    if (mapController.mapConditionChecker.CheckSwappable(selectedGrid, g))
                    {
                        // For test
                        //FakeSwapTiles(selectedGrid, g);

                        if (mapController.mapConditionChecker.CheckSwapExecutable(selectedGrid, g)
                            || mapController.mapConditionCheckerMatch2x2.CheckSwapExecutable(selectedGrid, g))
                        {
							SoundController.sound.Swap_Move ();
                            SwapTiles(selectedGrid, g);
                        }
                        else
                        {
							SoundController.sound.WrongSwap ();
                            FakeSwapTiles(selectedGrid, g);
                        }

                        // reset
                        selectedGrid = null;
                        //mapController.HideGridSelector();
                    }
                    else
                    {
                        // set new selected
                        TrySelectGrid(g);
                    }
                }
                else
                {
                    // set new selected
                    TrySelectGrid(g);
                }
            }
            else
            {
                Debug.Log("Not found grid");
            }
        }
        else if (inState == GameEnum.InputState.MOVE)
        {
			
            //Debug.Log("UpdateInput : GameEnum.InputState.MOVE");

            Grid g = obj.GetComponent<Grid>();
            if (g != null && selectedGrid != g)
            {
                // Swipe action
                if (selectedGrid != null)
                {
                    if (mapController.isSwapping)
                    {
                        return;
                    }

                    if (mapController.mapConditionChecker.CheckSwappable(selectedGrid, g))
                    {
                        // For test
                        //FakeSwapTiles(selectedGrid, g);

                        if (mapController.mapConditionChecker.CheckSwapExecutable(selectedGrid, g)
                            || mapController.mapConditionCheckerMatch2x2.CheckSwapExecutable(selectedGrid, g))
                        {
							SoundController.sound.Swap_Move ();
                            SwapTiles(selectedGrid, g);
                        }
                        else
                        {
							SoundController.sound.WrongSwap ();
                            FakeSwapTiles(selectedGrid, g);
                        }

                        // reset
                        selectedGrid = null;
                        //mapController.HideGridSelector();
                    }
                }
            }
        }
    }

    /// <summary>
    /// Select firt grid
    /// </summary>
    /// <param name="g"></param>
    void TrySelectGrid(Grid g)
    {
        //Debug.Log("TrySelectGrid");

        if ((g != null && g.gridData.gridType != GridData.GridType.NULL_GRID) 
            && (g.GetTile() != null && g.GetTile().isSwappable)
            && (!g.GetTile().isMoving && !g.GetTile().isOnClearingProcess))
        {
            selectedGrid = g;
            //mapController.ShowGridSelector(g);
        }
        else
        {
            selectedGrid = null;
            //mapController.HideGridSelector();
        }
    }

    /// <summary>
    /// Swap 2 tile, condition all checked
    /// </summary>
    void SwapTiles(Grid grid1, Grid grid2)
    {
        //Debug.Log("SwapTiles");

        if (!GameConstants.ENABLE_SWAP_WHILE_PROCESS && mapController.isProcessing)
        {
            return;
        }

        Tile tile1 = grid1.GetTile();
        Tile tile2 = grid2.GetTile();

        grid1.SetTile(null);
        grid2.SetTile(null);

        if (tile1 != null)
        {
            grid2.SetTile(tile1);
        }

        if (tile2 != null)
        {
            grid1.SetTile(tile2);
        }

        grid1.MoveTileToCenter(GameConstants.SPEED_SWAP_TILE, Tile.MoveFinishType.SWAP);
        grid2.MoveTileToCenter(GameConstants.SPEED_SWAP_TILE, Tile.MoveFinishType.SWAP);

        // Process 2 swapped tiles
        List<Grid> swappedGrids = new List<Grid>();
        swappedGrids.Add(grid1);
        swappedGrids.Add(grid2);

        // Wait until swapped animation finished
        LeanTween.delayedCall(GameConstants.SPEED_SWAP_TILE, () =>
        {
            if (GameManager.limitType == GameManager.LimitType.MOVE_LIMIT)
            {
                if (GameManager.MOVE > 0)
                {
                    GameManager.MOVE--;
                    GameManager.SPAW_TIME++;
                    GamePlayController.gamePlayController.UpdateMoveDisplay();
                }
            }

            /*GamePlayController.gamePlayController.*/
            mapController.FinishProcessingPhase(false);

            inputProcessor.ProcessInput(swappedGrids, GameEnum.ProcessType.SWAP);
        });
    }

    /// <summary>
    /// Just animation, no real swap
    /// </summary>
    void FakeSwapTiles(Grid grid1, Grid grid2)
    {
        //Debug.Log("FakeSwapTiles");

        if (!GameConstants.ENABLE_SWAP_WHILE_PROCESS && mapController.isProcessing)
        {
            return;
        }

        Tile tile1 = grid1.GetTile();
        Tile tile2 = grid2.GetTile();

        // Move -> check objective -> return if not changed anything
        if (tile1 != null)
        {
            grid2.SetTile(tile1);
            grid2.MoveTileToCenter(GameConstants.SPEED_SWAP_TILE, Tile.MoveFinishType.SWAP);
        }

        if (tile2 != null)
        {
            grid1.SetTile(tile2);
            grid1.MoveTileToCenter(GameConstants.SPEED_SWAP_TILE, Tile.MoveFinishType.SWAP);
        }

        mapController.isSwapping = true;

        LeanTween.delayedCall(GameConstants.SPEED_SWAP_TILE, () =>
        {
            //if (GameplayController.instance.UpdateObjectives(true))
            //{
            //    mapController.StartProcessingPhase();
            //    GameplayController.instance.executeFlag = true;

            //    GameplayController.instance.dropFlag = true;
            //    GameplayController.instance.SetExecutionDelay(GameConstants.SPEED_SWAP_TILE);

            //    mapController.isSwapping = false;
            //}
            //else
            //{
                //AudioController.PlaySFX("sfx_unswap");

                if (tile1 != null)
                {
                    grid1.SetTile(tile1);
                    grid1.MoveTileToCenter(GameConstants.SPEED_SWAP_TILE, Tile.MoveFinishType.SWAP);
                }
                if (tile2 != null)
                {
                    grid2.SetTile(tile2);
                    grid2.MoveTileToCenter(GameConstants.SPEED_SWAP_TILE, Tile.MoveFinishType.SWAP);
                }

                // Mark swapping process
                LeanTween.delayedCall(GameConstants.SPEED_SWAP_TILE, () =>
                {
                    mapController.isSwapping = false;
                });
            //}
        });
    }
}

﻿/*
 * @Author: CuongNH
 * @Description: Check condition match 2x2
 * */

using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class MapConditionCheckerMatch2x2 : IMapConditionChecker
{
    MapController mapController;

    //cache
    List<Grid> checkingGroup;

    /// <summary>
    /// Constructor with MapController
    /// </summary>
    /// <param name="mapCtrl"></param>
    public MapConditionCheckerMatch2x2(MapController mapCtrl)
    {
        mapController = mapCtrl;
        checkingGroup = new List<Grid>(GameConstants.CACHE_NUMBER_GRID_CHECKING);
    }

    /// <summary>
    /// Check has valid move
    /// </summary>
    /// <returns></returns>
    public bool CheckAvailableMove()
    {
        // Check for normal move
        for (int i = 0; i < mapController.gridList.Count; i++)
        {
            Grid grid = mapController.gridList[i];
            if (CheckNormalGemMoveInGrid(grid))
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Check move normal match 2x2
    /// </summary>
    /// <param name="grid"></param>
    /// <returns></returns>
    bool CheckNormalGemMoveInGrid(Grid grid)
    {
        if (grid == null || grid.gridData.gridType == GridData.GridType.NULL_GRID)
        {
            return false;
        }

        Tile tile = grid.GetTile();
		if (tile == null || !tile.isHit || tile.tileData.tileModifierType == TileData.TileModifierType.ice_cage ||
			tile.tileData.tileModifierType == TileData.TileModifierType.watermelon_O || tile.tileData.tileModifierType == TileData.TileModifierType.watermelon_I)
        {
            return false;
        }

        //Debug.Log("CheckNormalGemMoveInGrid 0 at " + grid.gridPosition.Column + " : " + grid.gridPosition.Row);

        checkingGroup.Clear();
        checkingGroup.Add(grid);
        for (int i = 0; i < GameConstants.DIRECTIONS_CHECK_MATCHABLE_2X2.Count; i++)
        {
            Grid.Direction dir = GameConstants.DIRECTIONS_CHECK_MATCHABLE_2X2[i];
            // Get References
            Grid nextGrid = grid.GetGridInDir(dir);
            if (nextGrid == null || nextGrid.gridData.gridType == GridData.GridType.NULL_GRID)
            {
                break;
            }

            //if (!nextGrid.isGemMatchEnabled || !nextGrid.isGemContainer) continue;
            Tile nextTile = nextGrid.GetTile();
            //if (nextTile == null && !GameConstants.ENABLE_SWAP_WITH_EMPTY_GRID) continue;
            if (nextTile == null || !nextTile.isSwappable)
            {
                break;
            }

            checkingGroup.Add(nextGrid);
        }

        if (checkingGroup.Count < GameConstants.MATCH2X2_MIN)
        {
            return false;
        }

        for (int i = 0; i < checkingGroup.Count; i++)
        {
            int countMatch = 1;
            Grid missGrid = null;
            Grid matchGrid = null;
            for (int j = 0; j < checkingGroup.Count; j++)
            {
                if (i != j)
                {
                    Tile tileCheck = checkingGroup[i].GetTile();
                    Tile nextTileCheck = checkingGroup[j].GetTile();
                    if (Tile.CheckSameHit(tileCheck, nextTileCheck))
                    {
                        countMatch++;
                    }
                    else
                    {
                        if (countMatch >= (GameConstants.MATCH2X2_MIN - 1))
                        {
                            missGrid = checkingGroup[j];
                            matchGrid = checkingGroup[i];

                            for (int ii = 0; ii < GameConstants.DIRECTIONS_SWAPPABLE.Count; ii++)
                            {
                                Grid.Direction cDir = GameConstants.DIRECTIONS_SWAPPABLE[ii];

                                Grid cGrid = missGrid.GetGridInDir(cDir);
                                if (cGrid == null || cGrid.gridData.gridType == GridData.GridType.NULL_GRID)
                                {
                                    continue;
                                }

                                if (checkingGroup.Contains(cGrid))
                                {
                                    continue;
                                }

                                //if (!cGrid.isGemMatchEnabled || !cGrid.isInsideGemSwappable) continue;
                                Tile cTile = cGrid.GetTile();
                                if (cTile == null || !cTile.isHit || !cTile.isSwappable)
                                {
                                    continue;
                                }

                                //Debug.Log("CheckNormalGemMoveInGrid 4 at " + grid.gridPosition.Column + " : " + grid.gridPosition.Row);

                                if (Tile.CheckSameHit(matchGrid.GetTile(), cTile))
                                {
                                    //Debug.Log("CheckNormalGemMoveInGrid 5 at " + grid.gridPosition.Column + " : " + grid.gridPosition.Row);
                                    // Save result found
									if(matchGrid.GetTile().tileData.tileModifierType != TileData.TileModifierType.ice_cage && cTile.tileData.tileModifierType != TileData.TileModifierType.ice_cage)
									{
	                                    mapController.SetHelpGrid(missGrid, cGrid);

	                                    return true;
									}
                                }
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    /// <summary>
    /// Check has hit at grid
    /// </summary>
    /// <param name="grid"></param>
    /// <returns></returns>
    public bool CheckHitInGrid(Grid grid)
    {
        if (grid == null || grid.gridData.gridType == GridData.GridType.NULL_GRID)
        {
            return false;
        }

        //if (!grid.isGemMatchEnabled || !grid.isGemContainer) return false;

        Tile tile = grid.GetTile();

        if (tile == null || !tile.isHit)
        {
            return false;
        }

        List<Tile> checkList = new List<Tile>();
        checkList.Add(tile);
        bool check = true;
        for (int i = 0; i < GameConstants.DIRECTIONS_CHECK_MATCHABLE_2X2.Count; i++)
        {
            Grid.Direction dir = GameConstants.DIRECTIONS_CHECK_MATCHABLE_2X2[i];

            // Get References
            Grid nextGrid = grid.GetGridInDir(dir);
            if (nextGrid == null || nextGrid.gridData.gridType == GridData.GridType.NULL_GRID)
            {
                return false;
            }

            Tile nextTile = nextGrid.GetTile();
            if (nextTile == null || !nextTile.isHit)
            {
                return false;
            }

            if(Tile.CheckSameHit(tile, nextTile))
            {
                check = check && true;
            }
            else
            {
                return false;
            }
        }

        return check;
    }

    /// <summary>
    /// Check swappable 2 grid
    /// </summary>
    /// <param name="grid1"></param>
    /// <param name="grid2"></param>
    /// <returns></returns>
    public bool CheckSwappable(Grid grid1, Grid grid2)
    {
        return false;
    }

    /// <summary>
    /// Check swap excutable 2 grid
    /// </summary>
    /// <param name="grid1"></param>
    /// <param name="grid2"></param>
    /// <returns></returns>
    public bool CheckSwapExecutable(Grid grid1, Grid grid2)
    {
        //Debug.Log("CheckSwapExecutable 0 :::::::::::");
        if (grid1 == null || grid2 == null || grid1.gridData.gridType == GridData.GridType.NULL_GRID
            || grid2.gridData.gridType == GridData.GridType.NULL_GRID)
        {
            return false;
        }

        //Debug.Log("CheckSwapExecutable 1 :::::::::::");

        Tile tile1 = grid1.GetTile();
        Tile tile2 = grid2.GetTile();
        if (tile1 == null || tile2 == null)
        {
            return false;
        }

        //Debug.Log("CheckSwapExecutable 2 :::::::::::");

        // Swap with normal Tile
        if (CheckMatchSwap(grid1.GetTile(), grid2, grid2.GetDirOfGrid(grid1)) ||
            CheckMatchSwap(grid2.GetTile(), grid1, grid1.GetDirOfGrid(grid2)))
        {
            //Debug.Log("CheckSwapExecutable 3 :::::::::::");

            return true;
        }

        return false;
    }

    /// <summary>
    /// Test match color
    /// </summary>
    /// <param name="testIile"></param>
    /// <param name="targetGrid"></param>
    /// <param name="comeDir"></param>
    /// <returns></returns>
    public bool CheckMatchSwap(Tile testIile, Grid targetGrid, Grid.Direction comeDir)
    {
        if (testIile == null || !testIile.isHit)
        {
            return false;
        }

        return CheckMatchNormalSwap(testIile, targetGrid, comeDir);
    }

    /// <summary>
    /// Check match with normal tile
    /// </summary>
    /// <param name="testTile"></param>
    /// <param name="targetGrid"></param>
    /// <param name="comeDir"></param>
    /// <returns></returns>
    bool CheckMatchNormalSwap(Tile testTile, Grid targetGrid, Grid.Direction comeDir)
    {
        bool result = false;
        //int matchCounter = 0; // Max length of match in one dir 
        //int matchDirCounter = 0; // Number of dir that have a match

        for (int i = 0; i < GameConstants.CORNER_ARRAY.Length; i++)
        {
            List<Grid.Direction> corner = GameConstants.CORNER_ARRAY[i];
            for (int ii = 0; ii < corner.Count; ii++)
            {
                Grid.Direction dir = corner[ii];
                if (dir != comeDir)
                {
                    Grid nextGrid = targetGrid.GetGridInDir(dir);
                    if (nextGrid == null || nextGrid.gridData.gridType == GridData.GridType.NULL_GRID)
                    {
                        result = false;
                        break;
                    }

                    Tile nextTile = nextGrid.GetTile();
                    if (nextTile == null || !nextTile.isHit)
                    {
                        result = false;
                        break;
                    }

                    if (Tile.CheckSameHit(testTile, nextTile))
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                        break;
                    }
                }
                else
                {
                    result = false;
                    break;
                }
            }

            if (result)
            {
                return true;
            }
            else
            {
                continue;
            }
        }

        return result;
    }
}

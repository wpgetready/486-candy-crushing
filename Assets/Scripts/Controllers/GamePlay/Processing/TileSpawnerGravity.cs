﻿/*
 * Spawn and shrinking with gravity
 * 
 * Corountine:  Shrink -> Spawn -> Repeat while still have changes
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileSpawnerGravity : ITileSpawner
{

    MapController mapController;
    TileManagerMatch3 tileManager;

    List<Grid> predefinedGrids;
    List<Grid> ignoredGrids; // Gem won't spawn here

    List<List<int>> groupStartIDListList, idListList;

    // cache
    List<Grid> processingGroup;



    public TileSpawnerGravity(MapController mc)
    {
        mapController = mc;
        tileManager = mapController.tileManager;

        // data
        predefinedGrids = new List<Grid>();
        ignoredGrids = new List<Grid>();

        // cache
        processingGroup = new List<Grid>();

        ConfigureIDLists();
    }

    void ConfigureIDLists()
    {
        groupStartIDListList = new List<List<int>>();
        idListList = new List<List<int>>();
        for (int i = 0; i < Grid.DIR_NUM; i++)
        {
            groupStartIDListList.Add(MapUtilities.GetGroupStartIDList((Grid.Direction)i));
            idListList.Add(MapUtilities.GetIDList((Grid.Direction)i));
        }
    }

    public float SpawnTiles(Grid.Direction direction)
    {
        float maxSpawnTime = 0;

        //List<int> idList = groupStartIDListList[(int)direction];
        //for (int i = 0; i < idList.Count; i++)
        //{
        //    Grid grid = mapController.gridList[idList[i]];
        //    // Re-check for sure, maybe not even nescessary
        //    if (grid == null) continue;
        //    if (ignoredGrids.Contains(grid)) continue;
        //    // Try to respawn
        //    if (GameConstants.ENABLE_PATCH_SPAWNER)
        //    {
        //        float t = SpawnTileAtGridPatch(grid, direction);
        //        maxSpawnTime = maxSpawnTime > t ? maxSpawnTime : t;
        //    }
        //    //else
        //    //ChangesDetect |= SpawnTileAtGrid(grid, direction);
        //}

        return (maxSpawnTime);
    }

    public float SpawnTilesNormal(Grid.Direction direction)
    {
        float maxSpawnTime = 0;

        //List<int> idList = groupStartIDListList[(int)direction];
        //for (int i = 0; i < idList.Count; i++)
        //{
        //    Grid grid = mapController.gridList[idList[i]];
        //    // Re-check for sure, maybe not even nescessary
        //    if (grid == null) continue;
        //    if (ignoredGrids.Contains(grid)) continue;
        //    // Try to respawn
        //    if (GameConstants.ENABLE_PATCH_SPAWNER)
        //    {
        //        float t = SpawnTileAtGridPatch(grid, direction, true);
        //        maxSpawnTime = maxSpawnTime > t ? maxSpawnTime : t;
        //    }
        //    //else
        //    //ChangesDetect |= SpawnTileAtGrid(grid, direction);
        //}

        return (maxSpawnTime);
    }


    /// <summary>
    /// Spawn whole pack of gem
    /// </summary>
    float SpawnTileAtGridPatch(Grid rootGrid, Grid.Direction direction, bool skipQueued = false)
    {
        float timeForSpawn = 0; // used to determine if there is anything change here
        //// Check each group, find all grid followed
        //processingGroup.Clear();
        //processingGroup.Add(rootGrid); // starter

        //// Find counter gravity dir
        //Grid.Direction opposedDir = Grid.GetOpposedDir(direction);

        //Grid nextgrid = rootGrid.GetGridInDir(opposedDir); // find next
        //while (nextgrid != null && nextgrid != rootGrid)
        //{
        //    processingGroup.Add(nextgrid);
        //    nextgrid = nextgrid.GetGridInDir(opposedDir);
        //}

        //// Process
        //bool lineOfSkyClear = false; // used to check if this grid have a line to sky, a tile will fall from heaven to it =))
        //int lineOfSkySpawnPos = 0; // from what grid the spawn will start at

        //for (int i = 0; i < processingGroup.Count; i++)
        //{
        //    Grid currentGrid = processingGroup[i];
        //    // Nothing changed there 
        //    //if (!currentGrid.isChangedLastTurn) continue;
        //    // If it already have a tile, why bother try to spawn?
        //    if (currentGrid.GetTile() != null) continue;
        //    // It CAN'T even contain a gem =.=
        //    if (!currentGrid.isGemContainer) continue;

        //    // Finding
        //    bool loS = true; // line of sky checker
        //    for (int j = i + 1; j < processingGroup.Count; j++)
        //    {
        //        Grid g = processingGroup[j];

        //        if (g.GetTile() == null)
        //        {
        //            if (g.isBlockGemMove || g.isHoldGemInside)
        //            //                      Gem will stuck here
        //            {
        //                loS = false;
        //                break;
        //            }
        //            else
        //                continue;
        //        }
        //        else
        //        {
        //            loS = false;
        //            break;
        //        }

        //    }
        //    // Finish
        //    if (loS)
        //    {
        //        lineOfSkyClear = true;
        //        lineOfSkySpawnPos = i;
        //        break;
        //    }
        //}

        //if (lineOfSkyClear) // instant spawn if have line of sky or gravity is centered
        //{
        //    int counter = 0;
        //    for (int i = lineOfSkySpawnPos; i < processingGroup.Count; i++)
        //    {
        //        Grid g = processingGroup[i];
        //        // Fill Spawn
        //        if (g.isGemContainer)
        //        {
        //            // Create Tile
        //            Tile newTile = ObjectManager.Instance.GetTile();
        //            newTile.transform.SetParent(g.transform, false);

        //            // Config newly created tile
        //            tileManager.TileSpawn(newTile, null, skipQueued);

        //            // Find position
        //            int top = processingGroup.Count - i + counter;
        //            Vector2 topPos = Grid.GetMapStepWithDirection(opposedDir) * top * GameConstants.GRID_SIZE;
        //            // SetPosition
        //            newTile.transform.localPosition = new Vector3(topPos.x, topPos.y, GameConstants.TILE_LOCAL_Z);
        //            g.SetTile(newTile);

        //            // 
        //            float time = 0;
        //            if (direction == Grid.Direction.CENTER)
        //            {
        //                time = 0.3f;
        //                time += g.MoveTileToCenter(time, Tile.MoveFinishType.GROW);

        //            }
        //            else
        //            {
        //                time = top * GameConstants.SPEED_DROP_TILE_EACH_STEP * Grid.GetDropTimeModifierWithDirection(direction);
        //                time += g.MoveTileToCenter(time, Tile.MoveFinishType.BOUNCE);
        //            }

        //            timeForSpawn = timeForSpawn > time ? timeForSpawn : time;
        //            counter++;
        //        } // if
        //    } // for
        //}// if

        return timeForSpawn;
    }

    /// <summary>
    /// Spawn gem at "top" only
    /// </summary>
    bool SpawnTileAtGrid(Grid rootGrid, Grid.Direction direction)
    {
        bool stateChanged = false; // used to determine if there is anything change here
        //// Check each group, find all grid followed
        //processingGroup.Clear();
        //processingGroup.Add(rootGrid); // starter

        //// Find counter gravity dir
        //Grid.Direction opposedDir = Grid.GetOpposedDir(direction);

        //Grid nextgrid = rootGrid.GetGridInDir(opposedDir); // find next
        //while (nextgrid != null)
        //{
        //    processingGroup.Add(nextgrid);
        //    nextgrid = nextgrid.GetGridInDir(opposedDir);
        //}

        //// Process
        //bool lineOfSkyClear = false; // used to check if this grid have a line to sky, a tile will fall from heaven to it =))
        //int lineOfSkySpawnPos = 0; // from what grid the spawn will start at

        //for (int i = processingGroup.Count - 1; i >= 0; i--)
        //{
        //    Grid currentGrid = processingGroup[i];
        //    // Nothing changed there 
        //    //if (!currentGrid.isChangedLastTurn) continue;
        //    // If it already have a tile, why bother try to spawn?
        //    //if (currentGrid.GetTile() == null) continue;
        //    // It CAN'T even contain a gem =.=
        //    if (!currentGrid.isGemContainer) continue;

        //    lineOfSkyClear = true;
        //    lineOfSkySpawnPos = i;
        //    break;
        //}

        //if (lineOfSkyClear || direction == Grid.Direction.CENTER) // instant spawn if have line of sky or gravity is centered
        //{
        //    int counter = 0;
        //    Grid g = processingGroup[lineOfSkySpawnPos];
        //    // Fill Spawn
        //    if (g.isGemContainer && g.GetTile() == null)
        //    {
        //        // Create Tile
        //        Tile newTile = ObjectManager.Instance.GetTile();
        //        newTile.transform.SetParent(g.transform, false);

        //        // Config newly created tile
        //        tileManager.TileSpawn(newTile);

        //        // Find position
        //        Vector2 topPos = Grid.GetMapStepWithDirection(opposedDir) * (processingGroup.Count - lineOfSkySpawnPos + counter) * GameConstants.GRID_SIZE;
        //        // SetPosition
        //        newTile.transform.localPosition = new Vector3(topPos.x, topPos.y, GameConstants.TILE_LOCAL_Z);

        //        g.SetTile(newTile);
        //        g.MoveTileToCenter();

        //        stateChanged = true;
        //    }

        //}

        return stateChanged;
    }

    #region Temporary close
    /*
    public float ShrinkTiles(Grid.Direction direction, bool diagonalDrop = false)
    {
        float maxDropTime = 0;

        List<int> idList = idListList[(int)direction];

        for (int i = 0; i < idList.Count; i++)
        {
            Grid grid = mapController.gridList[idList[i]];
            // Re-check for sure, maybe not even nescessary
            if (grid == null) continue;
            // Nothing changed there 
            //if (!grid.isChangedLastTurn) continue;
            // If it already have a tile, why bother try to shrink into?
            if (grid.GetTile() != null) continue;
            // It CAN'T even contain a gem =.=
            if (!grid.isGemContainer) continue;
            // Try to shrink
            if (diagonalDrop)
            {
                float t = ShrinkTileAtGridDiagonal(grid, direction);
                maxDropTime = maxDropTime > t ? maxDropTime : t;
            }
            else
            {
                float t = ShrinkTileAtGrid(grid, direction);
                maxDropTime = maxDropTime > t ? maxDropTime : t;
            }
        }

        return (maxDropTime);
    }

    float ShrinkTileAtGrid(Grid rootGrid, Grid.Direction direction)
    {
        // Exception
        if (direction == Grid.Direction.CENTER || direction == Grid.Direction.UNKNOWN)
            return 0;
        //==========

        float timeToDrop = 0; // used to determine if there is anything change here
        // Check if there is any direct drop (drop in main direction)

        // Find counter gravity dir
        Grid.Direction opposedDir = Grid.GetOpposedDir(direction);

        Grid nextgrid = rootGrid.GetGridInDir(opposedDir); // find next
        Grid targetGrid = null; // the grid that will drop here
        //bool haveLineOfSky = true;
        int counter = 1; // count number of grid it passed
        while (nextgrid != null)
        {
            if (nextgrid == rootGrid) break;

            if (nextgrid.GetTile() == null)
            {
                if (nextgrid.isBlockGemMove || nextgrid.isHoldGemInside)
                {
                    //haveLineOfSky = false;
                    break;
                }
                else
                {
                    nextgrid = nextgrid.GetGridInDir(opposedDir);
                    counter++;
                    continue;
                }
            }
            if (nextgrid.isHoldGemInside || !nextgrid.GetTile().isDroppable)
            {
                //haveLineOfSky = false;
                break;
            }
            if (nextgrid.GetTile().isDroppable)
            {
                targetGrid = nextgrid;
                break;
            }
            nextgrid = nextgrid.GetGridInDir(opposedDir);
            counter++;
        }
        
        // Drop down
        if (targetGrid != null && !nextgrid.GetTile().isMoving)
        {
            rootGrid.SetTile(targetGrid.GetTile());
            targetGrid.SetTile(null);

            int moveLength = counter;
            float time = moveLength * GameConstants.TIME_SPEED_PER_GRID * Grid.GetDropTimeModifierWithDirection(direction);
            rootGrid.MoveTileToCenter(time, 0, direction);

            timeToDrop = time;
        }
        //else
        //{
        //    // Check line of sky
        //    if (!haveLineOfSky)
        //    {
        //        return ShrinkTileAtGridDiagonal(rootGrid, direction);
        //    }
        //}

        return timeToDrop;
    }

    float ShrinkTileAtGridDiagonal(Grid rootGrid, Grid.Direction direction)
    {
        float timeToDrop = 0; // used to determine if there is anything change here
        // Check if there is any direct drop (drop in main direction)

        // Find counter gravity dir
        Grid.Direction opposedDir = Grid.GetOpposedDir(direction);

        Grid nextgrid = rootGrid.GetGridInDir(opposedDir); // find next
        bool haveLineOfSky = true;
        int counter = 1; // count number of grid it passed
        while (nextgrid != null)
        {
            if (nextgrid == rootGrid) break;

            if (nextgrid.GetTile() == null)
            {
                if (nextgrid.isBlockGemMove || nextgrid.isHoldGemInside)
                {
                    haveLineOfSky = false;
                    break;
                }
                else
                {
                    nextgrid = nextgrid.GetGridInDir(opposedDir);
                    counter++;
                    continue;
                }
            }
            if (nextgrid.isHoldGemInside || !nextgrid.GetTile().isDroppable)
            {
                haveLineOfSky = false;
                break;
            }
            nextgrid = nextgrid.GetGridInDir(opposedDir);
            counter++;
        }

        if (haveLineOfSky) return 0;

        // Check below
        Grid belowgrid = rootGrid.GetGridInDir(direction); // find below
        if (belowgrid != null)
        {
            if (belowgrid.isGemContainer)
            {
                if (!(belowgrid.GetTile() != null && !belowgrid.GetTile().isMoving))
                return 0;
            }
        }

        // Try adjacent
        List<Grid.Direction> AdjacentList = Grid.GetDroppableDiagonalDirections(opposedDir);
        List<Grid> mayDropgridList = new List<Grid>();

        for (int i = 0; i < AdjacentList.Count; i++)
        {
            Grid.Direction dir = AdjacentList[i];

            Grid g = rootGrid.GetGridInDir(dir);
            if (g == null) continue;// mayDropgridList.Add(fGrid);

            Grid gravityBelowGrid = g.GetGridInDir(direction);

            if (gravityBelowGrid != null)
            {
                // Check if can be dropped below
                if (gravityBelowGrid.isGemContainer && gravityBelowGrid.GetTile() == null)
                {
                    continue;
                }
            }

            if (g.GetTile() == null)
            {
                continue;
            }
            if (g.isHoldGemInside || !g.GetTile().isDroppable || g.GetTile().isMoving)
            {
                continue;
            }
            if (g.GetTile().isDroppable)
            {
                rootGrid.SetTile(g.GetTile());
                g.SetTile(null);

                int moveLength = 1;
                float time = moveLength * GameConstants.TIME_SPEED_PER_GRID *Grid.GetDropTimeModifierWithDirection(dir);
                rootGrid.MoveTileToCenter(time, 0, dir); // diagonal drop, cut 1/4 length

                timeToDrop = time / 2f;
                break;
            }
        }

        return timeToDrop;
    }
    */
    #endregion

    public float ShrinkTiles(Grid.Direction direction, bool diagonalDrop = false)
    {
        float maxDropTime = 0;

        List<int> idList = idListList[(int)direction];

        // Try to shrink
        if (diagonalDrop)
        {
            for (int i = 0; i < idList.Count; i++)
            {
                Grid grid = mapController.gridList[idList[i]];
                // Re-check for sure, maybe not even nescessary
                if (grid == null) continue;
                // Nothing changed there 
                //if (!grid.isChangedLastTurn) continue;
                // It CAN'T even contain a gem =.=
                if (!grid.isGemContainer || grid.isHoldGemInside) continue;
                // If it already have a tile, why bother try to shrink into?
                if (    grid.GetTile() == null ||
                        !grid.GetTile().isDroppable ||
                        grid.GetTile().isMoving ||
                        grid.GetTile().isOnClearingProcess
                    )
                    continue;
                float t = ShrinkTileAtGridDiagonal(grid, direction);
                maxDropTime = maxDropTime > t ? maxDropTime : t;
            }

        }
        else
        {
            for (int i = 0; i < idList.Count; i++)
            {
                Grid grid = mapController.gridList[idList[i]];
                // Re-check for sure, maybe not even nescessary
                if (grid == null) continue;
                // Nothing changed there 
                //if (!grid.isChangedLastTurn) continue;
                // If it already have a tile, why bother try to shrink into?
                if (grid.GetTile() != null) continue;
                // It CAN'T even contain a gem =.=
                if (!grid.isGemContainer) continue;

                float t = ShrinkTileAtGrid(grid, direction);
                maxDropTime = maxDropTime > t ? maxDropTime : t;
            }
        }


        return (maxDropTime);
    }

    float ShrinkTileAtGrid(Grid rootGrid, Grid.Direction direction)
    {
        // Exception
        if (direction == Grid.Direction.CENTER || direction == Grid.Direction.UNKNOWN)
            return 0;
        //==========

        float timeToDrop = 0; // used to determine if there is anything change here
        // Check if there is any direct drop (drop in main direction)

        //// Find counter gravity dir
        //Grid.Direction opposedDir = Grid.GetOpposedDir(direction);

        //Grid nextgrid = rootGrid.GetGridInDir(opposedDir); // find next
        //Grid targetGrid = null; // the grid that will drop here
        ////bool haveLineOfSky = true;
        //int counter = 1; // count number of grid it passed
        //while (nextgrid != null)
        //{
        //    if (nextgrid == rootGrid) break;
        //    if (nextgrid.GetTile() == null)
        //    {
        //        if (nextgrid.isBlockGemMove || nextgrid.isHoldGemInside)
        //        {
        //            //haveLineOfSky = false;
        //            break;
        //        }
        //        else
        //        {
        //            nextgrid = nextgrid.GetGridInDir(opposedDir);
        //            counter++;
        //            continue;
        //        }
        //    }
        //    if (nextgrid.isHoldGemInside || !nextgrid.GetTile().isDroppable || nextgrid.GetTile().isMoving || nextgrid.GetTile().isOnClearingProcess)
        //    {
        //        //haveLineOfSky = false;
        //        break;
        //    }
        //    if (nextgrid.GetTile().isDroppable)
        //    {
        //        targetGrid = nextgrid;
        //        break;
        //    }
        //    nextgrid = nextgrid.GetGridInDir(opposedDir);
        //    counter++;
        //}

        //// Drop down
        //if (targetGrid != null && !nextgrid.GetTile().isMoving)
        //{
        //    rootGrid.SetTile(targetGrid.GetTile());
        //    targetGrid.SetTile(null);

        //    int moveLength = counter;
        //    float time = moveLength * GameConstants.SPEED_DROP_TILE_EACH_STEP * Grid.GetDropTimeModifierWithDirection(direction);
        //    time += rootGrid.MoveTileToCenter(time, Tile.MoveFinishType.BOUNCE);

        //    timeToDrop = time;
        //}
        ////else
        ////{
        ////    // Check line of sky
        ////    if (!haveLineOfSky)
        ////    {
        ////        return ShrinkTileAtGridDiagonal(rootGrid, direction);
        ////    }
        ////}

        return timeToDrop;
    }

    float ShrinkTileAtGridDiagonal(Grid rootGrid, Grid.Direction direction)
    {
        float timeToDrop = 0; // used to determine if there is anything change here
        // Check if there is any direct drop (drop in main direction)

        // Find counter gravity dir
        //Grid.Direction opposedDir = Grid.GetOpposedDir(direction);

        //// Try adjacent
        //List<Grid.Direction> AdjacentList = Grid.GetDroppableDiagonalDirections(direction);

        //for (int i = 0; i < AdjacentList.Count; i++)
        //{
        //    Grid.Direction dir = AdjacentList[i];

        //    Grid g = rootGrid.GetGridInDir(dir);
        //    if (g == null) continue;// mayDropgridList.Add(fGrid);

        //    Grid gravityAboveGrid = g.GetGridInDir(opposedDir);

        //    if (gravityAboveGrid != null && gravityAboveGrid.isGemContainer && !gravityAboveGrid.isHoldGemInside)
        //    {
        //        // Check if can be dropped below
        //        if (gravityAboveGrid.GetTile() != null && gravityAboveGrid.GetTile().isDroppable)
        //        {
        //            continue;
        //        }
        //    }

        //    Grid gravityBelowGrid = g.GetGridInDir(direction);

        //    if (gravityBelowGrid != null && gravityBelowGrid.isGemContainer)
        //    {
        //        if (gravityBelowGrid.GetTile() != null && gravityBelowGrid.GetTile().isMoving)
        //        {
        //            continue;
        //        }

        //        Grid evenLowerGrid = gravityBelowGrid.GetGridInDir(direction);
        //        bool realEmptyBelow = true;
        //        while (evenLowerGrid != null)
        //        {
        //            if (evenLowerGrid.GetTile() != null && evenLowerGrid.GetTile().isMoving)
        //            {
        //                realEmptyBelow = false;
        //                break;
        //            }
        //            evenLowerGrid = evenLowerGrid.GetGridInDir(direction);
        //        }
        //        if (!realEmptyBelow) continue; // bigger continue
        //    }

        //    if (g.GetTile() != null)
        //    {
        //        continue;
        //    }
        //    if (g.isBlockGemMove || !g.isGemContainer)
        //    {
        //        continue;
        //    }

        //    g.SetTile(rootGrid.GetTile());
        //    rootGrid.SetTile(null);

        //    int moveLength = 1;
        //    float time = moveLength * GameConstants.SPEED_DROP_TILE_EACH_STEP * Grid.GetDropTimeModifierWithDirection(dir);
        //    time += g.MoveTileToCenter(time, Tile.MoveFinishType.NO_ACTION_DIAGONAL);

        //    timeToDrop = time / 2f;
        //    break;

        //}

        return timeToDrop;
    }


    /// <summary>
    /// Spawn all tile in map, include pre-defined one
    /// </summary>
    public void SpawnInitialTiles()
    {
        //SpawnPreDefinedTile();
        //// Init gems so they don't make no more move and instant hit
        //SpawnTilesNormal(Grid.Direction.CENTER); // Force create gems

        //// Generate swappable list
        //List<Grid> swappableGrids = new List<Grid>();

        //for (int i = 0; i < mapController.gridList.Count; i++)
        //{
        //    Grid g = mapController.gridList[i];
        //    if (predefinedGrids.Contains(g)) continue;
        //    Tile t = g.GetTile();
        //    if (t == null) continue;

        //    if (t.isSwappable && t.isDroppable)
        //    {
        //        t.transform.localScale = new Vector3(0, 0, 1);
        //        swappableGrids.Add(g);
        //    }

        //}


        //int counter = 0;
        //while (CheckInitialMatch()) // Loop until reached a point
        //{
        //    RandomSwapTiles(swappableGrids);
        //    counter++;
        //    if (counter > 10)
        //    {
        //        mapController.ResetMapState(false, true);
        //        SpawnInitialTiles();
                
        //        return;
        //    }
        //}

        //for (int i = 0; i < swappableGrids.Count; i++)
        //{
        //    Grid g = swappableGrids[i];
        //    if (predefinedGrids.Contains(g)) continue;
        //    Tile t = g.GetTile();
        //    if (t == null) continue;
        //    // Return to grid
        //    g.MoveTileToCenter(0.3f, Tile.MoveFinishType.GROW);
        //}

        //ignoredGrids.Clear();
    }

    // Reset board
    public void ResetBoard(List<Grid> swappableGrids)
    {
        int counter = 0;
        while (CheckInitialMatch()) // Loop until reached a point
        {
            RandomSwapTiles(swappableGrids);
            counter++;
            if (counter > 500)
            {
                //mapController.ResetMapState(false, true);
                //SpawnInitialTiles();

                //GameplayController.instance.finalTryFlag = true;
                //GameplayController.instance.nomove = true;

                return;
            }
        }
    }

    /// <summary>
    /// Load pre-defined tiles(if there is any)
    /// </summary>
    void SpawnPreDefinedTile()
    {
        //predefinedGrids.Clear();
        //for (int i = 0; i < mapController.mapData.PredefinedGems.Count; i++)
        //{
        //    Grid g = mapController.gridList[i];
        //    TileData tdata = mapController.mapData.PredefinedGems[i];
        //    if (g.isGemContainer && tdata.tileType != TileData.TileType.NOT_REAL)
        //    {
        //        if (tdata.tileType != TileData.TileType.NO_INIT_GEM_SLOT)
        //        {
        //            Tile newTile = ObjectManager.Instance.GetTile();
        //            newTile.transform.SetParent(g.transform, false);
        //            newTile.transform.localPosition = new Vector3(0, 0, GameConstants.TILE_LOCAL_Z);
        //            newTile.transform.localScale = new Vector3(0, 0, 1);
        //            // Config newly created tile
        //            tileManager.TileSpawn(newTile, tdata);

        //            g.SetTile(newTile);
        //            g.MoveTileToCenter(0.3f, Tile.MoveFinishType.GROW);

        //            predefinedGrids.Add(g);

        //            if (newTile.tileData.tileType == TileData.TileType.ICE_BOMB_GEM)
        //            {
        //                tileManager.randomTileType.Add(TileData.TileType.ICE_BOMB_GEM);
        //            }
        //        }
        //        else // skip create here
        //        {
        //            ignoredGrids.Add(g);
        //        }
        //    }
        //}
    }

    /// <summary>
    /// Check if there is any match from loaded spawn
    /// </summary>
    bool CheckInitialMatch()
    {
        for (int i = 0; i < mapController.gridList.Count; i++)
        {
            Grid g = mapController.gridList[i];
            if (mapController.mapConditionChecker.CheckHitInGrid(g))
            {
                return true;
            }
        }
        return !mapController.mapConditionChecker.CheckAvailableMove();
    }

    /// <summary>
    /// Randomly swap tiles in map
    /// </summary>
    public void RandomSwapTiles(List<Grid> swappableGrids)
    {
        // Swap list
        //do{
        for (int i = 0; i < swappableGrids.Count; i++)
        {
            // Select
            Grid grid1 = swappableGrids[Random.Range(0, swappableGrids.Count)];
            Grid grid2 = swappableGrids[Random.Range(0, swappableGrids.Count)];

            // Swap
            Tile tile1 = grid1.GetTile();
            Tile tile2 = grid2.GetTile();

            grid1.SetTile(tile2);
            grid2.SetTile(tile1);
        }

        //}
        //while (mapController.mapConditionChecker.CheckAvailableMove());    
    }
}
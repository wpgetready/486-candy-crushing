﻿/*
 * @Author: CuongNH
 * @Description: Check match 3
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapConditionCheckerMatch3 : IMapConditionChecker
{
    MapController mapController;

    //cache
    List<Grid> checkingGroup;

    public MapConditionCheckerMatch3(MapController mc)
    {
        mapController = mc;
        //cache
        checkingGroup = new List<Grid>(GameConstants.CACHE_NUMBER_GRID_CHECKING);
    }

    /// <summary>
    /// Check if there is an available move in a Map
    /// </summary>
    public bool CheckAvailableMove()
    {
        // Check for swap booster items
        for (int i = 0; i < mapController.gridList.Count; i++)
        {
            Grid grid = mapController.gridList[i];
            if (CheckToSwapBoosterItem(grid))
            {
                return true;
            }
        }

        // Check for normal move
        for (int i = 0; i < mapController.gridList.Count; i++)
        {
            Grid grid = mapController.gridList[i];
            if (CheckNormalGemMoveInGrid(grid))
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Check swap booster item
    /// </summary>
    /// <param name="grid"></param>
    /// <returns></returns>
    bool CheckToSwapBoosterItem(Grid grid)
    {
        if (grid == null || grid.gridData.gridType == GridData.GridType.NULL_GRID)
        {
            return false;
        }

        Tile tile = grid.GetTile();
        //if (tile == null || tile.isOnClearingProcess) return false;
		if (tile == null || !tile.isSpecialActivable || tile.tileData.tileModifierType == TileData.TileModifierType.ice_cage)
        {
            return false;
        }

        // Check for a possible move
        for (int i = 0; i < GameConstants.DIRECTIONS_SWAPPABLE.Count; i++)
        {
            Grid.Direction cDir = GameConstants.DIRECTIONS_SWAPPABLE[i];

            Grid connectedGrid = grid.GetGridInDir(cDir);
            if (connectedGrid == null || connectedGrid.gridData.gridType == GridData.GridType.NULL_GRID)
            {
                continue;
            }

            Tile connectedTile = connectedGrid.GetTile();
            //if (connectedTile == null || connectedTile.isOnClearingProcess) continue;
            if (connectedTile == null)
            {
                continue;
            }

            if (tile.tileData.tileType == TileData.TileType.rainbow
                || tile.tileData.tileType == TileData.TileType.magnet)
            {
                if (connectedTile.isSwappable && connectedTile.isColored && connectedTile.isHit)
                {
					if(grid.GetTile().tileData.tileModifierType != TileData.TileModifierType.ice_cage && connectedGrid.GetTile().tileData.tileModifierType != TileData.TileModifierType.ice_cage)
					{
	                    mapController.SetHelpGrid(grid, connectedGrid);

	                    return true;
					}
                }
            }
            else
            {
                if (connectedTile.isSpecialActivable)
                {
					if(grid.GetTile().tileData.tileModifierType != TileData.TileModifierType.ice_cage && connectedGrid.GetTile().tileData.tileModifierType != TileData.TileModifierType.ice_cage)
					{
	                    mapController.SetHelpGrid(grid, connectedGrid);

	                    return true;
					}
                }
            }
        }

        return false;
    }

    /// <summary>
    /// Check move normal
    /// </summary>
    /// <param name="grid"></param>
    /// <returns></returns>
    bool CheckNormalGemMoveInGrid(Grid grid)
    {
        if (grid == null || grid.gridData.gridType == GridData.GridType.NULL_GRID)
        {
            return false;
        }

        Tile tile = grid.GetTile();
        if (tile == null || !tile.isHit)
        {
            return false;
        }

        //Debug.Log("CheckNormalGemMoveInGrid 0 at " + grid.gridPosition.Column + " : " + grid.gridPosition.Row);

        for (int i = 0; i < GameConstants.DIRECTIONS_CHECK_MATCHABLE3.Count; i++)
        {
            Grid.Direction dir = GameConstants.DIRECTIONS_CHECK_MATCHABLE3[i];
            // Note: 3 possible situation: 1-1-0, 1-0-1, 0-1-1
            checkingGroup.Clear();
            checkingGroup.Add(grid);

            // Get References
            Grid next1 = grid.GetGridInDir(dir);
            if (next1 == null || next1.gridData.gridType == GridData.GridType.NULL_GRID)
            {
                continue;
            }

            //if (!next1.isGemMatchEnabled || !next1.isGemContainer) continue;
            Tile tile1 = next1.GetTile();
            //if (tile1 == null && !GameConstants.ENABLE_SWAP_WITH_EMPTY_GRID) continue;
            if (tile1 == null || !tile1.isSwappable)
            {
                continue;
            }

            checkingGroup.Add(next1);

            //Debug.Log("CheckNormalGemMoveInGrid 1 at " + grid.gridPosition.Column + " : " + grid.gridPosition.Row);

            Grid next2 = next1.GetGridInDir(dir);
            if (next2 == null || next2.gridData.gridType == GridData.GridType.NULL_GRID)
            {
                continue;
            }

            //if (!next2.isGemMatchEnabled || !next2.isGemContainer) continue;
            Tile tile2 = next2.GetTile();
            //if (tile2 == null && !GameConstants.ENABLE_SWAP_WITH_EMPTY_GRID) continue;
            if (tile2 == null || !tile2.isSwappable)
            {
                continue;
            }

            checkingGroup.Add(next2);

            //Debug.Log("CheckNormalGemMoveInGrid 2 at " + grid.gridPosition.Column + " : " + grid.gridPosition.Row);

            // grid to check match with 2 grid
            Grid missGrid = null;
            Grid matchGrid = null;
            //TileData.TileColor missColor = TileData.TileColor.NONE;

            // case, case everywhere :v 
            // find the missing grid and missing color
            if (Tile.CheckSameHit(tile, tile1))
            {
                missGrid = next2;
                matchGrid = grid;
                //missColor = (tile.tileData.tileColor == TileData.TileColor.WILDCARD) ? tile1.tileData.tileColor : tile.tileData.tileColor;
            }
            else if (Tile.CheckSameHit(tile1, tile2))
            {
                missGrid = grid;
                matchGrid = next1;
                //missColor = (tile1.tileData.tileColor == TileData.TileColor.WILDCARD) ? tile1.tileData.tileColor : tile2.tileData.tileColor;
            }
            else if (Tile.CheckSameHit(tile, tile2))
            {
                missGrid = next1;
                matchGrid = next2;
                //missColor = (tile.tileData.tileColor == TileData.TileColor.WILDCARD) ? tile.tileData.tileColor : tile2.tileData.tileColor;
            }

            // Find possible move in
            if (missGrid != null && matchGrid != null /*&& missColor != TileData.TileColor.NONE*/)
            {
                //if (!missGrid.isInsideGemSwappable) continue;

                //Debug.Log("CheckNormalGemMoveInGrid 3 at " + grid.gridPosition.Column + " : " + grid.gridPosition.Row);

                for (int ii = 0; ii < GameConstants.DIRECTIONS_SWAPPABLE.Count; ii++)
                {
                    Grid.Direction cDir = GameConstants.DIRECTIONS_SWAPPABLE[ii];

                    Grid cGrid = missGrid.GetGridInDir(cDir);
                    if (cGrid == null || cGrid.gridData.gridType == GridData.GridType.NULL_GRID)
                    {
                        continue;
                    }

                    if (checkingGroup.Contains(cGrid))
                    {
                        continue;
                    }

                    //if (!cGrid.isGemMatchEnabled || !cGrid.isInsideGemSwappable) continue;
                    Tile cTile = cGrid.GetTile();
                    if (cTile == null || !cTile.isHit || !cTile.isSwappable)
                    {
                        continue;
                    }

                    //Debug.Log("CheckNormalGemMoveInGrid 4 at " + grid.gridPosition.Column + " : " + grid.gridPosition.Row);

                    if (Tile.CheckSameHit(matchGrid.GetTile(), cTile))
                    {
                        //Debug.Log("CheckNormalGemMoveInGrid 5 at " + grid.gridPosition.Column + " : " + grid.gridPosition.Row);
                        // Save result found
						if(missGrid.GetTile().tileData.tileModifierType != TileData.TileModifierType.ice_cage && cGrid.GetTile().tileData.tileModifierType != TileData.TileModifierType.ice_cage)
						{
	                        mapController.SetHelpGrid(missGrid, cGrid);
	                        
	                        return true;
						}
                    }
                }
            }
        }

        return false;
    }

    /// <summary>
    /// Check if this grid have hit (3 matching tile)
    /// </summary>
    public bool CheckHitInGrid(Grid grid)
    {
        if (grid == null || grid.gridData.gridType == GridData.GridType.NULL_GRID)
        {
            return false;
        }

        //if (!grid.isGemMatchEnabled || !grid.isGemContainer) return false;

        Tile tile = grid.GetTile();

        if (tile == null || !tile.isHit)
        {
            return false;
        }

        for (int i = 0; i < GameConstants.DIRECTIONS_CHECK_MATCHABLE3.Count; i++)
        {
            Grid.Direction dir = GameConstants.DIRECTIONS_CHECK_MATCHABLE3[i];
            // Note: 3 possible situation: 1-1-0, 1-0-1, 0-1-1

            // Get References
            Grid next1 = grid.GetGridInDir(dir);
            if (next1 == null || next1.gridData.gridType == GridData.GridType.NULL_GRID)
            {
                continue;
            }

            //if (!next1.isGemMatchEnabled || !next1.isGemContainer) continue;
            Tile tile1 = next1.GetTile();
            if (tile1 == null || !tile1.isHit)
            {
                continue;
            }

            Grid next2 = next1.GetGridInDir(dir);
            if (next2 == null || next1.gridData.gridType == GridData.GridType.NULL_GRID)
            {
                continue;
            }

            //if (!next2.isGemMatchEnabled || !next2.isGemContainer) continue;
            Tile tile2 = next2.GetTile();
            if (tile2 == null || !tile2.isHit)
            {
                continue;
            }

            //// Check same color
            //if (Tile.CheckSameColor(tile, tile1, tile2))
            //{
            //    return true;
            //}

            // Check same hit
            if (Tile.CheckSameHit(tile, tile1, tile2))
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Check if 2 grid can be swappable
    /// </summary>
    public bool CheckSwappable(Grid grid1, Grid grid2)
    {
        if (grid1 == null || grid2 == null || grid1.gridData.gridType == GridData.GridType.NULL_GRID 
            || grid2.gridData.gridType == GridData.GridType.NULL_GRID)
        {
            return false;
        }

        // Check if 2 grid is adjacent
        Grid.Direction swapDir = grid1.GetDirOfGrid(grid2);

        if (!GameConstants.DIRECTIONS_SWAPPABLE.Contains(swapDir))
        {
            return false;
        }

        // Check if grid can perform swap
        Tile tile1 = grid1.GetTile();
        Tile tile2 = grid2.GetTile();

        // tile is null, no need to check
        if (tile1 == null || tile2 == null || !tile1.isSwappable || !tile2.isSwappable)
        {
            return false;
        }

        if (tile1.isMoving || tile2.isMoving || tile1.isOnClearingProcess || tile2.isOnClearingProcess)
        {
            return false;
        }

        if (tile1.isSwappable && tile2.isSwappable)
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Check if 2 grid swap make an effect (clear, specials,....)
    /// </summary>
    public bool CheckSwapExecutable(Grid grid1, Grid grid2)
    {
        //Debug.Log("CheckSwapExecutable 0 :::::::::::");
        if (grid1 == null || grid2 == null || grid1.gridData.gridType == GridData.GridType.NULL_GRID 
            || grid2.gridData.gridType == GridData.GridType.NULL_GRID)
        {
            return false;
        }

        //Debug.Log("CheckSwapExecutable 1 :::::::::::");

        Tile tile1 = grid1.GetTile();
        Tile tile2 = grid2.GetTile();
        if (tile1 == null || tile2 == null)
        {
            return false;
        }

        //Debug.Log("CheckSwapExecutable 2 :::::::::::");

        // Swap with normal Tile
        if (CheckMatchSwap(grid1.GetTile(), grid2, grid2.GetDirOfGrid(grid1)) ||
            CheckMatchSwap(grid2.GetTile(), grid1, grid1.GetDirOfGrid(grid2)))
        {
            //Debug.Log("CheckSwapExecutable 3 :::::::::::");

            return true;
        }

        // Special Combine
        if (tile1.isSpecialActivable && tile2.isSpecialActivable)
        {
            return true;
        }

        // rainbown or magnet
        if (tile1.tileData.tileType == TileData.TileType.rainbow || tile2.tileData.tileType == TileData.TileType.rainbow
            || tile1.tileData.tileType == TileData.TileType.magnet || tile2.tileData.tileType == TileData.TileType.magnet)
        {
            Tile specialTile;
            Tile normalTile;
            if (tile1.tileData.tileType == TileData.TileType.rainbow || tile1.tileData.tileType == TileData.TileType.magnet)
            {
                specialTile = tile1;
                normalTile = tile2;
            }
            else
            {
                specialTile = tile2;
                normalTile = tile1;
            }

            if (normalTile.isColored && normalTile.isHit)
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Test if a tile enter a grid will create a match
    /// </summary>
    public bool CheckMatchSwap(Tile testTile, Grid targetGrid, Grid.Direction comeDir)
    {
        if (testTile == null || !testTile.isHit)
        {
            return false;
        }

        return CheckMatchNormalSwap(testTile, targetGrid, comeDir);

        // Select main color for the match
        //if (testIile.tileData.tileColor != TileData.TileColor.WILDCARD)
        //{
        //    return TestColorMatchNormalTile(testIile, targetGrid, comeDir);
        //}
        //else
        //{
        //    return TestColorMatchWildCard(testIile, targetGrid, comeDir);
        //}
    }

    /// <summary>
    /// Check match with normal tile
    /// </summary>
    /// <param name="testTile"></param>
    /// <param name="targetGrid"></param>
    /// <param name="comeDir"></param>
    /// <returns></returns>
    bool CheckMatchNormalSwap(Tile testTile, Grid targetGrid, Grid.Direction comeDir)
    {
        bool result = false;
        int matchCounter = 0; // Max length of match in one dir 
        int matchDirCounter = 0; // Number of dir that have a match

        // Check adjacent direction only
        for (int i = 0; i < GameConstants.DIRECTIONS_CHECK_MATCHABLE3.Count; i++)
        {
            int currentMatchCounter = 0;

            Grid.Direction dir = GameConstants.DIRECTIONS_CHECK_MATCHABLE3[i];

            // Do not check the dir where tile come from
            if (dir != comeDir)
            {
                Grid g = targetGrid.GetGridInDir(dir);
                while (g != null && g.gridData.gridType == GridData.GridType.NORMAL_GRID)
                {
                    //if (!g.isGemMatchEnabled) break;
                    Tile t = g.GetTile();
                    if (t == null || !t.isHit)
                    {
                        break;
                    }
                    //if (!t.isColored) break;

                    if (Tile.CheckSameHit(testTile, t))
                    {
                        currentMatchCounter++;
                    }
                    else
                    {
                        break;
                    }

                    g = g.GetGridInDir(dir);
                }
            }

            // REVERSE DIR Check
            dir = Grid.GetOpposedDir(dir); //(Grid.Direction)(Grid.DIR_NUM - (int)dir); // Get Oposite Dir
            if (dir != comeDir)
            {
                Grid g = targetGrid.GetGridInDir(dir);
                while (g != null && g.gridData.gridType == GridData.GridType.NORMAL_GRID)
                {
                    //if (!g.isGemMatchEnabled) break;
                    Tile t = g.GetTile();
                    if (t == null || !t.isHit)
                    {
                        break;
                    }
                    //if (!t.isColored) break;

                    if (Tile.CheckSameHit(testTile, t))
                    {
                        currentMatchCounter++;
                    }
                    else
                    {
                        break;
                    }

                    g = g.GetGridInDir(dir);
                }
            }

            // Update match counter
            if (currentMatchCounter > matchCounter)
            {
                matchCounter = currentMatchCounter;
            }

            if (currentMatchCounter > (GameConstants.MATCH3_MIN - 1))
            {
                matchDirCounter++;
            }
        }

        if (matchCounter >= (GameConstants.MATCH3_MIN - 1))
        {
            result = true;
        }

        return result;
    }

    bool TestColorMatchWildCard(Tile testIile, Grid targetGrid, Grid.Direction comeDir)
    {
        // Note: Wildcard swap can result in many case, like 3(or even 4) different color match
        // so it need a special function to check

        int matchCounter = 0; // Max length of match in one dir 
        int matchDirCounter = 0; // Number of dir that have a match


        // Check adjacent direction only
        for (int i = 0; i < GameConstants.DIRECTIONS_CHECK_MATCHABLE3.Count; i++)
        {
            int currentMatchCounter = 0;

            Grid.Direction dir = GameConstants.DIRECTIONS_CHECK_MATCHABLE3[i];
            if (dir != comeDir)
            {
                Grid g1 = targetGrid.GetGridInDir(dir);
                Grid g2 = g1.GetGridInDir(dir);

                if (g1 != null && g2 != null)
                {
                    Tile t1 = g1.GetTile();
                    Tile t2 = g2.GetTile();

                    if (t1 != null && t2 != null)
                    {
                        if (Tile.CheckSameColor(t1, t2))
                        {
                            return true;
                        }
                    }
                }
            }
            // REVERSE DIR Check
            dir = (Grid.Direction)(Grid.DIR_NUM - (int)dir); // Get Oposite Dir
            if (dir != comeDir)
            {
                Grid g1 = targetGrid.GetGridInDir(dir);
                Grid g2 = g1.GetGridInDir(dir);

                if (g1 != null && g2 != null)
                {
                    Tile t1 = g1.GetTile();
                    Tile t2 = g2.GetTile();

                    if (t1 != null && t2 != null)
                    {
                        if (Tile.CheckSameColor(t1, t2))
                        {
                            return true;
                        }
                    }
                }
            }
            // Update match counter
            if (currentMatchCounter > matchCounter) matchCounter = currentMatchCounter;
            if (currentMatchCounter > 2) matchDirCounter++;
        }

        return false;
    }

}

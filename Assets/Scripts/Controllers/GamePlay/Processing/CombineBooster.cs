﻿/*
 * @Author: CuongNH
 * @Description: Xu ly viec swap cac vien dac biet
 * */

using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class CombineBooster {

    private static List<Grid> nearMagnetGridList = new List<Grid>(GameConstants.CACHE_NUMBER_GRID_CHECKING);

    /// <summary>
    /// Call when active swap boosters item
    /// </summary>
    /// <param name="mapController"></param>
    /// <param name="tile1"></param>
    /// <param name="tile2"></param>
    public static bool Active(MapController mapController, Tile tile1, Tile tile2)
    {
        // rainbow/magnet + rainbow/magnet
        if ((tile1.tileData.tileType == TileData.TileType.rainbow || tile1.tileData.tileType == TileData.TileType.magnet)
            && (tile2.tileData.tileType == TileData.TileType.rainbow || tile2.tileData.tileType == TileData.TileType.magnet))
        {
            MagicXMagic(mapController, tile1, tile1);

            return true;
        }

        #region Swap with rainbow
        // rainbow + normal
        if (tile1.tileData.tileType == TileData.TileType.rainbow && tile2.tileData.tileType == TileData.TileType.match_object)
        {
            RainbowXNormal(mapController, tile1, tile2);

            return true;
        }
        if (tile2.tileData.tileType == TileData.TileType.rainbow && tile1.tileData.tileType == TileData.TileType.match_object)
        {
            RainbowXNormal(mapController, tile2, tile1);

            return true;
        }

        // rainbow + row_breaker/column_breaker
        if (tile1.tileData.tileType == TileData.TileType.rainbow && (tile2.tileData.tileType
            == TileData.TileType.row_breaker || tile2.tileData.tileType == TileData.TileType.column_breaker))
        {
            RainbowXRowColumnBreaker(mapController, tile1, tile2);

            return true;
        }
        if (tile2.tileData.tileType == TileData.TileType.rainbow && (tile1.tileData.tileType
            == TileData.TileType.row_breaker || tile1.tileData.tileType == TileData.TileType.column_breaker))
        {
            RainbowXRowColumnBreaker(mapController, tile2, tile1);

            return true;
        }

        // rainbow + bomb_breaker
        if (tile1.tileData.tileType == TileData.TileType.rainbow && tile2.tileData.tileType == TileData.TileType.bomb_breaker)
        {
            RainbowXBombBreaker(mapController, tile1, tile2);

            return true;
        }
        if (tile2.tileData.tileType == TileData.TileType.rainbow && tile1.tileData.tileType == TileData.TileType.bomb_breaker)
        {
            RainbowXBombBreaker(mapController, tile2, tile1);

            return true;
        }

        // rainbow + x
        if (tile1.tileData.tileType == TileData.TileType.rainbow && tile2.tileData.tileType == TileData.TileType.x_breaker)
        {
            RainbowXXBreaker(mapController, tile1, tile2);

            return true;
        }
        if (tile2.tileData.tileType == TileData.TileType.rainbow && tile1.tileData.tileType == TileData.TileType.x_breaker)
        {
            RainbowXXBreaker(mapController, tile2, tile1);

            return true;
        }
        #endregion

        #region Swap with magnet
        // magnet + normal
        if (tile1.tileData.tileType == TileData.TileType.magnet && tile2.tileData.tileType == TileData.TileType.match_object)
        {
            MagnetXNormal(mapController, tile1, tile2);

            return true;
        }
        if (tile2.tileData.tileType == TileData.TileType.magnet && tile1.tileData.tileType == TileData.TileType.match_object)
        {
            MagnetXNormal(mapController, tile2, tile1);

            return true;
        }

        // magnet + row_breaker/column_breaker
        if (tile1.tileData.tileType == TileData.TileType.magnet && (tile2.tileData.tileType
            == TileData.TileType.row_breaker || tile2.tileData.tileType == TileData.TileType.column_breaker))
        {
            MagnetXRowColumnBreaker(mapController, tile1, tile2);

            return true;
        }
        if (tile2.tileData.tileType == TileData.TileType.magnet && (tile1.tileData.tileType
            == TileData.TileType.row_breaker || tile1.tileData.tileType == TileData.TileType.column_breaker))
        {
            MagnetXRowColumnBreaker(mapController, tile2, tile1);

            return true;
        }

        // magnet + bomb_breaker
        if (tile1.tileData.tileType == TileData.TileType.magnet && tile2.tileData.tileType == TileData.TileType.bomb_breaker)
        {
            MagnetXBombBreaker(mapController, tile1, tile2);

            return true;
        }
        if (tile2.tileData.tileType == TileData.TileType.magnet && tile1.tileData.tileType == TileData.TileType.bomb_breaker)
        {
            MagnetXBombBreaker(mapController, tile2, tile1);

            return true;
        }

        // magnet + x_breaker
        if (tile1.tileData.tileType == TileData.TileType.magnet && tile2.tileData.tileType == TileData.TileType.x_breaker)
        {
            MagnetXXBreaker(mapController, tile1, tile2);

            return true;
        }
        if (tile2.tileData.tileType == TileData.TileType.magnet && tile1.tileData.tileType == TileData.TileType.x_breaker)
        {
            MagnetXXBreaker(mapController, tile2, tile1);

            return true;
        }
        #endregion

        #region Others combined
        // bomb + row_breaker/column_breaker
        if (tile1.tileData.tileType == TileData.TileType.bomb_breaker && (tile2.tileData.tileType
            == TileData.TileType.row_breaker || tile2.tileData.tileType == TileData.TileType.column_breaker))
        {
            BombXRowColumnBreaker(mapController, tile1, tile2);
        }
        else if (tile2.tileData.tileType == TileData.TileType.bomb_breaker && (tile1.tileData.tileType
            == TileData.TileType.row_breaker || tile1.tileData.tileType == TileData.TileType.column_breaker))
        {
            BombXRowColumnBreaker(mapController, tile2, tile1);
        }

        // bomb + bomb
        if (tile1.tileData.tileType == TileData.TileType.bomb_breaker 
            && tile2.tileData.tileType == TileData.TileType.bomb_breaker)
        {
            BombXBomb(mapController, tile1, tile2);

            return true;
        }

        // row_breaker/column_breaker + row_breaker/column_breaker
        if ((tile1.tileData.tileType == TileData.TileType.row_breaker || tile1.tileData.tileType == TileData.TileType.column_breaker)
            && (tile2.tileData.tileType == TileData.TileType.row_breaker || tile2.tileData.tileType == TileData.TileType.column_breaker))
        {
            CombineRowColumnBreaker(mapController, tile1, tile2);

            return true;
        }

        // x_breaker + x_breaker / bomb_breaker / row_breaker / column_breaker
        if ((tile1.tileData.tileType == TileData.TileType.x_breaker || tile2.tileData.tileType == TileData.TileType.x_breaker)
            && (tile1.isSpecialActivable && tile2.isSpecialActivable))
        {
            if (tile1.tileData.tileType == TileData.TileType.x_breaker)
            {
                XBreakerXBreaker(mapController, tile1, tile2);
            }
            else
            {
                XBreakerXBreaker(mapController, tile2, tile1);
            }

            return true;
        }
        #endregion

        return false;
    }

    /// <summary>
    /// Active combine rainbow/magnet + rainbow/magnet
    /// </summary>
    /// <param name="mapController"></param>
    /// <param name="magic1"></param>
    /// <param name="magic2"></param>
    public static void MagicXMagic(MapController mapController, Tile magic1 = null, Tile magic2 = null)
    {
			mapController.tileSpawner.magicxmagic = false;
            GamePlayController.gamePlayController.gameInterceptorCount++;
            for (int i = 0; i < mapController.gridList.Count; i++)
            {
			//Debug.Log ("mapController.gridList.Count"+mapController.gridList.Count);
                Grid grid = mapController.gridList[i];
                if (grid != null && grid.gridData.gridType != GridData.GridType.NULL_GRID)
                {
                    Tile tile = grid.GetTile();
				if (tile != null && tile.isHit && tile.isColored && !tile.isMoving && !tile.isOnClearingProcess) 
				{
					if (i < mapController.gridList.Count - 1) {
						LeanTween.rotateAround (tile.gameObject, Vector3.forward, 360f,
							GameConstants.TIME_ROTATE_TILE).setOnComplete (() => {
							mapController.ExecuteTile (tile, grid, false, true);

						}).setLoopOnce ();
					} 
					else 
					{
						if (magic1 == null && magic2 == null)
						{						
							LeanTween.rotateAround (tile.gameObject, Vector3.forward, 360f,
								GameConstants.TIME_ROTATE_TILE).setOnComplete (() => {
									mapController.ExecuteTile (tile, grid, false, true);
								}).setLoopOnce ();
						}
					}
				}
				else 
				{
					if(tile != null && tile.tileData.tileModifierType != TileData.TileModifierType.watermelon_O && tile.tileData.tileModifierType != TileData.TileModifierType.color && tile.tileData.tileType != TileData.TileType.fountain)
					{
					LeanTween.rotateAround (tile.gameObject, Vector3.forward, 360f,
						GameConstants.TIME_ROTATE_TILE).setOnComplete (() => {
							grid.Damage(false);
								//mapController.ExecuteTile (tile, grid, false, true);
								//Debug.Log("111111");
						}).setLoopOnce ();
					}
				}
					
				if (magic1 != null && magic2 != null)
				{
		            LeanTween.scale(magic1.gameObject, Vector3.zero, GameConstants.TIME_ROTATE_TILE);
		            LeanTween.scale(magic2.gameObject, Vector3.zero, GameConstants.TIME_ROTATE_TILE);

		            LeanTween.rotateAround(magic1.gameObject, Vector3.forward, 360f,
		                GameConstants.TIME_ROTATE_TILE).setOnComplete(() =>
		                {
		                    mapController.ExecuteTile(magic1, magic1.grid, false, true);
		                });

		            LeanTween.rotateAround(magic2.gameObject, Vector3.forward, 360f,
		                GameConstants.TIME_ROTATE_TILE).setOnComplete(() =>
		                {
		                    mapController.ExecuteTile(magic2, magic2.grid, false, true);

		                    if (GamePlayController.gamePlayController.gameInterceptorCount > 0)
		                    {
		                        GamePlayController.gamePlayController.gameInterceptorCount--;
		                    }
		                });
	        	}
	    	}

			if (magic1 == null && magic2 == null) 
			{
				if (i == mapController.gridList.Count - 1) 
				{
					if (GamePlayController.gamePlayController.gameInterceptorCount > 0) 
					{
						GamePlayController.gamePlayController.gameInterceptorCount--;
						GamePlayController.gamePlayController.mapController.tileSpawner.isProcessMagicBoom = false;
					}
				}
			}
		}
	}
    /// <summary>
    /// Active rainbow with normal
    /// </summary>
    /// <param name="mapController"></param>
    /// <param name="rainbow"></param>
    /// <param name="normal"></param>
    public static void RainbowXNormal(MapController mapController, Tile rainbow, Tile normal)
    {
        if (rainbow != null && normal != null)
        {
            GamePlayController.gamePlayController.gameInterceptorCount++;
            for (int i = 0; i < mapController.gridList.Count; i++)
            {
                Grid grid = mapController.gridList[i];
                if (grid != null && grid.gridData.gridType != GridData.GridType.NULL_GRID)
                {
                    Tile tile = grid.GetTile();
                    if (tile != null && tile.isHit && tile.isColored && tile.tileData.tileColor
                        == normal.tileData.tileColor && !tile.isMoving && !tile.isOnClearingProcess)
                    {
                        LeanTween.rotateAround(tile.gameObject, Vector3.forward, 360f,
                            GameConstants.TIME_ROTATE_TILE).setOnComplete(() =>
                            {
                                mapController.ExecuteTile(tile, grid, false, true);
                            });
                    }
                }
            }

            LeanTween.scale(rainbow.gameObject, Vector3.zero, GameConstants.TIME_ROTATE_TILE);

            LeanTween.rotateAround(rainbow.gameObject, Vector3.forward, 360f,
                GameConstants.TIME_ROTATE_TILE).setOnComplete(() =>
                {
                    mapController.ExecuteTile(rainbow, rainbow.grid, false, true);

                    if (GamePlayController.gamePlayController.gameInterceptorCount > 0)
                    {
                        GamePlayController.gamePlayController.gameInterceptorCount--;
                    }
                });
        }
    }

    /// <summary>
    /// Active rainbow with row_breaker/column_breaker
    /// </summary>
    /// <param name="mapController"></param>
    /// <param name="rainbow"></param>
    /// <param name="thunder"></param>
    public static void RainbowXRowColumnBreaker(MapController mapController, Tile rainbow, Tile thunder)
    {
        if (rainbow != null && thunder != null)
        {
            GamePlayController.gamePlayController.gameInterceptorCount++;
            for (int i = 0; i < mapController.gridList.Count; i++)
            {
                Grid grid = mapController.gridList[i];
                if (grid != null && grid.gridData.gridType != GridData.GridType.NULL_GRID)
                {
                    Tile tile = grid.GetTile();
                    if (tile != null && tile.isHit && tile.isColored && tile.tileData.tileColor
                        == thunder.tileData.tileColor && !tile.isMoving && !tile.isOnClearingProcess)
                    {
                        if (tile != thunder)
                        {
                            int rand = Random.Range(0, 2);
                            if (rand == 0)
                            {
                                tile.TransformInto(new TileData(TileData.TileType.row_breaker, tile.tileData.tileColor));
                            }
                            else
                            {
                                tile.TransformInto(new TileData(TileData.TileType.column_breaker, tile.tileData.tileColor));
                            }
                        }

                        LeanTween.rotateAround(tile.gameObject, Vector3.forward, 360f,
                            GameConstants.TIME_ROTATE_TILE).setOnComplete(() =>
                            {
                                LeanTween.delayedCall(GameConstants.TIME_DELAY_ACTIVE_AFTER_TRANSFORM_BOOSTER, () =>
                                    {
                                        mapController.ExecuteTile(tile, grid, false, true);
                                    });
                            });
                    }
                }
            }

            LeanTween.scale(rainbow.gameObject, Vector3.zero, GameConstants.TIME_ROTATE_TILE 
                + GameConstants.TIME_DELAY_ACTIVE_AFTER_TRANSFORM_BOOSTER);

            LeanTween.rotateAround(rainbow.gameObject, Vector3.forward, 360f,
                GameConstants.TIME_ROTATE_TILE + GameConstants.TIME_DELAY_ACTIVE_AFTER_TRANSFORM_BOOSTER)
                .setOnComplete(() =>
                {
                    mapController.ExecuteTile(rainbow, rainbow.grid, false, true);

                    if (GamePlayController.gamePlayController.gameInterceptorCount > 0)
                    {
                        GamePlayController.gamePlayController.gameInterceptorCount--;
                    }
                });
        }
    }

    /// <summary>
    /// Active rainbow with bomb_breaker
    /// </summary>
    /// <param name="mapController"></param>
    /// <param name="rainbow"></param>
    /// <param name="bomb"></param>
    public static void RainbowXBombBreaker(MapController mapController, Tile rainbow, Tile bomb)
    {
        if (rainbow != null && bomb != null)
        {
            GamePlayController.gamePlayController.gameInterceptorCount++;
            for (int i = 0; i < mapController.gridList.Count; i++)
            {
                Grid grid = mapController.gridList[i];
                if (grid != null && grid.gridData.gridType != GridData.GridType.NULL_GRID)
                {
                    Tile tile = grid.GetTile();
                    if (tile != null && tile.isHit && tile.isColored && tile.tileData.tileColor
                        == bomb.tileData.tileColor && !tile.isMoving && !tile.isOnClearingProcess)
                    {
                        tile.TransformInto(new TileData(TileData.TileType.bomb_rainbow, tile.tileData.tileColor));

                        LeanTween.rotateAround(tile.gameObject, Vector3.forward, 360f,
                            GameConstants.TIME_ROTATE_TILE).setOnComplete(() =>
                            {
                                LeanTween.delayedCall(GameConstants.TIME_DELAY_ACTIVE_AFTER_TRANSFORM_BOOSTER, () =>
                                {
                                    mapController.ExecuteTile(tile, grid, false, true);
                                });
                            });
                    }
                }
            }

            LeanTween.scale(rainbow.gameObject, Vector3.zero, GameConstants.TIME_ROTATE_TILE
                + GameConstants.TIME_DELAY_ACTIVE_AFTER_TRANSFORM_BOOSTER);

            LeanTween.rotateAround(rainbow.gameObject, Vector3.forward, 360f,
                GameConstants.TIME_ROTATE_TILE + GameConstants.TIME_DELAY_ACTIVE_AFTER_TRANSFORM_BOOSTER)
                .setOnComplete(() =>
                {
                    mapController.ExecuteTile(rainbow, rainbow.grid, false, true);

                    if (GamePlayController.gamePlayController.gameInterceptorCount > 0)
                    {
                        GamePlayController.gamePlayController.gameInterceptorCount--;
                    }
                });
        }
    }

    /// <summary>
    /// Active rainbow with x_breaker
    /// </summary>
    /// <param name="mapController"></param>
    /// <param name="rainbow"></param>
    /// <param name="xBreaker"></param>
    public static void RainbowXXBreaker(MapController mapController, Tile rainbow, Tile xBreaker)
    {
        if (rainbow != null && xBreaker != null)
        {
            GamePlayController.gamePlayController.gameInterceptorCount++;
            for (int i = 0; i < mapController.gridList.Count; i++)
            {
                Grid grid = mapController.gridList[i];
                if (grid != null && grid.gridData.gridType != GridData.GridType.NULL_GRID)
                {
                    Tile tile = grid.GetTile();
                    if (tile != null && tile.isHit && tile.isColored && tile.tileData.tileColor
                        == xBreaker.tileData.tileColor && !tile.isMoving && !tile.isOnClearingProcess)
                    {
                        tile.TransformInto(new TileData(TileData.TileType.x_rainbow, tile.tileData.tileColor));

                        LeanTween.rotateAround(tile.gameObject, Vector3.forward, 360f,
                            GameConstants.TIME_ROTATE_TILE).setOnComplete(() =>
                            {
                                LeanTween.delayedCall(GameConstants.TIME_DELAY_ACTIVE_AFTER_TRANSFORM_BOOSTER, () =>
                                {
                                    mapController.ExecuteTile(tile, grid, false, true);
                                });
                            });
                    }
                }
            }

            LeanTween.scale(rainbow.gameObject, Vector3.zero, GameConstants.TIME_ROTATE_TILE
                + GameConstants.TIME_DELAY_ACTIVE_AFTER_TRANSFORM_BOOSTER);

            LeanTween.rotateAround(rainbow.gameObject, Vector3.forward, 360f,
                GameConstants.TIME_ROTATE_TILE + GameConstants.TIME_DELAY_ACTIVE_AFTER_TRANSFORM_BOOSTER)
                .setOnComplete(() =>
                {
                    mapController.ExecuteTile(rainbow, rainbow.grid, false, true);

                    if (GamePlayController.gamePlayController.gameInterceptorCount > 0)
                    {
                        GamePlayController.gamePlayController.gameInterceptorCount--;
                    }
                });
        }
    }

    /// <summary>
    /// Active magnet with normal
    /// </summary>
    /// <param name="mapController"></param>
    /// <param name="magnet"></param>
    /// <param name="normal"></param>
    public static void MagnetXNormal(MapController mapController, Tile magnet, Tile normal)
    {
        nearMagnetGridList.Clear();

        if (magnet != null && normal != null)
        {
            nearMagnetGridList.Add(magnet.grid);
            GamePlayController.gamePlayController.gameInterceptorCount++;
            for (int i = 0; i < mapController.gridList.Count; i++)
            {
                Grid grid = mapController.gridList[i];
                if (grid != null && grid.gridData.gridType != GridData.GridType.NULL_GRID)
                {
                    Tile tile = grid.GetTile();
                    if (tile != null && tile.isHit && tile.isColored && tile.tileData.tileColor
                        == normal.tileData.tileColor && !tile.isMoving && !tile.isOnClearingProcess)
                    {
                        Grid nearGrid = null;
                        if (magnet.grid.CheckIsAdjacent(tile.grid))
                        {
                            nearGrid = grid;
                        }
                        else
                        {
                            nearGrid = GetGridNearMagnet(magnet, normal);
                            if (nearGrid != null && nearGrid.gridData.gridType != GridData.GridType.NULL_GRID)
                            {
                                Tile oldTile = nearGrid.GetTile();
                                nearGrid.SetTile(tile);
                                grid.SetTile(null);
                                oldTile.grid = null;

                                tile.transform.localPosition += new Vector3(0f, 0f, -1f);
                                nearGrid.MoveTileToCenter(GameConstants.TIME_TO_MOVE_NEAR_MAGNET);

                                // TODO: push to pool
                                // For test
                                LeanTween.delayedCall(0.5f * GameConstants.TIME_TO_MOVE_NEAR_MAGNET, () =>
                                {
                                    //oldTile.gameObject.SetActive(false);
                                    PoolingManager.poolingManager.AddTileToPool(oldTile);
                                });
                                // end test
                            }
                            else
                            {
                                nearGrid = grid;
                            }
                        }

                        LeanTween.delayedCall(GameConstants.TIME_TO_MOVE_NEAR_MAGNET, () =>
                        {
                            LeanTween.rotateAround(tile.gameObject, Vector3.forward, 360f,
                                GameConstants.TIME_ROTATE_TILE).setOnComplete(() =>
                                {
                                    mapController.ExecuteTile(tile, nearGrid, false, true);
                                });
                        });

                    }
                }
            }

            LeanTween.scale(magnet.gameObject, Vector3.zero, 
                GameConstants.TIME_ROTATE_TILE + GameConstants.TIME_TO_MOVE_NEAR_MAGNET);

            LeanTween.rotateAround(magnet.gameObject, Vector3.forward, 360f,
                GameConstants.TIME_ROTATE_TILE + GameConstants.TIME_TO_MOVE_NEAR_MAGNET)
                .setOnComplete(() =>
                {
                    mapController.ExecuteTile(magnet, magnet.grid, false, true);

                    if (GamePlayController.gamePlayController.gameInterceptorCount > 0)
                    {
                        GamePlayController.gamePlayController.gameInterceptorCount--;
                    }
                });
        }
    }

    /// <summary>
    /// Active magnet with row_breaker/column_breaker
    /// </summary>
    /// <param name="mapController"></param>
    /// <param name="magnet"></param>
    /// <param name="thunder"></param>
    public static void MagnetXRowColumnBreaker(MapController mapController, Tile magnet, Tile thunder)
    {
        nearMagnetGridList.Clear();

        if (magnet != null && thunder != null)
        {
            nearMagnetGridList.Add(magnet.grid);
            GamePlayController.gamePlayController.gameInterceptorCount++;
            for (int i = 0; i < mapController.gridList.Count; i++)
            {
                Grid grid = mapController.gridList[i];
                if (grid != null && grid.gridData.gridType != GridData.GridType.NULL_GRID)
                {
                    Tile tile = grid.GetTile();
                    if (tile != null && tile.isHit && tile.isColored && tile.tileData.tileColor
                        == thunder.tileData.tileColor && !tile.isMoving && !tile.isOnClearingProcess)
                    {
                        Grid nearGrid = null;
                        if (magnet.grid.CheckIsAdjacent(tile.grid))
                        {
                            nearGrid = grid;
                        }
                        else
                        {
                            nearGrid = GetGridNearMagnet(magnet, thunder);
                            if (nearGrid != null && nearGrid.gridData.gridType != GridData.GridType.NULL_GRID)
                            {
                                Tile oldTile = nearGrid.GetTile();
                                nearGrid.SetTile(tile);
                                grid.SetTile(null);
                                oldTile.grid = null;

                                tile.transform.localPosition += new Vector3(0f, 0f, -1f);
                                nearGrid.MoveTileToCenter(GameConstants.TIME_TO_MOVE_NEAR_MAGNET);

                                // TODO: push to pool
                                // For test
                                LeanTween.delayedCall(0.5f * GameConstants.TIME_TO_MOVE_NEAR_MAGNET, () =>
                                {
                                    //oldTile.gameObject.SetActive(false);
                                    PoolingManager.poolingManager.AddTileToPool(oldTile);
                                });
                                // end test
                            }
                            else
                            {
                                nearGrid = grid;
                            }
                        }

                        if (tile != thunder)
                        {
                            int rand = Random.Range(0, 2);
                            if (rand == 0)
                            {
                                tile.TransformInto(new TileData(TileData.TileType.row_breaker, tile.tileData.tileColor));
                            }
                            else
                            {
                                tile.TransformInto(new TileData(TileData.TileType.column_breaker, tile.tileData.tileColor));
                            }
                        }

                        LeanTween.delayedCall(GameConstants.TIME_TO_MOVE_NEAR_MAGNET, () =>
                        {
                            LeanTween.rotateAround(tile.gameObject, Vector3.forward, 360f,
                                GameConstants.TIME_ROTATE_TILE).setOnComplete(() =>
                                {
                                    LeanTween.delayedCall(GameConstants.TIME_DELAY_ACTIVE_AFTER_TRANSFORM_BOOSTER, () =>
                                        {
                                            mapController.ExecuteTile(tile, nearGrid, false, true);
                                        });
                                });
                        });

                    }
                }
            }

            LeanTween.scale(magnet.gameObject, Vector3.zero, GameConstants.TIME_ROTATE_TILE
                + GameConstants.TIME_DELAY_ACTIVE_AFTER_TRANSFORM_BOOSTER + GameConstants.TIME_TO_MOVE_NEAR_MAGNET);

            LeanTween.rotateAround(magnet.gameObject, Vector3.forward, 360f, GameConstants.TIME_ROTATE_TILE 
                + GameConstants.TIME_DELAY_ACTIVE_AFTER_TRANSFORM_BOOSTER + GameConstants.TIME_TO_MOVE_NEAR_MAGNET)
                .setOnComplete(() =>
                {
                    mapController.ExecuteTile(magnet, magnet.grid, false, true);

                    if (GamePlayController.gamePlayController.gameInterceptorCount > 0)
                    {
                        GamePlayController.gamePlayController.gameInterceptorCount--;
                    }
                });
        }
    }

    /// <summary>
    /// Active magnet with bomb_breaker
    /// </summary>
    /// <param name="mapController"></param>
    /// <param name="magnet"></param>
    /// <param name="bomb"></param>
    public static void MagnetXBombBreaker(MapController mapController, Tile magnet, Tile bomb)
    {
        nearMagnetGridList.Clear();

        if (magnet != null && bomb != null)
        {
            nearMagnetGridList.Add(magnet.grid);
            GamePlayController.gamePlayController.gameInterceptorCount++;
            for (int i = 0; i < mapController.gridList.Count; i++)
            {
                Grid grid = mapController.gridList[i];
                if (grid != null && grid.gridData.gridType != GridData.GridType.NULL_GRID)
                {
                    Tile tile = grid.GetTile();
                    if (tile != null && tile.isHit && tile.isColored && tile.tileData.tileColor
                        == bomb.tileData.tileColor && !tile.isMoving && !tile.isOnClearingProcess)
                    {
                        tile.TransformInto(new TileData(TileData.TileType.bomb_rainbow, tile.tileData.tileColor));

                        Grid nearGrid = null;
                        if (magnet.grid.CheckIsAdjacent(tile.grid))
                        {
                            nearGrid = grid;
                        }
                        else
                        {
                            nearGrid = GetGridNearMagnet(magnet, bomb);
                            if (nearGrid != null && nearGrid.gridData.gridType != GridData.GridType.NULL_GRID)
                            {
                                Tile oldTile = nearGrid.GetTile();
                                nearGrid.SetTile(tile);
                                grid.SetTile(null);
                                oldTile.grid = null;

                                tile.transform.localPosition += new Vector3(0f, 0f, -1f);
                                nearGrid.MoveTileToCenter(GameConstants.TIME_TO_MOVE_NEAR_MAGNET);

                                // TODO: push to pool
                                // For test
                                LeanTween.delayedCall(0.5f * GameConstants.TIME_TO_MOVE_NEAR_MAGNET, () =>
                                {
                                    //oldTile.gameObject.SetActive(false);
                                    PoolingManager.poolingManager.AddTileToPool(oldTile);
                                });
                                // end test
                            }
                            else
                            {
                                nearGrid = grid;
                            }
                        }

                        LeanTween.delayedCall(GameConstants.TIME_TO_MOVE_NEAR_MAGNET, () =>
                        {
                            LeanTween.rotateAround(tile.gameObject, Vector3.forward, 360f,
                                GameConstants.TIME_ROTATE_TILE).setOnComplete(() =>
                                {
                                    LeanTween.delayedCall(GameConstants.TIME_DELAY_ACTIVE_AFTER_TRANSFORM_BOOSTER, () =>
                                    {
                                        mapController.ExecuteTile(tile, nearGrid, false, true);
                                    });
                                });
                        });

                    }
                }
            }

            LeanTween.scale(magnet.gameObject, Vector3.zero, GameConstants.TIME_ROTATE_TILE
                + GameConstants.TIME_DELAY_ACTIVE_AFTER_TRANSFORM_BOOSTER + GameConstants.TIME_TO_MOVE_NEAR_MAGNET);

            LeanTween.rotateAround(magnet.gameObject, Vector3.forward, 360f, GameConstants.TIME_ROTATE_TILE
                + GameConstants.TIME_DELAY_ACTIVE_AFTER_TRANSFORM_BOOSTER + GameConstants.TIME_TO_MOVE_NEAR_MAGNET)
                .setOnComplete(() =>
                {
                    mapController.ExecuteTile(magnet, magnet.grid, false, true);

                    if (GamePlayController.gamePlayController.gameInterceptorCount > 0)
                    {
                        GamePlayController.gamePlayController.gameInterceptorCount--;
                    }
                });
        }
    }

    /// <summary>
    /// Active magnet with x_breaker
    /// </summary>
    /// <param name="mapController"></param>
    /// <param name="magnet"></param>
    /// <param name="xBreaker"></param>
    public static void MagnetXXBreaker(MapController mapController, Tile magnet, Tile xBreaker)
    {
        nearMagnetGridList.Clear();

        if (magnet != null && xBreaker != null)
        {
            nearMagnetGridList.Add(magnet.grid);
            GamePlayController.gamePlayController.gameInterceptorCount++;
            for (int i = 0; i < mapController.gridList.Count; i++)
            {
                Grid grid = mapController.gridList[i];
                if (grid != null && grid.gridData.gridType != GridData.GridType.NULL_GRID)
                {
                    Tile tile = grid.GetTile();
                    if (tile != null && tile.isHit && tile.isColored && tile.tileData.tileColor
                        == xBreaker.tileData.tileColor && !tile.isMoving && !tile.isOnClearingProcess)
                    {
                        tile.TransformInto(new TileData(TileData.TileType.x_rainbow, tile.tileData.tileColor));

                        Grid nearGrid = null;
                        if (magnet.grid.CheckIsAdjacent(tile.grid))
                        {
                            nearGrid = grid;
                        }
                        else
                        {
                            nearGrid = GetGridNearMagnet(magnet, xBreaker);
                            if (nearGrid != null && nearGrid.gridData.gridType != GridData.GridType.NULL_GRID)
                            {
                                Tile oldTile = nearGrid.GetTile();
                                nearGrid.SetTile(tile);
                                grid.SetTile(null);
                                oldTile.grid = null;

                                tile.transform.localPosition += new Vector3(0f, 0f, -1f);
                                nearGrid.MoveTileToCenter(GameConstants.TIME_TO_MOVE_NEAR_MAGNET);

                                // TODO: push to pool
                                // For test
                                LeanTween.delayedCall(0.5f * GameConstants.TIME_TO_MOVE_NEAR_MAGNET, () =>
                                {
                                    //oldTile.gameObject.SetActive(false);
                                    PoolingManager.poolingManager.AddTileToPool(oldTile);
                                });
                                // end test
                            }
                            else
                            {
                                nearGrid = grid;
                            }
                        }

                        LeanTween.delayedCall(GameConstants.TIME_TO_MOVE_NEAR_MAGNET, () =>
                        {
                            LeanTween.rotateAround(tile.gameObject, Vector3.forward, 360f,
                                GameConstants.TIME_ROTATE_TILE).setOnComplete(() =>
                                {
                                    LeanTween.delayedCall(GameConstants.TIME_DELAY_ACTIVE_AFTER_TRANSFORM_BOOSTER, () =>
                                    {
                                        mapController.ExecuteTile(tile, nearGrid, false, true);
                                    });
                                });
                        });

                    }
                }
            }

            LeanTween.scale(magnet.gameObject, Vector3.zero, GameConstants.TIME_ROTATE_TILE
                + GameConstants.TIME_DELAY_ACTIVE_AFTER_TRANSFORM_BOOSTER + GameConstants.TIME_TO_MOVE_NEAR_MAGNET);

            LeanTween.rotateAround(magnet.gameObject, Vector3.forward, 360f, GameConstants.TIME_ROTATE_TILE
                + GameConstants.TIME_DELAY_ACTIVE_AFTER_TRANSFORM_BOOSTER + GameConstants.TIME_TO_MOVE_NEAR_MAGNET)
                .setOnComplete(() =>
                {
                    mapController.ExecuteTile(magnet, magnet.grid, false, true);

                    if (GamePlayController.gamePlayController.gameInterceptorCount > 0)
                    {
                        GamePlayController.gamePlayController.gameInterceptorCount--;
                    }
                });
        }
    }

    /// <summary>
    /// Active combine swap: x_breaker + x_breaker / bomb_breaker / row_breaker / column_breaker
    /// </summary>
    /// <param name="mapController"></param>
    /// <param name="xBreaker1"></param>
    /// <param name="xBreaker2"></param>
    public static void XBreakerXBreaker(MapController mapController, Tile xBreaker1, Tile xBreaker2)
    {
        if (xBreaker1 != null && xBreaker2 != null)
        {
            xBreaker1.grid.Damage(true);

            XBreaker.Activate(xBreaker1.grid);
            RowBreaker.Activate(xBreaker1.grid);
            ColumnBreaker.Activate(xBreaker1.grid);
        }
    }

    /// <summary>
    /// Active combine swap row_breaker/column_breaker + row_breaker/column_breaker
    /// </summary>
    /// <param name="mapController"></param>
    /// <param name="thunder1"></param>
    /// <param name="thunder2"></param>
    public static void CombineRowColumnBreaker(MapController mapController, Tile thunder1, Tile thunder2)
    {
        if (thunder1 != null && thunder2 != null)
        {
            if (thunder1.tileData.tileType == thunder2.tileData.tileType)
            {
                if (thunder1.tileData.tileType == TileData.TileType.row_breaker)
                {
                    thunder2.tileData.tileType = TileData.TileType.column_breaker;
                }
                else
                {
                    thunder2.tileData.tileType = TileData.TileType.row_breaker;
                }
            }

            mapController.ExecuteTile(thunder1, thunder1.grid, false, true);
            mapController.ExecuteTile(thunder2, thunder2.grid, false, true);
        }
    }

    /// <summary>
    /// Active combine swap bomb + row_breaker/column_breaker
    /// </summary>
    /// <param name="mapController"></param>
    /// <param name="bomb"></param>
    /// <param name="thunder"></param>
    public static void BombXRowColumnBreaker(MapController mapController, Tile bomb, Tile thunder)
    {
        if (bomb != null && thunder != null)
        {
            //List<Grid> activeGridList = new List<Grid>();
            thunder.grid.Damage();
            bomb.tileData.tileType = TileData.TileType.match_object;
            Grid grid = null;
            for (int i = 0; i < GameConstants.DIRECTIONS_TILE_CLEAR_ROW.Count; i++)
            {
                grid = thunder.grid.GetGridInDir(GameConstants.DIRECTIONS_TILE_CLEAR_ROW[i]);
                if (grid != null && grid.gridData.gridType != GridData.GridType.NULL_GRID)
                {
                    ColumnBreaker.Activate(grid, false);
                }
            }

            ColumnBreaker.Activate(thunder.grid, true, GameConstants.DIRECTIONS_TILE_CLEAR_ROW.Count + 1);

            for (int i = 0; i < GameConstants.DIRECTIONS_TILE_CLEAR_COLUMN.Count; i++)
            {
                grid = thunder.grid.GetGridInDir(GameConstants.DIRECTIONS_TILE_CLEAR_COLUMN[i]);
                if (grid != null && grid.gridData.gridType != GridData.GridType.NULL_GRID)
                {
                    RowBreaker.Activate(grid, false);
                }
            }

            RowBreaker.Activate(thunder.grid, true, GameConstants.DIRECTIONS_TILE_CLEAR_ROW.Count + 1);

            //bomb.grid.OnTileCleared();
            //bomb.grid.OnNearByTileCleared(bomb);

            //thunder.grid.OnTileCleared();
            //thunder.grid.OnNearByTileCleared(thunder);
        }
    }

    /// <summary>
    /// Active combine 2 bomb
    /// </summary>
    /// <param name="mapController"></param>
    /// <param name="bomb1"></param>
    /// <param name="bomb2"></param>
    public static void BombXBomb(MapController mapController, Tile bomb1, Tile bomb2)
    {
        if (bomb1 != null && bomb2 != null)
        {
            mapController.ExecuteTile(bomb1, bomb1.grid, false, true);
            mapController.ExecuteTile(bomb2, bomb2.grid, false, true);
        }
    }

    /// <summary>
    /// Get grid near magnet
    /// </summary>
    /// <param name="rootTile"></param>
    /// <param name="targetTile"></param>
    /// <returns></returns>
    private static Grid GetGridNearMagnet(Tile rootTile, Tile targetTile)
    {
        if (rootTile == null || targetTile == null)
        {
            return null;
        }

        Tile nearTile = null;
        Grid nearGrid = null;
        Grid finderGrid = null;
        Grid rootGrid = rootTile.grid;
        if (rootGrid == null || rootGrid.gridData.gridType == GridData.GridType.NULL_GRID)
        {
            return null;
        }

        for (int i = 0; i < nearMagnetGridList.Count; i++)
        {
            rootGrid = nearMagnetGridList[i];
            for (int ii = 0; ii < Grid.DIR_NUM; ii++)
            {
                nearGrid = rootGrid.connectedList[ii];
                if (nearGrid == null || nearGrid.gridData.gridType == GridData.GridType.NULL_GRID)
                {
                    continue;
                }

                if (nearMagnetGridList.Contains(nearGrid))
                {
                    continue;
                }

                nearTile = nearGrid.GetTile();
                if (nearTile == null || nearTile.isMoving || nearTile.isOnClearingProcess || nearTile.isTransforming)
                {
                    continue;
                }

                if (nearTile.isHit && nearTile.isColored && !nearTile.isSpecialActivable
                    /*&& nearTile.tileData.tileColor != targetTile.tileData.tileColor*/)
                {
                    nearMagnetGridList.Add(nearGrid);
                    finderGrid = nearGrid;

                    return finderGrid;
                }
            }
        }
        
        //if (finderGrid == null)
        //{
        //    finderGrid = GetGridNearMagnet(rootTile, targetTile);
        //}

        return finderGrid;
    }
	
}

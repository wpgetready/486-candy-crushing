﻿/*
 * @Author: CuongNH
 * @Description: Khoi tao, dieu phoi xu ly cac su kien trong Map(khung chinh trong gameplay) tren man choi
 * */

using UnityEngine;
using System.Collections;

using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MapController : MonoBehaviour {

    /// <summary>
    /// Danh sach tat ca cac grid tren Map
    /// </summary>
    public List<Grid> gridList;

    /// <summary>
    /// GridSpawner to transform grid
    /// </summary>
    public GridSpawner gridSpawner;

    /// <summary>
    /// TileSpawner to create and change tile
    /// </summary>
    public TileSpawner tileSpawner;

    /// <summary>
    /// ConveyorBeltController to controll Conveyor Belt
    /// </summary>
    public ConveyorBeltController conveyorBeltController;

    // Processing components
    //public ITileSpawner tileSpawner;
    public IMapConditionChecker mapConditionChecker;
    public IMapConditionChecker mapConditionCheckerMatch2x2;
    public TileManagerMatch3 tileManager;
    public IGameInputProcessor gameInputProcessor;
    public IGameInputControl gameInputControl;
    //public IGameInputProcessor gameInputProcessorMatch2x2;
    //public IGameInputControl gameInputControlMatch2x2;
    public TouchInputHandler touchInputHandler;

    // Common flags
    public bool isSwapping = false;
    public bool isProcessing = false;
    public bool canRunFinishTurn = false;
    //public bool isObjectiveFinished = false;

    // Process support
    public List<Grid> processingGrids; // Grid with tile moving in
    public List<Grid> emptyGrids; // Grid with no tile

    // Other
    public Tile helpTile1, helpTile2;
    //public Transform helpEffect;
    //public Transform gridSelector;
    //private Vector3 helpPosition = new Vector3();
    Vector3 moveForwardTileHelp1 = new Vector3();
    Vector3 moveForwardTileHelp2 = new Vector3();
    private int helpTile1ID = 0;
    private int helpTile2ID = 0;

    //public bool showGrid = true;

    public int dimCounter = 0;
    public GameObject blur; // darkened screen, used for many effect

    // No More Move
    public NoMoreMoveBottle noMoreMoveBottle;
	public Button pauseBT;

    //public List<Transform> collectionPositions; // collector positions
    //public IObjectiveHUD objectiveHUDControl; // use to call update

    //// Use this for initialization
    //void Start()
    //{
    //    // Set ratio scale
    //    //HomeController.ratioScale = (((float)Screen.height / Screen.width) 
    //    //    / (HomeController.BASE_SCREEN_SIZE.y / HomeController.BASE_SCREEN_SIZE.x));

    //    //this.transform.localScale = this.transform.localScale * HomeController.ratioScale;
    //}
	
    //// Update is called once per frame
    //void Update () {

    //}

    /// <summary>
    /// Move map to game play
    /// </summary>

	public CountStarLevel countStar;

	public Button bt1, bt3;

	public void GoPlay()
    {
        for (int i = 0; i < gridList.Count; i++)
        {
            Grid grid = gridList[i];
            if (grid != null && grid.gridData.gridType != GridData.GridType.NULL_GRID)
            {
                Tile tile = grid.GetTile();
                if (tile != null && tile.isHit && tile.isSwappable && tile.isDroppable)
                {
                    tile.transform.localScale = Vector3.zero;
                }
            }
        }

        float deylay = 0f;
        if (GameManager.gameMode == GameManager.GameMode.ORDER_FULFILLMENT)
        {
            deylay = GameConstants.TIME_MOVE_MAP_TO_PLAY;//2
        }
        else
        {
            deylay = GameConstants.TIME_MOVE_MAP_TO_PLAY / 3;
        }

        LeanTween.delayedCall(deylay, () =>
            {
                for (int i = 0; i < gridList.Count; i++)
                {
                    Grid grid = gridList[i];
                    if (grid != null && grid.gridData.gridType != GridData.GridType.NULL_GRID)
                    {
                        Tile tile = grid.GetTile();
                        if (tile != null && tile.isHit && tile.isSwappable && tile.isDroppable)
                        {
                            LeanTween.scale(tile.gameObject, Vector3.one, GameConstants.TIME_MOVE_MAP_TO_PLAY);
                            LeanTween.rotateAround(tile.gameObject, Vector3.back, 360,
                                GameConstants.TIME_MOVE_MAP_TO_PLAY / 2f + 0.2f).setRepeat(2);
                        }
                    }
                }
				Vector3 moveback = Vector3.zero;
			if(SceneManager.GetActiveScene().buildIndex == 3)
			{
                moveback = new Vector3(0.3f, this.transform.position.y, this.transform.position.z);
			}

                LeanTween.move(this.gameObject, new Vector3(0f, this.transform.position.y, this.transform.position.z),
                GameConstants.TIME_MOVE_MAP_TO_PLAY).setOnComplete(() =>
                {
                    		LeanTween.move(this.gameObject, moveback, 0.2f).setLoopPingPong().setRepeat(2).setOnComplete(() =>
                        {
									pauseBT.interactable = true;
                            GameManager.gameState = GameManager.GameState.PLAYING;
                            GamePlayController.gamePlayController.dropFlag = true;
									if(Contance.NUMBER_LEVEL == 1)
									{
										bt1.interactable = true;
									}

									if(Contance.NUMBER_LEVEL == 4)
									{
										bt3.interactable = true;
									}
                            this.isProcessing = true;
									});
                });
            });
    }

    #region Methods for handle help
    /// <summary>
    /// Set help grid
    /// </summary>
    /// <param name="grid"></param>
    /// <param name="swapGrid"></param>
    public void SetHelpGrid(Grid grid, Grid swapGrid)
    {
        //Debug.Log("SetHelpGrid");

        helpTile1 = grid.GetTile();
        helpTile2 = swapGrid.GetTile();
    }

    /// <summary>
    /// Show grid selector
    /// </summary>
    /// <param name="grid"></param>
    public void ShowGridSelector(Grid grid)
    {

    }

    /// <summary>
    /// Hide grid selecter
    /// </summary>
    public void HideGridSelector()
    {

    }

    /// <summary>
    /// Show help
    /// </summary>
    public void ShowHelp()
    {
        if (helpTile1 != null && helpTile1.grid != null && helpTile2 != null && helpTile2.grid != null)
        {
            //Debug.Log("ShowHelp");

            moveForwardTileHelp1 = (9 * helpTile1.transform.position + helpTile2.transform.position) / 10;
            moveForwardTileHelp2 = (helpTile1.transform.position + 9 * helpTile2.transform.position) / 10;

            helpTile1ID = LeanTween.move(helpTile1.gameObject, moveForwardTileHelp1, 
                GameConstants.TIME_MOVE_HELP).setLoopPingPong().setRepeat(-1).id;
            helpTile2ID = LeanTween.move(helpTile2.gameObject, moveForwardTileHelp2, 
                GameConstants.TIME_MOVE_HELP).setLoopPingPong().setRepeat(-1).id;
        }
    }

    /// <summary>
    /// Hide help
    /// </summary>
    public void HideHelp()
    {
        if (helpTile1 != null)
        {
            if (helpTile1ID != 0)
            {
                LeanTween.cancel(helpTile1.gameObject, helpTile1ID);
                helpTile1ID = 0;
            }

            helpTile1.transform.localPosition = Vector3.zero;
            helpTile1 = null;
        }

        if (helpTile2 != null)
        {
            if (helpTile2ID != 0)
            {
                LeanTween.cancel(helpTile2.gameObject, helpTile2ID);
                helpTile2ID = 0;
            }

            helpTile2.transform.localPosition = Vector3.zero;
            helpTile2 = null;
        }
    }
    #endregion

    #region Methods for handle tiles
    /// <summary>
    /// Upgrade for tile
    /// </summary>
    /// <param name="tile"></param>
    /// <param name="lengthCount"></param>
    /// <param name="dirCount"></param>
    /// <param name="swapDirection"></param>
    /// <param name="matchDirection"></param>
    /// <returns></returns>
    public bool UpgradeTile(Tile tile, int lengthCount, int dirCount, Grid.Direction swapDirection 
        = Grid.Direction.UNKNOWN, Grid.Direction matchDirection = Grid.Direction.UNKNOWN,
        List<Grid> matchedList = null, bool matched2x2 = false)
    {
        bool upgraded = false;

        switch (tile.tileData.tileType)
        {
            #region match_object
            case TileData.TileType.match_object:
                {
                    tile.OnUpgrade();

                    if (!tile.isSpecialActivable)
                    {
                        // Create rocket
                        if (matched2x2)
                        {
                            Tile rocketTile = MapUtilities.GetRandomTileFromList(gridList, tile.tileData.tileColor, matchedList);
                            //if (rocketTile == null)
                            //{
                            //    rocketTile = tile;
                            //}

                            if (rocketTile != null)
                            {
                                int rand = Random.Range(0, 2);
                                if (rand == 0)
                                {
                                    //rocketTile.TransformInto(new TileData(TileData.TileType.row_breaker, tile.tileData.tileColor));
                                    Rocket.TransformProcess(tile, rocketTile, TileData.TileType.row_breaker);
                                }
                                else
                                {
                                    //rocketTile.TransformInto(new TileData(TileData.TileType.column_breaker, tile.tileData.tileColor));
                                    Rocket.TransformProcess(tile, rocketTile, TileData.TileType.column_breaker);
                                }
                            }

                            upgraded = true;
                        }
                        // Create rainbow
                        else if (lengthCount >= 4)
                        {
                            if (dirCount >= 2)
                            {
                                tile.TransformInto(new TileData(TileData.TileType.magnet));
                            }
                            else
                            {
                                tile.TransformInto(new TileData(TileData.TileType.rainbow));
                            }

                            //tile.SetValue(4);

                            upgraded = true;
                        }
                        // Bomb convert
                        else if (dirCount >= 2)
                        {
                            if (dirCount >= 3)
                            {
                                tile.TransformInto(new TileData(TileData.TileType.x_breaker, tile.tileData.tileColor));
                            }
                            else
                            {
                                tile.TransformInto(new TileData(TileData.TileType.bomb_breaker, tile.tileData.tileColor));
                            }

                            //tile.SetValue(2);

                            upgraded = true;
                        }
                        // Set convert
                        else if (lengthCount == 3)
                        {
                            //tile.TransformInto(new TileData(TileData.TileType.BOMB_GEM, tile.tileData.tileColor));

                            if (swapDirection == Grid.Direction.LEFT || swapDirection == Grid.Direction.RIGHT)
                            {
                                // Create row_breaker
                                tile.TransformInto(new TileData(TileData.TileType.row_breaker, tile.tileData.tileColor));
                            }
                            else if (swapDirection == Grid.Direction.TOP || swapDirection == Grid.Direction.BOTTOM)
                            {
                                // Create column_breaker
                                tile.TransformInto(new TileData(TileData.TileType.column_breaker, tile.tileData.tileColor));
                            }
                            else
                            {
                                if (matchDirection == Grid.Direction.LEFT || matchDirection == Grid.Direction.RIGHT)
                                {
                                    // Create column_breaker
                                    tile.TransformInto(new TileData(TileData.TileType.column_breaker, tile.tileData.tileColor));
                                }
                                else if (matchDirection == Grid.Direction.TOP || matchDirection == Grid.Direction.BOTTOM)
                                {
                                    // Create row_breaker
                                    tile.TransformInto(new TileData(TileData.TileType.row_breaker, tile.tileData.tileColor));
                                }
                            }

                            //tile.SetValue(3);

                            upgraded = true;

                            return true;
                        }
                        else
                        {
                            //tile.SetValue(1);

                            return false;
                        }
                    }

                    break;
                }
            #endregion
            #region coconut
            case TileData.TileType.coconut_1:
                {
                    tile.TransformInto(new TileData(TileData.TileType.coconut_2));
                    if (matchedList != null && matchedList.Count > (GameConstants.MATCH3_MIN))
                    {
                        int upgradeNumber = matchedList.Count - (GameConstants.MATCH3_MIN - 1);
                        List<Grid> upgradeList = new List<Grid>();
                        upgradeList.Add(tile.grid);
                        for (int i = 0; i <= matchedList.Count; i++)
                        {

                            if (upgradeList.Count >= upgradeNumber)
                            {
                                break;
                            }
							
							
						if(i < matchedList.Count-1)
						{

                            if (upgradeList.Contains(matchedList[i]))
                            {
								
                                	continue;
								
								
                            }

						}
						else
						{
							break;
						}
		                            for (int j = 0; j < upgradeList.Count; j++)
                            {
                                if (matchedList[i].CheckIsAdjacent(upgradeList[j]))
                                {
                                    Tile nextTile = matchedList[i].GetTile();
                                    if (nextTile != null && !nextTile.isTransforming)
                                    {
                                        upgradeList.Add(matchedList[i]);
                                        nextTile.OnUpgrade();

                                        break;
                                    }
                                }
                                else
                                {
                                    continue;
                                }
                            }
                        }
                    }

                    upgraded = true;

                    return true;
                }
            #endregion
            default:
                break;
        }

        return upgraded;
    }

    /// <summary>
    /// Execute tile when swap valid
    /// </summary>
    /// <param name="tile"></param>
    /// <param name="effectGrid"></param>
    /// <param name="ignoreEffect"></param>
    /// <param name="affectNearby"></param>

	IEnumerator waitTime()
	{
		yield return new WaitForSeconds (1f);
	}

	IEnumerator Row(Grid _grid)
	{
		yield return new WaitForSeconds (1f);
		RowBreaker.Activate(_grid);
	}

	IEnumerator Column(Grid _grid)
	{
		yield return new WaitForSeconds (1f);
		ColumnBreaker.Activate(_grid);
	}

	public void ExecuteTile(Tile tile, Grid effectGrid, bool ignoreEffect = false, bool affectNearby = true)
    {
        if (tile == null)
        {
            return;
        }

        Grid tileGrid = tile.grid;
		if (!ignoreEffect && tileGrid != null)
        {   
            switch (tile.tileData.tileType)
            {
                case TileData.TileType.bomb_breaker:
			{//Debug.Log("special");
				countStar.CountStar(115);StartCoroutine(waitTime());
                        if (tileGrid.gridData.gridType != GridData.GridType.NULL_GRID)
                        {
                            tileGrid.Damage(affectNearby);
							//Debug.Log("special1");
                        }

                        if (effectGrid.gridData.gridType != GridData.GridType.NULL_GRID)
                        {
                            BombBreaker.Activate(effectGrid);
							//Debug.Log("special2");
                        }

                        break;
                    }
                case TileData.TileType.row_breaker:
			{//Debug.Log("special");
				countStar.CountStar(115);StartCoroutine(waitTime());
                        if (tileGrid.gridData.gridType != GridData.GridType.NULL_GRID)
                        {
                            tileGrid.Damage(affectNearby);
							//Debug.Log("special1");
                        }

                        if (effectGrid.gridData.gridType != GridData.GridType.NULL_GRID)
                        {
//							if (Contance.GAME_WIN == false) 
//							{
								RowBreaker.Activate (effectGrid);
//							} 
//							else 
//							{
//								StartCoroutine (Row(effectGrid));
//								gamePlayController.UpdateDrop ();
//							}
	                   	}

                        break;
                    }
                case TileData.TileType.column_breaker:
				{
					countStar.CountStar(115);StartCoroutine(waitTime());
                        if (tileGrid.gridData.gridType != GridData.GridType.NULL_GRID)
                        {
                            tileGrid.Damage(affectNearby);
							//Debug.Log("special1");
                        }

                        if (effectGrid.gridData.gridType != GridData.GridType.NULL_GRID)
                        {
//							if (Contance.GAME_WIN == false) 
//							{
								ColumnBreaker.Activate (effectGrid);
//							}
//							else 
//							{
//								StartCoroutine (Column(effectGrid));
//								gamePlayController.UpdateDrop ();
//							}

						}
							//Debug.Log("special2");
                        

                        break;
                    }
                case TileData.TileType.x_breaker:
			{//Debug.Log("special");
				countStar.CountStar(115);StartCoroutine(waitTime());
                        if (tileGrid.gridData.gridType != GridData.GridType.NULL_GRID)
                        {
                            tileGrid.Damage(affectNearby);
							//Debug.Log("special1");
                        }

                        if (effectGrid.gridData.gridType != GridData.GridType.NULL_GRID)
                        {
                            XBreaker.Activate(effectGrid);
							//Debug.Log("special2");
                        }

                        break;
                    }
                case TileData.TileType.bomb_rainbow:
			{
				countStar.CountStar(115);StartCoroutine(waitTime());
                        if (tileGrid.gridData.gridType != GridData.GridType.NULL_GRID)
                        {
                            tileGrid.Damage(affectNearby);
							//Debug.Log("special1");
                        }

                        if (effectGrid.gridData.gridType != GridData.GridType.NULL_GRID)
                        {
                            BombRainbow.Activate(effectGrid);
							//Debug.Log("special2");
                        }

                        break;
                    }
                case TileData.TileType.x_rainbow:
			{
				countStar.CountStar(115);StartCoroutine(waitTime());
                        if (tileGrid.gridData.gridType != GridData.GridType.NULL_GRID)
                        {
                            tileGrid.Damage(affectNearby);
							//Debug.Log("special1");
                        }

                        if (effectGrid.gridData.gridType != GridData.GridType.NULL_GRID)
                        {
                            XRainbow.Activate(effectGrid);
							//Debug.Log("special2");
                        }

                        break;
                    }
                default:
                    {
                        if (tileGrid != null)
				{StartCoroutine(waitTime());
                            tileGrid.Damage(affectNearby);
                        }
                        break;
                    }

            }


        }
        else
        {
            if (tileGrid != null)
			{StartCoroutine(waitTime());
				tileGrid.Damage(affectNearby);
            }
        }

        if (tileGrid != null)
        {
            if (!emptyGrids.Contains(tileGrid))
            {
                emptyGrids.Add(tileGrid);
            }
        }
    }

    /// <summary>
    /// Active booster swapped
    /// </summary>
    /// <param name="tile1"></param>
    /// <param name="tile2"></param>
    public void ActiveBoosterSwapped(Tile tile1, Tile tile2)
    {
        CombineBooster.Active(this, tile1, tile2);
    }
    #endregion

    #region Game Flow
    /// <summary>
    /// Called when an objective have been finished
    /// </summary>
    //public void OnObjectiveFinish()
    //{
    //    isObjectiveFinished = true;
    //    // Spawn star if this is a arcade level
    //    // 
    //}

    /// <summary>
    /// Begin processing phase if haven't been started
    /// </summary>
    public void StartProcessingPhase(bool newTurn = true)
    {
        //canRunFinishTurn = true;

        if (this.isProcessing == false && newTurn)
        {
            canRunFinishTurn = true;
            // Decrease move here
            //GameManager.Instance.MoveRemain--;
        }

        //HideGridSelector();
        HideHelp();
        this.isProcessing = true;
    }

	int countFFF;

	public MapBuilding mapBuilding;

	public void CheckColorGrid()
	{
		List<Grid> _listColorGrid = mapBuilding.listColorGrid;
		if(_listColorGrid.Count > 0)
		{
			foreach(Grid item in _listColorGrid)
			{
				if(item != null && item.GetTile() != null)
				{
					Tile _item = item.GetTile ();
					if(_item.tileData.tileType == TileData.TileType.match_object)
					{
						if((int)_item.tileData.tileColor != (int)item.gridData.gridColorType)
						{
							if(item.gridData.gridColorType == GridData.GridColorType.BLUE_GRID)
							{
								_item.tileData.tileColor = TileData.TileColor.blue;
								_item.SetMainRender (tileSpawner.matchObjectSprite[0]);
							}

							if(item.gridData.gridColorType == GridData.GridColorType.GREEN_GRID)
							{
								_item.tileData.tileColor = TileData.TileColor.green;
								_item.SetMainRender (tileSpawner.matchObjectSprite[1]);
							}

							if(item.gridData.gridColorType == GridData.GridColorType.ORANGE_GRID)
							{
								_item.tileData.tileColor = TileData.TileColor.orange;
								_item.SetMainRender (tileSpawner.matchObjectSprite[2]);
							}

							if(item.gridData.gridColorType == GridData.GridColorType.PURPLE_GRID)
							{
								_item.tileData.tileColor = TileData.TileColor.purple;
								_item.SetMainRender (tileSpawner.matchObjectSprite[3]);
							}

							if(item.gridData.gridColorType == GridData.GridColorType.RED_GRID)
							{
								_item.tileData.tileColor = TileData.TileColor.red;
								_item.SetMainRender (tileSpawner.matchObjectSprite[4]);
							}

							if(item.gridData.gridColorType == GridData.GridColorType.YELLOW_GRID)
							{
								_item.tileData.tileColor = TileData.TileColor.yellow;
								_item.SetMainRender (tileSpawner.matchObjectSprite[5]);
							}
							tileSpawner.ShowStarsEffect (_item);
						}
					}

					if(_item.tileData.tileType == TileData.TileType.bomb_breaker)
					{
						if((int)_item.tileData.tileColor != (int)item.gridData.gridColorType)
						{
							if (item.gridData.gridColorType == GridData.GridColorType.BLUE_GRID) 
							{
								_item.tileData.tileColor = TileData.TileColor.blue;
								_item.SetMainRender (tileSpawner.bombBreakerSprite [0]);
							}

							if (item.gridData.gridColorType == GridData.GridColorType.GREEN_GRID) {
								_item.tileData.tileColor = TileData.TileColor.green;
								_item.SetMainRender (tileSpawner.bombBreakerSprite [1]);
							}

							if (item.gridData.gridColorType == GridData.GridColorType.ORANGE_GRID) {
								_item.tileData.tileColor = TileData.TileColor.orange;
								_item.SetMainRender (tileSpawner.bombBreakerSprite [2]);
							}

							if (item.gridData.gridColorType == GridData.GridColorType.PURPLE_GRID) {
								_item.tileData.tileColor = TileData.TileColor.purple;
								_item.SetMainRender (tileSpawner.bombBreakerSprite [3]);
							}

							if (item.gridData.gridColorType == GridData.GridColorType.RED_GRID) {
								_item.tileData.tileColor = TileData.TileColor.red;
								_item.SetMainRender (tileSpawner.bombBreakerSprite [4]);
							}

							if (item.gridData.gridColorType == GridData.GridColorType.YELLOW_GRID) {
								_item.tileData.tileColor = TileData.TileColor.yellow;
								_item.SetMainRender (tileSpawner.bombBreakerSprite [5]);
							}
							tileSpawner.ShowStarsEffect (_item);
						}
					}

					if(_item.tileData.tileType == TileData.TileType.column_breaker)
					{
						if((int)_item.tileData.tileColor != (int)item.gridData.gridColorType)
						{
							if (item.gridData.gridColorType == GridData.GridColorType.BLUE_GRID) {
								_item.tileData.tileColor = TileData.TileColor.blue;
								_item.SetMainRender (tileSpawner.columnBreakerSprite [0]);
							}

							if (item.gridData.gridColorType == GridData.GridColorType.GREEN_GRID) {
								_item.tileData.tileColor = TileData.TileColor.green;
								_item.SetMainRender (tileSpawner.columnBreakerSprite [1]);
							}

							if (item.gridData.gridColorType == GridData.GridColorType.ORANGE_GRID) {
								_item.tileData.tileColor = TileData.TileColor.orange;
								_item.SetMainRender (tileSpawner.columnBreakerSprite [2]);
							}

							if (item.gridData.gridColorType == GridData.GridColorType.PURPLE_GRID) {
								_item.tileData.tileColor = TileData.TileColor.purple;
								_item.SetMainRender (tileSpawner.columnBreakerSprite [3]);
							}

							if (item.gridData.gridColorType == GridData.GridColorType.RED_GRID) {
								_item.tileData.tileColor = TileData.TileColor.red;
								_item.SetMainRender (tileSpawner.columnBreakerSprite [4]);
							}

							if (item.gridData.gridColorType == GridData.GridColorType.YELLOW_GRID) {
								_item.tileData.tileColor = TileData.TileColor.yellow;
								_item.SetMainRender (tileSpawner.columnBreakerSprite [5]);
							}
							tileSpawner.ShowStarsEffect (_item);
						}
					}

					if(_item.tileData.tileType == TileData.TileType.row_breaker)
					{
						if((int)_item.tileData.tileColor != (int)item.gridData.gridColorType)
						{
							if (item.gridData.gridColorType == GridData.GridColorType.BLUE_GRID) {
								_item.tileData.tileColor = TileData.TileColor.blue;
								_item.SetMainRender (tileSpawner.rowBreakerSprite [0]);
							}

							if (item.gridData.gridColorType == GridData.GridColorType.GREEN_GRID) {
								_item.tileData.tileColor = TileData.TileColor.green;
								_item.SetMainRender (tileSpawner.rowBreakerSprite [1]);
							}

							if (item.gridData.gridColorType == GridData.GridColorType.ORANGE_GRID) {
								_item.tileData.tileColor = TileData.TileColor.orange;
								_item.SetMainRender (tileSpawner.rowBreakerSprite [2]);
							}

							if (item.gridData.gridColorType == GridData.GridColorType.PURPLE_GRID) {
								_item.tileData.tileColor = TileData.TileColor.purple;
								_item.SetMainRender (tileSpawner.rowBreakerSprite [3]);
							}

							if (item.gridData.gridColorType == GridData.GridColorType.RED_GRID) {
								_item.tileData.tileColor = TileData.TileColor.red;
								_item.SetMainRender (tileSpawner.rowBreakerSprite [4]);
							}

							if (item.gridData.gridColorType == GridData.GridColorType.YELLOW_GRID) {
								_item.tileData.tileColor = TileData.TileColor.yellow;
								_item.SetMainRender (tileSpawner.rowBreakerSprite [5]);
							}
							tileSpawner.ShowStarsEffect (_item);
						}
					}
				}
			}
		}
	}

	public GamePlayController gamePlayController;

    /// <summary>
    /// Stop processing, turn end mark here
    /// </summary>
    public void FinishProcessingPhase(bool isFinishTurn = true)
    {
//		bool noMoveMore = false;

		countFFF++;
		//Debug.Log ("countFFF: " + countFFF.ToString());
        if (canRunFinishTurn || !isFinishTurn)
        {
            float conveyTime = 0f;
            bool hasConvey = false;
            if (isFinishTurn)
            {
                //Debug.Log("FinishProcessingPhase");

                canRunFinishTurn = false;

                // Convey conveyor and belt
                hasConvey = conveyorBeltController.HandleConvey(ref conveyTime);
            }

            // finish turn
            bool finishTurnMakeChange = false;
            float maxTime = 0; // Max time of all animations, logic for turn finish
            for (int i = 0; i < gridList.Count; i++)
            {
                finishTurnMakeChange |= gridList[i].OnTurnEnded(ref maxTime, isFinishTurn);
            }

            maxTime = (maxTime > conveyTime) ? maxTime : conveyTime;

            if (finishTurnMakeChange || hasConvey)
            {
                GamePlayController.gamePlayController.SetExecutionDelay(maxTime);
            }

            if (hasConvey)
            {
                HideHelp();
                GamePlayController.gamePlayController.dropFlag = true;
            }

            GamePlayController.gamePlayController.cascadeFlag = true;
        }
        else
        {
            this.isProcessing = false;

            StartCoroutine( GamePlayController.gamePlayController.CheckStateGame());

            GamePlayController.gamePlayController.BossDropBlocker();


            // Turn ended, check available move, run no more move sequence
				//Debug.Log("aaaaaaaa");
	            if (!mapConditionChecker.CheckAvailableMove() && !mapConditionCheckerMatch2x2.CheckAvailableMove())
	            {
//					for (int i = 0; i < gridList.Count; i++)
//					{
//						if(gridList[i].GetTile() != null && gridList[i].GetTile().tileData.tileType == TileData.TileType.match_object)
//						{
//							noMoveMore =  true;Debug.Log("iii: " + i);Debug.Log(gridList[i].GetTile().tileData.tileType.ToString());
//							break;
//						}
//					}
//
					//Debug.Log("Contance.NUMBER_LEVEL: " + Contance.NUMBER_LEVEL);
					if(Contance.NUMBER_LEVEL != 4)
					{
						NoMoreMove();
					}
					else
					{
						if(countFFF != 4)
						{
							NoMoreMove();
							//this.isProcessing = true;
						}
					}
	            }
			// Clear processing list
			processingGrids.Clear();
            // Reset yogurt flag
            Yogurt.convertedThisTurn = false;
        }
    }

    /// <summary>
    /// Drop tiles
    /// </summary>
    public float Drop()
    {

        float result = tileSpawner.ShrinkTiles(Grid.Direction.BOTTOM);
        float t = tileSpawner.SpawnTiles(Grid.Direction.BOTTOM);

        result = result > t ? result : t;
        return result;
    }

    /// <summary>
    /// Spawn tile
    /// </summary>
    /// <returns></returns>
    public float Spawn()
    {
        float result = tileSpawner.SpawnTiles(Grid.Direction.BOTTOM);

        return result;
    }

    /// <summary>
    /// Drop tiles diagonal
    /// </summary>
    public float DropDiagonal()
    {
        //Debug.Log("MapController : DropDiagonal");

        float result = tileSpawner.ShrinkTiles(Grid.Direction.BOTTOM, true);

        //float t = 0;
        //t = tileSpawner.ShrinkTiles(Grid.Direction.BOTTOM);

        //result = result > t ? result : t;

        //t = tileSpawner.SpawnTiles(Grid.Direction.BOTTOM);

       // result = result > t ? result : t;
        return result;
    }

    /// <summary>
    /// Run to check cascade
    /// </summary>
    public bool Cascade()
    {
        //Debug.Log("MapController : Cascade");

        bool result = false;

        result |= gameInputProcessor.ProcessInput(gridList, GameEnum.ProcessType.CASCADE);

        //Debug.Log("MapController : Cascade : result : " + result);

        // Add a cascading tile to force process
        for (int count = 0; count < 1; count++)
        {
            Tile choosenTile = null;
            for (int i = 0; i < tileManager.cascadingTiles.Count; i++)
            {
                if (tileManager.cascadingTiles[i].isMoving)
                {
                    continue;
                }

                if (tileManager.cascadingTiles[i].isOnClearingProcess)
                {
                    tileManager.cascadingTiles.RemoveAt(i);
                    i--;
                    continue;
                }

                choosenTile = tileManager.cascadingTiles[i];

                if (choosenTile != null)
                {
                    tileManager.cascadingTiles.Remove(choosenTile);
                    ExecuteTile(choosenTile, choosenTile.grid);
                    result = true;
                }

                //break;
            }
        }

        //if (choosenTile != null)
        //{
        //    tileManager.cascadingTiles.Remove(choosenTile);
        //    ExecuteTile(choosenTile, choosenTile.grid);
        //    result = true;
        //}


        return result; // Cascade cause processing list size increase => trigger execution 
    }

    /// <summary>
    /// No more move
    /// </summary>
    public void NoMoreMove()
    {
        //List<Grid> swappableList = new List<Grid>();
        //for (int i = 0; i < gridList.Count; i++)
        //{
        //    if (gridList[i] != null && gridList[i].gridData.gridType != GridData.GridType.NULL_GRID)
        //    {
        //        Tile tile = gridList[i].GetTile();
        //        if (tile != null && tile.isHit && tile.isSwappable && tile.isDroppable && !tile.isSpecialActivable)
        //        {
        //            swappableList.Add(gridList[i]);
        //        }
        //    }
        //}

        //tileSpawner.ResetBoard(swappableList);

        if (noMoreMoveBottle.animating)
        {
            return;
        }

        GamePlayController.gamePlayController.gameInterceptorCount++;

        float bottleTime = noMoreMoveBottle.ShowBottle(0f);
        bottleTime += noMoreMoveBottle.OpenCap(bottleTime);

        LeanTween.delayedCall(bottleTime, () =>
        {
            // Find elegible grids
            List<Grid> swappableGrids = new List<Grid>();
            List<int> idList = tileSpawner.indexList;
            // Create swappable list

            for (int i = 0; i < gridList.Count; i++)
            {
                if (gridList[i] != null && gridList[i].gridData.gridType != GridData.GridType.NULL_GRID)
                {
                    Tile tile = gridList[i].GetTile();
                    if (tile != null && tile.isHit && tile.isSwappable && tile.isDroppable && !tile.isSpecialActivable)
                    {
                        swappableGrids.Add(gridList[i]);
                    }
                }
            }

            //Vector3 centerOfGridMap = new Vector3(this.transform.position.x, this.transform.position.y, 0f);

            float maxTime = 0;
            float delayTime = 0;
            // Move to bottle
            for (int i = 0; i < swappableGrids.Count; i++)
            {
                Grid g = swappableGrids[i];
                Tile t = g.GetTile();

                delayTime += 0.005f;
                float moveTime = noMoreMoveBottle.PutTileInBottle(t, delayTime);

                // Find necessary time to make tile move to bottle
                if (moveTime > maxTime)
                {
                    maxTime = moveTime;
                }
            }

            // Randomize
            tileSpawner.ResetBoard(swappableGrids);

            //Move tile back to there grids 
            maxTime += noMoreMoveBottle.CloseCap(maxTime);
            maxTime += noMoreMoveBottle.ActiveSwapping(maxTime);
            maxTime += noMoreMoveBottle.OpenCap(maxTime);
            maxTime += noMoreMoveBottle.ReturnAllTilesToGrids(maxTime);
            maxTime += noMoreMoveBottle.CloseCap(maxTime);
            maxTime += noMoreMoveBottle.HideBottle(maxTime);

            // Activate, need to do some wait here
            LeanTween.delayedCall(maxTime + GameConstants.SPEED_SWAP_TILE, () =>
            {
                GamePlayController.gamePlayController.SetExecutionDelay(GameConstants.TIME_DELAY_AFTER_REROLLMAP);
                
                //StartProcessingPhase();
                if (GamePlayController.gamePlayController.gameInterceptorCount > 0)
                {
                    GamePlayController.gamePlayController.gameInterceptorCount--;
                }

                GamePlayController.gamePlayController.cascadeFlag = true;
            });
        });
    }
    #endregion
}

﻿/*
 * @Author: CuongNH
 * @Description: 
 * */

using UnityEngine;
using System.Collections;

public class DemoGamePlay : MonoBehaviour {

    /// <summary>
    /// List sprite for Tile
    /// </summary>
    public Sprite[] mainRenderTileList;
    public Sprite[] enhanceRenderTileList;

    /// <summary>
    /// List sprite for Grid
    /// </summary>
    public Sprite[] mainRenderGridList;
    public Sprite[] staticRenderGridList;
    public Sprite[] dynamicRenderTileList;

    /// <summary>
    /// Tile and Grid
    /// </summary>
    public Tile tile;
    public Grid grid;

	// Use this for initialization
	void Start () {
        // Set render for tile
        SetRenderTile();

        // Set render for grid
        SetRenderGrid();
	}
	
    //// Update is called once per frame
    //void Update () {
	
    //}

    /// <summary>
    /// Set renders for tile
    /// </summary>
    public void SetRenderTile()
    {
        VGDebug.LogDebug("SetRenderTile");

        int rand = Random.Range(0, mainRenderTileList.Length);
        tile.SetMainRender(mainRenderTileList[rand]);

        int rande = Random.Range(0, enhanceRenderTileList.Length);
        tile.SetEnhanceRender(enhanceRenderTileList[rande]);

        Invoke("SetRenderTile", 2f);
    }

    /// <summary>
    /// Set renders for grid
    /// </summary>
    public void SetRenderGrid()
    {
        VGDebug.LogDebug("SetRenderGrid");

        int rand = Random.Range(0, mainRenderGridList.Length);
        grid.SetMainRender(mainRenderGridList[rand]);

        int srand = Random.Range(0, staticRenderGridList.Length);
        grid.SetStaticRender(staticRenderGridList[srand]);

        int drand = Random.Range(0, dynamicRenderTileList.Length);
        grid.SetDynamicRender(dynamicRenderTileList[drand]);

        grid.gridPosition = new GridPosition();
        grid.gridPosition.Row = srand;
        grid.gridPosition.Column = drand;

        VGDebug.LogDebug("grid.postion.row: " + grid.gridPosition.Row);
        VGDebug.LogDebug("grid.postion.column: " + grid.gridPosition.Column);

        tile.transform.SetParent(grid.transform);
        tile.transform.localPosition = new Vector3();

        Invoke("SetRenderGrid", 3f);
    }
}

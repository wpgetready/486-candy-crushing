﻿/*
 * @Author: CuongNH
 * @Description: Su dung de tao cac Grid
 * */

using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class GridSpawner : MonoBehaviour
{

    #region Properties, Data, Components
    /// <summary>
    /// Grid Prefab de clone grid moi
    /// </summary>
    public Grid gridPrefab;

    /// <summary>
    /// Conveyor Prefab to clone Conveyor moi
    /// </summary>
    public GameObject conveyorPrefab;

    /// <summary>
    /// Portal Prefab to clone Portal
    /// </summary>
    public GameObject portalPrefab;

    /// <summary>
    /// Cac sprite hien thi cell background
    /// </summary>
    public Sprite[] mainSpriteGrid;

    /// <summary>
    /// Cac sprite hien thi cac thanh phan tinh cua grid: bang chuyen, portal
    /// </summary>
    public Sprite[] staticSpriteGrid;

	public Sprite[] colorSpriteGrid;

    /// <summary>
    /// Cac sprite hien thi cac thanh phan dong cua grid: ice, honey, cracker
    /// </summary>
    public Sprite[] dynamicSpriteGrid;

    /// <summary>
    /// Dinh nghia cac index de lay cac Static Sprite
    /// </summary>
    public enum StaticSpriteIndex
    {
        CONVEYOR_CORNER_INDEX = 0,
        CONVEYOR_LINE_INDEX = 1,
        PORTAL_INLET_INDEX = 2,
        PORTAL_OUTLET_INDEX = 3
    }

	public enum ColorSpriteIndex
	{
		NONE_GRID_INDEX = 0,
		RED_GRID_INDEX = 1,
		GREEN_GRID_INDEX = 2,
		BLUE_GRID_INDEX = 3,
		ORANGE_GRID_INDEX = 4,
		YELLOW_GRID_INDEX = 5,
		PURPLE_GRID_INDEX = 6
	}

    /// <summary>
    /// Dinh nghia cac index de lay cac Dynamic Sprite
    /// </summary>
    public enum DynamicSpriteIndex
    {
        CRACKER_1X = 0,
        CRACKER_2X = 1,
        CRACKER_3X = 2,
        HONEY = 3,
        ICE_1X = 4,
        ICE_2X = 5,
        ICE_3X = 6
    }
    #endregion

    //// Use this for initialization
    //void Start () {
	
    //}
	
    //// Update is called once per frame
    //void Update () {

    //}

    #region Methos to Create, Set Data, Properties for Grid
    /// <summary>
    /// Tao Grid moi
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="position"></param>
    /// <param name="mainSpriteGridIndex"></param>
    public Grid CreateNewGrid(Transform parent, Vector3 position, int mainSpriteGridIndex = 0)
    {
        try
        {
            GameObject gridObject = (GameObject)Instantiate(gridPrefab.gameObject, position, Quaternion.identity);

            gridObject.transform.SetParent(parent);
            gridObject.transform.localScale = Vector3.one;
            gridObject.transform.localPosition = position;

            Grid grid = gridObject.GetComponent<Grid>();
            grid.SetMainRender(mainSpriteGrid[mainSpriteGridIndex]);

            return grid;
        }
        catch (System.Exception e)
        {
            VGDebug.LogError(e.Message);
        }

        return null;
    }

    /// <summary>
    /// Set properties for grid
    /// </summary>
    /// <param name="grid"></param>
    /// <param name="gridData"></param>
    public void SetGridProperties(Grid grid, GridData gridData)
    {
        grid.SetGridData(gridData);

        SetGridDynamicRender(grid);
    }

    /// <summary>
    /// Set render for DynamicRender of Grid
    /// </summary>
    /// <param name="grid"></param>
    public void SetGridDynamicRender(Grid grid)
    {
        switch (grid.gridData.gridDynamicType)
        {
            case GridData.GridDynamicType.ICE_GRID:
            {
                switch (grid.gridData.gridDynamicData)
                {
                    case GameConstants.GRID_DYNAMIC_DATA_3X:
                    {
                        grid.SetDynamicRender(dynamicSpriteGrid[(int)DynamicSpriteIndex.ICE_3X]);
                        break;
                    }
                    case GameConstants.GRID_DYNAMIC_DATA_2X:
                    {
                        grid.SetDynamicRender(dynamicSpriteGrid[(int)DynamicSpriteIndex.ICE_2X]);
                        break;
                    }
                    case GameConstants.GRID_DYNAMIC_DATA_1X:
                    {
                        grid.SetDynamicRender(dynamicSpriteGrid[(int)DynamicSpriteIndex.ICE_1X]);
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
                break;
            }
            case GridData.GridDynamicType.HONEY_GRID:
            {
                grid.SetDynamicRender(dynamicSpriteGrid[(int)DynamicSpriteIndex.HONEY]);
                break;
            }
            case GridData.GridDynamicType.CRACKER_GRID:
            {
                switch (grid.gridData.gridDynamicData)
                {
                    case GameConstants.GRID_DYNAMIC_DATA_3X:
                    {
                        grid.SetDynamicRender(dynamicSpriteGrid[(int)DynamicSpriteIndex.CRACKER_3X]);
                        break;
                    }
                    case GameConstants.GRID_DYNAMIC_DATA_2X:
                    {
                        grid.SetDynamicRender(dynamicSpriteGrid[(int)DynamicSpriteIndex.CRACKER_2X]);
                        break;
                    }
                    case GameConstants.GRID_DYNAMIC_DATA_1X:
                    {
                        grid.SetDynamicRender(dynamicSpriteGrid[(int)DynamicSpriteIndex.CRACKER_1X]);
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
                break;
            }
            default:
                break;
        }
    }

    /// <summary>
    /// Set render for StaticRender of Grid
    /// </summary>
    /// <param name="grid"></param>
    public void SetGridStaticRender(Grid grid)
    {
		switch (grid.gridData.gridColorType)
		{
		case GridData.GridColorType.RED_GRID:
			{
				grid.SetStaticRender (colorSpriteGrid[(int)ColorSpriteIndex.RED_GRID_INDEX], true);
				break;
			}
		case GridData.GridColorType.GREEN_GRID:
			{
				grid.SetStaticRender (colorSpriteGrid[(int)ColorSpriteIndex.GREEN_GRID_INDEX], true);
				break;
			}
		case GridData.GridColorType.BLUE_GRID:
			{
				grid.SetStaticRender (colorSpriteGrid[(int)ColorSpriteIndex.BLUE_GRID_INDEX], true);
				break;
			}
		case GridData.GridColorType.ORANGE_GRID:
			{
				grid.SetStaticRender (colorSpriteGrid[(int)ColorSpriteIndex.ORANGE_GRID_INDEX], true);
				break;
			}
		case GridData.GridColorType.YELLOW_GRID:
			{
				grid.SetStaticRender (colorSpriteGrid[(int)ColorSpriteIndex.YELLOW_GRID_INDEX], true);
				break;
			}
		case GridData.GridColorType.PURPLE_GRID:
			{
				grid.SetStaticRender (colorSpriteGrid[(int)ColorSpriteIndex.PURPLE_GRID_INDEX], true);
				break;
			}
		default:
			break;
		}
    }

    /// <summary>
    /// Set connected for grids
    /// </summary>
    /// <param name="grid"></param>
    /// <param name="gridArray"></param>
    /// <param name="width"></param>
    /// <param name="height"></param>
    /// <param name="px"></param>
    /// <param name="py"></param>
    public void CreateGridConnectedList(Grid grid, Grid[,] gridArray, int width, int height, int px, int py)
    {
        gridArray[px, py] = grid;
        grid.connectedList = new List<Grid>();
        for (int d = 0; d < Grid.DIR_NUM; d++)
        {
            grid.connectedList.Add(null);
        }
        ConnectGrid(grid, gridArray, width, height, px, py);
    }

    /// <summary>
    /// Create link connect for grid
    /// </summary>
    /// <param name="grid"></param>
    /// <param name="gridArray"></param>
    /// <param name="colNumber"></param>
    /// <param name="rowNumber"></param>
    /// <param name="px"></param>
    /// <param name="py"></param>
    void ConnectGrid(Grid grid, Grid[,] gridArray, int colNumber, int rowNumber, int px, int py)
    {
        for (int i = 0; i < Grid.DIR_NUM; i++)
        {
            int posX = -1, posY = -1;
            this.GetDirectionInDir(px, py, i, out posX, out posY);

            Grid foundGrid = GetGridInArray(gridArray, colNumber, rowNumber, posX, posY);
            if (foundGrid != null && i != 0) // skip WORLD CENTER
            {
                grid.connectedList[i] = foundGrid; // set link
                foundGrid.connectedList[Grid.DIR_NUM - i] = grid; // set bounce link
            }
        }
    }

    /// <summary>
    /// Get grid in array with px, py
    /// </summary>
    /// <param name="gridArray"></param>
    /// <param name="colNumber"></param>
    /// <param name="rowNumber"></param>
    /// <param name="px"></param>
    /// <param name="py"></param>
    /// <returns></returns>
    Grid GetGridInArray(Grid[,] gridArray, int colNumber, int rowNumber, int px, int py)
    {

        if (px >= 0 && px < colNumber && py >= 0 && py < rowNumber)
        {
            Grid result = gridArray[px, py];

            return result;
        }

        return null;
    }

    /// <summary>
    /// Get direction 
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="dir"></param>
    /// <param name="outx"></param>
    /// <param name="outy"></param>
    void GetDirectionInDir(int x, int y, int dir, out int outx, out int outy)
    {
        // change x direction
        if (dir == (int)Grid.Direction.BOTTOM_LEFT || dir == (int)Grid.Direction.LEFT
            || dir == (int)Grid.Direction.TOP_LEFT) // left
        {
            outx = x - 1;
        }
        else if (dir == (int)Grid.Direction.BOTTOM_RIGHT || dir == (int)Grid.Direction.RIGHT
            || dir == (int)Grid.Direction.TOP_RIGHT) // right
        {
            outx = x + 1;
        }
        else
        {
            outx = x;
        }

        // change y direction
        if (dir == (int)Grid.Direction.TOP_LEFT || dir == (int)Grid.Direction.TOP
            || dir == (int)Grid.Direction.TOP_RIGHT) // up
        {
            outy = y + 1;
        }
        else if (dir == (int)Grid.Direction.BOTTOM_LEFT || dir == (int)Grid.Direction.BOTTOM
            || dir == (int)Grid.Direction.BOTTOM_RIGHT) // down
        {
            outy = y - 1;
        }
        else
        {
            outy = y;
        }
    }


    /// <summary>
    /// Create new Conveyor
    /// </summary>
    /// <param name="grid"></param>
    /// <param name="pConveyor"></param>
    /// <returns></returns>
    public Conveyor CreateNewConveyor(Grid grid, P_Conveyor pConveyor)
    {
        if (grid != null && !string.IsNullOrEmpty(pConveyor.type_Conveyor) 
            && !string.IsNullOrEmpty(pConveyor.data_Conveyor))
        {
            GameObject conveyorObj = (GameObject)Instantiate(conveyorPrefab, Vector3.zero, Quaternion.identity);
            if (conveyorObj != null)
            {
                Conveyor conveyor = conveyorObj.GetComponent<Conveyor>();
                if (conveyor != null)
                {
                    switch (pConveyor.data_Conveyor.Trim())
                    {
                        case GameConstants.up_conveyor_direction:
                            {
                                conveyor.SetConveyorDirection(Conveyor.ConveyorDirection.UP);
                                break;
                            }
                        case GameConstants.down_conveyor_direction:
                            {
                                conveyor.SetConveyorDirection(Conveyor.ConveyorDirection.DOWN);
                                break;
                            }
                        case GameConstants.left_conveyor_direction:
                            {
                                conveyor.SetConveyorDirection(Conveyor.ConveyorDirection.LEFT);
                                break;
                            }
                        case GameConstants.right_conveyor_direction:
                            {
                                conveyor.SetConveyorDirection(Conveyor.ConveyorDirection.RIGHT);
                                break;
                            }
                        default:
                            break;
                    }

                    grid.SetConveyor(conveyor);

                    return conveyor;
                }
            }
        }

        return null;
    }

    /// <summary>
    /// Set properties of conveyors
    /// </summary>
    /// <param name="conveyorList"></param>
    public void SetConveyorsProperties(List<Conveyor> conveyorList)
    {
        if (conveyorList != null && conveyorList.Count > 0)
        {
            for (int i = 0; i < conveyorList.Count; i++)
            {
                try
                {
                    Conveyor conveyor = conveyorList[i];
                    Grid grid = conveyor.transform.parent.GetComponent<Grid>();

                    if (grid != null)
                    {
                        Conveyor.ConveyorType conveyorType = grid.GetConveyorType();
                        if (conveyorType == Conveyor.ConveyorType.NONE)
                        {

                        }
                        else if (conveyorType == Conveyor.ConveyorType.LINE)
                        {
                            conveyor.SetProperties(grid.GetConveyorType(), conveyor.conveyorDirection, 
                                staticSpriteGrid[(int)StaticSpriteIndex.CONVEYOR_LINE_INDEX]);
                        }
                        else
                        {
                            conveyor.SetProperties(grid.GetConveyorType(), conveyor.conveyorDirection,
                                staticSpriteGrid[(int)StaticSpriteIndex.CONVEYOR_CORNER_INDEX]);
                        }
                    }
                }
                catch (System.Exception e)
                {
                    Debug.Log(e.Message);
                }
            }
        }
    }

    /// <summary>
    /// Create new Portal
    /// </summary>
    /// <param name="grid"></param>
    /// <param name="pPortal"></param>
    public Portal CreateNewPortal(Grid grid, P_Portal pPortal)
    {
        if (grid != null && !string.IsNullOrEmpty(pPortal.type))
        {
            GameObject portalObj = (GameObject)Instantiate(portalPrefab, Vector3.zero, Quaternion.identity);
            if (portalObj != null)
            {
                Portal portal = portalObj.GetComponent<Portal>();
                if (portal != null)
                {
                    switch (pPortal.type.Trim())
                    {
                        case GameConstants.portal_inlet:
                            {
                                portal.SetProperties(Portal.PortalType.INTLET, pPortal.data, 
                                    staticSpriteGrid[(int)StaticSpriteIndex.PORTAL_INLET_INDEX]);
                                break;
                            }
                        case GameConstants.portal_outlet:
                            {
                                portal.SetProperties(Portal.PortalType.OUTLET, pPortal.data,
                                    staticSpriteGrid[(int)StaticSpriteIndex.PORTAL_OUTLET_INDEX]);
                                break;
                            }
                        default:
                            break;
                    }

                    grid.SetPortal(portal);
                    portal.SetGrid(grid);

                    return portal;
                }
            }
        }

        return null;
    }
    #endregion
}

﻿/*
 * @Author: CuongNH
 * @Description: Handle combo breaker
 * */

using UnityEngine;
using System.Collections;
using Spine.Unity;

public class ComboController : MonoBehaviour {

    //// Use this for initialization
    //void Start () {
	
    //}
	
    //// Update is called once per frame
    //void Update () {
	
    //}

    private const string animationName = "animation";
    private const float breakerAniamationTime = 0.5f;
    private const float columnBreakerAniamationTime = 0.7f;
    private const float rowBreakerlAniamationTime = 1f;

    /// <summary>
    /// Call when active booster combo
    /// </summary>
    /// <param name="boosterType"></param>
    /// <param name="grid"></param>
    /// <param name="time"></param>
    public static bool ActiveCombo(Booster.BoosterType boosterType, Grid grid, SkeletonAnimation skeleton, ref float time)
    {
        switch (boosterType)
        {
            case Booster.BoosterType.BREAKER:
                {
                    return ActiveBreakerCombo(grid, skeleton, ref time);
                }
            case Booster.BoosterType.ROW_BREAKER:
                {
                    return ActiveRowBreakerCombo(grid, skeleton, ref time);
                }
            case Booster.BoosterType.COLUMN_BREAKER:
                {
                    return ActiveColumnBreakerCombo(grid, skeleton, ref time);
                }
            default:
                break;
        }

        return false;
    }

    /// <summary>
    /// Active breaker combo
    /// </summary>
    /// <param name="grid"></param>
    /// <param name="time"></param>
    /// <returns></returns>
    private static bool ActiveBreakerCombo(Grid grid, SkeletonAnimation skeleton, ref float time)
    {
        if (grid == null || grid.gridData.gridType == GridData.GridType.NULL_GRID 
            || grid.GetTile() == null || grid.GetTile().isMoving || grid.GetTile().isOnClearingProcess)
        {
            return false;
        }

        float t = 0f;
        
        if (skeleton != null)
        {
            t = breakerAniamationTime;

            skeleton.AnimationName = animationName;
			//skeleton.Reset();
			skeleton.Awake();
        }

        LeanTween.delayedCall(t / 2f, () =>
            {
                GamePlayController.gamePlayController.mapController.ExecuteTile(grid.GetTile(), grid, false, false);

                if (skeleton != null)
                {
                    LeanTween.delayedCall(t, () =>
                        {
                            skeleton.AnimationName = "";
                            //skeleton.Reset();
							skeleton.Awake();
                        });
                }
            });

        t += GameConstants.TIME_CLEAR_TILE;
        time = t;

        return true;
    }

    /// <summary>
    /// Active row breaker combo
    /// </summary>
    /// <param name="grid"></param>
    /// <param name="time"></param>
    /// <returns></returns>
    private static bool ActiveRowBreakerCombo(Grid grid, SkeletonAnimation skeleton, ref float time)
    {
        if (grid == null || grid.gridData.gridType == GridData.GridType.NULL_GRID
            || grid.GetTile() == null || grid.GetTile().isMoving || grid.GetTile().isOnClearingProcess)
        {
            return false;
        }

        float t = 0f;

        if (skeleton != null)
        {
            t = rowBreakerlAniamationTime;
			float pos_X = 2.55f;
//			if(Contance.width == 7)
//			{
//				pos_X = 1.35f;
//			}
//			if(Contance.width == 8)
//			{
//				pos_X = 1.55f;
//			}
//			if(Contance.width == 9)
//			{
//				pos_X = 1.75f;
//			}
			iTween.MoveTo(skeleton.transform.parent.gameObject, iTween.Hash("position", new Vector3(pos_X,skeleton.transform.parent.gameObject.transform.position.y,0), "time", t-0.1f, "easetype", iTween.EaseType.easeInBack));
            skeleton.AnimationName = animationName;
            //skeleton.Reset();
			skeleton.Awake();
        }

        LeanTween.delayedCall(t / 2f, () =>
            {
                //grid.GetTile().tileData.tileType = TileData.TileType.row_breaker;
                GamePlayController.gamePlayController.mapController.ExecuteTile(grid.GetTile(), grid, false, false);
                RowBreaker.Activate(grid, true);

                if (skeleton != null)
                {
                    LeanTween.delayedCall(t, () =>
                    {
                        skeleton.AnimationName = "";
                        //skeleton.Reset();
							skeleton.Awake();
                    });
                }
            });

        t += GameConstants.TIME_CLEAR_TILE;
        time = t;

        return true;
    }

    /// <summary>
    /// Active column breaker combo
    /// </summary>
    /// <param name="grid"></param>
    /// <param name="time"></param>
    /// <returns></returns>
    private static bool ActiveColumnBreakerCombo(Grid grid, SkeletonAnimation skeleton, ref float time)
    {
        if (grid == null || grid.gridData.gridType == GridData.GridType.NULL_GRID
            || grid.GetTile() == null || grid.GetTile().isMoving || grid.GetTile().isOnClearingProcess)
        {
            return false;
        }

        float t = 0f;

        if (skeleton != null)
        {
            t = columnBreakerAniamationTime;

            skeleton.AnimationName = animationName;
            //skeleton.Reset();
			skeleton.Awake();
        }

        LeanTween.delayedCall(t / 2f, () =>
            {
                //grid.GetTile().tileData.tileType = TileData.TileType.column_breaker;
                GamePlayController.gamePlayController.mapController.ExecuteTile(grid.GetTile(), grid, false, false);
                ColumnBreaker.Activate(grid, true);

                if (skeleton != null)
                {
                    LeanTween.delayedCall(t, () =>
                    {
                        skeleton.AnimationName = "";
                        //skeleton.Reset();
							skeleton.Awake();
                    });
                }
            });

        t += GameConstants.TIME_CLEAR_TILE;
        time = t;

        return true;
    }
}

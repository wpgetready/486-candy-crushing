﻿/*
 * @Author: CuongNH
 * @Description: Xu ly cac su kien dieu khien bang chuyen
 * */

using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class ConveyorBeltController : MonoBehaviour {

    /// <summary>
    /// Danh sach cac o bang chuyen
    /// </summary>
    public List<Conveyor> conveyorList;

    /// <summary>
    /// Danh sach cac cau truc o bang chuyen
    /// </summary>
    [SerializeField]
    public List<ConveyorBelt> conveyorBeltList;

    /// <summary>
    /// Xac dinh man choi co bang chuyen khong
    /// </summary>
    public bool hasConveyor = false;

    /// <summary>
    /// Grid tam de luu grid truoc khi convey
    /// </summary>
    private Grid templateGrid = null;

    /// <summary>
    /// New List of Tile after convey
    /// </summary>
    private List<Tile> newTileList;

    /// <summary>
    /// New List of GridDynamicType after convey
    /// </summary>
    private List<GridData.GridDynamicType> newGridDynamicTypeList;

    /// <summary>
    /// New list of data of dynamic type
    /// </summary>
    private List<int> newDynamicDataList;

    /// <summary>
    /// New List of DynamicTransform after convey
    /// </summary>
    private List<Transform> newDynamicTransformList;

	private List<Transform> newStaticTransformList;

    //// Use this for initialization
    //void Start () {
	
    //}
	
    //// Update is called once per frame
    //void Update () {
	
    //}

    /// <summary>
    /// Init cache data
    /// </summary>
    /// <param name="numberCache"></param>
    public void InitCacheData(int numberCache)
    {
        newTileList = new List<Tile>(numberCache);
        newGridDynamicTypeList = new List<GridData.GridDynamicType>(numberCache);
        newDynamicDataList = new List<int>(numberCache);
        newDynamicTransformList = new List<Transform>(numberCache);
		newStaticTransformList = new List<Transform> (numberCache);
    }

    /// <summary>
    /// Handle convey for conveyor and belt
    /// </summary>
	public bool HandleConvey(ref float time)
    {
        //float time = 0f;

        if (!hasConveyor)
        {
            time = 0f;
            return false;
        }

        GamePlayController.gamePlayController.gameInterceptorCount++;

        newTileList.Clear();
        newGridDynamicTypeList.Clear();
        newDynamicTransformList.Clear();
		newStaticTransformList.Clear ();
        newDynamicDataList.Clear();

        #region Get sources before convey
        Conveyor conveyor = null;
        Conveyor toConveyor = null;
        Grid grid = null;
        //Grid sourceGrid = null;
        Tile tile = null;
        bool hasBelt = false;
        for (int i = 0; i < conveyorList.Count; i++)
        {
            conveyor = conveyorList[i];
            //hasBelt = CheckConveyorInBelt(conveyor, out toConveyor);
            if (conveyor != null)
            {
                grid = conveyorList[i].grid;
                if (grid != null && grid.gridData.gridType != GridData.GridType.NULL_GRID)
                {
                    tile = grid.GetTile();
                    //if (tile != null)
                    //{
                        newTileList.Add(tile);
                    //}

                    newGridDynamicTypeList.Add(grid.gridData.gridDynamicType);
                    newDynamicDataList.Add(grid.gridData.gridDynamicData);
                    newDynamicTransformList.Add(grid.dynamicComponents);
					newStaticTransformList.Add(grid.staticComponents);
                }
            }
        }
        #endregion

        #region Handle Convey
        Grid targetGrid = null;
        int j = 0;

		GamePlayController.gamePlayController.mapController.mapBuilding.listColorGrid.Clear();

        for (int i = 0; i < conveyorList.Count; i++)
        {
            conveyor = conveyorList[i];
            hasBelt = CheckConveyorInBelt(conveyor, out toConveyor);
            if (conveyor != null)
            {
                if (hasBelt)
                {
                    targetGrid = toConveyor.grid;
                }
                else
                {
                    targetGrid = conveyor.GetDestGrid();
                }

                if (targetGrid != null && targetGrid.gridData.gridType != GridData.GridType.NULL_GRID)
                {
                    targetGrid.gridData.gridDynamicType = newGridDynamicTypeList[j];
                    targetGrid.gridData.gridDynamicData = newDynamicDataList[j];
                    targetGrid.SetDynamicComponent(newDynamicTransformList[j]);
					targetGrid.SetStaticComponent(newStaticTransformList[j]);
                    targetGrid.SetTile(newTileList[j]);
                    j++;

                    if (hasBelt)
                    {
                        Vector3 destOfFrom = conveyor.GetDestPosition();
                        Vector3 destFromMove = destOfFrom - new Vector3(targetGrid.gridPosition.Column,
                            targetGrid.gridPosition.Row, 0f);

                        Vector3 sourceOfTo = toConveyor.GetSourcePosition();
                        Vector3 sourceToMove = sourceOfTo - new Vector3(targetGrid.gridPosition.Column, 
                            targetGrid.gridPosition.Row, 0f);

                        targetGrid.MoveDynamicComponentByBelt(destFromMove, sourceToMove, 
                            GameConstants.SPEED_DROP_TILE_EACH_STEP / 2f, GameConstants.SPEED_DROP_TILE_EACH_STEP / 2f);
						
						targetGrid.MoveStaticComponentByBelt(destFromMove, sourceToMove, 
							GameConstants.SPEED_DROP_TILE_EACH_STEP / 2f, GameConstants.SPEED_DROP_TILE_EACH_STEP / 2f);
                    }
                    else
                    {
                        targetGrid.MoveDynamicComponentToCenter(GameConstants.SPEED_DROP_TILE_EACH_STEP);
						targetGrid.MoveStaticComponentToCenter(GameConstants.SPEED_DROP_TILE_EACH_STEP);
                    }

//					if(targetGrid.hasPopsicle == true)
//					{
//						targetGrid.MovePopsicleToCenter(GameConstants.SPEED_DROP_TILE_EACH_STEP,toConveyor);
//					}

//					if(targetGrid.hasPopsicle == true)
//					{
//						Vector3 destOfFrom = conveyor.GetDestPosition();
//						Vector3 destFromMove = destOfFrom - new Vector3(targetGrid.gridPosition.Column,
//							targetGrid.gridPosition.Row, 0f);
//
//						Vector3 sourceOfTo = toConveyor.GetSourcePosition();
//						Vector3 sourceToMove = sourceOfTo - new Vector3(targetGrid.gridPosition.Column, 
//							targetGrid.gridPosition.Row, 0f);
//						targetGrid.MovePopByBelt(destFromMove, sourceToMove, 
//							GameConstants.SPEED_DROP_TILE_EACH_STEP / 2f, GameConstants.SPEED_DROP_TILE_EACH_STEP / 2f);
//					}

					if(targetGrid.staticRender.enabled == true)
					{
						if(targetGrid.staticRender.sprite.name == "_red")
						{
							targetGrid.gridData.gridColorType = GridData.GridColorType.RED_GRID;
						}

						if(targetGrid.staticRender.sprite.name == "_green")
						{
							targetGrid.gridData.gridColorType = GridData.GridColorType.GREEN_GRID;
						}

						if(targetGrid.staticRender.sprite.name == "_blue")
						{
							targetGrid.gridData.gridColorType = GridData.GridColorType.BLUE_GRID;
						}

						if(targetGrid.staticRender.sprite.name == "_orange")
						{
							targetGrid.gridData.gridColorType = GridData.GridColorType.ORANGE_GRID;
						}

						if(targetGrid.staticRender.sprite.name == "_yellow")
						{
							//Debug.Log("333");
							targetGrid.gridData.gridColorType = GridData.GridColorType.YELLOW_GRID;
						}

						if(targetGrid.staticRender.sprite.name == "_purple")
						{
							targetGrid.gridData.gridColorType = GridData.GridColorType.PURPLE_GRID;
						}

						GamePlayController.gamePlayController.mapController.mapBuilding.listColorGrid.Add(targetGrid);

					}
					else
					{
						targetGrid.gridData.gridColorType = GridData.GridColorType.NONE;
					}
                }
            }
        }
        #endregion

        LeanTween.delayedCall(GameConstants.SPEED_DROP_TILE_EACH_STEP, () =>
            { 
                if (GamePlayController.gamePlayController.gameInterceptorCount > 0)
                {
                    GamePlayController.gamePlayController.gameInterceptorCount--;
                }
            });

        time = GameConstants.SPEED_DROP_TILE_EACH_STEP;
        return true;
    }

    /// <summary>
    /// Check conveyor co trong Conveyor Belt khong
    /// </summary>
    /// <param name="conveyor"></param>
    /// <returns></returns>
    private bool CheckConveyorInBelt(Conveyor conveyor, out Conveyor toConveyor)
    {
        if (conveyor == null || conveyorBeltList == null || conveyorBeltList.Count < 1)
        {
            toConveyor = null;
            return false;
        }

        for (int i = 0; i < conveyorBeltList.Count; i++)
        {
            if (conveyorBeltList[i].fromConveyor == conveyor)
            {
                toConveyor = conveyorBeltList[i].toConveyor;
                return true;
            }

            //if (conveyorBeltList[i].toConveyor == conveyor)
            //{
            //    return true;
            //}
        }

        toConveyor = null;
        return false;
    }
}

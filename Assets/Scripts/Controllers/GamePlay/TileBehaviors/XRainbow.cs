﻿/*
 * @Author: CuongNH
 * @Description: Handle behavior for x_rainbow type
 * */

using UnityEngine;
using System.Collections;

public class XRainbow {

    public static void OnCreated(Tile tile)
    {

    }

    /// <summary>
    /// Do transform and calculate time needed
    /// </summary>
    public static float TransformProcess(Tile tile)
    {
        return GameConstants.TIME_TRANSFORM_PROCESS;
    }

    public static void OnUpgrade(Tile tile)
    {
        //Activate(tile.grid);
    }

    public static void OnTurnEnded(Tile tile)
    {

    }

    /// <summary>
    /// Run destroying animation and return time it take to
    /// </summary>
    public static float OnClear(Tile tile)
    {
		tile.ExtraCount ();
        float timeNeeded = GameConstants.TIME_CLEAR_TILE;

        LeanTween.scale(tile.gameObject, Vector3.zero, timeNeeded);

        return timeNeeded;
    }

    /// <summary>
    /// Update graphic for tile
    /// </summary>
    /// <param name="tile"></param>
    public static void UpdateGraphic(Tile tile)
    {
        //tile.SetTileActive(true);
        tile.gameObject.transform.localScale = Vector3.one;
		GamePlayController.gamePlayController.mapController.tileSpawner.SetTileMainRender(tile);
        tile.SetEnableSpriteRender(tile.enhanceRender, false);
        //GamePlayController.gamePlayController.mapController.tileSpawner.SetTileEnhanceRender(tile);
    }

    /// <summary>
    /// Call when active
    /// </summary>
    /// <param name="grid"></param>
    public static void Activate(Grid grid)
    {
        Grid nextGrid = null;
        Tile nextTile = null;

        for (int i = 0; i < GameConstants.DIRECTIONS_TILE_CLEAR_X.Count; i++)
        {
            nextGrid = grid.GetGridInDir(GameConstants.DIRECTIONS_TILE_CLEAR_X[i]);

            if (nextGrid != null && nextGrid.gridData.gridType != GridData.GridType.NULL_GRID)
            {
                nextTile = nextGrid.GetTile();
                if (nextTile != null && !nextTile.isMoving && !nextTile.isOnClearingProcess)
                {
                    GamePlayController.gamePlayController.mapController.ExecuteTile(nextTile, nextGrid, false, false);
                };
            }
        }

        GamePlayController.gamePlayController.effectControll.CreateXRainBow(grid,0.4f);

        //GamePlayController.gamePlayController.effectControll.CreateEffectColumnBreaker(grid, 2, 1, true, -45);
        //GamePlayController.gamePlayController.effectControll.CreateEffectColumnBreaker(grid, 2, 1, false, 45);
        //GamePlayController.gamePlayController.effectControll.CreateEffectColumnBreaker(grid, 2, 1, true, 45);
        //GamePlayController.gamePlayController.effectControll.CreateEffectColumnBreaker(grid, 2, 1, false, -45);

        //AudioController.PlaySFX("sfx_elec");
    }
}

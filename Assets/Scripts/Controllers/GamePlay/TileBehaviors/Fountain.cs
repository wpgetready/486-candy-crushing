﻿/*
 * @Author: CuongNH
 * @Description: Handle behavior for fountain type
 * */

using UnityEngine;
using System.Collections;
using Spine.Unity;

using System.Collections.Generic;

public class Fountain
{

    /// <summary>
    /// Number state of fountain
    /// </summary>
    public const int STATE_NUMBER = 4;

    /// <summary>
    /// So qua moi lan cay sinh ra
    /// </summary>
    public const int FRUIT_NUMBER = 3;

    /// <summary>
    /// List of grid to transform
    /// </summary>
    private static List<Grid> gridTransformList = new List<Grid>(FRUIT_NUMBER);

    /// <summary>
    /// Do lon cua qua khi moi sinh
    /// </summary>
    private readonly static Vector3 SCALE_FRUIT = new Vector3(0.3f, 0.3f, 1f);

    public static void OnCreated(Tile tile)
    {

    }

    /// <summary>
    /// Do transform and calculate time needed
    /// </summary>
    public static float TransformProcess(Tile tile)
    {
        return GameConstants.TIME_TRANSFORM_PROCESS;
    }

    /// <summary>
    /// Call when want upgrade to transform type
    /// </summary>
    /// <param name="tile"></param>
    public static void OnUpgrade(Tile tile)
    {
        if (tile != null)
        {
            if (tile.tileData.data < STATE_NUMBER)
            {
                tile.tileData.data++;
            }

            if (tile.tileData.data == STATE_NUMBER)
            {
                  Activate(tile.grid);

            }
            else
            {
				UpdateGraphic(tile);

            }
        }
    }

    public static void OnTurnEnded(Tile tile)
    {

    }

    /// <summary>
    /// Run destroying animation and return time it take to
    /// </summary>
    public static float OnClear(Tile tile)
    {
        float timeNeeded = GameConstants.TIME_CLEAR_TILE;

        LeanTween.scale(tile.gameObject, Vector3.zero, timeNeeded).setOnComplete(() =>
        {
            //tile.SetTileActive(false);
        });

        return timeNeeded;
    }

    /// <summary>
    /// Update graphic for tile
    /// </summary>
    /// <param name="tile"></param>
	public static void UpdateGraphic(Tile tile, bool isAnimation = true)
    {
        tile.isTransforming = true;

        float t = TransformProcess(tile);

		tile._ft.GetComponent<Renderer>().sortingOrder = 1;


		if(isAnimation == true)
		{
			tile._ft.state.SetAnimation(0,"animation",false);
		}

		LeanTween.scale (tile.gameObject, Vector3.one * 1.2f, 0.35f).setLoopPingPong (1).setEase (LeanTweenType.easeInBounce);
		LeanTween.alpha(tile.gameObject.transform.GetChild(0).gameObject, 0.3f, t).setOnComplete(() =>
            {
                GamePlayController.gamePlayController.mapController.tileSpawner.SetTileMainRender(tile);
				LeanTween.alpha(tile.gameObject.transform.GetChild(0).gameObject, 1f, t).setOnComplete(() =>
                    {
                        tile.isTransforming = false;
                    });
            });
    }

    /// <summary>
    /// Call when active
    /// </summary>
    /// <param name="grid"></param>
    public static void Activate(Grid grid)
    {
        Tile tile = grid.GetTile();
        if (tile == null)
        {
            return;
        }

        tile.isTransforming = true;

        GamePlayController.gamePlayController.gameInterceptorCount++;

        float t = TransformProcess(tile);

		tile.tileData.data = 1;
        UpdateGraphic(tile);

        gridTransformList.Clear();
        Grid finder = null;

        LeanTween.delayedCall(2 * t, () =>
            {
                for (int i = 0; i < FRUIT_NUMBER; i++)
                {
                    finder = MapUtilities.GetRandomGridFromList(GamePlayController
                        .gamePlayController.mapController.gridList, gridTransformList);

                    if (finder != null)
                    {
                        gridTransformList.Add(finder);
                    }
                }

                //Debug.Log("gridTransformList.Count: " + gridTransformList.Count);

                // TODO: Use pooling
                for (int i = 0; i < gridTransformList.Count; i++)
                {
                    //Debug.Log("i: " + i);

                    Grid gridTransform = null;
                    gridTransform = gridTransformList[i];
                    Tile oldTile = gridTransform.GetTile();

                    // For test
                    //LeanTween.delayedCall(2 * t, () =>
                    //{
                        //Tile newTile = GamePlayController.gamePlayController.mapController.tileSpawner.CreateNewTile();
                        
					if(tile.tileData.tileColor != TileData.TileColor.honey)
						{
							Tile newTile = PoolingManager.poolingManager.GetTile();
	                        newTile.transform.position = tile.transform.position;
	                        newTile.SetProperties(TileData.TileType.match_object, tile.tileData.tileColor);
	                        newTile.UpdateTileDisplay();
						if(newTile.transform.childCount < 3)
						{
							GamePlayController.gamePlayController.Leaves(newTile.transform.position,newTile);
						}

                        gridTransform.SetTile(newTile);
                        newTile.transform.localScale = SCALE_FRUIT;

                        if (oldTile != null)
                        {
                            oldTile.grid = null;
                        }

                        gridTransform.GetTile().transform.localPosition += new Vector3(0f, 0f, -1f);


                        gridTransform.MoveTileToCenter(0.7f);//2*t
						LeanTween.scale(newTile.gameObject, Vector3.one, 0.7f).setOnComplete(() => {
							newTile.leavesEffect.GetComponent<ParticleSystem>().loop = false;
							newTile.leavesEffect.GetComponent<ParticleSystem>().Stop();
						});//t

                        LeanTween.delayedCall(t, () =>
                        {
                            if (oldTile != null)
                            {
                                //oldTile.gameObject.SetActive(false);
                                PoolingManager.poolingManager.AddTileToPool(oldTile);
                            }
							});

						LeanTween.delayedCall(5f, () =>
							{
								GamePlayController.gamePlayController.DestroyLeaves(newTile.transform.GetChild(2).gameObject);
							});
					}
					else
					{
						gridTransform.gridData.gridDynamicType = GridData.GridDynamicType.HONEY_GRID;
						gridTransform.dynamicRender.gameObject.transform.position = grid.dynamicRender.gameObject.transform.position;
						gridTransform.dynamicRender.gameObject.SetActive(true);
						GamePlayController.gamePlayController.mapController.gridSpawner.SetGridDynamicRender(gridTransform);
						//iTween.MoveTo(gridTransform.dynamicRender.gameObject, iTween.Hash("position", originPos, "time", 2*t, "easetype", iTween.EaseType.easeInCirc));
						gridTransform.dynamicRender.sortingLayerName = "Tile";
						if(gridTransform.dynamicRender.transform.childCount < 1)
						{
							GamePlayController.gamePlayController.Leaves(gridTransform.dynamicRender.transform.position,null,gridTransform.dynamicRender.gameObject);
						}
						LeanTween.moveLocal(gridTransform.dynamicRender.gameObject, Vector3.zero, 0.55f).setOnComplete(
							() => {
								
							gridTransform.dynamicRender.sortingLayerName = "Grid";
								ParticleSystem p = gridTransform.dynamicRender.transform.GetChild(0).GetComponent<ParticleSystem>();
								p.loop = false;
								p.Stop();
							}
						);

						LeanTween.delayedCall(5f, () =>
							{
								GamePlayController.gamePlayController.DestroyLeaves(gridTransform.dynamicRender.transform.GetChild(0).gameObject);
							});
					}
                }
            });



        LeanTween.delayedCall(tile.gameObject, 0.7f, () =>
            {
                //tile.tileData.data = 1;
				//UpdateGraphic(tile, true);

                LeanTween.delayedCall(tile.gameObject, 0.7f, () =>
                    {
                        tile.isTransforming = false;
                        if (GamePlayController.gamePlayController.gameInterceptorCount > 0)
                        {
                            GamePlayController.gamePlayController.gameInterceptorCount--;
                        }
                    });
            });//2*t

        //AudioController.PlaySFX("sfx_elec");
    }
}

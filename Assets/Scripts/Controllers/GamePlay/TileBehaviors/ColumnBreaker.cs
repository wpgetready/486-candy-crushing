﻿/*
 * @Author: CuongNH
 * @Description: Handle behavior for column_breaker type
 * */

using UnityEngine;
using System.Collections;

public class ColumnBreaker {

    public static void OnCreated(Tile tile)
    {

    }

    /// <summary>
    /// Do transform and calculate time needed
    /// </summary>
    public static float TransformProcess(Tile tile)
    {
        return GameConstants.TIME_TRANSFORM_PROCESS;
    }

    public static void OnUpgrade(Tile tile)
    {
        //Activate(tile.grid);
    }

    public static void OnTurnEnded(Tile tile)
    {

    }

    /// <summary>
    /// Run destroying animation and return time it take to
    /// </summary>
    public static float OnClear(Tile tile)
    {
		tile.ExtraCount ();
        float timeNeeded = GameConstants.TIME_CLEAR_TILE;

        LeanTween.scale(tile.gameObject, Vector3.zero, timeNeeded);
        //tile.SetTileActive(false);
        GamePlayController.gamePlayController.effectControll.CreateEffectTileColor(tile);
		
        //LeanTween.delayedCall(timeNeeded, () =>
        //{
        //    tile.SetTileActive(false);
        //});


        //ObjectManager.Instance.particleSpitter.SpitGemBreak(tile.transform.position, tile.tileData.tileColor);

        //AudioController.PlaySFX("sfx_clr");

        return timeNeeded;
    }

    /// <summary>
    /// Update graphic for tile
    /// </summary>
    /// <param name="tile"></param>
    public static void UpdateGraphic(Tile tile)
    {
        //tile.SetTileActive(true);
        tile.gameObject.transform.localScale = Vector3.one;
		GamePlayController.gamePlayController.mapController.tileSpawner.SetTileMainRender(tile);
        tile.SetEnableSpriteRender(tile.enhanceRender, false);
        //GamePlayController.gamePlayController.mapController.tileSpawner.SetTileEnhanceRender(tile);

    }

    /// <summary>
    /// Call when active
    /// </summary>
    /// <param name="grid"></param>
    public static void Activate(Grid grid, bool showEffect = true, int scale = 1)
    {
        Grid nextGrid = null;
        Tile nextTile = null;
        int count = 0;
        int top = 0;
        int bottom = 0;

        for (int i = 0; i < GameConstants.DIRECTIONS_TILE_CLEAR_COLUMN.Count; i++)
        {
            nextGrid = grid.GetGridInDir(GameConstants.DIRECTIONS_TILE_CLEAR_COLUMN[i]);
            while (nextGrid != null)
            {
                // For test: check out of map
                count++;
                if (count > 10)
                {
                    Debug.Log("Out of map");
                    break;
                }
                // end test

                if (nextGrid.gridData.gridType != GridData.GridType.NULL_GRID)
                {
                    nextTile = nextGrid.GetTile();
                    if (nextTile != null && !nextTile.isMoving && !nextTile.isOnClearingProcess)
					{
						//Debug.Log ("exe" + grid.GetTile().tileData.tileColor.ToString());
						GamePlayController.gamePlayController.mapController.ExecuteTile(nextTile, nextGrid, false, false);
						if(nextTile.tileData.tileModifierType == TileData.TileModifierType.color)
						{
							nextTile.tileData.data--;
							nextTile.UpdateTileDisplay(grid.GetTile().tileData.tileColor.ToString());
						}
                    }
                }

                nextGrid = nextGrid.GetGridInDir(GameConstants.DIRECTIONS_TILE_CLEAR_COLUMN[i]);
            }
            if (i == 0)
            {
                top = count;
            }
            else
            {
                bottom = count;
            }

            count = 0;
        }

        if (showEffect)
        {
            if (top > bottom)
            {
                GamePlayController.gamePlayController.effectControll.CreateEffectColumnBreaker(grid, top + 1, scale, true, -90);
                GamePlayController.gamePlayController.effectControll.CreateEffectColumnBreaker(grid, top + 1, scale, false, -90);
            }
            else
            {
                GamePlayController.gamePlayController.effectControll.CreateEffectColumnBreaker(grid, bottom + 1, scale, true, -90);
                GamePlayController.gamePlayController.effectControll.CreateEffectColumnBreaker(grid, bottom + 1, scale, false, -90);
            }
        }

        //AudioController.PlaySFX("sfx_elec");
        SoundController.sound.ExplodeThunder(GameConstants.TIME_CLEAR_TILE);

    }
}

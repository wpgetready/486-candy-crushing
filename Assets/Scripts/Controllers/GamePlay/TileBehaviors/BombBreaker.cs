﻿/*
 * @Author: CuongNH
 * @Description: Handle behavior for bomb_breaker type
 * */

using UnityEngine;
using System.Collections;

public class BombBreaker {

    public static void OnCreated(Tile tile)
    {

    }

    /// <summary>
    /// Do transform and calculate time needed
    /// </summary>
    public static float TransformProcess(Tile tile)
    {
        return GameConstants.TIME_TRANSFORM_PROCESS;
    }

    public static void OnUpgrade(Tile tile)
    {
        //Activate(tile.grid);
    }

    public static void OnTurnEnded(Tile tile)
    {

    }

    /// <summary>
    /// Run destroying animation and return time it take to
    /// </summary>
    public static float OnClear(Tile tile)
    {
		tile.ExtraCount ();
        float timeNeeded = GameConstants.TIME_CLEAR_TILE;

        LeanTween.cancel(tile.mainRender.gameObject);
        LeanTween.scale(tile.gameObject, Vector3.zero, timeNeeded);
        //tile.SetTileActive(false);
        GamePlayController.gamePlayController.effectControll.CreateEffectTileColor(tile);

        return timeNeeded;
    }

    /// <summary>
    /// Update graphic for tile
    /// </summary>
    /// <param name="tile"></param>
    public static void UpdateGraphic(Tile tile)
    {
        //tile.SetTileActive(true);
        tile.gameObject.transform.localScale = Vector3.one;
		GamePlayController.gamePlayController.mapController.tileSpawner.SetTileMainRender(tile);
        //GamePlayController.gamePlayController.mapController.tileSpawner.SetTileEnhanceRender(tile);

        LeanTween.scale(tile.mainRender.gameObject, new Vector3(1.05f, 1.05f, 1f), 0.5f)
            .setLoopPingPong().setRepeat(-1);

    }

    /// <summary>
    /// Call when active
    /// </summary>
    /// <param name="grid"></param>
    public static void Activate(Grid grid)
    {
        Grid nextGrid = null;
        Tile nextTile = null;
        for (int i = 0; i < GameConstants.DIRECTIONS_TILE_CLEAR_BOMB.Count; i++)
        {
            nextGrid = grid.GetGridInDir(GameConstants.DIRECTIONS_TILE_CLEAR_BOMB[i]);
            for (int j = 0; j < GameConstants.NUMBER_TILE_CLEAR_BOMB[i]; j ++)
            {
                if (nextGrid != null)
                {
                    if (nextGrid.gridData.gridType != GridData.GridType.NULL_GRID)
                    {
                        nextTile = nextGrid.GetTile();
                        if (nextTile != null && !nextTile.isMoving && !nextTile.isOnClearingProcess)
                        {
                            GamePlayController.gamePlayController.mapController
                                .ExecuteTile(nextTile, nextGrid, false, false);
							if(nextTile.tileData.tileModifierType == TileData.TileModifierType.color)
							{
								nextTile.tileData.data--;
								nextTile.UpdateTileDisplay(grid.GetTile().tileData.tileColor.ToString());
							}
                        }
                    }

                    nextGrid = nextGrid.GetGridInDir(GameConstants.DIRECTIONS_TILE_CLEAR_BOMB[i]);
                }
                else
                {

                }
            }
        }

        GamePlayController.gamePlayController.effectControll.CreateEffectBombBreaker(grid, 0.5f);
        //AudioController.PlaySFX("sfx_elec");
        SoundController.sound.ExplodeBoom(GameConstants.TIME_CLEAR_TILE);
    }
}

﻿/*
 * @Author: CuongNH
 * @Description: Handle behavior for match object type
 * */

using UnityEngine;
using System.Collections;

public class MatchObject {

    public static void OnCreated(Tile tile)
    {

    }

    /// <summary>
    /// Do transform and calculate time needed
    /// </summary>
    public static float TransformProcess(Tile tile)
    {
        return GameConstants.TIME_TRANSFORM_PROCESS;
    }

    public static void OnUpgrade(Tile tile)
    {
        
    }

    public static void OnTurnEnded(Tile tile)
    {

    }

    /// <summary>
    /// Run destroying animation and return time it take to
    /// </summary>
    public static float OnClear(Tile tile)
    {
        float timeNeeded = GameConstants.TIME_CLEAR_TILE;

		LeanTween.scale(tile.gameObject, Vector3.zero, timeNeeded).setLoopOnce();
        //tile.SetTileActive(false);
        GamePlayController.gamePlayController.effectControll.CreateEffectTileColor(tile);

        //LeanTween.delayedCall(timeNeeded, () =>
        //{
        //    tile.SetTileActive(false);
        //});


        //ObjectManager.Instance.particleSpitter.SpitGemBreak(tile.transform.position, tile.tileData.tileColor);

        //AudioController.PlaySFX("sfx_clr");
        SoundController.sound.MatchSwap(timeNeeded);

        return timeNeeded;
    }

    /// <summary>
    /// Update graphic for tile
    /// </summary>
    /// <param name="tile"></param>
    public static void UpdateGraphic(Tile tile)
    {
        //tile.ResetDisplay();
        //tile.SetTileSprite("Gem" + (int)tile.tileData.tileColor);
        //tile.SetTileEnhanceActive(false);
        //tile.SetTileBundleEffect("");

        //tile.SetTileActive(true);
        tile.gameObject.transform.localScale = Vector3.one;
        GamePlayController.gamePlayController.mapController.tileSpawner.SetTileMainRender(tile);
        //GamePlayController.gamePlayController.mapController.tileSpawner.SetTileEnhanceRender(tile);
    }

    public static void Activate(Grid grid, Tile tile)
    {

    }
}

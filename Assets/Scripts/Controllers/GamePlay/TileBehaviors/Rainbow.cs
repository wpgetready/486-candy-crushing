﻿/*
 * @Author: CuongNH
 * @Description: Handle behavior for rainbown type
 * */

using UnityEngine;
using System.Collections;

public class Rainbow {

    public static void OnCreated(Tile tile)
    {

    }

    /// <summary>
    /// Do transform and calculate time needed
    /// </summary>
    public static float TransformProcess(Tile tile)
    {
        return GameConstants.TIME_TRANSFORM_PROCESS;
    }

    public static void OnUpgrade(Tile tile)
    {
        //Activate(tile.grid);
    }

    public static void OnTurnEnded(Tile tile)
    {

    }

    /// <summary>
    /// Run destroying animation and return time it take to
    /// </summary>
    public static float OnClear(Tile tile)
    {
		tile.ExtraCount ();
        float timeNeeded = GameConstants.TIME_CLEAR_TILE;

        LeanTween.scale(tile.gameObject, Vector3.zero, timeNeeded);

        return timeNeeded;
    }

    public static void UpdateGraphic(Tile tile)
    {
        //tile.SetTileActive(true);
        tile.gameObject.transform.localScale = Vector3.one;
        GamePlayController.gamePlayController.mapController.tileSpawner.SetTileMainRender(tile);
        //GamePlayController.gamePlayController.mapController.tileSpawner.SetTileEnhanceRender(tile);

        //tile.transform.localScale = new Vector3(1.1f, 1.1f, 1f);
        //LeanTween.scale(tile.gameObject, new Vector3(1.25f, 1.2f, 1f), 0.5f)
        //    .setLoopPingPong().setRepeat(-1);

    }

    public static void Activate(Grid grid)
    {
        //MapController mapController = grid.mapController;
        //if (mapController != null)
        //{
        //    CommonEffects.ActiveLightningEffectSplash(mapController, grid, Grid.Direction.LEFT);
        //    CommonEffects.ActiveLightningEffectSplash(mapController, grid, Grid.Direction.RIGHT);
        //}

        //AudioController.PlaySFX("sfx_elec");
    }
}

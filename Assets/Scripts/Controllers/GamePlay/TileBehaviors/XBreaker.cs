﻿/*
 * @Author: CuongNH
 * @Description: Handle behavior for x_breaker type
 * */

using UnityEngine;
using System.Collections;

public class XBreaker {

    public static void OnCreated(Tile tile)
    {

    }

    /// <summary>
    /// Do transform and calculate time needed
    /// </summary>
    public static float TransformProcess(Tile tile)
    {
        return GameConstants.TIME_TRANSFORM_PROCESS;
    }

    public static void OnUpgrade(Tile tile)
    {
        //Activate(tile.grid);
    }

    public static void OnTurnEnded(Tile tile)
    {

    }

    /// <summary>
    /// Run destroying animation and return time it take to
    /// </summary>
    public static float OnClear(Tile tile)
    {
        float timeNeeded = GameConstants.TIME_CLEAR_TILE;

        LeanTween.scale(tile.gameObject, Vector3.zero, timeNeeded);
        //tile.SetTileActive(false);
        GamePlayController.gamePlayController.effectControll.CreateEffectTileColor(tile);

        //LeanTween.delayedCall(timeNeeded, () =>
        //{
        //    tile.SetTileActive(false);
        //});


        //ObjectManager.Instance.particleSpitter.SpitGemBreak(tile.transform.position, tile.tileData.tileColor);

        //AudioController.PlaySFX("sfx_clr");

        return timeNeeded;
    }

    /// <summary>
    /// Update graphic for tile
    /// </summary>
    /// <param name="tile"></param>
    public static void UpdateGraphic(Tile tile)
    {
        //tile.SetTileActive(true);
        tile.gameObject.transform.localScale = Vector3.one;
		GamePlayController.gamePlayController.mapController.tileSpawner.SetTileMainRender(tile);
        //GamePlayController.gamePlayController.mapController.tileSpawner.SetTileEnhanceRender(tile);

        //tile.transform.localScale = new Vector3(1.1f, 1.1f, 1f);
        //LeanTween.scale(tile.gameObject, new Vector3(1.25f, 1.2f, 1f), 0.5f)
        //    .setLoopPingPong().setRepeat(-1);

    }

    /// <summary>
    /// Call when active
    /// </summary>
    /// <param name="grid"></param>
    public static void Activate(Grid grid)
    {
        Grid nextGrid = null;
        Tile nextTile = null;

        int topLeft = 0;
        int topRight = 0;
        int bottomRight = 0;
        int bottomLeft = 0;

        for (int i = 0; i < GameConstants.DIRECTIONS_TILE_CLEAR_X.Count; i++)
        {
            nextGrid = grid.GetGridInDir(GameConstants.DIRECTIONS_TILE_CLEAR_X[i]);
            int count = 0;
            while (nextGrid != null)
            {
                // For test: check out of map
                count++;
                if (count > 10)
                {
                    Debug.Log("Out of map");
                    break;
                }
                // end test

                if (nextGrid.gridData.gridType != GridData.GridType.NULL_GRID)
                {
                    nextTile = nextGrid.GetTile();
                    if (nextTile != null && !nextTile.isMoving && !nextTile.isOnClearingProcess)
                    {
                        GamePlayController.gamePlayController.mapController.ExecuteTile(nextTile, nextGrid, false, false);
						if(nextTile.tileData.tileModifierType == TileData.TileModifierType.color)
						{
							nextTile.tileData.data--;
							nextTile.UpdateTileDisplay(grid.GetTile().tileData.tileColor.ToString());
						}
                    }
                }

                nextGrid = nextGrid.GetGridInDir(GameConstants.DIRECTIONS_TILE_CLEAR_X[i]);
            }
            if(i == 0)
            {
                topLeft = count;
            }
            else if(i == 1)
            {
                topRight = count;
            }
            else if (i == 2)
            {
                bottomRight = count;
            }
            else if (i == 3)
            {
                bottomLeft = count;
            }
        }

        int maxCell = Mathf.Max(topLeft,topRight,bottomRight,bottomLeft);

        GamePlayController.gamePlayController.effectControll.CreateEffectColumnBreaker(grid, maxCell + 1, 1, true, -45);
        GamePlayController.gamePlayController.effectControll.CreateEffectColumnBreaker(grid, maxCell + 1, 1, false, 45);
        GamePlayController.gamePlayController.effectControll.CreateEffectColumnBreaker(grid, maxCell + 1, 1, true, 45);
        GamePlayController.gamePlayController.effectControll.CreateEffectColumnBreaker(grid, maxCell + 1, 1, false, -45);

        //AudioController.PlaySFX("sfx_elec");
        SoundController.sound.ExplodeThunder(GameConstants.TIME_CLEAR_TILE);
    }
}

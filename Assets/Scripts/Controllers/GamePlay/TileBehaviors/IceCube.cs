﻿/*
 * @Author: CuongNH
 * @Description: Handle behavior for bee type
 * */

using UnityEngine;
using System.Collections;

public class IceCube {

    /// <summary>
    /// Update graphic for tile
    /// </summary>
    /// <param name="tile"></param>
    public static void UpdateGraphic(Tile tile)
    {
        tile.gameObject.transform.localScale = Vector3.one;
        GamePlayController.gamePlayController.mapController.tileSpawner.SetTileMainRender(tile);
		//GamePlayController.gamePlayController.UpdateDrop ();
    }
}

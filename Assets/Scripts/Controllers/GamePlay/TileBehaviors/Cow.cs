﻿/*
 * @Author: CuongNH
 * @Description: Handle behavior for cow type
 * */

using UnityEngine;
using System.Collections;
using Spine.Unity;

public class Cow {

    /// <summary>
    /// Name of animation
    /// </summary>
    public const string ANIMATION_NAME = "smile";
	public const string ANIMATION_NAME_Idle = "idle";

    /// <summary>
    /// Time of animation
    /// </summary>
    //private const float ANIMATION_TIMIE = 1.5f;

    public static void OnCreated(Tile tile)
    {

    }

    /// <summary>
    /// Do transform and calculate time needed
    /// </summary>
    public static float TransformProcess(Tile tile)
    {
        return GameConstants.TIME_TRANSFORM_PROCESS;
    }

    public static void OnUpgrade(Tile tile)
    {
        //Activate(tile.grid);
    }

    public static void OnTurnEnded(Tile tile)
    {

    }

    /// <summary>
    /// Run destroying animation and return time it take to
    /// </summary>
    public static float OnClear(Tile tile)
    {
        float timeNeeded = GameConstants.TIME_CLEAR_TILE;

        return timeNeeded;
    }

    /// <summary>
    /// Update graphic for tile
    /// </summary>
    /// <param name="tile"></param>
    public static void UpdateGraphic(Tile tile)
    {
        //tile.SetTileActive(true);
        tile.gameObject.transform.localScale = Vector3.one;
        GamePlayController.gamePlayController.mapController.tileSpawner.SetTileMainRender(tile);
        //GamePlayController.gamePlayController.mapController.tileSpawner.SetTileEnhanceRender(tile);

    }

    /// <summary>
    /// Call when active
    /// </summary>
    /// <param name="grid"></param>
	public static void Activate(Grid grid, string _ANIMATION_NAME)
    {
        Tile tile = grid.GetTile();
        if (tile != null)
        {
			if(_ANIMATION_NAME == "smile")
			{
            	GamePlayController.gamePlayController.effectControll.CreateEffectTileType(tile);
			}

            try 
	        {
                SkeletonAnimation ske = tile.enhanceRender.transform.GetChild(0).GetChild(0).GetComponent<SkeletonAnimation>();
                if (ske != null)
                {
                    ske.AnimationName = _ANIMATION_NAME;
                    //ske.Reset();
					//ske.state.SetAnimation(0, _ANIMATION_NAME, false);
					ske.Awake();
                }
	        }
	        catch (System.Exception e)
	        {
		        Debug.Log(e.Message);
	        }
        }

        //AudioController.PlaySFX("sfx_elec");
    }
}

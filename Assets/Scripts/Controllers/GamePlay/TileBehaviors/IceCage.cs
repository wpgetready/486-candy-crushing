﻿/*
 * @Author: CuongNH
 * @Description: Handle behavior for ice_cage type
 * */

using UnityEngine;
using System.Collections;
using Spine.Unity;

public class IceCage {

    public static void OnCreated(Tile tile)
    {

    }

    /// <summary>
    /// Do transform and calculate time needed
    /// </summary>
    public static float TransformProcess(Tile tile)
    {
        return GameConstants.TIME_TRANSFORM_PROCESS;
    }

    /// <summary>
    /// Handle on upgrade to change type
    /// </summary>
    /// <param name="tile"></param>
    public static void OnUpgrade(Tile tile)
    {
        tile.isTransforming = true;

        float t = TransformProcess(tile);
        LeanTween.alpha(tile.enhanceRender.gameObject, 0f, 0).setOnComplete(() =>
            {
				GamePlayController.gamePlayController.showIceCageEff(tile.enhanceRender.transform.position);
                tile.SetEnableSpriteRender(tile.enhanceRender, false);
                LeanTween.alpha(tile.enhanceRender.gameObject, 1f, 0f);
                tile.tileData.tileModifierType = TileData.TileModifierType.none;
                tile.SetTileType(TileData.TileType.match_object);

                tile.isTransforming = false;
            });
    }

    public static void OnTurnEnded(Tile tile)
    {

    }

    /// <summary>
    /// Run destroying animation and return time it take to
    /// </summary>
    public static float OnClear(Tile tile)
    {
        float timeNeeded = GameConstants.TIME_CLEAR_TILE;


        return timeNeeded;
    }

    /// <summary>
    /// Update graphic for tile
    /// </summary>
    /// <param name="tile"></param>
    public static void UpdateGraphic(Tile tile)
    {
        //tile.ResetDisplay();
        //tile.SetTileSprite("Gem" + (int)tile.tileData.tileColor);
        //tile.SetTileEnhanceActive(false);
        //tile.SetTileBundleEffect("");

        //tile.SetTileActive(true);
        tile.gameObject.transform.localScale = Vector3.one;
        GamePlayController.gamePlayController.mapController.tileSpawner.SetTileMainRender(tile);
        //GamePlayController.gamePlayController.mapController.tileSpawner.SetTileEnhanceRender(tile);
    }

    public static void Activate(Grid grid, Tile tile)
    {

    }
}

﻿using UnityEngine;
using System.Collections;

public class Sugar {

	public static void UpdateGraphic(Tile tile, string _color = null)
    {
		if (tile.tileData.tileModifierType != TileData.TileModifierType.watermelon_I) {
			//Debug.Log ("Here");
			tile.gameObject.transform.localScale = Vector3.one;
			if(tile.tileData.tileModifierType == TileData.TileModifierType.watermelon_O)
			{
				SyncDataFromO (tile);
			}

			GamePlayController.gamePlayController.mapController.tileSpawner.SetTileMainRender (tile, _color);

			Sugar.HandleEffectYollowData (tile);
		} 
		else 
		{
			SyncDataFromI (tile);
			GamePlayController.gamePlayController.mapController.tileSpawner.SetTileMainRender (tile);
		}
    }

    public static void HandleEffectYollowData(Tile tile)
    {
        GamePlayController.gamePlayController.effectControll.CreateEffectTileType(tile);
    }

	public static void SyncDataFromI(Tile tile)
	{
		if (tile.tileData.tileModifierType == TileData.TileModifierType.watermelon_I) 
		{
			if(tile.watermelon_Org == null)
			{
				foreach (Grid item in tile.grid.connectedList) 
				{
					if (item != null) 
					{
						if (item.GetTile () != null && item.GetTile ().tileData.tileType == TileData.TileType.sugar && item.GetTile ().tileData.tileModifierType == TileData.TileModifierType.watermelon_O) 
						{
							if (item.gridPosition.Column <= tile.grid.gridPosition.Column && item.gridPosition.Row <= tile.grid.gridPosition.Row) 
							{
								tile.watermelon_Org = item.GetTile ();
								//Debug.Log ("Origin");
								if(tile.watermelon_Org.watermelon_I.Count == 0)
								{
									foreach(Grid _item in tile.watermelon_Org.grid.connectedList)
									{
										if (_item!= null && _item.GetTile () != null && _item.GetTile ().tileData.tileType == TileData.TileType.sugar && _item.GetTile ().tileData.tileModifierType == TileData.TileModifierType.watermelon_I) 
										{
											if (_item.gridPosition.Column >= tile.watermelon_Org.grid.gridPosition.Column && _item.gridPosition.Row >= tile.watermelon_Org.grid.gridPosition.Row) 
											{
												tile.watermelon_Org.watermelon_I.Add (_item.GetTile ());
												if (tile.watermelon_Org.watermelon_I.Count == 3) 
												{
													//Debug.Log ("fulllllllllll");
													break;
												}
											}
										}
									}
								}
								break;
							}
						}
					}
				}
			}
		}
	}

	public static void SyncDataFromO(Tile tile)
	{
		if (tile.watermelon_I.Count == 0) 
		{
			foreach (Grid items in tile.grid.connectedList) 
			{
				if (items != null) {
					if (items.GetTile () != null && items.GetTile ().tileData.tileType == TileData.TileType.sugar && items.GetTile ().tileData.tileModifierType == TileData.TileModifierType.watermelon_I) 
					{
						if (items.gridPosition.Column >= tile.grid.gridPosition.Column && items.gridPosition.Row >= tile.grid.gridPosition.Row) 
						{
							tile.watermelon_I.Add (items.GetTile ());
							if (tile.watermelon_I.Count == 3) 
							{
								//Debug.Log ("fulllllllllll");
								break;
							}
						}
					}
				}
			}
		} 
	}
}

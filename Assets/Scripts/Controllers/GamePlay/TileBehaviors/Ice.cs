﻿using UnityEngine;
using System.Collections;

public class Ice : MonoBehaviour {

	
	
	
    public static void UpdateGraphic(Grid grid)
    {
        if(grid.gridData.gridDynamicData > 0)
        {
            grid.gridData.gridDynamicData--;
            GamePlayController.gamePlayController.effectControll.CreateEffectDynamic(grid);
            if(grid.gridData.gridDynamicData <= 0)
            {
               
                grid.gridData.gridDynamicType = GridData.GridDynamicType.NORMAL_GRID;
                grid.dynamicRender.sprite = null;
            }
            else
            {
                GamePlayController.gamePlayController.mapController.gridSpawner.SetGridProperties(grid, grid.gridData);
            }
			SoundController.sound.IceSoundPlay ();
        }
    }
}

﻿/*
 * @Author: CuongNH
 * @Description: Handle behavior for yougurt type
 * */


using UnityEngine;
using System.Collections;

public class Yogurt {

    /// <summary>
    /// Danh dau de co convert khong: false -> convert, true -> no action
    /// </summary>
    public static bool convertedThisTurn = false;

    public static void OnCreated(Tile tile)
    {

    }

    /// <summary>
    /// Do transform and calculate time needed
    /// </summary>
    public static float TransformProcess(Tile tile)
    {
        return GameConstants.TIME_TRANSFORM_PROCESS / 2f;
    }

    public static void OnUpgrade(Tile tile)
    {

    }

    /// <summary>
    /// Call when end turn
    /// </summary>
    /// <param name="tile"></param>
    /// <returns></returns>
    public static float OnTurnEnded(Tile tile)
    {
        if (tile.grid != null && !convertedThisTurn)
        {
            Activate(tile.grid);
            return 3f * TransformProcess(tile);
        }

        return 0;
    }

    /// <summary>
    /// Run destroying animation and return time it take to
    /// </summary>
    public static float OnClear(Tile tile)
    {
        convertedThisTurn = true;
        if (tile.isDestroyable)
        {
            float timeNeeded = 0.25f;
            LeanTween.cancel(tile.gameObject);
            //LeanTween.scale(tile.gameObject, Vector3.zero, timeNeeded);
            LeanTween.rotateAround(tile.gameObject, Vector3.back, 360, 0.25f).setRepeat(-1);
            LeanTween.scale(tile.gameObject, Vector3.zero, 0.25f).setEase(LeanTweenType.easeInBack);

            GamePlayController.gamePlayController.effectControll.CreateEffectYogurt(tile);
            return timeNeeded;
        }
        else
        {
            return 0;
        }
    }

    /// <summary>
    /// Update graphic for tile
    /// </summary>
    /// <param name="tile"></param>
    public static void UpdateGraphic(Tile tile)
    {
        tile.transform.localScale = Vector3.zero;
        //tile.SetTileActive(true);
        GamePlayController.gamePlayController.mapController.tileSpawner.SetTileMainRender(tile);

        LeanTween.cancel(tile.gameObject);
        LeanTween.scale(tile.gameObject, Vector3.one, 2f * TransformProcess(tile));
    }

    /// <summary>
    /// Call when active
    /// </summary>
    /// <param name="grid"></param>
    public static void Activate(Grid grid)
    {
        if (convertedThisTurn)
        {
            return;
        }

        for (int i = 0; i < GameConstants.DIRECTIONS_TILE_CLEAR_AFFECTING.Count; i++)
        {
            Grid nearGrid = grid.GetGridInDir(GameConstants.DIRECTIONS_TILE_CLEAR_AFFECTING[i]);
            if (nearGrid != null && nearGrid.gridData.gridType != GridData.GridType.NULL_GRID)
            {
                Tile t = nearGrid.GetTile();
				if (t != null && t.isColored && t.isHit && t.isSwappable && t.tileData.tileType != TileData.TileType.bomb_breaker
					&& t.tileData.tileType != TileData.TileType.row_breaker && t.tileData.tileType != TileData.TileType.column_breaker
					&& t.tileData.tileType != TileData.TileType.bomb_rainbow && t.tileData.tileType != TileData.TileType.magnet
					&& t.tileData.tileType != TileData.TileType.rainbow && t.tileData.tileType != TileData.TileType.x_breaker
					&& t.tileData.tileType != TileData.TileType.x_rainbow)
                {
                    t.TransformInto(new TileData(TileData.TileType.yogurt));
                    convertedThisTurn = true;
                    break;
                }
            }
        }

        //AudioController.PlaySFX("sfx_mucusb");
    }
}

﻿/*
 * @Author: CuongNH
 * @Description: Handle behavior for coconut1 type
 * */

using UnityEngine;
using System.Collections;

public class Coconut1 {

    public static void OnCreated(Tile tile)
    {

    }

    /// <summary>
    /// Do transform and calculate time needed
    /// </summary>
    public static float TransformProcess(Tile tile)
    {
        return GameConstants.TIME_TRANSFORM_PROCESS;
    }

    /// <summary>
    /// Call when want upgrade to transform type
    /// </summary>
    /// <param name="tile"></param>
    public static void OnUpgrade(Tile tile)
    {
        tile.isTransforming = true;

        float t = TransformProcess(tile);
        LeanTween.alpha(tile.gameObject, 0.2f, t).setOnComplete(() =>
        {
            tile.SetProperties(TileData.TileType.coconut_2, tile.tileData.tileColor);

            LeanTween.alpha(tile.gameObject, 1f, t / 2f).setOnComplete(() =>
                {
                    tile.isTransforming = false;
                });
            
        });
    }

    public static void OnTurnEnded(Tile tile)
    {

    }

    /// <summary>
    /// Run destroying animation and return time it take to
    /// </summary>
    public static float OnClear(Tile tile)
    {
        float timeNeeded = GameConstants.TIME_CLEAR_TILE;

        LeanTween.scale(tile.gameObject, Vector3.zero, timeNeeded).setOnComplete(() =>
            {
                //tile.SetTileActive(false);
            });

        return timeNeeded;
    }

    /// <summary>
    /// Update graphic for tile
    /// </summary>
    /// <param name="tile"></param>
    public static void UpdateGraphic(Tile tile)
    {
        //tile.SetTileActive(true);
        tile.gameObject.transform.localScale = Vector3.one;
        GamePlayController.gamePlayController.mapController.tileSpawner.SetTileMainRender(tile);
        //GamePlayController.gamePlayController.mapController.tileSpawner.SetTileEnhanceRender(tile);

        //tile.transform.localScale = new Vector3(1.1f, 1.1f, 1f);
        //LeanTween.scale(tile.gameObject, new Vector3(1.25f, 1.2f, 1f), 0.5f)
        //    .setLoopPingPong().setRepeat(-1);

    }

    /// <summary>
    /// Call when active
    /// </summary>
    /// <param name="grid"></param>
    public static void Activate(Grid grid)
    {

        //AudioController.PlaySFX("sfx_elec");
    }
}

﻿/*
 * @Author: CuongNH
 * @Description: Handle behavior for rocket type
 * */

using UnityEngine;
using System.Collections;

public class Rocket {

    public static void OnCreated(Tile tile)
    {

    }

    /// <summary>
    /// Do transform and calculate time needed
    /// </summary>
    public static float TransformProcess(Tile source, Tile dest, TileData.TileType tileType)
    {
        if (source != null && dest != null)
        {
            GamePlayController.gamePlayController.gameInterceptorCount++;

            dest.isTransforming = true;

            //GameObject rocket = null;

			GamePlayController.gamePlayController.effectControll.CreateEffectRocket(source,dest, tileType);

            // TODO: Clone rocket from prefab and move from source to dest

//            LeanTween.delayedCall(dest.gameObject, GameConstants.TIME_FIRE_ROCKET, () =>
//                {
//                    dest.TransformInto(new TileData(tileType, source.tileData.tileColor));
//
//                    LeanTween.rotateAround(dest.gameObject, Vector3.forward, 360f, 
//                        GameConstants.TIME_TRANSFORM_PROCESS).setOnComplete(() =>
//                    {
//                        dest.isTransforming = false;
//                    });
//
//                    if (GamePlayController.gamePlayController.gameInterceptorCount > 0)
//                    {
//                        GamePlayController.gamePlayController.gameInterceptorCount--;
//                    }
//                });
        }

        return (GameConstants.TIME_TRANSFORM_PROCESS + GameConstants.TIME_FIRE_ROCKET);
    }

    public static void OnUpgrade(Tile tile)
    {
        //Activate(tile.grid);
    }

    public static void OnTurnEnded(Tile tile)
    {

    }

    /// <summary>
    /// Run destroying animation and return time it take to
    /// </summary>
    public static float OnClear(Tile tile)
    {
        float timeNeeded = GameConstants.TIME_CLEAR_TILE;

        return timeNeeded;
    }

    /// <summary>
    /// Update graphic for tile
    /// </summary>
    /// <param name="tile"></param>
    public static void UpdateGraphic(Tile tile)
    {
        //tile.SetTileActive(true);
        tile.gameObject.transform.localScale = Vector3.one;
        GamePlayController.gamePlayController.mapController.tileSpawner.SetTileMainRender(tile);
        //GamePlayController.gamePlayController.mapController.tileSpawner.SetTileEnhanceRender(tile);

    }

    /// <summary>
    /// Call when active
    /// </summary>
    /// <param name="grid"></param>
    public static void Activate(Grid grid)
    {

        //AudioController.PlaySFX("sfx_elec");
    }
}

﻿using UnityEngine;
using System.Collections;

public class Muffin {
    public static void UpdateGraphic(Tile tile)
    {
        //tile.SetTileActive(true);
        tile.gameObject.transform.localScale = Vector3.one;
        //GamePlayController.gamePlayController.effectControll.CreateEffectTileType(tile);
		//GamePlayController.gamePlayController.muffinEff3.state.SetAnimation (0,"animation",false);
        GamePlayController.gamePlayController.mapController.tileSpawner.SetTileMainRender(tile);
        //GamePlayController.gamePlayController.mapController.tileSpawner.SetTileEnhanceRender(tile);

        //tile.transform.localScale = new Vector3(1.1f, 1.1f, 1f);
        //LeanTween.scale(tile.gameObject, Vector3.zero, 0.5f);
        //    .setLoopPingPong().setRepeat(-1);

    }

    public static void HandleEffectYollowData(Tile tile)
    {
		LeanTween.scale(tile.gameObject, Vector3.zero, 0.2f);
        GamePlayController.gamePlayController.effectControll.CreateEffectTileType(tile);
    }
}

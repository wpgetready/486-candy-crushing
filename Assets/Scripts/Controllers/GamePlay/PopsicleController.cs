﻿/*
 * @Author: CuongNH
 * @Description: Xu ly cac su kien cac Popsicles tren man choi
 * */

using UnityEngine;
using System.Collections;

using System.Collections.Generic;
using Spine.Unity;
public class PopsicleController : MonoBehaviour {

    /// <summary>
    /// Popsicle prefabs to clone
    /// </summary>
    public GameObject popsiclePrefab;

    /// <summary>
    /// MapController
    /// </summary>
    public MapController mapController;

    /// <summary>
    /// Cac sprites de hien thi popsicle
    /// </summary>
    public Sprite[] popsicleSprites;

    public enum PopsicleSpriteIndex
    {
        SQUARE = 0,
        RECTANGLE = 1
    }

    /// <summary>
    /// Danh sach cac popsicles
    /// </summary>
    public List<Popsicle> popsicleList;

	// Use this for initialization
    //void Start () {
	
    //}
	
    //// Update is called once per frame
    //void Update () {
	
    //}

    public TextMesh countPopsicle;
    public GameObject item;
    public GameObject ObjectPopsicle;

    private int posicleCollect;
    // Use this for initialization
    void Start()
    {
        posicleCollect = Contance.popsiclesToCollect;
        countPopsicle.text = posicleCollect.ToString();
    }

    /// <summary>
    /// Create new popsicle
    /// </summary>
    /// <param name="pPopsicle"></param>
    /// <returns></returns>
    public Popsicle CreateNewPopsicle(P_Popsicle pPopsicle)
    {
        if (!string.IsNullOrEmpty(pPopsicle.type_Popsicle) && !string.IsNullOrEmpty(pPopsicle.data_Popsicle))
        {
            GameObject popsicleObj = (GameObject)Instantiate(popsiclePrefab, Vector3.zero, Quaternion.identity);
            if (popsicleObj != null)
            {
                Popsicle popsicle = popsicleObj.GetComponent<Popsicle>();
                if (popsicle != null)
                {
                    #region Set properties for popsicles
                    string type = "";
                    string data = pPopsicle.data_Popsicle.Trim();
                    string[] dataArray = data.Split('_');
                    if (dataArray.Length >= 2)
                    {
                        type = dataArray[1];
                    }

                    type = "set_" + type;
                    switch (type)
                    {
                        case GameConstants.set_1x1:
                            {
                                popsicle.SetProperties(Popsicle.PopsicleType.SET_1x1, 
                                    popsicleSprites[(int)PopsicleSpriteIndex.SQUARE]);
                                break;
                            }
                        case GameConstants.set_2x2:
                            {
                                popsicle.SetProperties(Popsicle.PopsicleType.SET_2x2,
                                    popsicleSprites[(int)PopsicleSpriteIndex.SQUARE]);
                                break;
                            }
                        case GameConstants.set_3x3:
                            {
                                popsicle.SetProperties(Popsicle.PopsicleType.SET_3x3,
                                    popsicleSprites[(int)PopsicleSpriteIndex.SQUARE]);
                                break;
                            }
                        case GameConstants.set_4x4:
                            {
                                popsicle.SetProperties(Popsicle.PopsicleType.SET_4x4,
                                    popsicleSprites[(int)PopsicleSpriteIndex.SQUARE]);
                                break;
                            }
                        case GameConstants.set_1x2:
                            {
                                popsicle.SetProperties(Popsicle.PopsicleType.SET_1x2,
                                    popsicleSprites[(int)PopsicleSpriteIndex.RECTANGLE]);
                                break;
                            }
                        case GameConstants.set_2x1:
                            {
                                popsicle.SetProperties(Popsicle.PopsicleType.SET_2x1,
                                    popsicleSprites[(int)PopsicleSpriteIndex.RECTANGLE]);
                                break;
                            }
                        case GameConstants.set_2x4:
                            {
                                popsicle.SetProperties(Popsicle.PopsicleType.SET_2x4,
                                    popsicleSprites[(int)PopsicleSpriteIndex.RECTANGLE]);
                                break;
                            }
                        case GameConstants.set_4x2:
                            {
                                popsicle.SetProperties(Popsicle.PopsicleType.SET_4x2,
                                    popsicleSprites[(int)PopsicleSpriteIndex.RECTANGLE]);
                                break;
                            }
                        default:
                            break;
                    }
                    #endregion

                    #region Set transform, add list grid to popsicle
                    int popsicleWidth = 1;
                    int popsicleHeight = 1;
                    GetPopsicleSize(pPopsicle.data_Popsicle, ref popsicleWidth, ref popsicleHeight);

                    try
                    {
                        Grid grid = mapController.gridList[(pPopsicle.row_Popsicle - (popsicleHeight - 1)) 
                            * Contance.width + pPopsicle.col_Popsicle];

                        if (grid != null)
                        {
                            grid.SetPopsicle(popsicle);
							grid.hasPopsicle = true;
                        }
                    }
                    catch (System.Exception e)
                    {
                        Debug.Log(e.Message);
                    }

                    for (int i = 0; i < popsicleWidth; i++)
                    {
                        for (int j = (popsicleHeight - 1); j >= 0; j--)
                        {
                            try
                            {
                                Grid grid = mapController.gridList[(pPopsicle.row_Popsicle - j)
                                    * Contance.width + (pPopsicle.col_Popsicle + i)];
                                if (grid != null)
                                {
                                    popsicle.gridList.Add(grid);
                                }
                            }
                            catch (System.Exception e)
                            {
                                Debug.Log(e.Message);
                            }
                        }
                    }
                    #endregion

                    return popsicle;
                }
            }
        }
        return null;
    }

    /// <summary>
    /// Get width and heigh of popsicle with data
    /// </summary>
    /// <param name="data"></param>
    /// <param name="width"></param>
    /// <param name="height"></param>
    public void GetPopsicleSize(string data, ref int width, ref int height)
    {
        if (!string.IsNullOrEmpty(data))
        {
            try
            {
                data = data.Trim();
                string[] dataArray = data.Split('_');
                if (dataArray.Length >= 2)
                {
                    string[] sizeArray = dataArray[1].Split('x');
                    if (sizeArray.Length >= 2)
                    {
                        width = int.Parse(sizeArray[0]);
                        height = int.Parse(sizeArray[1]);
                    }
                }
            }
            catch (System.Exception e)
            {
                Debug.Log(e.Message);
            }
        }
    }

    public void HandlePopsicle()
    {
        if(popsicleList.Count > 0)
        {
            for(int i = 0; i < popsicleList.Count; i++)
            {
                if (!popsicleList[i].collected)
                {
                    for (int j = 0; j < popsicleList[i].gridList.Count; j++)
                    {
                        if (popsicleList[i].gridList[j].gridData.gridDynamicType == GridData.GridDynamicType.ICE_GRID)
                            break;
                        else if (j == popsicleList[i].gridList.Count - 1)
                            popsicleList[i].collected = true;
                    }

                    if (popsicleList[i].collected)
                    {
                        popsicleList[i].render.sortingLayerName = "Effect";
						GameObject stars = Instantiate (popsicleList [i].stars.gameObject, popsicleList [i].transform.position, Quaternion.identity) as GameObject;
						Destroy (stars, 3);
                        MoveObjectCollected(popsicleList[i].gameObject);
						if(popsicleList[i].render.GetComponent<SpriteRenderer>().sprite.name == "popsicle_1")
						{
                       	 	LeanTween.scale(popsicleList[i].render.gameObject, new Vector3(1f, 1f, 1f), 0.7f);
						}
						else
						{
							LeanTween.scale(popsicleList[i].render.gameObject, new Vector3(1f, 0.5f, 1f), 0.7f);
						}
                        LeanTween.rotate(popsicleList[i].render.gameObject, Vector3.zero, 0.1f);
                        //Destroy(popsicleList[i].gameObject);
                        popsicleList.RemoveAt(i);
                    }
                }
            }
        }
    }

    public void MoveObjectCollected(GameObject obj)
    {
        LeanTween.scale(obj, new Vector3(1.2f, 1.2f, 1.2f), 0.1f);
        //LeanTween.rotate(obj, Vector3.zero, 0.1f);
        LeanTween.move(obj, new Vector3(obj.transform.position.x,obj.transform.position.y - 0.5f,0), 0.25f);

        LeanTween.delayedCall(obj, 0.2f, () =>
        {
			SoundController.sound.iceCreamPlay();
            LeanTween.scale(obj, new Vector3(1f, 1f, 1f), 0.5f);
			if(obj.GetComponent<Popsicle>().popsicleType == Popsicle.PopsicleType.SET_2x1)
			{
					LeanTween.move(obj, new Vector3(item.transform.position.x, item.transform.position.y-0.5f,1), 0.5f);
			}
			else
			{
				if(obj.GetComponent<Popsicle>().popsicleType == Popsicle.PopsicleType.SET_4x2)
				{
					LeanTween.move(obj, new Vector3(item.transform.position.x, item.transform.position.y-1f,1), 0.5f);
				}
				else
				{
					LeanTween.move(obj, item.transform.position, 0.5f);
				}
			}
            
        });

        LeanTween.delayedCall(obj, 0.8f, () =>
        {
            AnimationStatePopsicle(ObjectPopsicle,Contance.AN_KEM,false);
            ScaleItem();
            Destroy(obj);
        });
    }

    public void ScaleItem()
    {
        LeanTween.scale(item, new Vector3(0.7f, 0.7f, 0.7f), 0.1f);
        LeanTween.delayedCall(item, 0.1f, () =>
        {
            LeanTween.scale(item, new Vector3(1f, 1f, 1f), 0.1f);
            posicleCollect--;
            countPopsicle.text = posicleCollect.ToString();

            if (posicleCollect <= 0)
            {
                AnimationWinOrLose(ObjectPopsicle, Contance.THANG, true);
                item.SetActive(false);
                Contance.GAME_WIN = true;
                //AnimationWinOrLose(gameObject, Contance.DI, true);
                StartCoroutine(OutPutObj());
            }
        });
    }

    public IEnumerator OutPutObj ()
    {
        yield return new WaitForSeconds(1f);
        AnimationWinOrLose(ObjectPopsicle, Contance.DI, true);
        iTween.MoveTo(ObjectPopsicle, iTween.Hash("position", new Vector3(-10, gameObject.transform.position.y, 0), "speed", 1, "easetype", iTween.EaseType.linear));
    }

    public void AnimationStatePopsicle(GameObject obj, string nameAnimation, bool loop)
    {
        if (!obj.GetComponent<SkeletonAnimation>().AnimationName.Equals(nameAnimation))
        {
            obj.GetComponent<SkeletonAnimation>().state.SetAnimation(0, nameAnimation, loop);
            obj.GetComponent<SkeletonAnimation>().state.AddAnimation(0, Contance.NGOI, true, 0f);
        }
    }

    public void AnimationWinOrLose(GameObject obj, string nameAnimation, bool loop)
    {
        if (!obj.GetComponent<SkeletonAnimation>().AnimationName.Equals(nameAnimation))
        {
            obj.GetComponent<SkeletonAnimation>().state.SetAnimation(0, nameAnimation, loop);
            if(Contance.GAME_WIN)
                obj.GetComponent<SkeletonAnimation>().state.AddAnimation(0, Contance.DI, true, 0f);
        }
    }

    public void Popsicle_Lose()
    {
        AnimationWinOrLose(ObjectPopsicle, Contance.THUA, true);
    }
}

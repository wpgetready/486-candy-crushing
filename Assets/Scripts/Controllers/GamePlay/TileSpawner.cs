﻿/*
 * @Author: CuongNH
 * @Description: Su dung de tao cac Tile
 * */

using UnityEngine;
using System.Collections;

using System.Collections.Generic;
using Spine.Unity;

public class TileSpawner : MonoBehaviour
{

    #region Properties, Data, Components
    /// <summary>
    /// MapController
    /// </summary>
    public MapController mapController;

    /// <summary>
    /// TilePrefab dung de clone cac tile
    /// </summary>
    public Tile tilePrefab;

    /// <summary>
    /// Danh sach cac mau duoc dung de lay ngau nhieu (lay mau cho cac tile co color la random..)
    /// </summary>
    public List<string> randomTileColorList = new List<string>();

    /// <summary>
    /// Chua cac mau da duoc lay ngau nhien
    /// </summary>
    public Dictionary<string, string> randomTileColorDict = new Dictionary<string, string>();

    ///// <summary>
    ///// Cac sprite de hien thi main render cua tile
    ///// </summary>
    //public Sprite[] mainSpriteTile;

    ///// <summary>
    ///// Cac sprite de hien thi enhance render cua tile
    ///// </summary>
    //public Sprite[] enhanceSpriteTile;

    /// <summary>
    /// Cac sprite de hien thi match object
    /// </summary>
    public Sprite[] matchObjectSprite;

    /// <summary>
    /// Cac sprite de hien thi bomb_breaker
    /// </summary>
    public Sprite[] bombBreakerSprite;

    /// <summary>
    /// Cac sprite de hien thi row_breaker
    /// </summary>
    public Sprite[] rowBreakerSprite;

    /// <summary>
    /// Cac sprite de hien thi column_breaker
    /// </summary>
    public Sprite[] columnBreakerSprite;

    /// <summary>
    /// Cac sprite de hien thi x_breaker
    /// </summary>
    public Sprite[] xBreakerSprite;

    /// <summary>
    /// Cac sprite de hien thi sugars
    /// </summary>
    public Sprite[] sugarSprite;

	public Sprite[] waterMelon;

	public GameObject sugar_C5;

	public GameObject sugar_C6;

	public bool isProcessMagicBoom;

	public List<Tile> waterMelon_O_tile;
    /// <summary>
    /// Cac sprite de hien thi muffins
    /// </summary>
    public Sprite[] muffinSprite;

    /// <summary>
    /// Cac sprite de hien thi fountain
    /// </summary>
    public Sprite[] fountainSprite;

    /// <summary>
    /// Cac sprite de hien thi coconut
    /// </summary>
    public Sprite[] coconutSprite;

    /// <summary>
    /// Cac sprite de hien thi cow
    /// </summary>
    public Sprite[] cowSprite;

    /// <summary>
    /// Preabe cow
    /// </summary>
    public GameObject cowPrefab;

    /// <summary>
    /// Sprite de hien thi ice_cage
    /// </summary>
    public Sprite iceCageSprite;

    /// <summary>
    /// Sprite de hien thi bee
    /// </summary>
    public Sprite beeSprite;

    /// <summary>
    /// Sprite de hien thi ice_cube
    /// </summary>
    public Sprite iceCubeSprite;

    /// <summary>
    /// Sprite de hien thi yogurt_cup
    /// </summary>
    public Sprite yogurtCupSprite;

    /// <summary>
    /// Sprite de hien thi yogurt
    /// </summary>
    public Sprite yogurtSprite;

    /// <summary>
    /// Sprite de hien thi rainbow
    /// </summary>
    public Sprite rainbowSprite;

    /// <summary>
    /// Sprite de hien thi magnet
    /// </summary>
    public Sprite magnetSprite;

    /// <summary>
    /// Sprite de hien thi x_rainbow
    /// </summary>
    public Sprite xRainbowSprite;

    /// <summary>
    /// Sprite de hien thi bomb_rainbow
    /// </summary>
    public Sprite bombRainbowSprite;

    public Material defaultMaterial;

    #endregion

    //// Use this for initialization
    //void Start () {
	
    //}
	
    //// Update is called once per frame
    //void Update () {

    //}

    #region Methods to Create, Set Data, Properties for Tile
    /// <summary>
    /// Create new tile no grid
    /// </summary>
    /// <returns></returns>
    public Tile CreateNewTile()
    {
        GameObject tileObject = (GameObject)Instantiate(tilePrefab.gameObject, Vector3.zero, Quaternion.identity);

        if (tileObject != null)
        {
            Tile tile = tileObject.GetComponent<Tile>();

            return tile;
        }

        return null;
    }

    /// <summary>
    /// Tao Tile moi tu prefab
    /// </summary>
    /// <param name="grid"></param>
    /// <param name="position"></param>
    public Tile CreateNewTile(Grid grid, P_Object pObject, Vector3 position)
    {
        //try
        //{
            if (grid != null && !string.IsNullOrEmpty(pObject.type_Object))
            {
                GameObject tileObject = (GameObject)Instantiate(tilePrefab.gameObject, position, Quaternion.identity);

                if (tileObject != null)
                {
                    tileObject.transform.SetParent(grid.dynamicComponents);
                    //tileObject.transform.localScale = Vector3.one;
                    //tileObject.transform.localPosition = position;

                    Tile tile = tileObject.GetComponent<Tile>();
                    if (tile != null)
                    {
                        P_Object newPOject = new P_Object();
                        newPOject.row_Object = pObject.row_Object;
                        newPOject.col_Object = pObject.col_Object;
                        newPOject.type_Object = pObject.type_Object;
                        newPOject.modifierType_Object = pObject.modifierType_Object;
                        newPOject.color_Object = pObject.color_Object;
                        newPOject.data_Object = pObject.data_Object;

                        //Debug.Log("pObject.color_Object 0: " + pObject.color_Object);

                        newPOject.color_Object = GetRandomTileColor(pObject.color_Object);

                        //Debug.Log("newPOject.color_Object: " + newPOject.color_Object);
                        //Debug.Log("pObject.color_Object 1: " + pObject.color_Object);

                        tile.SetTileData(newPOject);

                        //grid.SetTile(tile);
                        
                        SetTileMainRender(tile);

                        SetTileEnhanceRender(tile);

                        return tile;
                    }
                }
            }
        //}
        //catch (System.Exception e)
        //{
        //    VGDebug.LogError(e.Message);
        //}

        return null;
    }

    /// <summary>
    /// Lay mau ngau nhien cho Tile
    /// </summary>
    /// <param name="randomName"></param>
    /// <returns></returns>
    public string GetRandomTileColor(string randomName = "")
    {
        string tileColor = "";
        int rand = 0;
        try
        {
            if (!string.IsNullOrEmpty(randomName))
            {
                randomName = randomName.Trim();
            }
            
            if (string.IsNullOrEmpty(randomName) || randomName.Equals(GameConstants.any_tile_color) 
                || randomName.StartsWith(GameConstants.random_tile_color))
            {
                if (string.IsNullOrEmpty(randomName) || randomName.Equals(GameConstants.any_tile_color))
                {
                    rand = Random.Range(0, Contance.LIST_COLOR.Count);
                    tileColor = Contance.LIST_COLOR[rand].color;
                }
                else
                {
                    if (randomTileColorDict.ContainsKey(randomName))
                    {
                        tileColor = randomTileColorDict[randomName];
                    }
                    else
                    {
                        rand = Random.Range(0, randomTileColorList.Count);
                        tileColor = randomTileColorList[rand];
                        randomTileColorList.RemoveAt(rand);

                        randomTileColorDict.Add(randomName, tileColor);
                    }
                }
            }
            else
            {
                return randomName;
            }
        }
        catch (System.Exception e)
        {
            VGDebug.LogError(e.Message);
        }

        return tileColor;
    }

    /// <summary>
    /// Set tile data for tile
    /// </summary>
    /// <param name="pOpject"></param>
    public void ReSetTileData(Grid grid, P_Object pObject)
    {
        if (grid != null && grid.GetTile() != null && !string.IsNullOrEmpty(pObject.type_Object))
        {
            P_Object newPOject = new P_Object();
            newPOject.row_Object = pObject.row_Object;
            newPOject.col_Object = pObject.col_Object;
            newPOject.type_Object = pObject.type_Object;
            newPOject.modifierType_Object = pObject.modifierType_Object;
            newPOject.color_Object = pObject.color_Object;
            newPOject.data_Object = pObject.data_Object;

            newPOject.color_Object = GetRandomTileColor(pObject.color_Object);
            grid.GetTile().SetTileData(newPOject);

            //int count = 0;
            //while (mapController.mapConditionChecker.CheckHitInGrid(grid)
            //    || mapController.mapConditionCheckerMatch2x2.CheckHitInGrid(grid))
            //{
            //    count++;
            //    Debug.Log(count);

            //    newPOject.color_Object = GetRandomTileColor(pObject.color_Object);
            //    grid.GetTile().SetTileData(newPOject);

            //    if (count > 20)
            //    {
            //        Debug.Log(grid.gridPosition.Column + " : " + grid.gridPosition.Row);
            //        break;
            //    }
            //}

            SetTileMainRender(grid.GetTile());

            SetTileEnhanceRender(grid.GetTile());
        }
    }

    /// <summary>
    /// Set main render for tile
    /// </summary>
    /// <param name="tile"></param>

	public bool isDone = false;

	public bool magicxmagic;

	bool oneTimeSugar_Color;

	public void SetTranformTile(Tile _tile)
	{
		_tile.mainRender.transform.localPosition = Vector3.zero;
		_tile.mainRender.transform.localScale = Vector3.one;
		//Debug.Log (_tile.transform.rotation.ToString());
		_tile.transform.eulerAngles = new Vector3 (0,0,0);
		//Debug.Log (_tile.transform.rotation.ToString());
	}

	public GameObject redeffect;

	public GameObject colorCircle;

	public void MagicBomb()
	{
		if(magicxmagic == false)
		{
			magicxmagic = true;
			//Debug.Log("MagicXMagic");
			CombineBooster.MagicXMagic(mapController);
		}
	}

	public GameObject starseffect;

	public void ShowStarsEffect(Tile tile)
	{
		GameObject twinkles = Instantiate (starseffect);
		twinkles.transform.position = tile.transform.position;
		LeanTween.scale (twinkles, twinkles.transform.localScale * 1.2f, 0.15f).setLoopOnce ();
		LeanTween.alpha (twinkles, 1f, 0.15f).setLoopOnce ().setOnComplete(
			() =>
			{
				LeanTween.cancel(twinkles);
				Destroy(twinkles,0.05f);
			}
		);
	}

	public void SetTileMainRender(Tile tile, string _color = null)
    { 
        if (tile != null)
        {
            // TODO: Optimize: material bi thay doi
            if (tile.mainRender.sharedMaterial != defaultMaterial)
            {
                //Debug.Log("Has change material");
                tile.mainRender.material = defaultMaterial;
            }

            switch (tile.tileData.tileType)
            {
                #region Set render for match_object
                case TileData.TileType.match_object:
                    {
                        //Debug.Log("(int)tile.tileData.tileColor: " + (int)tile.tileData.tileColor);
                        //Debug.Log("tile.tileData.tileColor: " + tile.tileData.tileColor.ToString());
						SetTranformTile(tile);
						tile.SetMainRender(matchObjectSprite[(int)tile.tileData.tileColor]);
                        break;
                    }
                #endregion
                #region Set render for sugar
                case TileData.TileType.sugar:
                    {
					SetTranformTile(tile);
                        int data = (tile.tileData.data > 0) ? (tile.tileData.data - 1) : tile.tileData.data;
					//Debug.Log("data"+data);

					if(tile.tileData.tileModifierType == TileData.TileModifierType.none )
					{
						tile.SetMainRender(sugarSprite[data]);
					}
						
					if(tile.tileData.tileModifierType == TileData.TileModifierType.watermelon_O)
					{
						//Debug.Log("water_O");
						if(isDone == false)
						{
							if(waterMelon_O_tile.Contains(tile) == false)
							{
								waterMelon_O_tile.Add(tile);
							}

							tile.SetMainRender(waterMelon[data]);
							tile.mainRender.transform.localPosition = new Vector3(0.5f,0.5f,0);
							tile.mainRender.transform.localScale = new Vector3(0.79f,0.79f,1);
							tile.tileData.data--;
						}


						//Debug.Log("waterO");
						if(tile.isMatch == true)
						{
							//Debug.Log("waterO_1");
							tile.SetMainRender(waterMelon[data]);
							foreach(Tile item in tile.watermelon_I)
							{
								item.tileData.data = tile.tileData.data;
							}

							GameObject red = Instantiate(redeffect, tile.mainRender.transform.position, Quaternion.identity) as GameObject;
							Destroy(red,0.45f);

							tile.isMatch = false;

							//							foreach(Tile _tile in waterMelon_O_tile)
							//							{
							//								_tile.isMatch = false;
							//							}
						}
						else
						{
							tile.tileData.data++;
						}
						tile.mainRender.transform.localPosition = new Vector3(0.5f,0.5f,0);
						tile.mainRender.transform.localScale = new Vector3(0.79f,0.79f,1);
					}

					if(tile.tileData.tileModifierType == TileData.TileModifierType.watermelon_I)
					{//Debug.Log("aa1");
						if(tile.watermelon_Org != null)
						{///Debug.Log("aa2");
							if(tile.tileData.data < tile.watermelon_Org.tileData.data)
							{//Debug.Log("aa3");
								if(tile.watermelon_Org.isMatch == true)
								{
									tile.watermelon_Org.isMatch = false;
//									foreach(Tile _tile in waterMelon_O_tile)
//									{
//										_tile.isMatch = false;
//									}
									tile.watermelon_Org.tileData.data = tile.tileData.data;
									tile.watermelon_Org.SetMainRender(waterMelon[data]);
									GameObject red = Instantiate(redeffect, tile.watermelon_Org.mainRender.transform.position, Quaternion.identity) as GameObject;
									Destroy(red,0.45f);
									//Debug.Log(tile.watermelon_Org.watermelon_I.Count);

									foreach(Tile item in tile.watermelon_Org.watermelon_I)
									{
										//Debug.Log("11111111111");
										item.tileData.data = tile.watermelon_Org.tileData.data;
									}
									//Debug.Log(tile.watermelon_Org.tileData.data);
								}
								else
								{
									tile.tileData.data++;
								}
							}
						}
					}



					if(tile.tileData.tileModifierType == TileData.TileModifierType.color)
					{
						if(tile.sugar_scr == null)
						{
							GameObject obj = null;
							if(data == 4)
							{
								obj = Instantiate(sugar_C5);
							}

							if(data == 5)
							{
								obj = Instantiate(sugar_C6);
							}

							if(obj != null)
							{
								obj.transform.SetParent(tile.mainRender.transform);
								//obj.transform.localPosition = Vector3.zero;
								//obj.transform.localScale = Vector3.one;
								tile.sugar_scr = obj.GetComponent<sugarColor>();
							}
						}
						else
						{
							if(data>=0)
							{
								//Debug.Log("data>0");
								bool _Show = tile.sugar_scr.SetandShow(_color);
								if(_Show == false)
								{
									tile.tileData.data++;
									data++;
								}
								//Debug.Log("data"+data);
							}

							if(tile.tileData.data <= 0)
							{
								if(isProcessMagicBoom == false)
								{
									DestroyImmediate(tile.mainRender.transform.GetChild(0).gameObject);
									tile.OnClear();
									GamePlayController.gamePlayController.mapController.tileSpawner.isProcessMagicBoom = true;
									EffectSugarColor(tile);
									GamePlayController.gamePlayController.mapController.tileSpawner.MagicBomb ();
								}
								else
								{
									DestroyImmediate(tile.mainRender.transform.GetChild(0).gameObject);
									tile.OnClear();
								}
							}
						}
					}
						
					tile.isHit = false;
                    break;
                    }
                #endregion
                #region Set render for bee
                case TileData.TileType.bee:
                    {
						SetTranformTile(tile);
                        tile.SetMainRender(beeSprite);
                        break;
                    }
                #endregion
                #region Set render for ice_cube
                case TileData.TileType.ice_cube:
                    {
						SetTranformTile(tile);
                        tile.SetMainRender(iceCubeSprite);
                        break;
                    }
                #endregion
                #region Set render for muffin
                case TileData.TileType.muffin:
                    {
						SetTranformTile(tile);
                        int data = (tile.tileData.data > 0) ? (tile.tileData.data - 1) : tile.tileData.data;
                        tile.SetMainRender(muffinSprite[data]);
                        break;
                    }
                #endregion
                #region Set render for cow
                case TileData.TileType.cow:
                    {
						SetTranformTile(tile);
                        tile.SetMainRender(cowSprite[0]);
                        break;
                    }
                #endregion
                #region Set render for fountain
                case TileData.TileType.fountain:
                    {
						SetTranformTile(tile);
                        int data = (tile.tileData.data > 0) ? tile.tileData.data : 1;
                        tile.tileData.data = data;
                        tile.SetMainRender(fountainSprite[GameConstants.numberStateFountain * (int)tile.tileData.tileColor + (data - 1)]);
                        break;
                    }
                #endregion
                #region Set render for coconut_1
                case TileData.TileType.coconut_1:
                    {
						SetTranformTile(tile);
                        tile.SetMainRender(coconutSprite[0]);
                        break;
                    }
                #endregion
                #region Set render for coconut_2
                case TileData.TileType.coconut_2:
                    {
						SetTranformTile(tile);
                        tile.SetMainRender(coconutSprite[1]);
                        break;
                    }
                #endregion
                #region Set render for yogurt_cup
                case TileData.TileType.yogurt_cup:
                    {
						SetTranformTile(tile);
                        tile.SetMainRender(yogurtCupSprite);
                        break;
                    }
                #endregion
                #region Set render for yogurt
                case TileData.TileType.yogurt:
                    {
						SetTranformTile(tile);
                        tile.SetMainRender(yogurtSprite);
                        break;
                    }
                #endregion
                #region Set render for row_breaker
                case TileData.TileType.row_breaker:
                    {
						SetTranformTile(tile);
                        tile.SetMainRender(rowBreakerSprite[(int)tile.tileData.tileColor]);
                        break;
                    }
                #endregion
                #region Set render for column_breaker
                case TileData.TileType.column_breaker:
                    {
						SetTranformTile(tile);
                        tile.SetMainRender(columnBreakerSprite[(int)tile.tileData.tileColor]);
                        break;
                    }
                #endregion
                #region Set render for bomb_breaker
                case TileData.TileType.bomb_breaker:
                    {
						SetTranformTile(tile);
                        tile.SetMainRender(bombBreakerSprite[(int)tile.tileData.tileColor]);
                        break;
                    }
                #endregion
                #region Set render for rainbow
                case TileData.TileType.rainbow:
                    {
						SetTranformTile(tile);
                        tile.SetMainRender(rainbowSprite);
                        break;
                    }
                #endregion
                #region Set render for x_breaker
                case TileData.TileType.x_breaker:
                    {
						SetTranformTile(tile);
                        tile.SetMainRender(xBreakerSprite[(int)tile.tileData.tileColor]);
                        break;
                    }
                #endregion
                #region Set render for magnet
                case TileData.TileType.magnet:
                    {
						SetTranformTile(tile);
                        tile.SetMainRender(magnetSprite);
                        break;
                    }
                #endregion
                #region Set render for x_rainbow
                case TileData.TileType.x_rainbow:
                    {
						SetTranformTile(tile);
                        tile.SetMainRender(xRainbowSprite);
                        break;
                    }
                #endregion
                #region Set render for bomb_rainbow
                case TileData.TileType.bomb_rainbow:
                    {
						SetTranformTile(tile);
                        tile.SetMainRender(bombRainbowSprite);
                        break;
                    }
                #endregion
                default:
					SetTranformTile(tile);
                    break;
            }
        }
    }

	void EffectSugarColor(Tile tile)
	{
		//Debug.Log ("run");
		GameObject rainbowCircle = Instantiate (colorCircle);
		rainbowCircle.transform.position = tile.transform.position;
		rainbowCircle.transform.localScale = Vector3.one*0.1f;

		LeanTween.scale (rainbowCircle, Vector3.one * 3, 1f).setLoopOnce ().setOnComplete (
			() => {
				rainbowCircle.SetActive(false);
				LeanTween.cancel(rainbowCircle);
				Destroy (rainbowCircle);
			}
		);
	}



    /// <summary>
    /// Set enhance render for tile
    /// </summary>
    /// <param name="tile"></param>
    public void SetTileEnhanceRender(Tile tile)
    {
        if (tile != null)
        {
            // TODO: Optimize: material bi thay doi
            if (tile.enhanceRender.sharedMaterial != defaultMaterial)
            {
                tile.enhanceRender.material = defaultMaterial;
            }

            switch (tile.tileData.tileType)
            {
                case TileData.TileType.cow:
                    {
                        //tile.SetEnhanceRender(cowSprite[1]);
                        try
                        {
                            SkeletonAnimation ske = tile.enhanceRender.transform.GetChild(0).GetChild(0).GetComponent<SkeletonAnimation>();
                            if (ske != null)
                            {
                                return;
                            }
                        }
                        catch (System.Exception e)
                        {
                            Debug.Log(e.Message);
                        }

                        GameObject cowObj = (GameObject)Instantiate(cowPrefab, Vector3.zero, Quaternion.identity);
                        cowObj.transform.SetParent(tile.enhanceRender.transform);
                        cowObj.transform.localPosition = Vector3.zero;
                        cowObj.transform.localScale = Vector3.one;
                        break;
                    }
                default:
                    {
                        //tile.SetEnableSpriteRender(tile.enhanceRender, false);
                        break;
                    }
            }

            switch (tile.tileData.tileModifierType)
            {
                case TileData.TileModifierType.ice_cage:
                    {
                        tile.SetEnhanceRender(iceCageSprite);
                        //LeanTween.alpha(tile.enhanceRender.gameObject, 1f, 0f);
                        break;
                    }
                default:
                    {
                        //tile.SetEnableSpriteRender(tile.enhanceRender, false);
                        break;
                    }
            }
        }
    }
    #endregion

    #region Check valid and reset board
    /// <summary>
    /// Check if there is any match from loaded spawn
    /// </summary>
    public bool CheckInitialMatch()
    {
        for (int i = 0; i < mapController.gridList.Count; i++)
        {
            Grid g = mapController.gridList[i];
            if (mapController.mapConditionChecker.CheckHitInGrid(g) 
                || mapController.mapConditionCheckerMatch2x2.CheckHitInGrid(g))
            {
                return true;
            }
        }

        return !(mapController.mapConditionChecker.CheckAvailableMove()
            || mapController.mapConditionCheckerMatch2x2.CheckAvailableMove());
    }

    /// <summary>
    /// ResetBoard
    /// </summary>
    /// <param name="swappableGrids"></param>
    public void ResetBoard(List<Grid> swappableGrids)
    {
        int count = 0;

        // For test
        RandomSwapTiles(swappableGrids);
        // End test

        while (CheckInitialMatch()) // Loop until reached a point
        {
            count++;

            //Debug.Log("ResetBoard:::::::: " + count);

            if (count > GameConstants.MAX_RESET)
            {
                Debug.Log("ResetBoard:::::::: No any move");

                //GameplayController.instance.finalTryFlag = true;
                GamePlayController.gamePlayController.noAnyMove = true;

                return;
            }

            RandomSwapTiles(swappableGrids);
        }
    }

    /// <summary>
    /// Randomly swap tiles in map
    /// </summary>
    public void RandomSwapTiles(List<Grid> swappableGrids)
    {
        for (int i = 0; i < ((swappableGrids.Count + 1) / 2); i++)
        {
            // Select
            Grid grid1 = swappableGrids[Random.Range(0, swappableGrids.Count)];
            Grid grid2 = swappableGrids[Random.Range(0, swappableGrids.Count)];

            // Swap
            Tile tile1 = grid1.GetTile();
            Tile tile2 = grid2.GetTile();

            grid1.SetTile(tile2, false);
            grid2.SetTile(tile1, false);
        }
    }
    #endregion

    #region Handle Shrink, Spawn Tile
    
    /// <summary>
    /// List chua cac index phia duoi cung cua map (index start cho qua trinh check spawn)
    /// </summary>
    //List<List<int>> startedIndexList;

    /// <summary>
    /// List chua cac index lan luot de check spawn
    /// </summary>
    //List<List<int>> indexList;

    /// <summary>
    /// Huong roi theo trong luc cua cac tile
    /// </summary>
    public Grid.Direction gravitationDirection = Grid.Direction.BOTTOM;

    // cache
    public List<Grid> processingGroup;
    public List<int> startedIndexList;
    public List<int> indexList;

    // data
    List<Grid> predefinedGrids;
    List<Grid> ignoredGrids; // Gem won't spawn here

    /// <summary>
    /// Init Data to process spawn
    /// </summary>
    public void InitProcessData()
    {
        // data
        predefinedGrids = new List<Grid>();
        ignoredGrids = new List<Grid>();

        // cache
        processingGroup = new List<Grid>();

        ConfigureIndexLists();
    }

    /// <summary>
    /// Config List of index
    /// </summary>
    void ConfigureIndexLists()
    {
        startedIndexList = new List<int>();
        indexList = new List<int>();

        startedIndexList = MapUtilities.GetStartedIndexList(mapController.gridList, gravitationDirection);
        
//		foreach (int count in startedIndexList) 
//		{
//			Debug.Log ("count_"+count);
//		}

        SetStartedGrids(startedIndexList);

        //indexList = MapUtilities.GetIndexList(Contance.width, Contance.height, gravitationDirection);
        indexList = MapUtilities.GetIndexListFromStartedIndex(mapController.gridList, 
            startedIndexList, gravitationDirection);
    }

    /// <summary>
    /// Create Started Grid List
    /// </summary>
    /// <param name="idList"></param>
    void SetStartedGrids(List<int> idList)
    {
        //startedGridList = new List<Grid>();
        for (int i = 0; i < idList.Count; i++)
        {
            //startedGridList.Add(mapController.gridList[idList[i]]);
            mapController.gridList[idList[i]].isBottom = true;
        }
    }

    /// <summary>
    /// Spawn tiles with Grid Direction
    /// </summary>
    /// <param name="direction"></param>
    /// <returns></returns>
    public float SpawnTiles(Grid.Direction direction)
    {
        float maxSpawnTime = 0;

        List<int> idList = startedIndexList; // startedIndexList[(int)direction];
        for (int i = 0; i < idList.Count; i++)
        {
            Grid grid = mapController.gridList[idList[i]];
            // Re-check for sure, maybe not even nescessary
            if (grid == null || grid.gridData.gridType == GridData.GridType.NULL_GRID)
            {
                continue;
            }

            if (ignoredGrids.Contains(grid))
            {
                continue;
            }

            // Try to respawn
            if (GameConstants.ENABLE_PATCH_SPAWNER)
            {
                float t = SpawnTileAtGridPatch(grid, direction);
                maxSpawnTime = maxSpawnTime > t ? maxSpawnTime : t;
            }
            //else
            //ChangesDetect |= SpawnTileAtGrid(grid, direction);
        }

        return (maxSpawnTime);
    }

    /// <summary>
    /// Spawn whole pack of gem
    /// </summary>
    float SpawnTileAtGridPatch(Grid rootGrid, Grid.Direction direction, bool skipQueued = false)
    {
        float timeForSpawn = 0; // used to determine if there is anything change here
        // Check each group, find all grid followed
        processingGroup.Clear();
        processingGroup.Add(rootGrid); // starter

        // Find counter gravity dir
        Grid.Direction opposedDir = Grid.GetOpposedDir(direction);

        if (rootGrid.gridData.gridStaticType == GridData.GridStaticType.SPAWNER_GRID)
        {

        }
        else
        {
            Grid nextGrid = rootGrid.GetGridInDir(opposedDir); // find next
			while (nextGrid != null && nextGrid != rootGrid && !nextGrid.isBottom)
            {
                processingGroup.Add(nextGrid);
                if (nextGrid.gridData.gridStaticType == GridData.GridStaticType.SPAWNER_GRID)
                {
                    break;
                }

                if (nextGrid.gridData.gridStaticType == GridData.GridStaticType.PORTAL_GRID
                    && nextGrid.portal != null && nextGrid.portal.portalType == Portal.PortalType.OUTLET
                    && nextGrid.portal.portalMatched != null && nextGrid.portal.portalMatched.grid != null)
                {
                    //Debug.Log("HasPortal : HasPortal : HasPortal : HasPortal");
                    nextGrid = nextGrid.portal.portalMatched.grid;
                    //break;
                }
                else
                {
                    nextGrid = nextGrid.GetGridInDir(opposedDir);
                }

                if (nextGrid != null && nextGrid.isBottom)
                {
                    Grid dirGrid = nextGrid.GetGridInDir(direction);
                    while (dirGrid != null && dirGrid.gridData.gridType == GridData.GridType.NULL_GRID)
                    {
                        processingGroup.Remove(dirGrid);
                        dirGrid = dirGrid.GetGridInDir(direction);
                    }
                }
            }
        }

        //Debug.Log("processingGroup: " + processingGroup.Count);

        bool hasPortal = false;
        Grid portalInGrid = null;
        Grid portalOutGrid = null;
        int indexOfPortalOut = 0;

        // Process
        bool lineOfSkyClear = false; // used to check if this grid have a line to sky, a tile will fall from heaven to it =))
        int lineOfSkySpawnPos = 0; // from what grid the spawn will start at

        if (processingGroup[processingGroup.Count - 1].gridData.gridStaticType != GridData.GridStaticType.SPAWNER_GRID)
        {
            lineOfSkyClear = false;
        }
        else
        {
            for (int i = 0; i < processingGroup.Count; i++)
            {
                Grid currentGrid = processingGroup[i];
                // If it already have a tile, why bother try to spawn?
                if (currentGrid.GetTile() != null)
                {
                    continue;
                }

                if (currentGrid.gridData.gridStaticType == GridData.GridStaticType.PORTAL_GRID
                    && currentGrid.portal != null && currentGrid.portal.portalType == Portal.PortalType.OUTLET
                    && currentGrid.portal.portalMatched != null && currentGrid.portal.portalMatched.grid != null)
                {
                    indexOfPortalOut = i;
                    portalOutGrid = currentGrid;
                    portalInGrid = currentGrid.portal.portalMatched.grid;
                }

                // Finding
                bool loS = true; // line of sky checker
                for (int j = i + 1; j < processingGroup.Count; j++)
                {
                    Grid g = processingGroup[j];
                    Tile t = g.GetTile();

                    if (g.gridData.gridStaticType == GridData.GridStaticType.PORTAL_GRID
                    && g.portal != null && g.portal.portalType == Portal.PortalType.OUTLET
                    && g.portal.portalMatched != null && g.portal.portalMatched.grid != null)
                    {
                        indexOfPortalOut = j;
                        portalOutGrid = g;
                        portalInGrid = g.portal.portalMatched.grid;
                    }

                    if (t == null)
                    {
                        continue;
                    }
                    else
                    {
                        loS = false;
                        break;
                    }

                }
                // Finish

                if (loS)
                {
                    lineOfSkyClear = true;
                    lineOfSkySpawnPos = i;
                    break;
                }
            }
        }

        if (lineOfSkyClear) // instant spawn if have line of sky or gravity is centered
        {
            int counter = 0;
            Grid topGrid = processingGroup[processingGroup.Count - 1];
            for (int i = lineOfSkySpawnPos; i < processingGroup.Count; i++)
            {
                if (portalOutGrid != null && indexOfPortalOut >= i)
                {
                    //Debug.Log("Spawn portal");
                    hasPortal = true;
                }
                else
                {
                    hasPortal = false;
                }

                Grid g = processingGroup[i];
                // Fill Spawn
                if (g != null && g.gridData.gridType != GridData.GridType.NULL_GRID)
                {
                    // Create Tile For test
                    //Tile newTile = CreateNewTile();
                    Tile newTile = PoolingManager.poolingManager.GetTile();
                    TileData tileData = mapController.tileManager.GetTileData(topGrid);
                    newTile.SetTileData(tileData);
                    //string color = GetRandomTileColor();
                    //newTile.SetTileType(TileData.TileType.match_object);
                    //newTile.SetTileColor(color);
                    // End test

                    // TODO: Get from pool
                    //Tile newTile = ObjectManager.Instance.GetTile();
                    //newTile.transform.SetParent(g.dynamicComponents, false);

                    // TODO: Config newly created tile
                    //mapController.tileManager.TileSpawn(newTile, null, skipQueued);

                    // Find position
                    int top = processingGroup.Count - i + counter;
                    Vector3 topPos = Grid.GetMapStepWithDirection(opposedDir) * top;
                    // SetPosition
                    //newTile.transform.localPosition = topPos;
                    newTile.UpdateTileDisplay();
                    //newTile.SetTileActive(false);
                    g.SetTile(newTile);

                    // 
                    float time = 0;
                    if (direction == Grid.Direction.CENTER)
                    {
                        time = 0.3f;
						time += g.MoveTileToCenter(time, Tile.MoveFinishType.GROW);
						//SoundController.sound.dropSoundPlay ();
                    }
                    else
                    {
                        Vector3 spawnerPosition = new Vector3(topGrid.gridPosition.Column, topGrid.gridPosition.Row + 1f, 0f)
                            - new Vector3(g.gridPosition.Column, g.gridPosition.Row, 0f);

                        //hasPortal = false;
                        if (hasPortal)
                        {
                            float timeToMoveToSpawner = (top - Mathf.Abs(topGrid.gridPosition.Row - (portalInGrid.gridPosition.Row - 1)) 
                                - Mathf.Abs(g.gridPosition.Row - (portalOutGrid.gridPosition.Row + 1))) 
                                * GameConstants.SPEED_DROP_TILE_EACH_STEP * Grid.GetDropTimeModifierWithDirection(direction);

                            Vector3 portalInPosition = new Vector3(portalInGrid.gridPosition.Column, portalInGrid.gridPosition.Row - 1f, 0f)
                                - new Vector3(g.gridPosition.Column, g.gridPosition.Row, 0f);
                            Vector3 portalOutPosition = new Vector3(portalOutGrid.gridPosition.Column, portalOutGrid.gridPosition.Row + 1f, 0f)
                                - new Vector3(g.gridPosition.Column, g.gridPosition.Row, 0f);

                            float timeToInPortal = Mathf.Abs(topGrid.gridPosition.Row + 1 - (portalInGrid.gridPosition.Row - 1))
                                * GameConstants.SPEED_DROP_TILE_EACH_STEP * Grid.GetDropTimeModifierWithDirection(direction);
                            float timeToMoveCenterGrid = Mathf.Abs(g.gridPosition.Row - (portalOutGrid.gridPosition.Row + 1))
                                * GameConstants.SPEED_DROP_TILE_EACH_STEP * Grid.GetDropTimeModifierWithDirection(direction);

                            time = timeToMoveToSpawner + timeToInPortal + timeToMoveCenterGrid;

                            LeanTween.delayedCall(timeToMoveToSpawner, () =>
                                {
									if(g.GetTile() != null)
									{
	                                    g.GetTile().transform.localPosition = spawnerPosition;
	                                    //g.GetTile().SetTileActive(true);
										if(i == lineOfSkySpawnPos)
										{
		                                    g.MoveTileThroughPortal(portalInPosition, portalOutPosition, 
												timeToInPortal, timeToMoveCenterGrid, Tile.MoveFinishType.BOUNCE, true);
										}
										else
										{
											g.MoveTileThroughPortal(portalInPosition, portalOutPosition, 
												timeToInPortal, timeToMoveCenterGrid, Tile.MoveFinishType.BOUNCE);
										}
									}
                                });
							//Debug.Log ("A");
                        }
                        else
                        {
                            float timeToMoveToSpawner = (top - spawnerPosition.y) * GameConstants.SPEED_DROP_TILE_EACH_STEP
                                * Grid.GetDropTimeModifierWithDirection(direction);

                            float timeToMoveCenterGrid = (topGrid.gridPosition.Row + 1 - g.gridPosition.Row)
                                * GameConstants.SPEED_DROP_TILE_EACH_STEP * Grid.GetDropTimeModifierWithDirection(direction);

//                            time = top * GameConstants.SPEED_DROP_TILE_EACH_STEP * Grid.GetDropTimeModifierWithDirection(direction);
//                            time += g.MoveTileToCenter(time, Tile.MoveFinishType.BOUNCE);

							if (i == lineOfSkySpawnPos) 
							{
								time = timeToMoveToSpawner + timeToMoveCenterGrid + g.MoveTileFromSpawner (spawnerPosition,
									timeToMoveToSpawner, timeToMoveCenterGrid, Tile.MoveFinishType.BOUNCE, true);
							}
							else 
							{
								time = timeToMoveToSpawner + timeToMoveCenterGrid + g.MoveTileFromSpawner (spawnerPosition,
									timeToMoveToSpawner, timeToMoveCenterGrid, Tile.MoveFinishType.BOUNCE);
							}
							//Debug.Log ("B");
                        }
                    }

                    timeForSpawn = timeForSpawn > time ? timeForSpawn : time;
					counter++;
                }
            }
        }
		//SoundController.sound.dropSoundPlay ();

        return timeForSpawn;
    }

    /// <summary>
    /// Shrink tiles with Grid Direction
    /// </summary>
    /// <param name="direction"></param>
    /// <param name="diagonalDrop"></param>
    /// <returns></returns>
    public float ShrinkTiles(Grid.Direction direction, bool diagonalDrop = false)
    {
        float maxDropTime = 0;

        List<int> idList = indexList; // indexList[(int)direction];

        // Try to shrink
        if (diagonalDrop)
        {
            for (int i = 0; i < idList.Count; i++)
            {
                Grid grid = mapController.gridList[idList[i]];
                // Re-check for sure, maybe not even nescessary
                if (grid == null || grid.gridData.gridType == GridData.GridType.NULL_GRID)
                {
                    continue;
                }

                Tile tile = grid.GetTile();
                if (tile == null || !tile.isDroppable || tile.isMoving || tile.isOnClearingProcess)
                {
                    continue;
                }

                float t = ShrinkTileAtGridDiagonal(grid, direction);
                maxDropTime = maxDropTime > t ? maxDropTime : t;
            }
        }
        else
        {
            for (int i = 0; i < idList.Count; i++)
            {
                Grid grid = mapController.gridList[idList[i]];

                // Re-check for sure, maybe not even nescessary
                if (grid == null || grid.gridData.gridType == GridData.GridType.NULL_GRID)
                {
                    continue;
                }

                if (grid.GetTile() != null)
                {
                    continue;
                }

                bool hasDropPortal = false;
                float t = 0f;
                t = ShrinkTileAtGrid(grid, direction, ref hasDropPortal);

                maxDropTime = maxDropTime > t ? maxDropTime : t;
            }
        }

        return (maxDropTime);
    }

    /// <summary>
    /// Normal shrink tile at grid
    /// </summary>
    /// <param name="rootGrid"></param>
    /// <param name="direction"></param>
    /// <returns></returns>
    float ShrinkTileAtGrid(Grid rootGrid, Grid.Direction direction, ref bool hasDropPortal)
    {
        // Exception
        if (direction == Grid.Direction.CENTER || direction == Grid.Direction.UNKNOWN)
        {
            return 0;
        }
        //==========

        float timeToDrop = 0; // used to determine if there is anything change here
        // Check if there is any direct drop (drop in main direction)

        // Find counter gravity dir
        Grid.Direction opposedDir = Grid.GetOpposedDir(direction);

        bool hasPortal = false;

        Grid nextGrid = null;
        Grid portalInGrid = null;
        Grid portalOutGrid = null;

        if (rootGrid.gridData.gridStaticType == GridData.GridStaticType.PORTAL_GRID
            && rootGrid.portal != null && rootGrid.portal.portalType == Portal.PortalType.OUTLET
            && rootGrid.portal.portalMatched != null && rootGrid.portal.portalMatched.grid != null)
        {
            hasPortal = true;
            portalOutGrid = rootGrid;
            portalInGrid = rootGrid.portal.portalMatched.grid;

            nextGrid = rootGrid.portal.portalMatched.grid;
        }
        else
        {
            nextGrid = rootGrid.GetGridInDir(opposedDir); // find next
        }

        Grid targetGrid = null; // the grid that will drop here
        //bool haveLineOfSky = true;
        int counter = 1; // count number of grid it passed
        while (nextGrid != null && nextGrid != rootGrid && !nextGrid.isBottom)
        {
            Tile nextTile = nextGrid.GetTile();
            if (nextTile == null)
            {
                // Handle portal
                if (nextGrid.gridData.gridStaticType == GridData.GridStaticType.PORTAL_GRID
                    && nextGrid.portal != null && nextGrid.portal.portalType == Portal.PortalType.OUTLET
                    && nextGrid.portal.portalMatched != null && nextGrid.portal.portalMatched.grid != null)
                {
                    hasPortal = true;
                    portalOutGrid = nextGrid;
                    portalInGrid = nextGrid.portal.portalMatched.grid;

                    nextGrid = nextGrid.portal.portalMatched.grid;
                }
                // end hand portal
                else
                {
                    nextGrid = nextGrid.GetGridInDir(opposedDir);
                }

                counter++;
                continue;
            }

            if (!nextTile.isDroppable || nextTile.isMoving || nextTile.isOnClearingProcess)
            {
                //haveLineOfSky = false;
                break;
            }

            if (nextTile.isDroppable)
            {
                targetGrid = nextGrid;
                break;
            }

            nextGrid = nextGrid.GetGridInDir(opposedDir);
            counter++;
        }

        // Drop down
        if (targetGrid != null && targetGrid.gridData.gridType != GridData.GridType.NULL_GRID && !nextGrid.GetTile().isMoving)
        {
            rootGrid.SetTile(targetGrid.GetTile());
            targetGrid.SetTile(null);

            int moveLength = counter;
            float time = 0f;
            if (hasPortal)
            {
                Vector3 portalInPosition = new Vector3(portalInGrid.gridPosition.Column, portalInGrid.gridPosition.Row - 1f, 0f)
                    - new Vector3(rootGrid.gridPosition.Column, rootGrid.gridPosition.Row, 0f);
                Vector3 portalOutPosition = new Vector3(portalOutGrid.gridPosition.Column, portalOutGrid.gridPosition.Row + 1f, 0f)
                    - new Vector3(rootGrid.gridPosition.Column, rootGrid.gridPosition.Row, 0f);

                float timeToInPortal = Mathf.Abs(targetGrid.gridPosition.Row - (portalInGrid.gridPosition.Row - 1)) 
                    * GameConstants.SPEED_DROP_TILE_EACH_STEP * Grid.GetDropTimeModifierWithDirection(direction);
                float timeToMoveCenterGrid = Mathf.Abs(rootGrid.gridPosition.Row - (portalOutGrid.gridPosition.Row + 1))
                    * GameConstants.SPEED_DROP_TILE_EACH_STEP * Grid.GetDropTimeModifierWithDirection(direction);

                time = timeToInPortal + timeToMoveCenterGrid + rootGrid.MoveTileThroughPortal(
                    portalInPosition, portalOutPosition, timeToInPortal, timeToMoveCenterGrid, Tile.MoveFinishType.BOUNCE);

                //rootGrid.GetTile().transform.localPosition = new Vector3(0f, moveLength, 0f);
            }
            else
            {
                time = moveLength * GameConstants.SPEED_DROP_TILE_EACH_STEP * Grid.GetDropTimeModifierWithDirection(direction);
                time += rootGrid.MoveTileToCenter(time, Tile.MoveFinishType.BOUNCE);
				//SoundController.sound.dropSoundPlay ();
            }

            timeToDrop = time;
        }

        hasDropPortal = hasPortal;

        return timeToDrop;
    }

    /// <summary>
    /// Diagonal Shrink tile at grid
    /// </summary>
    /// <param name="rootGrid"></param>
    /// <param name="direction"></param>
    /// <returns></returns>
    float ShrinkTileAtGridDiagonal(Grid rootGrid, Grid.Direction direction)
    {
        // Exception
        if (direction == Grid.Direction.CENTER || direction == Grid.Direction.UNKNOWN)
        {
            return 0;
        }
        //==========

        float timeToDrop = 0; // used to determine if there is anything change here
        // Check if there is any direct drop (drop in main direction)

        // Find counter gravity dir
        Grid.Direction opposedDir = Grid.GetOpposedDir(direction);

        // Try adjacent
        List<Grid.Direction> adjacentList = Grid.GetDroppableDiagonalDirections(direction);

        for (int i = 0; i < adjacentList.Count; i++)
        {
            Grid.Direction dir = adjacentList[i];

            Grid g = rootGrid.GetGridInDir(dir);
            if (g == null || g.gridData.gridType == GridData.GridType.NULL_GRID)
            {
                continue;// mayDropGridList.Add(fGrid);
            }

            Grid gravityAboveGrid = g.GetGridInDir(opposedDir);

            if (gravityAboveGrid != null && gravityAboveGrid.gridData.gridType != GridData.GridType.NULL_GRID)
            {
                // Check if can be dropped below
                if (gravityAboveGrid.GetTile() != null && gravityAboveGrid.GetTile().isDroppable)
                {
                    continue;
                }
            }

            Grid gravityBelowGrid = g.GetGridInDir(direction);

            if (gravityBelowGrid != null && gravityBelowGrid.gridData.gridType != GridData.GridType.NULL_GRID)
            {
                if (gravityBelowGrid.GetTile() != null && gravityBelowGrid.GetTile().isMoving)
                {
                    continue;
                }

                Grid evenLowerGrid = gravityBelowGrid.GetGridInDir(direction);
                bool realEmptyBelow = true;
                while (evenLowerGrid != null && evenLowerGrid.gridData.gridType != GridData.GridType.NULL_GRID)
                {
                    if (evenLowerGrid.GetTile() != null && evenLowerGrid.GetTile().isMoving)
                    {
                        realEmptyBelow = false;
                        break;
                    }

                    evenLowerGrid = evenLowerGrid.GetGridInDir(direction);
                }

                if (!realEmptyBelow)
                {
                    continue; // bigger continue
                }
            }

            if (g.GetTile() != null)
            {
                continue;
            }

            g.SetTile(rootGrid.GetTile());
            rootGrid.SetTile(null);

            int moveLength = 1;
            float time = moveLength * GameConstants.SPEED_DROP_TILE_EACH_STEP * Grid.GetDropTimeModifierWithDirection(dir);
            time += g.MoveTileToCenter(time, Tile.MoveFinishType.NO_ACTION_DIAGONAL);
			//SoundController.sound.dropSoundPlay ();

            timeToDrop = time / 2f;
            break;

        }

        // TESTING: return 0 to optimize
        return 0f;
    }

    #endregion
}

﻿/*
 * @Author: Nguyen Bui
 * @Modify by: CuongNH
 * @Description: Xu ly hieu ung no more move
 * */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.UI;

public class NoMoreMoveBottle : MonoBehaviour {

    public List<Tile> tileList;
    //public tk2dSprite bottleSprite;
    public Transform bottleBase;
    public Transform midleBottle;
    public Transform bottleCap;
	public Transform bottleRest;

    public Transform bottomPoint, topPoint;
    public Transform originPoint, showupPoint;

    public bool animating = false;

    public string tileLayerName = "Tile";
    public string effectLayerName = "Effect";

    public float changeLayerToTileTime;
    public float changeLayerToEffectTime;

    /// <summary>
    /// Show bottle
    /// </summary>
    /// <param name="delayTime"></param>
    /// <returns></returns>
    public float ShowBottle(float delayTime)
    {
        animating = true;
        float time = 0.5f;

        LeanTween.delayedCall(delayTime, () =>
        {
            bottleBase.position = originPoint.position;

            LeanTween.move(bottleBase.gameObject, showupPoint.position, time).setOnComplete(() =>
                {
                    LeanTween.delayedCall(changeLayerToTileTime, () =>
                    {
								//GamePlayController.gamePlayController.mapController.
                        midleBottle.GetComponent<SpriteRenderer>().sortingLayerName = tileLayerName;
                    });
                });
        });

        return time;
    }

    /// <summary>
    /// Hide bottle
    /// </summary>
    /// <param name="delayTime"></param>
    /// <returns></returns>
    public float HideBottle(float delayTime)
    {
        float time = 0.5f;
        LeanTween.delayedCall(delayTime, () =>
        {
            bottleBase.position = showupPoint.position;
            LeanTween.move(bottleBase.gameObject, originPoint.position, time)
                .setOnComplete(() =>
                {
                    animating = false;
                });
        });
        return time;
    }

    /// <summary>
    /// Open bottle
    /// </summary>
    /// <param name="delayTime"></param>
    /// <returns></returns>
    public float OpenCap(float delayTime)
    {
        float moveTime = 0.2f;
        LeanTween.delayedCall(delayTime, () =>
        {
            Vector3 rootPostion = bottomPoint.transform.position;
            Vector3[] route = new Vector3[] {Vector3.zero, Vector3.zero, 
                topPoint.transform.localPosition, new Vector3(1.5f, 0.5f, 0), new Vector3(1.5f, 0.5f, 0) };

            LeanTween.moveSplineLocal(bottleCap.gameObject, route, moveTime);

            LeanTween.delayedCall(moveTime / 2f, () =>
                {
                    LeanTween.rotate(bottleCap.gameObject, new Vector3(0, 0, -60), moveTime / 2f);
                });
            
            //openedTop.SetActive(true);
            //closedTop.SetActive(false);
        });
        return moveTime;
    }

    /// <summary>
    /// Close bottle
    /// </summary>
    /// <param name="delayTime"></param>
    /// <returns></returns>
    public float CloseCap(float delayTime)
    {
        float moveTime = 0.2f;
        LeanTween.delayedCall(delayTime, () =>
        {

            Vector3[] route = new Vector3[] {new Vector3(1.5f, 0.5f, 0), new Vector3(1.5f, 0.5f, 0), 
                topPoint.transform.localPosition, Vector3.zero, Vector3.zero };

            LeanTween.moveSplineLocal(bottleCap.gameObject, route, moveTime);

            LeanTween.delayedCall(moveTime / 2f, () =>
            {
                LeanTween.rotate(bottleCap.gameObject, new Vector3(0, 0, 0), moveTime / 2f);
            });

            //openedTop.SetActive(false);
            //closedTop.SetActive(true);
        });

        return moveTime;
    }

    /// <summary>
    /// Active reset effect
    /// </summary>
    /// <param name="delayTime"></param>
    /// <returns></returns>
    public float ActiveSwapping(float delayTime)
    {
        float moveTime = 1f;
        LeanTween.delayedCall(delayTime, () =>
        {
            //LeanTween.rotateAround(bottleBase.gameObject, Vector3.forward, 360f, moveTime / 5f).setRepeat(5);
			SoundController.sound.BlendedSound();
            LeanTween.rotateLocal(bottleBase.gameObject, new Vector3(0, 0, -30), moveTime / 10f * 2f)
                .setOnComplete(() =>
                {
                    LeanTween.rotateLocal(bottleBase.gameObject, new Vector3(0, 0, 30), moveTime / 10f)
                        .setLoopPingPong().setRepeat(3).setOnComplete(() =>
                        {
                            LeanTween.rotateLocal(bottleBase.gameObject, new Vector3(0, 0, 0), moveTime / 10f * 2f);
                        });
                });
        });

        return moveTime;
    }

    /// <summary>
    /// Put tiles in bottle
    /// </summary>
    /// <param name="t"></param>
    /// <param name="delayTime"></param>
    /// <returns></returns>
    public float PutTileInBottle(Tile t, float delayTime)
    {
        // move tile to bottle
        float range = 0;
        float moveTime = PreConvertingEffect(1, t.transform, bottomPoint.transform, ref range) * 0.75f;
        LeanTween.delayedCall(delayTime, () =>
        {
            Vector3 rootPostion = bottomPoint.transform.position;
            Vector3[] route = new Vector3[] {t.transform.position, t.transform.position, 
                topPoint.transform.position, rootPostion, rootPostion };

				LeanTween.moveSpline(t.gameObject, route, moveTime);
				t.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 2;
		
            tileList.Add(t);
        });

        return (moveTime + delayTime);
    }

    /// <summary>
    /// Return tiles to grids
    /// </summary>
    /// <param name="delayTime"></param>
    /// <returns></returns>
    public float ReturnAllTilesToGrids(float delayTime)
    {
        float moveTime = 1f;

        LeanTween.delayedCall(delayTime, () =>
        {
            for (int i = 0; i < tileList.Count; i++)
            {
                Tile t = tileList[i];

                if (t.grid != null)
                {
                    Vector3 rootPostion = Vector3.zero; 
                    // new Vector3(t.grid.transform.position.x, t.grid.transform.position.y, t.grid.transform.position.z + GameConsts.TILE_LOCAL_Z);
                    Vector3[] route = new Vector3[] {t.transform.position, t.transform.position, 
                        topPoint.transform.position, rootPostion, rootPostion };
                    LeanTween.move(bottleBase.gameObject, bottleBase.transform.position
                        + topPoint.transform.position / 3f, moveTime / 10f).setLoopPingPong().setRepeat(2);

                    LeanTween.moveSpline(t.gameObject, route, moveTime).setOnComplete(() =>
                    {
                        t.grid.MoveTileToCenter(0.1f);
						t.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 0;
                    });
                }

            }

            LeanTween.delayedCall(changeLayerToEffectTime, () =>
            {
                midleBottle.GetComponent<SpriteRenderer>().sortingLayerName = effectLayerName;
            });

            tileList.Clear();
        });

        return moveTime;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns>Time used to do effect</returns>
    private float PreConvertingEffect(float scaleRate, Transform rootTile, Transform targetTile, ref float getRange)
    {
        float range = Vector2.Distance(new Vector2(rootTile.position.x, rootTile.position.y), 
            new Vector2(targetTile.position.x, targetTile.position.y));
        range *= scaleRate; //real range
        float moveTime = range * GameConstants.SPEED_PER_GRID;

        getRange = range;
        return moveTime;
    }
}

﻿/*
 * @Author: CuongNH
 * @Description: Get item when user touch
 * */

using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class TouchInputHandler : MonoBehaviour {

    public LayerMask touchInputMask;
    public IGameInputControl inputReceiver;

    public static Vector2 LastestTouchPosition;

    private List<GameObject> touchList = new List<GameObject>();
    private GameObject[] touchesOld;
    private RaycastHit2D hit;

    // Update is called once per frame
    public void UpdateUserInput()
    {
        if (inputReceiver == null)
        {
            return; // Don't have receiver
        }

		#if UNITY_EDITOR || UNITY_STANDALONE_WIN
        if (Input.GetMouseButton(0) || Input.GetMouseButtonDown(0) || Input.GetMouseButtonUp(0))
		{
            touchesOld = new GameObject[touchList.Count];
            touchList.CopyTo(touchesOld);
            touchList.Clear();


            Vector2 ray = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            hit = Physics2D.Raycast(ray, Vector2.zero, Mathf.Infinity, touchInputMask);

            LastestTouchPosition = ray;

            if (hit != null && hit.collider != null)
            {

                GameObject recipient = hit.transform.gameObject;
                touchList.Add(recipient);

                if (Input.GetMouseButtonDown(0))
                {
					inputReceiver.UpdateInput(ray, recipient, GameEnum.InputState.BEGIN);GamePlayController.gamePlayController.isCheckColorGrid =false;
                    //recipient.SendMessage("OnTouchDown", hit.point, SendMessageOptions.DontRequireReceiver);

                    // DEBUG Message system
                    //if (GameMessageManager.Instance != null)
                    //    GameMessageManager.Instance.SendMessageToPopup("touchbegin");
                }
                if (Input.GetMouseButtonUp(0))
                {
                    inputReceiver.UpdateInput(ray, recipient, GameEnum.InputState.END);
                    //recipient.SendMessage("OnTouchUp", hit.point, SendMessageOptions.DontRequireReceiver);

                    // DEBUG Message system
                    //if (GameMessageManager.Instance != null)
                    //    GameMessageManager.Instance.SendMessageToPopup("touchend");
                }
                if (Input.GetMouseButton(0))
                {
                    inputReceiver.UpdateInput(ray, recipient, GameEnum.InputState.MOVE);
                    //recipient.SendMessage("OnTouchStay", hit.point, SendMessageOptions.DontRequireReceiver);

                    // DEBUG Message system
                    //if (GameMessageManager.Instance != null)
                    //    GameMessageManager.Instance.SendMessageToPopup("touchmove");
                }

            }

            foreach (GameObject g in touchesOld)
            {
                if (!touchList.Contains(g))
                {
                    //g.SendMessage("OnTouchExit", hit.point, SendMessageOptions.DontRequireReceiver);
                }
            }
        }
#endif

        if (Input.touchCount > 0)
		{
            touchesOld = new GameObject[touchList.Count];
            touchList.CopyTo(touchesOld);
            touchList.Clear();

            Touch touch = Input.touches[0];

            //foreach (Touch touch in Input.touches)
            //{
                Vector2 ray = Camera.main.ScreenToWorldPoint(touch.position);
                hit = Physics2D.Raycast(ray, Vector2.zero, Mathf.Infinity, touchInputMask);

                // save position
                LastestTouchPosition = ray;

                if (hit != null && hit.collider != null)
                {

                    GameObject recipient = hit.transform.gameObject;
                    touchList.Add(recipient);

                    if (touch.phase == TouchPhase.Began)
                    {
					inputReceiver.UpdateInput(ray, recipient, GameEnum.InputState.BEGIN);GamePlayController.gamePlayController.isCheckColorGrid =false;
                        //recipient.SendMessage("OnTouchDown", hit.point, SendMessageOptions.DontRequireReceiver);

                        // DEBUG Message system
                        //if (GameMessageManager.Instance != null)
                        //    GameMessageManager.Instance.SendMessageToPopup("touchbegin");

                    }
                    if (touch.phase == TouchPhase.Ended)
                    {
                        inputReceiver.UpdateInput(ray, recipient, GameEnum.InputState.END);
                        //recipient.SendMessage("OnTouchUp", hit.point, SendMessageOptions.DontRequireReceiver);

                        // DEBUG Message system
                        //if (GameMessageManager.Instance != null)
                        //    GameMessageManager.Instance.SendMessageToPopup("touchend");

                    }
                    if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved)
                    {
                        inputReceiver.UpdateInput(ray, recipient, GameEnum.InputState.MOVE);
                        //recipient.SendMessage("OnTouchStay", hit.point, SendMessageOptions.DontRequireReceiver);

                        // DEBUG Message system
                        //if (GameMessageManager.Instance != null)
                        //    GameMessageManager.Instance.SendMessageToPopup("touchmove");

                    }
                    if (touch.phase == TouchPhase.Canceled)
                    {
                        inputReceiver.UpdateInput(ray, recipient, GameEnum.InputState.CANCEL);
                        //recipient.SendMessage("OnTouchExit", hit.point, SendMessageOptions.DontRequireReceiver);
                    }
                }
            //}

            foreach (GameObject g in touchesOld)
            {
                if (!touchList.Contains(g))
                {
                    //g.SendMessage("OnTouchExit", hit.point, SendMessageOptions.DontRequireReceiver);
                }
            }
        }
    }

    public static Vector2 GetLatestTouchPosition()
    {
        return LastestTouchPosition;
    }
}

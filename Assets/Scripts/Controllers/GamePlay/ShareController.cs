﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;
using System.Linq;
using System.IO;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

public class ShareController : MonoBehaviour {
    string mensaje;
	public string caption;
	public string description;
	public string URLimg;
	public Texture2D mytexture;

	// Use this for initialization
	void Start () {
		#if UNITY_ANDROID
		mensaje = "https://play.google.com/store/apps/details?id=" + Application.identifier;
		#elif UNITY_IOS
		mensaje = "https://itunes.apple.com/us/app/candy-fruit-juice/id1228059324?ls=1&mt=8";
		#endif
	}

    public void ShareFB()
    {
		SoundController.sound.Click ();
        if (!FB.IsLoggedIn)
        {
            FB.LogInWithReadPermissions(new List<string> { "user_friends" });
			StartCoroutine(ShareFBBt());
        }
        else
        {
			StartCoroutine(ShareFBBt());
       }
    }
//    void LoginCallBack(ILoginResult result)
//    {
//        if (result.Error == null)
//        {
//            Debug.Log(" FB has logged in");
//        }
//        else {
//            Debug.Log("Error during login " + result.Error);
//        }
//    }
   
    private IEnumerator ShareFBBt()
    {
        yield return new WaitForEndOfFrame();


        FB.ShareLink(
                    new Uri(mensaje),
                    caption,
					description,
					new Uri(URLimg));
                    //callback: Callback);
    }

//    void LogCallback(IResult response)
//    {
//        Debug.Log("Worked");
//    }
//    void Callback(IResult result)
//    {
//        if (result.Cancelled)
//        {
// 
//            Debug.Log(" Share Cancelled");
//        }
//    }

	public void ShareTwitter()
	{
		SoundController.sound.Click ();
		//Debug.Log ("shareTwitter");
		SPShareUtility.TwitterShare ("Play this game!\n" + mensaje + "\n", mytexture);
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BuyCoinsController : MonoBehaviour
{
	public GameObject BuyCoinsPanel;
	Vector3 firstPos;
	public GameObject Panel;
	public Text CoinNumbers, otherCoinNumbers;
	public PopupGame popupGame;
	public AllPanel allPanel;

	void Awake()
	{
		if(CoinNumbers != null)
		{
			CoinNumbers.text = PlayerPrefs.GetInt ("coins").ToString();
			otherCoinNumbers.text = PlayerPrefs.GetInt ("coins").ToString();
		}
	}

	public void Show_BuyCoinsPanel()
	{
		if (otherCoinNumbers != null) {
			otherCoinNumbers.text = PlayerPrefs.GetInt ("coins").ToString ();
		}
		SoundController.sound.Click ();
		firstPos = BuyCoinsPanel.transform.position;
		if (popupGame != null) {
			if (popupGame.dialogBuyMove.activeInHierarchy == false) {
				Panel.SetActive (true);
				Panel.GetComponent<Image> ().enabled = true;
			}
		} 
		else 
		{
			Panel.SetActive (true);
			Panel.GetComponent<Image> ().enabled = true;
		}
		BuyCoinsPanel.SetActive (true);
		BuyCoinsPanel.transform.position = new Vector3 (0,11f);
		iTween.MoveTo (BuyCoinsPanel, iTween.Hash ("position",Vector3.zero,"time",0.25f,"easytype",iTween.EaseType.easeOutBack));
	}

	public void Hide_BuyCoinsPanel()
	{
		SoundController.sound.Click ();

		if (popupGame != null) {
			if (popupGame.bg_2.activeInHierarchy == false) {
				Panel.GetComponent<Image> ().enabled = false;
				iTween.MoveTo (BuyCoinsPanel, iTween.Hash ("position", new Vector3 (firstPos.x, -firstPos.y), "easytype", iTween.EaseType.easeOutBack, "time", 0.25f, "oncomplete", "MoveFirstPost", "oncompletetarget", this.gameObject));
			}

			if (popupGame.dialogBuyMove.activeInHierarchy == true) {
				popupGame._Coins ();
				iTween.MoveTo (popupGame.dialogBuyMove, iTween.Hash ("position", new Vector3 (0, 0, 0), "time", 0.25f, "easetype", iTween.EaseType.easeOutBack));
				iTween.MoveTo (BuyCoinsPanel, iTween.Hash ("position", new Vector3 (0, 11), "easytype", iTween.EaseType.easeOutBack, "time", 0.25f, "oncomplete", "MoveFirstPost", "oncompletetarget", this.gameObject));
			}
		} 
		else 
		{
			Panel.GetComponent<Image> ().enabled = false;
			iTween.MoveTo (BuyCoinsPanel, iTween.Hash ("position", new Vector3(0,-11f),"easytype",iTween.EaseType.easeOutBack,"time",0.25f,"oncomplete","MoveFirstPost","oncompletetarget",this.gameObject));
			if(allPanel.popupStart.activeInHierarchy == true)
			{
				allPanel.popupStart.SetActive (false);
			}
		}
	}

	public void MoveFirstPost()
	{
		BuyCoinsPanel.transform.position = firstPos;
		BuyCoinsPanel.SetActive (false);
	}
}

﻿/*
 * @Author: CuongNH
 * @Description: Su dung de tao Map trong man choi: GridContainer, Grid, Tile, ...
 * */

using UnityEngine;
using System.Collections;

using System.Collections.Generic;

using Spine.Unity;

public class MapBuilding : MonoBehaviour
{

    #region Properties, Data, Components
    /// <summary>
    /// MapController
    /// </summary>
    public MapController mapController;

    /// <summary>
    /// JsonParser to parse file map
    /// </summary>
    public ReadJson jsonParser;

    /// <summary>
    /// GridSpawner to create grid
    /// </summary>
    public GridSpawner gridSpawner;

    /// <summary>
    /// TileSpawner to create tile
    /// </summary>
    public TileSpawner tileSpawner;

    /// <summary>
    /// ConveyorBeltController to create ConveyorBelt
    /// </summary>
    public ConveyorBeltController conveyorBeltController;

    /// <summary>
    /// PopsicleController to create Popsicles
    /// </summary>
    public PopsicleController popsicleController;

    /// <summary>
    /// PortalController to cointan list portal
    /// </summary>
    public PortalController portalController;

    /// <summary>
    /// BorderPainter to paint border
    /// </summary>
    public BorderPainter borderPainter;

    /// <summary>
    /// Level to load
    /// </summary>
    [SerializeField]
    public int level = 1;

    ///// <summary>
    ///// Danh sach chua tat ca cac grid tren Map
    ///// </summary>
    //public List<Grid> gridList;

    /// <summary>
    /// Chua cac grid trong GridContainer
    /// </summary>
    public Transform gridContainer;

    #endregion

    void Awake()
    {
        level = Contance.NUMBER_LEVEL;
		//Debug.Log (level);
		TextAsset jsonFile = Resources.Load ("MapJuiceJam" + level.ToString()) as TextAsset;
		//Debug.Log (jsonFile.name);
		string levelText = jsonFile.text;
		//Debug.Log (levelText);
		//Debug.Log ("level..."+level);
        ReadMapData(levelText,ref level);
//		ReadMapData (jsonParser.level.text, ref level);
    }

    // Use this for initialization
	void Start () {
        //try
        //{
            CreateProcessingComponents();

            //ReadMapData(jsonParser.level.text, ref level);

            // Init number color of tile in map
            InitTileColorList();

            // Create grid, tile, and components of map
            CreateGridContainer(Contance.width, Contance.height);

            // Init data to process spawn and shrink
            tileSpawner.InitProcessData();

            // Init pool
            PoolingManager.poolingManager.InitPool();

            // Init data in TileManager
            mapController.tileManager.InitData();

            // Go play
            mapController.GoPlay();

        //}
        //catch (System.Exception e)
        //{
        //    VGDebug.LogError(e.Message);
        //}
	}

    //// Update is called once per frame
    //void Update()
    //{
    //    if (Input.GetMouseButtonUp(0))
    //    {
    //        // TestResetBoard
    //        TestResetBoard();
    //    }
    //}

    /// <summary>
    /// Use to test resetboard
    /// </summary>
    public void TestResetBoard()
    {
        //Debug.Log("TestResetBoard...........TestResetBoard...............");

        List<Grid> testGridList = new List<Grid>();
        //Debug.Log("mapController.gridList.Count: " + mapController.gridList.Count);
        for (int i = 0; i < mapController.gridList.Count; i++)
        {
            Grid grid = mapController.gridList[i];
            if (grid != null && grid.gridData.gridType == GridData.GridType.NORMAL_GRID)
            {
                Tile tile = grid.GetTile();
                if (tile != null && tile.isSwappable && tile.isHit && !tile.isSpecialActivable)
                {
                    testGridList.Add(grid);
                }
            }
        }

        //Debug.Log("testGridList.Count: " + testGridList.Count);

        try
        {
            tileSpawner.ResetBoard(testGridList);
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);
        }
    }

    #region Methods to Create, Init Components of map
    /// <summary>
    /// Create Processing components
    /// </summary>
    public void CreateProcessingComponents()
    {
        mapController.tileManager = new TileManagerMatch3();

        mapController.mapConditionChecker = new MapConditionCheckerMatch3(mapController);
        mapController.mapConditionCheckerMatch2x2 = new MapConditionCheckerMatch2x2(mapController);

        // Game Input
        mapController.gameInputProcessor = new InputProcessorMatch3(mapController);
        //mapController.gameInputProcessorMatch2x2 = new InputProcessorMatch2x2(mapController);
        mapController.gameInputControl = new InputControlMatch3(mapController);
        mapController.touchInputHandler.inputReceiver = mapController.gameInputControl;
    }

    /// <summary>
    /// Doc du lieu map
    /// </summary>
    /// <param name="text"></param>
    /// <param name="level"></param>
    private void ReadMapData(string text, ref int level)
    {
        // Comment tam thoi de xac dinh vi tri loi
        try
        {
            #region Clear old data
            if (Contance.LIST_OBJECT != null && Contance.LIST_OBJECT.Count > 0)
            {
                Contance.LIST_OBJECT.Clear();
            }

            if (Contance.LIST_OBJECT_ICE != null && Contance.LIST_OBJECT_ICE.Count > 0)
            {
                Contance.LIST_OBJECT_ICE.Clear();
            }

            if (Contance.LIST_OBJECT_CRACKER != null && Contance.LIST_OBJECT_CRACKER.Count > 0)
            {
                Contance.LIST_OBJECT_CRACKER.Clear();
            }

            if (Contance.LIST_OBJECT_HONEY != null && Contance.LIST_OBJECT_HONEY.Count > 0)
            {
                Contance.LIST_OBJECT_HONEY.Clear();
            }

            if (Contance.LIST_OBJECT_CONVEYOR != null && Contance.LIST_OBJECT_CONVEYOR.Count > 0)
            {
                Contance.LIST_OBJECT_CONVEYOR.Clear();
            }

			if (Contance.LIST_OBJECT_GRID_COLOR != null && Contance.LIST_OBJECT_GRID_COLOR.Count > 0)
			{
				Contance.LIST_OBJECT_GRID_COLOR.Clear();
			}

            if (Contance.LIST_CONVEYOR_BELT != null && Contance.LIST_CONVEYOR_BELT.Count > 0)
            {
                Contance.LIST_CONVEYOR_BELT.Clear();
            }

            if (Contance.LIST_PORTAL_INFO_18 != null && Contance.LIST_PORTAL_INFO_18.Count > 0)
            {
                Contance.LIST_PORTAL_INFO_18.Clear();
            }

            if (Contance.LIST_OBJECT_18 != null && Contance.LIST_OBJECT_18.Count > 0)
            {
                Contance.LIST_OBJECT_18.Clear();
            }

            if (Contance.LIST_POPSICLE != null && Contance.LIST_POPSICLE.Count > 0)
            {
                Contance.LIST_POPSICLE.Clear();
            }

            if (Contance.LIST_SPAWNTABLE != null && Contance.LIST_SPAWNTABLE.Count > 0)
            {
                Contance.LIST_SPAWNTABLE.Clear();
            }

            if (Contance.LIST_FORCEDSPAWNQUEUE != null && Contance.LIST_FORCEDSPAWNQUEUE.Count > 0)
            {
                Contance.LIST_FORCEDSPAWNQUEUE.Clear();
            }

            if (Contance.LIST_COLOR != null && Contance.LIST_COLOR.Count > 0)
            {
                Contance.LIST_COLOR.Clear();
            }
            #endregion

            //level = (level > 0) ? (level - 1) : Random.Range(0, 125);
			//Debug.Log(level);
			jsonParser.ReadDataJson(text, 0);
        }
        catch (System.Exception e)
        {
            VGDebug.LogError(e.Message);
        }
    }

    /// <summary>
    /// Khoi tao danh sach cac tile color
    /// </summary>
    public void InitTileColorList()
	{//Debug.Log ("111");
        tileSpawner.randomTileColorList.Clear();
        tileSpawner.randomTileColorDict.Clear();
		//Debug.Log (Contance.LIST_COLOR.Count);
        for (int i = 0; i < Contance.LIST_COLOR.Count; i++)
        {
            tileSpawner.randomTileColorList.Add(Contance.LIST_COLOR[i].color);
        }
    }

    /// <summary>
    /// Tao GridContainer
    /// </summary>
    /// <param name="width">Do lon chieu rong</param>
    /// <param name="height">Do lon chieu cao</param>
    public void CreateGridContainer(int width, int height)
    {
        if (width > GameConstants.MAP_WIDTH || width <= 0)
        {
            width = GameConstants.MAP_WIDTH;
        }

        if (height > GameConstants.MAP_HEIGHT || height <= 0)
        {
            height = GameConstants.MAP_HEIGHT;
        }

        //gridList = new List<Grid>();
        mapController.gridList = new List<Grid>();

        // Tao mang grid tam thoi de tao ConnectedList cua moi grid
        Grid[,] gridArray = new Grid[width, height];

        #region Create grids in GridContainer
        int count = 0;
        int mainSpriteGridIndex = 0;
        for (int j = 0 ; j < height; j++)
        {
            for (int i = 0; i < width; i++)
            {
                mainSpriteGridIndex = count % 2;
                Grid grid = gridSpawner.CreateNewGrid(gridContainer, new Vector3(i - ((float)(width - 1) / 2), 
                    j - ((float)(height - 1) / 2), 0f), mainSpriteGridIndex);

                grid.SetGridPosition(new GridPosition(j, i));

                //gridList.Add(grid);
                mapController.gridList.Add(grid);
                
                // Create connectedList for grid
                gridSpawner.CreateGridConnectedList(grid, gridArray, width, height, i, j);

                count++;
            }

            if ((width % 2) == 0)
            {
                count++;
            }
        }
        #endregion

        #region Create componnets of Grids
        // Fill Tiles for Grids
        FillTilesForGrids();

        // CheckValidFillTiles
        CheckValidFillTiles();

        // Fill ices for Grids
        FillIcesForGrids();

        // Fill cracker for Grids
        FillCrackersForGrids();

        // Fill honey for Grids
        FillHoneyForGrids();

		FillColorForGrids();

        // Create Conveyors for Grids
        CreateConveyors();

        // CreateConveyorBelts for game play
        CreateConveyorBelts();

        // CreatePortals for grids
        CreatePortals();

        // SetSpawnersForGrids
        SetSpawnersForGrids();

        // CreatePopsicles for game play
        CreatePopsicles();

        // Create borders
        borderPainter.CreateBorders(mapController.gridList);
        #endregion
    }

    /// <summary>
    /// Set tile cho cac grid trong GridContainer
    /// </summary>
    public void FillTilesForGrids()
    {
        try
        {
            if (Contance.LIST_OBJECT.Count < 1)
            {
                return;
            }
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);

            return;
        }

        for (int i = 0; i < Contance.LIST_OBJECT.Count; i++)
        {
            //try
            //{
                P_Object pObj = Contance.LIST_OBJECT[i];

                Grid grid = mapController.gridList[pObj.row_Object * Contance.width + pObj.col_Object];
                if (grid != null)
                {
                    //grid.SetGridPosition(new GridPosition(pObj.row_Object, pObj.col_Object));
                    if (string.IsNullOrEmpty(pObj.type_Object) || pObj.type_Object.Trim().Equals(GameConstants.empty_tile_type)
                        || pObj.type_Object.Trim().Equals(GameConstants.invisible_brick_tile_type))
                    {
                        grid.SetGridData(GridData.GridType.NULL_GRID);
                    }
                    else
                    {
                        grid.SetGridData(GridData.GridType.NORMAL_GRID);

                        Tile tile = tileSpawner.CreateNewTile(grid, pObj, Vector3.zero);
						
					if(tile.tileData.tileType == TileData.TileType.fountain)
					{
						GameObject obj = Instantiate (GamePlayController.gamePlayController.leavesEff);
						tile._ft = obj.GetComponent<SkeletonAnimation> ();
						obj.transform.SetParent (tile.transform);
						obj.name = "leaves";
					}

                        grid.SetTile(tile, true);

                        //VGDebug.LogDebug("::::::::::::::::: " + (int)tile.tileData.tileColor);
                        //VGDebug.LogDebug("::::::::::::::::: " + tile.tileData.tileColor.ToString());
                    }
                }
                
            //}
            //catch (System.Exception e)
            //{
            //    VGDebug.LogError(e.Message);
            //}
        }
    }

    /// <summary>
    /// Check xem cac tile tren game play da valid chua (khong co chuoi nao an san)
    /// </summary>
    public void CheckValidFillTiles()
    {
        try
        {
            if (Contance.LIST_OBJECT.Count < 1)
            {
                return;
            }
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);

            return;
        }

        int count = 0;
        while (tileSpawner.CheckInitialMatch())
        {
            count++;
            //Debug.Log("CheckValidFillTiles::::::::: " + count);

            InitTileColorList();
            for (int i = 0; i < Contance.LIST_OBJECT.Count; i++)
            {
                //try
                //{
                P_Object pObj = Contance.LIST_OBJECT[i];

                Grid grid = mapController.gridList[pObj.row_Object * Contance.width + pObj.col_Object];
                //grid.SetGridPosition(new GridPosition(pObj.row_Object, pObj.col_Object));
                if (grid != null && grid.gridData.gridType == GridData.GridType.NORMAL_GRID
                    && (string.IsNullOrEmpty(pObj.color_Object) || pObj.color_Object.Equals(GameConstants
                    .any_tile_color) || pObj.color_Object.StartsWith(GameConstants.random_tile_color))
                    /*&& (mapController.mapConditionChecker.CheckHitInGrid(grid)
                    || mapController.mapConditionCheckerMatch2x2.CheckHitInGrid(grid))*/)
                {
                    tileSpawner.ReSetTileData(grid, pObj);
                }
                //}
                //catch (System.Exception e)
                //{
                //    VGDebug.LogError(e.Message);
                //}
            }

            if (count > GameConstants.MAX_RESET)
            {
                break;
            }
        }
    }

    /// <summary>
    /// Set cac lop ice len cac grids
    /// </summary>
    public void FillIcesForGrids()
    {
        try
        {
            if (Contance.LIST_OBJECT_ICE.Count < 1)
            {
                return;
            }
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);

            return;
        }

        for (int i = 0; i < Contance.LIST_OBJECT_ICE.Count; i++)
        {
            //try
            //{
                P_Object pObj = Contance.LIST_OBJECT_ICE[i];

                Grid grid = mapController.gridList[pObj.row_Object * Contance.width + pObj.col_Object];

                if (grid != null && grid.gridData.gridType == GridData.GridType.NORMAL_GRID
                    && !string.IsNullOrEmpty(pObj.type_Object) && pObj.type_Object.Trim().Equals(GameConstants.ice_grid))
                {
                    grid.gridData.gridDynamicType = GridData.GridDynamicType.ICE_GRID;
                    grid.gridData.gridDynamicData = (pObj.data_Object > 0) ? pObj.data_Object : 1;

                    gridSpawner.SetGridDynamicRender(grid);
                }
            //}
            //catch (System.Exception e)
            //{
            //    Debug.Log(e.Message);
            //}
        }
    }

    /// <summary>
    /// Set cracker cho cac grids co cracker
    /// </summary>
    public void FillCrackersForGrids()
    {
        try
        {
            if (Contance.LIST_OBJECT_CRACKER.Count < 1)
            {
                return;
            }
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);

            return;
        }

        //Debug.Log("Has cracker");
        for (int i = 0; i < Contance.LIST_OBJECT_CRACKER.Count; i++)
        {
            //try
            //{
                P_Object pObj = Contance.LIST_OBJECT_CRACKER[i];

                Grid grid = mapController.gridList[pObj.row_Object * Contance.width + pObj.col_Object];

                //Debug.Log("pObj.row_Object: " + pObj.row_Object);
                //Debug.Log("pObj.col_Object: " + pObj.col_Object);

                if (grid != null && grid.gridData.gridType == GridData.GridType.NORMAL_GRID 
                    && !string.IsNullOrEmpty(pObj.type_Object) && pObj.type_Object.Trim().Equals(GameConstants.cracker_grid))
                {
                    grid.gridData.gridDynamicType = GridData.GridDynamicType.CRACKER_GRID;
                    grid.gridData.gridDynamicData = (pObj.data_Object > 0) ? pObj.data_Object : 1;

                    gridSpawner.SetGridDynamicRender(grid);
                }
            //}
            //catch (System.Exception e)
            //{
            //    Debug.Log(e.Message);
            //}
        }
    }

    /// <summary>
    /// Set honey for grids
    /// </summary>
    public void FillHoneyForGrids()
    {
        try
        {
            if (Contance.LIST_OBJECT_HONEY.Count < 1)
            {
                return;
            }
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);

            return;
        }

        for (int i = 0; i < Contance.LIST_OBJECT_HONEY.Count; i++)
        {
            //try
            //{
                P_Object pObj = Contance.LIST_OBJECT_HONEY[i];

                //Debug.Log("FillHoneyForGrids:::: type object:::: " + pObj.type_Object);
                //Debug.Log("FillHoneyForGrids:::: " + pObj.row_Object + ", " + pObj.col_Object);

                Grid grid = mapController.gridList[pObj.row_Object * Contance.width + pObj.col_Object];

                if (grid != null && grid.gridData.gridType == GridData.GridType.NORMAL_GRID
                    && !string.IsNullOrEmpty(pObj.type_Object) && pObj.type_Object.Trim().Equals(GameConstants.honey_grid))
                {
                    //Debug.Log("FillHoneyForGrids:::: " + pObj.row_Object + ", " + pObj.col_Object);
                    //grid.SetGridData(GridData.GridDynamicType.HONEY_GRID);
                    grid.gridData.gridDynamicType = GridData.GridDynamicType.HONEY_GRID;
                    //grid.gridData.gridDynamicData = (pObj.data_Object > 0) ? pObj.data_Object : 1;

                    gridSpawner.SetGridDynamicRender(grid);
                }
            //}
            //catch (System.Exception e)
            //{
            //    Debug.Log(e.Message);
            //}
        }
    }

	public List<Grid> listColorGrid = new List<Grid>();



	public void FillColorForGrids()
	{
		try
		{
			if (Contance.LIST_OBJECT_GRID_COLOR.Count < 1)
			{
				return;
			}
		}
		catch (System.Exception e)
		{
			Debug.Log(e.Message);

			return;
		}

		for (int i = 0; i < Contance.LIST_OBJECT_GRID_COLOR.Count; i++)
		{
			//try
			//{
			P_Color_Grid pObj = Contance.LIST_OBJECT_GRID_COLOR[i];

			//Debug.Log("FillHoneyForGrids:::: type object:::: " + pObj.type_Object);
			//Debug.Log("FillHoneyForGrids:::: " + pObj.row_Object + ", " + pObj.col_Object);

			Grid grid = mapController.gridList[pObj.row_Color_Grid * Contance.width + pObj.column_Color_Grid];
			//Debug.Log ("FillColorForGrids");
			//Debug.Log ("pObj.color_Color_Grid..."+pObj.color_Color_Grid);
			if (grid != null && grid.gridData.gridType == GridData.GridType.NORMAL_GRID
				&& !string.IsNullOrEmpty(pObj.color_Color_Grid))
			{
				//Debug.Log ("setStaticRender");
				//Debug.Log("FillHoneyForGrids:::: " + pObj.row_Object + ", " + pObj.col_Object);
				//grid.SetGridData(GridData.GridDynamicType.HONEY_GRID);
				if(pObj.color_Color_Grid == "red")
				{
					grid.gridData.gridColorType = GridData.GridColorType.RED_GRID;
				}

				if(pObj.color_Color_Grid == "green")
				{
					grid.gridData.gridColorType = GridData.GridColorType.GREEN_GRID;
				}

				if(pObj.color_Color_Grid == "blue")
				{
					grid.gridData.gridColorType = GridData.GridColorType.BLUE_GRID;
				}

				if(pObj.color_Color_Grid == "orange")
				{
					grid.gridData.gridColorType = GridData.GridColorType.ORANGE_GRID;
				}

				if(pObj.color_Color_Grid == "yellow")
				{
					grid.gridData.gridColorType = GridData.GridColorType.YELLOW_GRID;
				}

				if(pObj.color_Color_Grid == "purple")
				{
					grid.gridData.gridColorType = GridData.GridColorType.PURPLE_GRID;
				}
				//grid.gridData.gridDynamicData = (pObj.data_Object > 0) ? pObj.data_Object : 1;

				gridSpawner.SetGridStaticRender(grid);

				if(listColorGrid.Contains(grid) == false)
				{
					listColorGrid.Add (grid);
				}
			}
			//}
			//catch (System.Exception e)
			//{
			//    Debug.Log(e.Message);
			//}
		}
	}

    /// <summary>
    /// Create conveyors in map
    /// </summary>
    public void CreateConveyors()
    {
        try
        {
            if (Contance.LIST_OBJECT_CONVEYOR.Count < 1)
            {
                return;
            }
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);

            return;
        }

        #region Create conveyors
        conveyorBeltController.conveyorList = new List<Conveyor>();
        for (int i = 0; i < Contance.LIST_OBJECT_CONVEYOR.Count; i++)
        {
            try
            {
                P_Conveyor pConveyor = Contance.LIST_OBJECT_CONVEYOR[i];

                Grid grid = mapController.gridList[pConveyor.row_Conveyor * Contance.width + pConveyor.column_Conveyor];

                if (grid != null && grid.gridData.gridType == GridData.GridType.NORMAL_GRID
                    && !string.IsNullOrEmpty(pConveyor.type_Conveyor) && !string.IsNullOrEmpty(pConveyor.data_Conveyor) 
                    && pConveyor.type_Conveyor.Trim().Equals(GameConstants.conveyor_belt))
                {
                    pConveyor.data_Conveyor = pConveyor.data_Conveyor.Trim();
                    grid.hasConveyor = true;

                    Conveyor conveyor = gridSpawner.CreateNewConveyor(grid, pConveyor);
                    if (conveyor != null)
                    {
                        conveyorBeltController.conveyorList.Add(conveyor);
                    }
                }
            }
            catch (System.Exception e)
            {
                Debug.Log(e.Message);
            }
        }

        conveyorBeltController.hasConveyor = true;
        conveyorBeltController.InitCacheData(Contance.LIST_OBJECT_CONVEYOR.Count);
        #endregion

        // Set properties for conveyors
        gridSpawner.SetConveyorsProperties(conveyorBeltController.conveyorList);
    }

    /// <summary>
    /// Create conveyorbelts in map
    /// </summary>
    public void CreateConveyorBelts()
    {
        try
        {
            if (Contance.LIST_CONVEYOR_BELT.Count < 1)
            {
                return;
            }
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);

            return;
        }

        conveyorBeltController.conveyorBeltList = new List<ConveyorBelt>();
        for (int i = 0; i < Contance.LIST_CONVEYOR_BELT.Count; i++)
        {
            try
            {
                P_Conveyor_Belt pConveyorBelt = Contance.LIST_CONVEYOR_BELT[i];

                Grid fromGrid = mapController.gridList[pConveyorBelt.from_row * Contance.width + pConveyorBelt.from_column];
                Grid toGrid = mapController.gridList[pConveyorBelt.to_row * Contance.width + pConveyorBelt.to_column];

                if (fromGrid != null && fromGrid.hasConveyor && fromGrid.conveyor != null
                    && toGrid != null && toGrid.hasConveyor && toGrid.conveyor != null)
                {
                    ConveyorBelt conveyorBelt = new ConveyorBelt(fromGrid.conveyor, toGrid.conveyor);
                    conveyorBeltController.conveyorBeltList.Add(conveyorBelt);
                }
            }
            catch (System.Exception e)
            {
                Debug.Log(e.Message);
            }
        }
    }

    /// <summary>
    /// Create portals for grids
    /// </summary>
    public void CreatePortals()
    {
        try
        {
            if (Contance.LIST_PORTAL_INFO_18.Count < 1)
            {
                return;
            }
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);

            return;
        }

        #region Create portals
        portalController.portalList = new List<Portal>();
        for (int i = 0; i < Contance.LIST_PORTAL_INFO_18.Count; i++)
        {
            try
            {
                P_Portal pPortal = Contance.LIST_PORTAL_INFO_18[i];

                Grid grid = mapController.gridList[pPortal.row * Contance.width + pPortal.colum];

                if (grid != null && grid.gridData.gridType == GridData.GridType.NORMAL_GRID
                    && !string.IsNullOrEmpty(pPortal.type) && (pPortal.type.Trim().Equals(GameConstants.portal_inlet)
                    || pPortal.type.Trim().Equals(GameConstants.portal_outlet)))
                {
                    grid.gridData.gridStaticType = GridData.GridStaticType.PORTAL_GRID;

                    Portal portal = gridSpawner.CreateNewPortal(grid, pPortal);
                    if (portal != null)
                    {
                        portalController.portalList.Add(portal);
                    }
                }
            }
            catch (System.Exception e)
            {
                Debug.Log(e.Message);
            }
        }
        #endregion

        #region Set matched portal
        if (portalController.portalList.Count >= 2)
        {
            //Debug.Log("Set matched portal......................");
            for (int i = 0; i < portalController.portalList.Count; i++)
            {
                if (portalController.portalList[i].portalMatched != null)
                {
                    //Debug.Log("Has portal matched !!!!!!!!!!");
                    continue;
                }
                else
                {
                    for (int j = portalController.portalList.Count - 1; j > 0; j--)
                    {
                        if ((portalController.portalList[i].portalType != portalController.portalList[j].portalType)
                            && (portalController.portalList[i].data == portalController.portalList[j].data))
                        {
                            portalController.portalList[i].SetPortalMatched(portalController.portalList[j]);
                            portalController.portalList[j].SetPortalMatched(portalController.portalList[i]);

                            break;
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
            }
        }
        #endregion
    }

    /// <summary>
    /// Set Spawners for grids
    /// </summary>
    public void SetSpawnersForGrids()
    {
        #region Load spawners from file map
        if (Contance.LIST_OBJECT_18 != null && Contance.LIST_OBJECT_18.Count > 0)
        {
            for (int i = 0; i < Contance.LIST_OBJECT_18.Count; i++)
            {
                try
                {
                    P_Object_18 pObj = Contance.LIST_OBJECT_18[i];

                    Grid grid = mapController.gridList[pObj.row * Contance.width + pObj.column];

                    if (grid != null && grid.gridData.gridType == GridData.GridType.NORMAL_GRID
                        && !string.IsNullOrEmpty(pObj.type) && pObj.type.Trim().Equals(GameConstants.spawner))
                    {
                        grid.gridData.gridStaticType = GridData.GridStaticType.SPAWNER_GRID;
                    }
                }
                catch (System.Exception e)
                {
                    Debug.Log(e.Message);
                }
            }
        }
        #endregion
        #region Check is top to set spawner
        else
        {
            for (int i = 0; i < Contance.width; i++)
            {
                for (int j = Contance.height - 1; j >= 0; j--)
                {
                    try
                    {
                        Grid grid = mapController.gridList[j * Contance.width + i];
                        if (grid != null && grid.gridData.gridType == GridData.GridType.NORMAL_GRID)
                        {
                            if (grid.gridData.gridStaticType == GridData.GridStaticType.PORTAL_GRID
                                || grid.gridData.gridStaticType == GridData.GridStaticType.SPAWNER_GRID)
                            {
                                break;
                            }
                            else
                            {
                                grid.gridData.gridStaticType = GridData.GridStaticType.SPAWNER_GRID;
                                break;
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                    catch (System.Exception e)
                    {
                        Debug.Log(e.Message);
                    }
                }
            }
        }
        #endregion
    }

    /// <summary>
    /// Create Popsicles for game play
    /// </summary>
    public void CreatePopsicles()
    {
        try
        {
            if (Contance.LIST_POPSICLE.Count < 1)
            {
                return;
            }
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);

            return;
        }

        #region Create Popsicles
        popsicleController.popsicleList = new List<Popsicle>();
        for (int i = 0; i < Contance.LIST_POPSICLE.Count; i++)
        {
            try
            {
                P_Popsicle pPopsicle = Contance.LIST_POPSICLE[i];

                //Grid grid = mapController.gridList[pPopsicle.row_Popsicle * Contance.width + pPopsicle.col_Popsicle];
                if (!string.IsNullOrEmpty(pPopsicle.type_Popsicle) && !string.IsNullOrEmpty(pPopsicle.data_Popsicle)
                    && pPopsicle.type_Popsicle.Trim().Equals(GameConstants.popsicle))
                {
                    Popsicle popsicle = popsicleController.CreateNewPopsicle(pPopsicle);

                    if (popsicle != null)
                    {
                        popsicleController.popsicleList.Add(popsicle);
                    }
                }
            }
            catch (System.Exception e)
            {
                Debug.Log(e.Message);
            }
        }
        #endregion
    }

    #endregion
}

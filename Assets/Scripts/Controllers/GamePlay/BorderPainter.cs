﻿/*
 * @Author: CuongNH
 * @Description: Dung de thuc hien viec hien thi borders cho map
 * */

using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class BorderPainter : MonoBehaviour {

    /// <summary>
    /// Prefab border dung de tao ra cac border
    /// </summary>
    public GameObject borderPrefab;

    /// <summary>
    /// Cac sprite de hien thi cac borders
    /// </summary>
    public Sprite[] borderSprite;

    //// Use this for initialization
    //void Start () {
	
    //}

    /// <summary>
    /// Create border for map
    /// </summary>
    public void CreateBorders(List<Grid> gridList)
    {
        for (int i = 0; i < gridList.Count; i++)
        {
            CreateBorder(gridList[i]);
        }
    }
	
    /// <summary>
    /// Create border for grid
    /// </summary>
    /// <param name="grid"></param>
    private void CreateBorder(Grid grid)
    {
        if (grid != null)
        {
            #region Create inner corner for null grid
            if (grid.gridData.gridType == GridData.GridType.NULL_GRID)
            {
                // INNER_CORNER_TOP_LEFT
                if (grid.HasGridInDir(Grid.Direction.LEFT) && grid.HasGridInDir(Grid.Direction.TOP))
                {
                    Border innerCornerTopLeft = CreateNewBorder();
                    grid.SetBorder(innerCornerTopLeft);
                    innerCornerTopLeft.SetProperties(BorderData.BorderType.INNER_CORNER_TOP_LEFT, 
                        borderSprite[(int)BorderData.BorderType.INNER_CORNER_TOP_LEFT]);
                }

                // INNER_CORNER_TOP_RIGHT
                if (grid.HasGridInDir(Grid.Direction.RIGHT) && grid.HasGridInDir(Grid.Direction.TOP))
                {
                    Border innerCornerTopRight = CreateNewBorder();
                    grid.SetBorder(innerCornerTopRight);
                    innerCornerTopRight.SetProperties(BorderData.BorderType.INNER_CORNER_TOP_RIGHT,
                        borderSprite[(int)BorderData.BorderType.INNER_CORNER_TOP_RIGHT]);
                }

                // INNER_CORNER_BOTTOM_RIGHT
                if (grid.HasGridInDir(Grid.Direction.RIGHT) && grid.HasGridInDir(Grid.Direction.BOTTOM))
                {
                    Border innerCornerBottomRight = CreateNewBorder();
                    grid.SetBorder(innerCornerBottomRight);
                    innerCornerBottomRight.SetProperties(BorderData.BorderType.INNER_CORNER_BOTTOM_RIGHT,
                        borderSprite[(int)BorderData.BorderType.INNER_CORNER_BOTTOM_RIGHT]);
                }

                // INNER_CORNER_BOTTOM_LEFT
                if (grid.HasGridInDir(Grid.Direction.LEFT) && grid.HasGridInDir(Grid.Direction.BOTTOM))
                {
                    Border innerCornerBottomLeft = CreateNewBorder();
                    grid.SetBorder(innerCornerBottomLeft);
                    innerCornerBottomLeft.SetProperties(BorderData.BorderType.INNER_CORNER_BOTTOM_LEFT,
                        borderSprite[(int)BorderData.BorderType.INNER_CORNER_BOTTOM_LEFT]);
                }
            }
            #endregion

            #region Create border for normal grid
            if (grid.gridData.gridType == GridData.GridType.NORMAL_GRID)
            {
                // BORDER_CORNER_TOP_LEFT
                if (!grid.HasGridInDir(Grid.Direction.LEFT) && !grid.HasGridInDir(Grid.Direction.TOP_LEFT) 
                    && !grid.HasGridInDir(Grid.Direction.TOP))
                {
                    Border borderCornerTopLeft = CreateNewBorder();
                    grid.SetBorder(borderCornerTopLeft);
                    borderCornerTopLeft.SetProperties(BorderData.BorderType.BORDER_CORNER_TOP_LEFT, 
                        borderSprite[(int)BorderData.BorderType.BORDER_CORNER_TOP_LEFT]);
                }

                // BORDER_CORNER_TOP_RIGHT
                if (!grid.HasGridInDir(Grid.Direction.RIGHT) && !grid.HasGridInDir(Grid.Direction.TOP_RIGHT)
                    && !grid.HasGridInDir(Grid.Direction.TOP))
                {
                    Border borderCornerTopRight = CreateNewBorder();
                    grid.SetBorder(borderCornerTopRight);
                    borderCornerTopRight.SetProperties(BorderData.BorderType.BORDER_CORNER_TOP_RIGHT,
                        borderSprite[(int)BorderData.BorderType.BORDER_CORNER_TOP_RIGHT]);
                }

                // BORDER_CORNER_BOTTOM_RIGHT
                if (!grid.HasGridInDir(Grid.Direction.RIGHT) && !grid.HasGridInDir(Grid.Direction.BOTTOM_RIGHT)
                    && !grid.HasGridInDir(Grid.Direction.BOTTOM))
                {
                    Border borderCornerBottomRight = CreateNewBorder();
                    grid.SetBorder(borderCornerBottomRight);
                    borderCornerBottomRight.SetProperties(BorderData.BorderType.BORDER_CORNER_BOTTOM_RIGHT,
                        borderSprite[(int)BorderData.BorderType.BORDER_CORNER_BOTTOM_RIGHT]);
                }

                // BORDER_CORNER_BOTTOM_LEFT
                if (!grid.HasGridInDir(Grid.Direction.LEFT) && !grid.HasGridInDir(Grid.Direction.BOTTOM_LEFT)
                    && !grid.HasGridInDir(Grid.Direction.BOTTOM))
                {
                    Border borderCornerBottomLeft = CreateNewBorder();
                    grid.SetBorder(borderCornerBottomLeft);
                    borderCornerBottomLeft.SetProperties(BorderData.BorderType.BORDER_CORNER_BOTTOM_LEFT,
                        borderSprite[(int)BorderData.BorderType.BORDER_CORNER_BOTTOM_LEFT]);
                }

                // BORDER_TOP
                if (!grid.HasGridInDir(Grid.Direction.TOP))
                {
                    Border borderTop = CreateNewBorder();
                    grid.SetBorder(borderTop);
                    borderTop.SetProperties(BorderData.BorderType.BORDER_TOP,
                        borderSprite[(int)BorderData.BorderType.BORDER_TOP]);
                }

                // BORDER_RIGHT
                if (!grid.HasGridInDir(Grid.Direction.RIGHT))
                {
                    Border borderRight = CreateNewBorder();
                    grid.SetBorder(borderRight);
                    borderRight.SetProperties(BorderData.BorderType.BORDER_RIGHT,
                        borderSprite[(int)BorderData.BorderType.BORDER_RIGHT]);
                }

                // BORDER_BOTTOM
                if (!grid.HasGridInDir(Grid.Direction.BOTTOM))
                {
                    Border borderBottom = CreateNewBorder();
                    grid.SetBorder(borderBottom);
                    borderBottom.SetProperties(BorderData.BorderType.BORDER_BOTTOM,
                        borderSprite[(int)BorderData.BorderType.BORDER_BOTTOM]);
                }

                // BORDER_LEFT
                if (!grid.HasGridInDir(Grid.Direction.LEFT))
                {
                    Border borderLeft = CreateNewBorder();
                    grid.SetBorder(borderLeft);
                    borderLeft.SetProperties(BorderData.BorderType.BORDER_LEFT,
                        borderSprite[(int)BorderData.BorderType.BORDER_LEFT]);
                }
            }
            #endregion
        }
    }

    /// <summary>
    /// Create new border
    /// </summary>
    /// <returns></returns>
    private Border CreateNewBorder()
    {
        try
        {
            GameObject borderObj = (GameObject)Instantiate(borderPrefab, Vector3.zero, Quaternion.identity);

            Border border = borderObj.GetComponent<Border>();

            return border;
        }
        catch (System.Exception e)
        {
            VGDebug.LogError(e.Message);
        }

        return null;
    }
}

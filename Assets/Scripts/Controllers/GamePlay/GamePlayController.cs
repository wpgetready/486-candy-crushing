﻿/*
 * @Author: CuongNH
 * @Description: Controll game follow
 * */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Spine.Unity;

public class GamePlayController : MonoBehaviour {

    /// <summary>
    /// Mapcontroller to controll event in map
    /// </summary>
    public MapController mapController;
    public EffectController effectControll;
    public PopsicleController popsicleControll;
    public BossScript bossScript;
    public Clear_JuiceScript clearJuice;
	public PopupGame popupGame;
	public GameObject winPanle, losePanel, pausePanel, coinPanel, bg2, tut, background, top, bottom, _moves, _goal, boss_health, lv1, lv3, _score;

    /// <summary>
    /// Khai bao bien tinh de dung
    /// </summary>
    public static GamePlayController gamePlayController;

    // Flags
    public bool dropFlag, dropDiagonalFlag, cascadeFlag, executeFlag, isReceivingInput;
    public bool finalTryFlag, bonusingFlag, noAnyMove;

    // Counter
    public int gameInterceptorCount;
    public float executionDelay;
    public float helpDelay;

    public UnityEngine.UI.Text moves;
    private int quantity = 0;
    //public int limit;

	public GameObject scoreRibbon;

	public SkeletonAnimation littleGirlske;

	public GameObject introduce;

	public LoadModeGame loadModeGame;

	public GameObject leavesEff;

	public GameObject leavesEff3;

	public SkeletonAnimation _lock;

	public SkeletonAnimation muffinEff, muffinEff3;

    // Use when awake 
    void Awake()
    {
        gamePlayController = this;
		if(Contance.NUMBER_LEVEL == 10000 || Contance.NUMBER_LEVEL == 40000)
		{
			introduce.SetActive (true);
			mapController.touchInputHandler.touchInputMask = LayerMask.GetMask ("Ignore Raycast");
			isTurnHelp = false;
		}
		AdsControl.instance.addCoins.SetTag ();
    }

	void popupScale(GameObject obj, double _scale)
	{
		obj.transform.localScale = obj.transform.localScale * (float)_scale;
		obj.GetComponent<RectTransform>().anchoredPosition = new Vector2 (obj.GetComponent<RectTransform>().anchoredPosition.x, 
			obj.GetComponent<RectTransform>().anchoredPosition.y*(float)_scale);
	}

	void UIScale(GameObject obj, double _scale)
	{
		obj.GetComponent<RectTransform>().sizeDelta = new Vector2(obj.GetComponent<RectTransform>().rect.width, obj.GetComponent<RectTransform>().rect.height * (float)_scale);
		obj.GetComponent<RectTransform>().anchoredPosition = new Vector2 (obj.GetComponent<RectTransform>().anchoredPosition.x, 
			obj.GetComponent<RectTransform>().anchoredPosition.y*(float)_scale);
	}

	// Use this for initialization
	void Start () {
		MusicController.music.isRunOne = false;
		MusicController.music.isRunOneLose = false;
		MusicController.music.PlayOnGamePlay ();
		//Debug.Log ("SOUND "+PlayerPrefs.GetString("SOUND"));

		double _scale = System.Math.Round((1-(Camera.main.aspect-(double)9/(double)16)*1.2f),2);

		Vector3 _number = Vector3.one * (float)_scale;

		winPanle.transform.localScale = _number;
		losePanel.transform.localScale = _number;
		pausePanel.transform.localScale = _number;
		coinPanel.transform.localScale = _number;
		bg2.transform.localScale = _number;

		popupScale (_moves, _scale);
		popupScale (_goal, _scale);
		popupScale (boss_health, _scale);

		popupScale (_score, _scale);

		UIScale (background, _scale);
		UIScale (bottom, _scale);
		UIScale (top, _scale);

		if(Contance.NUMBER_LEVEL == 10000)
		{
			for(int i = 0; i < lv1.transform.childCount; i++)
			{
				UIScale (lv1.transform.GetChild (i).gameObject, _scale);
			}
		}

		if(Contance.NUMBER_LEVEL == 40000)
		{
			for(int i = 0; i < lv3.transform.childCount; i++)
			{
				UIScale (lv3.transform.GetChild (i).gameObject, _scale);
			}
		}

		if(AdsControl.isNoAds == "false")
		{
			Invoke ("ShowFBBanner",3);
		}
        //gamePlayController = this;

        // Set initial properties
        //isGameStarted = false;
        //isGamePaused = false;
        //isGameFreezed = false;
        //isGameEnded = false;
        isReceivingInput = true;

        dropFlag = false;
        dropDiagonalFlag = false;

        cascadeFlag = true;

        executeFlag = false;
        finalTryFlag = false;
        bonusingFlag = false;
        //enableSwap = true;

        gameInterceptorCount = 0;
        executionDelay = -1f;
        helpDelay = GameConstants.TIME_DELAY_BEFORE_SHOWING_HELP;

        //limit = Contance.LIMIT;
        GameManager.MOVE = Contance.LIMIT;
        //moves.text = limit.ToString();
        UpdateMoveDisplay();
		AdsControl.instance.GA.LogEvent ("Level" + Contance.NUMBER_LEVEL, "playing", "Level" + Contance.NUMBER_LEVEL, 1);
	}

	public void SugarEff(Tile tile)
	{
		tile.sugarsEffet = Instantiate (muffinEff.gameObject, tile.transform.position, Quaternion.identity) as GameObject;
	}

	public void showSugarEff(Vector3 pos, Tile tile)
	{
		tile.sugarsEffet.transform.position = pos;
		tile.sugarsEffet.GetComponent<MeshRenderer> ().enabled = true;
		tile.sugarsEffet.GetComponent<SkeletonAnimation> ().state.SetAnimation (0,"animation",false);
		Destroy (tile.sugarsEffet, 3f);
	}

	public void showIceCageEff(Vector3 pos)
	{
		GameObject ske = Instantiate (_lock.gameObject, pos, Quaternion.identity) as GameObject;
		ske.GetComponent<SkeletonAnimation> ().state.Complete += delegate {
			Destroy (ske.gameObject);
		};
		ske.GetComponent<SkeletonAnimation> ().state.SetAnimation (0,"animation",false).OnComplete();
	}

	public void Leaves(Vector3 pos, Tile tile = null, GameObject obj = null)
	{
		GameObject eff = Instantiate (leavesEff3, pos, Quaternion.identity) as GameObject;
		if(tile != null)
		{
			tile.leavesEffect = eff;
			eff.transform.SetParent (tile.gameObject.transform);
		}
		else
		{
			eff.transform.SetParent (obj.transform);
		}
		eff.transform.localScale = Vector3.one;
		eff.GetComponent<ParticleSystem> ().Play ();
	}

	public void DestroyLeaves(GameObject obj)
	{
		Destroy (obj);
	}

	void ShowFBBanner()
	{
		AdsControl.instance.facebookAdsControl.ShowBannerFB ();
	}

    int count = 0;
	public bool isCheckColorGrid;

	public void NoTouch()
	{
		mapController.touchInputHandler.touchInputMask = LayerMask.GetMask("UI");
	}

	public void YesTouch()
	{
		mapController.touchInputHandler.touchInputMask = LayerMask.GetMask("Grid");
	}

	void Update () {
        if (GameManager.gameState == GameManager.GameState.PLAYING && gameInterceptorCount <= 0)
        {
            if (isReceivingInput)
            {
                mapController.touchInputHandler.UpdateUserInput();
            }

            UpdateGameFlow();
        }
    }

    #region Game Flow


    /// <summary>
    /// Update display move number
    /// </summary>

    public void UpdateMoveDisplay()
    {
        moves.text = GameManager.MOVE.ToString();
        if (Contance.GAME_WIN)
        {
            isReceivingInput = false;
        }
    }

    public void BossDropBlocker()
    {
        if (GameManager.gameMode == GameManager.GameMode.BOSS_BATTLE)
        {
            if (GameManager.SPAW_TIME == Contance.bossScripts.spawn_time_B)
            {
                bossScript.BossDropObstacle_Blocker();
                GameManager.SPAW_TIME = 0;
            }
        }
    }

	public bool isTurnHelp = true;
    
	float executionDelayTime = 0;

	/// <summary>
    /// Update game flow
    /// </summary>
    void UpdateGameFlow()
    {
        if (!mapController.isProcessing)
        {
            // Counter for help display
			if (helpDelay > 0 && isTurnHelp == true)
            {
                helpDelay -= Time.deltaTime;
                if (helpDelay <= 0)
                {
                    mapController.ShowHelp();
                }
            }
            return;
        }

        if (executeFlag)
        {
            mapController.HideHelp();
            helpDelay = GameConstants.TIME_DELAY_BEFORE_SHOWING_HELP;

            executeFlag = false;

           	SetExecutionDelay(GameConstants.TIME_BREAK_ANIMATION);

            dropFlag = true;
        }

        if (executionDelay >= 0)
        {
            executionDelay -= Time.deltaTime;
            return;
        }

		if (UpdateDrop ()) {
			return;
		}

		if (mapController.processingGrids.Count > 0) 
		{
			return;
		}

		if (dropFlag == false && dropDiagonalFlag == false && mapController.processingGrids.Count <= 0) {
			GridColorEffect ();
		}

		if(mapController.mapBuilding.listColorGrid.Count > 0)
		{
			if (executionDelayTime >= 0)
			{
				executionDelayTime -= Time.deltaTime;
				return;
			}
		}

        UpdateCascade();
    }

	bool isPassFirstTime;
    
	/// <summary>
    /// Update cascade
    /// </summary>
    void UpdateCascade()
    {
        if (cascadeFlag)
        {
            cascadeFlag = false;

			bool result = /*UpdateObjectives()*/ false || mapController.Cascade();
            //Debug.Log("UpdateCascade: result: " + result);

            if (result)
            {
                dropFlag = true;
                SetExecutionDelay(GameConstants.TIME_BREAK_ANIMATION);
            }
            else
            {
                // Stop
                mapController.FinishProcessingPhase();
            }
        }
    }

	void GridColorEffect()
	{
		if (isCheckColorGrid == false && isPassFirstTime == true) 
		{
			isCheckColorGrid = true;
			mapController.CheckColorGrid ();
		} 
		else 
		{
			isPassFirstTime = true;
		}
	}

    /// <summary>
    /// Update drop
    /// </summary>
    /// <returns></returns>
    public bool UpdateDrop()
    {
        if (dropFlag)
        {
            dropFlag = false;
            dropDiagonalFlag = true;
            float result = mapController.Drop();

            if (result > 0)
            {
                SetExecutionDelay(result);
                return true;
            }
            else
            {
                cascadeFlag = true;
            }
        }

        if (dropDiagonalFlag)
        {
            dropDiagonalFlag = false;
            float result = mapController.DropDiagonal();

            if (result > 0)
            {
                SetExecutionDelay(result);
                dropFlag = true;
                return true;
            }
            else
            {
                cascadeFlag = true;
            }
        }
        return false;
    }

    /// <summary>
    /// Set delay execution
    /// </summary>
    /// <param name="time"></param>
    public void SetExecutionDelay(float time, float _time = 0.15f)
    {
        if (executionDelay < time)
        {
			executionDelay = time + _time;
			if (mapController.mapBuilding.listColorGrid.Count > 0) 
			{
				executionDelayTime = time + _time + 0.15f;
			}
			else 
			{
				executionDelayTime = time + _time+0.1f;
			}
        }
    }

    /// <summary>
    /// Call whenever a tile is cleared, trigger drop and other execution.
    /// </summary>
    public void OnTileCleared(Vector3 position = new Vector3())
    {
        if (!mapController.isProcessing)
        {
            mapController.StartProcessingPhase(false);
        }

        if (!dropFlag && !dropDiagonalFlag)
        {
            dropFlag = true;
        }
    }

    /// <summary>
    /// Call when tile finish moving
    /// </summary>
    public void OnTileFinishedMoving()
    {
        if (!mapController.isProcessing)
        {
            mapController.StartProcessingPhase(false);
        }

        if (GameManager.gameState == GameManager.GameState.PLAYING)
        {
            if (!dropFlag && !dropDiagonalFlag)
            {
                //dropDiagonalFlag = true;
                dropFlag = true;
            }
        }
    }
    #endregion

    #region For test
    public void RePlay()
    {
		SceneManager.LoadScene (GameConstants.OBJECT_EDITOR_SCENE_NAME);
    }
    #endregion
	int runLittleGirl = 0;
	public IEnumerator ShowLittleGirl()
	{
		if(runLittleGirl == 0)
		{
			yield return new WaitForSeconds (2f);
			littleGirlske.gameObject.SetActive (true);
			littleGirlske.enabled = true;
			littleGirlske.state.SetAnimation (0, "jump", false);
			yield return new WaitForSeconds (2.2f);
			littleGirlske.state.SetAnimation (1, "dance", true);
			runLittleGirl = 1;
		}
	}

	void ShowAds()
	{
		try
		{
			if(AdsControl.isNoAds == "false")
			{
				if(AdsControl.instance.facebookAdsControl.isShowFBBanner == true)
				{
					AdsControl.instance.facebookAdsControl.ClearBannerFB();
					isClearFBBanner = true;
					AdsControl.instance.facebookAdsControl.isShowFBBanner = false;
				}

				if(AdsControl.instance.facebookAdsControl.isShowAdmobBanner == true)
				{
					AdsControl.instance.admobControl.ClearBannerAdmob();
					isClearAdmobBanner = true;
					AdsControl.instance.facebookAdsControl.isShowAdmobBanner = false;
				}

				if(isShowOne == false)
				{isShowOne = true;
					if(AdsControl.instance.facebookAdsControl.isFBAdsFirst == true)
					{
						if(AdsControl.instance.facebookAdsControl.isHasInads() == true)
						{
							AdsControl.instance.facebookAdsControl.ShowInterstitial();
							isShowFBInads = true;
						}
						else
						{
							AdsControl.instance.admobControl.ShowInadsAdmob();
							isShowAdmobInads = true;
						}
					}
					else
					{
						if(AdsControl.instance.admobControl.interstitial.IsLoaded() == true)
						{
							AdsControl.instance.admobControl.ShowInadsAdmob();
							isShowAdmobInads = true;
						}
						else
						{
							if (AdsControl.instance.facebookAdsControl.isHasInads() == true) 
							{
								AdsControl.instance.facebookAdsControl.ShowInterstitial ();
								isShowFBInads = true;
							}
						}
					}
				}
			}
		}
		catch
		{
			
		}
	}

	[HideInInspector]
	public bool isShowBuyMove;
    public IEnumerator CheckStateGame()
    {
        if(Contance.GAME_WIN || GameManager.MOVE <= 0)
        {
			
            isReceivingInput = false;
        }
        yield return new WaitForSeconds(0.55f);
        if (GameManager.MOVE <= 0 && !Contance.GAME_PAUSE)
        {
            GameManager.MOVE = 0;

            if (!Contance.GAME_WIN)
            {
                StateLoseCharacter();
                Contance.GAME_LOSE = true;
				if(isShowBuyMove == false)
				{
					popupGame.BeforeLose ();
					isShowBuyMove = true;
				}
				else
				{
					if(popupGame.dialogBuyMove.activeInHierarchy == false)
					{
						soundLose ();
						ShowAds ();

						yield return new WaitForSeconds(0.5f);
		                popupGame.Lose();
					}
				}
            }
            else
            {
				PlayerPrefs.SetString("Level"+Contance.NUMBER_LEVEL,"Finish");
				PlayerPrefs.Save();
				soundWin ();
                StartCoroutine( AutoEatSpecial());

				if(isWinEffect == false)
				{
					StartCoroutine (WinEffect());
				}
				StartCoroutine (ShowLittleGirl());
            }
        }
        else if (Contance.GAME_WIN)
        {
			isReceivingInput = false;
			soundWin ();

			if(isWinEffect == false)
			{
				StartCoroutine (WinEffect());
			}

            StartCoroutine(SetTileType());

			PlayerPrefs.SetString("Level"+Contance.NUMBER_LEVEL,"Finish");
			PlayerPrefs.Save();

			StartCoroutine (ShowLittleGirl());
        }
    }
	bool isWinEffect;
	public GameObject textWin;
	IEnumerator WinEffect()
	{
		GameObject[] boss_effect = new GameObject[15];
		int count = 0;

		while(count < 15)
		{
			yield return new WaitForSeconds (0.15f);
			boss_effect[count] = Instantiate (bossScript.effectHitBossColor [(int)Random.Range (0, 6)], new Vector3 (Random.Range (-1.7f, 1.7f), Random.Range (-2.25f, 1.65f),0), Quaternion.identity) as GameObject;
			boss_effect [count].GetComponent<ParticleSystem> ().startSize = 3;
			boss_effect [count].GetComponent<ParticleSystem> ().loop = false;
			Destroy (boss_effect [count], 1);
			count++;
		}

		textWin.transform.localScale = Vector3.zero;
		textWin.SetActive (true);
		iTween.ScaleTo (textWin, iTween.Hash("scale",Vector3.one*0.85f,"easytype",iTween.EaseType.easeInBack,"time",0.65f,"looptype", iTween.LoopType.none,"oncomplete","HideTextwin","oncompletetarget",gameObject));
		isWinEffect = true;
	}

	IEnumerator HideTextwin()
	{
		yield return new WaitForSeconds (0.35f);
		textWin.SetActive (false);
	}
		
	public void soundWin()
	{
		MusicController.music.PlayWin ();
	}

	public void soundLose()
	{
		MusicController.music.PlayLose ();
	}

    public IEnumerator SetTileType()
    {
		yield return new WaitForSeconds (5f);
        int bonus = GameManager.MOVE;
        for(int i = 0; i < bonus; i++)
        {
            if (GameManager.MOVE > 0)
            {
                GameManager.MOVE--;
            }

            UpdateMoveDisplay();

            var tile = MapUtilities.GetRandomTile(mapController.gridList);
            effectControll.CreateEffectStarEnd(tile);
            yield return new WaitForSeconds(0.05f);
            if(i == bonus-1)
            {
                yield return new WaitForSeconds(0.35f);
                StartCoroutine( AutoEatSpecial());
            }
        }
    }
	bool isShowOne = false;

    IEnumerator AutoEatSpecial()
    {
        bool checkPopup = false;
        for(int i = 0; i < mapController.gridList.Count; i++)
        {
            Grid grid = null;
            Tile tile = null;

            grid = mapController.gridList[i];

            if (grid != null && grid.gridData.gridType != GridData.GridType.NULL_GRID)
            {
                tile = grid.GetTile();
                if (tile != null && tile.isSpecialActivable && tile.tileData.tileType != TileData.TileType.rainbow)
                {
					yield return new WaitForSeconds(0.5f);
                    mapController.ExecuteTile(tile,grid,false,true);
                    checkPopup = true;
                    //break;
                }
				if (tile != null && (tile.tileData.tileType == TileData.TileType.rainbow || tile.tileData.tileType == TileData.TileType.magnet))
				{
					int count = 0;
					foreach (Grid _grid in tile.grid.connectedList)
					{
						count++;
						if(_grid != null && _grid.GetTile() != null)
						{
							if (_grid.GetTile ().tileData.tileType == TileData.TileType.match_object && (_grid == tile.grid.GetGridInDir(Grid.Direction.BOTTOM) || _grid == tile.grid.GetGridInDir(Grid.Direction.BOTTOM_LEFT)
								|| _grid == tile.grid.GetGridInDir(Grid.Direction.RIGHT) || _grid == tile.grid.GetGridInDir(Grid.Direction.TOP))) 
							{
								CombineBooster.Active (mapController, tile, _grid.GetTile ());
								break;
							}
						}
						if(count == tile.grid.connectedList.Count)
						{
							tile.Damage ();
						}
					}
				}
            }
				
			if (i == mapController.gridList.Count - 1 && !checkPopup && dropFlag == false && dropDiagonalFlag == false && mapController.processingGrids.Count <= 0)
            {
                SetExecutionDelay(0.5f);
				mapController.HideHelp ();
				ShowAds ();

				yield return new WaitForSeconds(1.5f);
                popupGame.Win();
            }
        }
    }

    public void RandomTileType(Tile tile)
    {
        if (GameManager.gameMode == GameManager.GameMode.ORDER_FULFILLMENT)
        {
            int rand = Random.Range(0, 2);
            if (rand == 0)
            {
                tile.TransformInto(new TileData(TileData.TileType.row_breaker, tile.tileData.tileColor));
            }
            else
            {
                tile.TransformInto(new TileData(TileData.TileType.column_breaker, tile.tileData.tileColor));
            }
        }
        else if (GameManager.gameMode == GameManager.GameMode.BOSS_BATTLE)
        {
            int rand = Random.Range(0, 2);
            if (rand == 0)
            {
                tile.TransformInto(new TileData(TileData.TileType.row_breaker, tile.tileData.tileColor));
            }
            else
            {
                tile.TransformInto(new TileData(TileData.TileType.column_breaker, tile.tileData.tileColor));
            }
        }
        else if (GameManager.gameMode == GameManager.GameMode.POPSICLE)
        {
            tile.TransformInto(new TileData(TileData.TileType.bomb_rainbow, tile.tileData.tileColor));
        }
        else if (GameManager.gameMode == GameManager.GameMode.CLEAR_JUICE)
        {
            tile.TransformInto(new TileData(TileData.TileType.x_rainbow, tile.tileData.tileColor));
        }
    }

    public void StateLoseCharacter()
    {
        if(GameManager.gameMode == GameManager.GameMode.BOSS_BATTLE)
        {
			bossScript.AnimationWinOrLose(bossScript.gameObject,Contance.THANG_Boss,true);
        }
        else if (GameManager.gameMode == GameManager.GameMode.POPSICLE)
        {
            popsicleControll.Popsicle_Lose();
        }
        else if (GameManager.gameMode == GameManager.GameMode.CLEAR_JUICE)
        {
            clearJuice.Clear_Juice_Lose();
        }
    }
	bool isClearFBBanner = false;
	bool isClearAdmobBanner = false;
	bool isShowFBInads = false;
	bool isShowAdmobInads = false;

	void OnDestroy()
	{
		ClearAndReloadAds ();
	}

	void ClearAndReloadAds()
	{
		if(AdsControl.isNoAds == "false")
		{
			if(AdsControl.instance.facebookAdsControl.isShowFBBanner == true)
			{
				AdsControl.instance.facebookAdsControl.ClearBannerFB();
				isClearFBBanner = true;
				AdsControl.instance.facebookAdsControl.isShowFBBanner = false;
			}

			if(AdsControl.instance.facebookAdsControl.isShowAdmobBanner == true)
			{
				AdsControl.instance.admobControl.ClearBannerAdmob();
				isClearAdmobBanner = true;
				AdsControl.instance.facebookAdsControl.isShowAdmobBanner = false;
			}

			if(isClearFBBanner == true)
			{
				AdsControl.instance.facebookAdsControl.LoadBannerFB();
			}

			if (isClearAdmobBanner == true) 
			{
				AdsControl.instance.admobControl.RequestBannerAdmob ();
			}

			if(isShowFBInads == true)
			{
				AdsControl.instance.facebookAdsControl.ClearInadsFacebook ();
				AdsControl.instance.facebookAdsControl.LoadInterstitial ();
			}

			if (isShowAdmobInads == true) 
			{
				AdsControl.instance.admobControl.ClearInadsAdmob ();
				AdsControl.instance.admobControl.RequestInterstitial ();
			}
		}
	}
}
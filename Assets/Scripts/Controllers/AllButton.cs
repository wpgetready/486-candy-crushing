﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AllButton : MonoBehaviour 
{
	public GameObject Popup_Quit;
	Vector3 posUp = new Vector3(0,11,0);
	Vector3 posDown = new Vector3(0,-11,0);
	Vector3 posLeft = new Vector3(-11,0,0);
	Vector3 posRight = new Vector3(11,0,0);
	public GameObject coinPanle, bg_2, playPopup;
	public CoinController coinController;
	public Bg2Controller bg2Controller;
	public SizeScroll sizeScroll;
	public AllPanel allPanel;

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Escape))
		{
			if(coinPanle.activeInHierarchy == true)
			{
				coinController.MoveUp ();
			}
			if(bg_2.activeInHierarchy == true)
			{
				bg2Controller.CloseButton (true);
			}
			if(playPopup.activeInHierarchy == true)
			{
				sizeScroll.Btn_Exit ();
			}
			if(allPanel.isShow == true)
			{
				allPanel.CloseButton ();
			}

			allPanel.firstPanel.SetActive (true);
			allPanel.firstPanel.GetComponent<Image> ().enabled = true;
			Popup_Quit.SetActive(true);
			MoveIn_Popup(Popup_Quit);
		}
	}

	public void Quit_Yes()
	{
		Application.Quit ();
	}

	public void MoveIn_Popup(GameObject obj)
	{
		iTween.MoveTo(obj, iTween.Hash("position", Vector3.zero, "time", 0.25f, "easetype", iTween.EaseType.easeOutBack));
	}

	public void MoveOut_Popup(GameObject obj)
	{
		allPanel.firstPanel.GetComponent<Image> ().enabled = false;
		iTween.MoveTo(obj, iTween.Hash("position", posUp, "time", 0.25f, "easetype", iTween.EaseType.easeOutBack, "oncomplete", "HideObjectTarget", "oncompletetarget", gameObject, "oncompleteparams",obj));
	}

	public void HideObjectTarget(GameObject obj)
	{
		obj.SetActive (false);
	}
}

﻿using UnityEngine;
using System.Collections;

public class AdconlonyController : MonoBehaviour 
{
	private string versionGame;

	public string appID_Android;
	public string zoneID_Android;

	public string appID_IOS;
	public string zoneID_IOS;

	[HideInInspector]
	public string appID;
	[HideInInspector]
	public string zoneID;

	void Awake()
	{
		#if UNITY_ANDROID
		appID = appID_Android;
		zoneID = zoneID_Android;
		#else
		appID = appID_IOS;
		zoneID = zoneID_IOS;
		#endif

		versionGame = Application.version;
	}

	public void SetDelegate()
	{
		AdColony.OnVideoFinished = this.OnVideoFinished;
		AdColony.OnV4VCResult = this.OnV4VCResult;
		//AdColony.OnAdAvailabilityChange = this.OnAdAvailabilityChange;
	}

	public void Initialize()
	{
		// Assign any AdColony Delegates before calling Configure
//		AdColony.OnVideoFinished = this.OnVideoFinished;
//		AdColony.OnV4VCResult = this.OnV4VCResult;
//		AdColony.OnAdAvailabilityChange = this.OnAdAvailabilityChange;
		
		// If you wish to use a the customID feature, you should call  that now.
		// Then, configure AdColony:

		AdColony.Configure
		(
			versionGame, // Arbitrary app version and Android app store declaration.
			appID,   // ADC App ID from adcolony.com
			zoneID // A zone ID from adcolony.com
		);

	}
	
	// When a video is available, you may choose to play it in any fashion you like.
	// Generally you will play them automatically during breaks in your game,
	// or in response to a user action like clicking a button.
	// Below is a method that could be called, or attached to a GUI action.
	public void PlayV4VCAd( string zoneID, bool prePopup, bool postPopup )
	{
		// Check to see if a video for V4VC is available in the zone.
		if(AdColony.IsV4VCAvailable(zoneID))
		{
			//Debug.Log("Play AdColony V4VC Ad");
			// The AdColony class exposes two methods for showing V4VC Ads.
			// ---------------------------------------
			// The first `ShowV4VC`, plays a V4VC Ad and, optionally, displays
			// a popup when the video is finished.
			// ---------------------------------------
			// The second is `OfferV4VC`, which popups a confirmation before
			// playing the ad and, optionally, displays popup when the video 
			// is finished.
			
			// Call one of the V4VC Video methods:
			// Note that you should also pause your game here (audio, etc.) AdColony will not
			// pause your app for you.
			if (prePopup) AdColony.OfferV4VC( postPopup, zoneID );
			else          AdColony.ShowV4VC( postPopup, zoneID );
			#if UNITY_IOS
			SoundController.sound.audioSource.mute = true;
			MusicController.music.audioSource.mute = true;
			#endif
		}
		else
		{
			//Debug.Log("V4VC Ad Not Available");
		}
	}
	
	private void OnVideoFinished(bool ad_was_shown)
	{
		#if UNITY_IOS
		SoundController.sound.audioSource.mute = false;
		MusicController.music.audioSource.mute = false;
		#endif
		//Debug.Log("On Video Finished");
		// Resume your app here.
	}
	
	// The V4VCResult Delegate assigned in Initialize -- AdColony calls this after confirming V4VC transactions with your server
	// success - true: transaction completed, virtual currency awarded by your server - false: transaction failed, no virtual currency awarded
	// name - The name of your virtual currency, defined in your AdColony account
	// amount - The amount of virtual currency awarded for watching the video, defined in your AdColony account
	private void OnV4VCResult(bool success, string name, int amount)
	{
		if(success)
		{
			//Debug.Log("V4VC SUCCESS: name = " + name + ", amount = " + amount);
			AdsControl.instance.addCoins.AddReward(true,AddCoins.rewardAds);
			Initialize();
		}
		else
		{
			Initialize();
			//Debug.LogWarning("V4VC FAILED!");
		}
	}

	private void OnAdAvailabilityChange(bool available, string zone_id)
	{
		if (available) 
		{
			GameObject.FindGameObjectWithTag("AdcolonyBT").SetActive(true);
		} 
		else 
		{
			GameObject.FindGameObjectWithTag("AdcolonyBT").SetActive(false);
			if(Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork || Application.internetReachability ==NetworkReachability.ReachableViaCarrierDataNetwork)
			{
				//Debug.Log("adcolony not load");
				Initialize();
			}
		}
	}
}

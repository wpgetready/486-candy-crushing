﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Bg2Controller : MonoBehaviour 
{
	public Text ownCoins;
	public GameObject StartPopup;
	public AllPanel allPanel;
	public GameObject panel;
	public Text testtt;
	public GameObject buyButton, getFreeCoinsBT, buyCoinsBT;
	public SizeScroll sizeScroll;
	int priceItem;

	public void SetPriceItem(int _priceItem)
	{
		priceItem = _priceItem;
		checkPriceItem ();
	}

	public void checkPriceItem()
	{
		int coins = PlayerPrefs.GetInt ("coins");
		ownCoins.text = coins.ToString();
		if (coins >= priceItem) 
		{
			testtt.text = "Are you sure to buy a item?";
			buyButton.SetActive (true);
			getFreeCoinsBT.SetActive (false);
			buyCoinsBT.SetActive (false);
		}
		else 
		{
			testtt.text = "You don't have enough coins!";
			buyButton.SetActive (false);
			getFreeCoinsBT.SetActive (true);
			buyCoinsBT.SetActive (true);
		}
	}

	public void CloseButton(bool isOtherBT)
	{
		SoundController.sound.Click ();
		if (isOtherBT == false) 
		{
			if (StartPopup != null && StartPopup.activeInHierarchy == true) 
			{
				iTween.MoveTo (StartPopup, iTween.Hash ("position", new Vector3 (0, 0, 0), "time", 0.25f, "easetype", iTween.EaseType.easeOutBack));
			} 
			else 
			{
				if (allPanel != null) 
				{
					allPanel.OnClickButton (allPanel.savedName);
				}
			}

			if (panel != null && StartPopup != null && StartPopup.activeInHierarchy == false) {
				panel.SetActive (false);	
			}
			else 
			{
				if(SceneManager.GetActiveScene().buildIndex == 3)
				{
					panel.SetActive (false);
				}
			}
		}
		else 
		{
			//sizeScroll.PopupLevel.transform.GetChild (0).GetChild (0).position = new Vector3 (0, 11f);
			//StartPopup
//			if (allPanel.firstPanel.activeInHierarchy == true) 
//			{
//				sizeScroll.Btn_Exit ();
//			}
//			else
//			{
//				allPanel.CloseButton (false);
//			}
		}

		iTween.MoveTo(gameObject, iTween.Hash("position", new Vector3(0,11,0), "time", 0.25f, "easetype", iTween.EaseType.easeOutBack, "oncomplete", "HideBg2", "oncompletetarget", gameObject));
	}

	void HideBg2()
	{
		gameObject.SetActive (false);
	}
}
﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Lv1Introduce : MonoBehaviour 
{
	public GameObject[] panel;
	int number = 1;
	int number_3 = 1;
	public Text content;
	public GameObject square;
	public GameObject[] _play;
	public GameObject panel1, panel3;
	MapController _mapControl;

	void Awake()
	{
		_mapControl = GamePlayController.gamePlayController.mapController;
		foreach(GameObject obj in panel)
		{
			obj.SetActive (true);
		}

		if (Contance.NUMBER_LEVEL == 1) 
		{
			content.text = "Thirsty customers want drinks! Make matches to fill orders!";
			panel1.SetActive (true);
			panel3.SetActive (false);
			_play [0].SetActive (true);
			_play [1].SetActive (false);
		} 
		else 
		{
			if(Contance.NUMBER_LEVEL == 4)
			{
				content.text = "Solomon is up to no good! Match  the Goal Board pieces and drive him off!";
				panel1.SetActive (false);
				panel3.SetActive (true);
				panel [2].SetActive (false);
				_play [0].SetActive (false);
				_play [1].SetActive (true);
				GameObject obj = GamePlayController.gamePlayController.loadModeGame.Goal_Boss;
				LeanTween.scale (obj, obj.transform.localScale * 1.15f, 0.35f).setLoopPingPong ();
			}
		}


		square.SetActive (false);
	}

	public void Click()
	{
		SoundController.sound.Click ();
		switch(number)
		{
			case 1:
				GameObject obj = GamePlayController.gamePlayController.loadModeGame.listGoalCurrent[0].Bg_item;
				LeanTween.scale (obj, obj.transform.localScale * 1.35f, 0.35f).setLoopPingPong ();
				panel[0].SetActive (false);
				content.text = "Customer orders appear here! Banana needs 3 bananas to fill his juice order.";
				number = 2;
				break;
			case 2:
				LeanTween.cancel (GamePlayController.gamePlayController.loadModeGame.listGoalCurrent[0].Bg_item);
				panel [0].SetActive (true);
				panel [1].SetActive (false);
				content.text = "Make this match to fill Banana's order.";
				_play[0].SetActive (false);
				_mapControl.gridList [2].gameObject.layer = 2;
				_mapControl.gridList [3].gameObject.layer = 2;
				_mapControl.gridList [4].gameObject.layer = 2;
				_mapControl.gridList [9].gameObject.layer = 2;
				_mapControl.gridList [10].gameObject.layer = 2;
				_mapControl.gridList [11].gameObject.layer = 2;
				StartCoroutine (showSquare(10,3));
				number = 3;
						break;
				default:
					break;
		}
	}

	IEnumerator showSquare(int grid1, int grid2)
	{
		Vector3 pos_1 = _mapControl.gridList [grid1].transform.position;
		Vector3 pos_2 = _mapControl.gridList [grid2].transform.position;

		square.transform.position = pos_1;
		square.SetActive (true);
		int count = 2;
		while(true)
		{
			yield return new WaitForSeconds (0.75f);
			if (count == 1) 
			{
				square.transform.position = pos_1;
				count = 2;
			}
			else 
			{
				square.transform.position = pos_2;
				count = 1;
			}
		}
	}

	public void Click_3()
	{
		SoundController.sound.Click ();
		switch(number_3)
		{
		case 1:
			LeanTween.cancel (GamePlayController.gamePlayController.loadModeGame.Goal_Boss);
			panel [2].SetActive (true);
			panel [3].SetActive (false);
			_play [1].SetActive (false);
			content.text = "Make this match to start splatting that cat!";
			content.transform.parent.localPosition = new Vector3 (0,-76);
			_mapControl.gridList [44].gameObject.layer = 2;
			_mapControl.gridList [45].gameObject.layer = 2;
			_mapControl.gridList [46].gameObject.layer = 2;
			_mapControl.gridList [51].gameObject.layer = 2;
			_mapControl.gridList [52].gameObject.layer = 2;
			_mapControl.gridList [53].gameObject.layer = 2;
			StartCoroutine (showSquare(45,52));
			number_3 = 2;
			break;
		case 2:
			_mapControl.touchInputHandler.touchInputMask = LayerMask.GetMask ("Grid");
			_mapControl.gridList [44].gameObject.layer = 15;
			_mapControl.gridList [45].gameObject.layer = 15;
			_mapControl.gridList [46].gameObject.layer = 15;
			_mapControl.gridList [51].gameObject.layer = 15;
			_mapControl.gridList [52].gameObject.layer = 15;
			_mapControl.gridList [53].gameObject.layer = 15;
			GamePlayController.gamePlayController.introduce.SetActive (false);
			GamePlayController.gamePlayController.isTurnHelp = true;
			number_3 = 3;
			break;
		default:
			break;
		}
	}
}
﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Spine.Unity;

public class TipController : MonoBehaviour 
{
	public GameObject[] items;
	public GameObject rocket_red;
	public GameObject ice_cream;
	public GameObject row;
	public GameObject column;
	public GameObject contents;
	public GameObject[] cow;
	public Sprite milk1;
	public GameObject sugar;
	public GameObject[] sugarPieces;
	public GameObject sugarColorEff;
	public GameObject itemeffs;
	public GameObject transformColor;
	public Text texts;
	public Image title;

	public Sprite[] titles;
	Vector3 OriginPos = new Vector3(-1.2f,-0.15f);
	float distance = 0.48f;
	[HideInInspector]
	public static int CaseNumber = 1;

	Vector3[] Case_1 = {new Vector3 (1,1),new Vector3(2,1),new Vector3(1,3),new Vector3(2,2),new Vector3(5,3)};
	Vector3[] Case_2 = {new Vector3 (1,2),new Vector3(2,2),new Vector3(3,2),new Vector3(1,1),new Vector3(2,1),
		new Vector3 (3,1),new Vector3(1,0),new Vector3(2,0),new Vector3(3,0),new Vector3(1,0),
		new Vector3 (2,0),new Vector3(3,0),new Vector3(1,1),new Vector3(2,1),new Vector3(3,1)};
	Vector3[] Case_3 = {new Vector3 (2,1),new Vector3(3,1)};
	Vector3[] Case_4 = {new Vector3 (1,1),new Vector3(2,1),new Vector3 (3,1),new Vector3(1,2),new Vector3 (2,2),new Vector3(3,2),new Vector3 (1,3),new Vector3(2,3),new Vector3 (3,3),
		new Vector3 (1,2),new Vector3(2,2),new Vector3 (3,3),new Vector3(1,3),new Vector3 (2,3),new Vector3(3,3)};
	Vector3[] Case_5 = {new Vector3 (1,0),new Vector3(1,1),new Vector3 (2,2),new Vector3(2,0),new Vector3 (2,1),new Vector3(3,2)};
	Vector3[] Case_6 = {new Vector3 (3,0),new Vector3(3,1),new Vector3 (4,2),new Vector3(2,0),new Vector3 (2,1),new Vector3(2,2)};
	Vector3[] Case_7 = {new Vector3 (1,1),new Vector3(1,2),new Vector3 (1,3),new Vector3(3,3),new Vector3 (2,1),new Vector3(2,2),new Vector3(2,3),new Vector3(3,2),new Vector3 (3,1),new Vector3(2,4),
		new Vector3 (2,5),new Vector3(2,6),new Vector3 (2,4),new Vector3(2,5),new Vector3 (2,6),new Vector3(1,1),new Vector3(1,1),new Vector3 (1,1)};
	Vector3[] Case_8 = {new Vector3 (1,0),new Vector3(2,0),new Vector3 (3,0),new Vector3(1,1),new Vector3 (2,1),new Vector3(3,1),new Vector3(3,2),new Vector3 (1,0),new Vector3(2,0),
		new Vector3 (3,0)};
	Vector3[] Case_9 = {new Vector3 (1,1),new Vector3(2,1),new Vector3 (3,2),new Vector3(2,1),new Vector3(3,1)};
	Vector3[] Case_10 = {new Vector3 (1,1),new Vector3(3,1),new Vector3 (2,1),new Vector3(3,2),new Vector3 (2,3),new Vector3 (2,2)};
	Vector3[] Case_11 = {new Vector3 (1,1),new Vector3(2,1),new Vector3 (3,1),new Vector3(1,2),new Vector3 (2,2), new Vector3 (3,3), new Vector3 (3,2)};
	Vector3[] Case_12 = {new Vector3 (0,2),new Vector3(1,2),new Vector3 (2,2),new Vector3(3,3),new Vector3 (4,2),new Vector3 (3,2)};
	Vector3[] Case_13 = {new Vector3 (1,1),new Vector3(0,2),new Vector3 (1,3),
		new Vector3 (1,4),new Vector3 (1,5),new Vector3(1,6),
		new Vector3 (1,4),new Vector3(1,5),new Vector3 (1,6),
		new Vector3 (1,4),new Vector3(1,5),new Vector3 (1,6),
		new Vector3 (1,4),new Vector3(1,5),new Vector3 (1,6),
		new Vector3 (4,0),new Vector3(5,0),new Vector3 (4,1),new Vector3 (5,1),new Vector3(4,2),new Vector3 (5,2),new Vector3 (4,3),new Vector3(5,3),new Vector3 (1,2)};
	Vector3[] Case_14 = { new Vector3 (3, 1), new Vector3 (3, 2)};

	public void TitleAndText(int _case)
	{
		switch (_case) 
		{
		case 1:
			title.sprite = titles [_case-1];
				texts.text = "A square match makes a Rocket. It turns a piece of the same color into a Blaster!";
				break;

			case 2:
			title.sprite = titles [_case-1];
				//title.text = "Clear Crackers!";
				texts.text = "Match over the crackers to feed the Dog!";
				break;

			case 3:
			title.sprite = titles [_case-1];
			//title.text = "Power-Ups!";
				texts.text = "Combine power-ups for a more powerful explosion!";
				break;

			case 4:
			title.sprite = titles [_case-1];
			//title.text = "Popsicle Mode!";
				texts.text = "Clear ice to find popsicles for the penguins!";
				break;

			case 5:
			title.sprite = titles [_case-1];
			//title.text = "Muffins!";
				texts.text = "Match fruit next to muffins to serve them!";
				break;

			case 6:
			title.sprite = titles [_case-1];
			//title.text = "Make Milk!";
				texts.text = "Match fruit next to a cow to serve milk!";
				break;

			case 7:
			title.sprite = titles [_case-1];
			//title.text = "Grow Fruit!";
				texts.text = "Match 3 times next to a bush to grow more fruit!";
				break;

			case 8:
			title.sprite = titles [_case-1];
			//title.text = "Match Bees!";
				texts.text = "Match 3 bees to create honey. Match over honey to serve it.";
				break;

			case 9:
			title.sprite = titles [_case-1];
			//title.text = "Remove Nets!";
				texts.text = "Make a match to unlock fruits!";
				break;

			case 10:
			title.sprite = titles [_case-1];
			//title.text = "Match Coconuts!";
				texts.text = "Match coconuts to create a drink. Match drinks to serve!";
				break;

			case 11:
			title.sprite = titles [_case-1];
			//title.text = "Ice Cubes!";
				texts.text = "ice Cubes turn into whatever pieces you match next to them!";
				break;

			case 12:
			title.sprite = titles [_case-1];
				//title.text = "Jam!";
				texts.text = "Jam spreads across the board and covers pieces! Match next to jam to clear it!";
				break;

			case 13:
			title.sprite = titles [_case-1];
			//title.text = "Fruit Pie!";
				texts.text = "Match adjacent to the Fruit Pie with all available colors!";
				break;

			case 14:
			title.sprite = titles [_case-1];
			//title.text = "Color Changer!";
				texts.text = "Every turn, the Color Changer transform any fruit over it!";
				break;

			default:
				break;
		}
	}

	void ShowAnm()
	{
		string nameFunction = "animtt_" + CaseNumber;
		StartCoroutine (nameFunction);
	}

	IEnumerator animtt_1()
	{		
		TileSpawner tileSpawn = GamePlayController.gamePlayController.mapController.tileSpawner;
		while (true) 
		{
			// Tranfrom
			for (int i = 0; i < items.Length; i++) 
			{
				if (i < 5) 
				{
					items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [4];
					items [i].GetComponent<RectTransform>().position = new Vector3(OriginPos.x+Case_1[i].x*distance,OriginPos.y+Case_1[i].y*distance);
					items [i].SetActive (true);
				} else {
					items [i].SetActive (false);
				}
			}

			rocket_red.GetComponent<RectTransform>().position = new Vector3(OriginPos.x+1*distance,OriginPos.y+2*distance);

			yield return new WaitForSeconds (1f);
			//Action and loop
			LeanTween.move (items [2], new Vector3 (OriginPos.x + 1 * distance, OriginPos.y + 2 * distance), 0.35f);
			yield return new WaitForSeconds (0.45f);

			for (int i = 0; i < 4; i++) {
				items [i].SetActive (false);
				GameObject obj = Instantiate (itemeffs);
				obj.transform.position = items [i].transform.position;
				Destroy (obj,0.25f);
			}
			yield return new WaitForSeconds (0.25f);
			rocket_red.SetActive (true);
			yield return new WaitForSeconds (0.35f);
			LeanTween.scale(rocket_red.transform.GetChild(0).GetComponent<RectTransform>(), new Vector3(0.4f,0.5f), 0.15f).setLoopClamp();

			LeanTween.move (rocket_red, items [4].GetComponent<RectTransform>().position, 0.55f).setOnComplete (() => {
				rocket_red.SetActive (false);
				items [4].GetComponent<Image> ().sprite = tileSpawn.rowBreakerSprite [4];
				LeanTween.rotate(items [4].GetComponent<RectTransform>(),-360,0.45f).setLoopOnce();
			});

			yield return new WaitForSeconds (1.5f);
		}
	}

	IEnumerator animtt_2()
	{
		// Tranfrom
		TileSpawner tileSpawn = GamePlayController.gamePlayController.mapController.tileSpawner;
		GridSpawner gridSpawn = GamePlayController.gamePlayController.mapController.gridSpawner;
		while (true) 
		{
			for (int i = 0; i < items.Length; i++) 
			{
				if (i < 15) {
					if (i >= 9) 
					{
						if (i != 10 && i != 12 && i != 14) 
						{
							items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [4];
						} 
						else 
						{
							items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [3];
						}
					} 
					else 
					{
						items [i].GetComponent<Image> ().sprite = gridSpawn.dynamicSpriteGrid [0];
					}
					// Position
					items [i].GetComponent<RectTransform>().position = new Vector3(OriginPos.x+Case_2[i].x*distance,OriginPos.y+Case_2[i].y*distance);
					items [i].SetActive (true);
				} else {
					items [i].SetActive (false);
				}
		}

			yield return new WaitForSeconds (1f);
			//Action and loop
			LeanTween.move (items [10], items [13].GetComponent<RectTransform>().position, 0.35f);
			LeanTween.move (items [13], items [10].GetComponent<RectTransform>().position, 0.35f).setOnComplete (() => {
				for (int i = 0; i < 15; i++) {
					if(i>=3)
					{
						items [i].SetActive (false);
						GameObject obj = Instantiate (itemeffs);
						obj.transform.position = items [i].transform.position;
						Destroy (obj,0.25f);
					}

				}});

			yield return new WaitForSeconds (1.5f);
		}
	}

	IEnumerator animtt_3()
	{
		// Tranfrom
		TileSpawner tileSpawn = GamePlayController.gamePlayController.mapController.tileSpawner;yield return new WaitForSeconds (0.45f);
		while (true) 
		{
			for (int i = 0; i < items.Length; i++) 
			{
				if (i < 2) {
					if(i == 0)
					{
						items [i].GetComponent<Image> ().sprite = tileSpawn.rowBreakerSprite [4];
					}
					else
					{
						items [i].GetComponent<Image> ().sprite = tileSpawn.columnBreakerSprite [1];
					}
					items [i].transform.position = new Vector3(OriginPos.x+Case_3[i].x*distance,OriginPos.y+Case_3[i].y*distance);
					items [i].SetActive (true);
				} 
				else 
				{
					items [i].SetActive (false);
				}
			}
			yield return new WaitForSeconds (1f);
			//Action and loop
			LeanTween.move (items [0], items[1].transform.position, 0.35f).setOnComplete (() => {
				items [0].SetActive(false);
				items [1].SetActive(false);
				row.SetActive(true);
				column.SetActive(true);
				LeanTween.scale(row.GetComponent<RectTransform>(),new Vector3 (15,1,1),0.55f);
				LeanTween.scale(column.GetComponent<RectTransform>(),new Vector3 (15,1,1),0.55f).setOnComplete(() =>
					{
						row.SetActive(false);
						column.SetActive(false);
						row.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
						column.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
					});
				});
			yield return new WaitForSeconds (1.5f);
		}
	}

	IEnumerator animtt_4()
	{
		// Tranfrom
		TileSpawner tileSpawn = GamePlayController.gamePlayController.mapController.tileSpawner;
		GridSpawner gridSpawn = GamePlayController.gamePlayController.mapController.gridSpawner;
		while (true) 
		{
			ice_cream.SetActive (true);
			ice_cream.GetComponent<RectTransform> ().localScale = Vector3.one * 0.45f;
			for (int i = 0; i < items.Length; i++) 
			{
				if (i < 15) 
				{
					if (i < 9) 
					{
						items [i].GetComponent<Image> ().sprite = gridSpawn.dynamicSpriteGrid [4];
						items [i].SetActive (true);
					} 
					else 
					{
						if (i >= 9 && i < 12) 
						{
							items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [4];
							items [i].SetActive (true);
						} 
						else 
						{
							items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [1];
							items [i].SetActive (false);
						}
					}
					items [i].transform.position = new Vector3 (OriginPos.x + Case_4 [i].x * distance, OriginPos.y + Case_4 [i].y * distance);
				}
				else 
				{
					items [i].SetActive (false);
				}
																																
			}
			yield return new WaitForSeconds (1f);
			//Action and loop
			LeanTween.move (items [11], items[5].transform.position, 0.45f).setOnComplete (() => {

				for(int i = 3; i < 12; i++)
				{
					if(i < 6 || i >=9)
					{
						items[i].SetActive(false);
						GameObject obj = Instantiate (itemeffs);
						obj.transform.position = items [i].transform.position;
						Destroy (obj,0.25f);
					}
				}
				for(int i = 12; i < 15; i++)
				{
					items[i].SetActive(true);
					if(i < 14)
					{
						LeanTween.move(items[i],items[i-12].transform.position,0.55f);
					}
					else
					{
						LeanTween.move(items[i],items[i-12].transform.position,0.55f).setOnComplete(() => {
							for(i = 0; i < 15; i++)
							{
								if(i < 3 || i > 11)
								{
									items[i].SetActive(false);
									GameObject obj = Instantiate (itemeffs);
									obj.transform.position = items [i].transform.position;
									Destroy (obj,0.25f);
								}
							}
							ice_cream.SetActive(true);
							LeanTween.scale(ice_cream.GetComponent<RectTransform>(),new Vector3(0.55f,0.55f,1),0.25f).setOnComplete(() =>
								{
									ice_cream.SetActive(false);
									ice_cream.GetComponent<RectTransform>().localScale = new Vector3(0.55f,0.55f,1);
								});
						});
					}
				}
			});
			yield return new WaitForSeconds (2f);
		}
	}

	IEnumerator animtt_5()
	{
		// Tranfrom
		TileSpawner tileSpawn = GamePlayController.gamePlayController.mapController.tileSpawner;
		while (true) 
		{
			for (int i = 0; i < items.Length; i++) 
			{
				if (i < 3) {
					items [i].GetComponent<Image> ().sprite = tileSpawn.muffinSprite [0];
					items [i].transform.position = new Vector3(OriginPos.x+Case_5[i].x*distance,OriginPos.y+Case_5[i].y*distance);
					items [i].SetActive (true);
				} 
				else
				{
					if(i>=3 && i<6)
					{
						items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [4];
						items [i].transform.position = new Vector3(OriginPos.x+Case_5[i].x*distance,OriginPos.y+Case_5[i].y*distance);
						items [i].SetActive (true);
					}
					else 
					{
						items [i].SetActive (false);
					}
				}
			}
			yield return new WaitForSeconds (1f);
			//Action and loop
			LeanTween.move (items [5], items[2].transform.position, 0.35f);
			LeanTween.move (items [2], items[5].transform.position, 0.45f).setOnComplete (() => {
				for (int i = 0; i < 6; i++) {
					items [i].SetActive (false);
					GameObject obj = Instantiate (itemeffs);
					obj.transform.position = items [i].transform.position;
					Destroy (obj,0.25f);
				}});

			yield return new WaitForSeconds (1.5f);
		}
	}

	IEnumerator animtt_6()
	{
		// Tranfrom
		TileSpawner tileSpawn = GamePlayController.gamePlayController.mapController.tileSpawner;
		while (true) 
		{
			for (int i = 0; i < items.Length; i++) 
			{
				if (i < 6) {
					if (i < 3) 
					{
						items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [4];
						items [i].SetActive (true);
						cow [i].SetActive (true);
					} 
					else 
					{
						items [i].GetComponent<Image> ().sprite = milk1;
						items [i].SetActive (false);
					}
					items [i].transform.position = new Vector3(OriginPos.x+Case_6[i].x*distance,OriginPos.y+Case_6[i].y*distance);
				} 
				else 
				{
					items [i].SetActive (false);
				}
			}
			yield return new WaitForSeconds (1f);
			//Action and loop
			LeanTween.move (items [2], new Vector3(items [2].transform.position.x-distance, items [2].transform.position.y), 0.35f).setOnComplete (() => {
				for (int i = 0; i < 3; i++) {
					items [i].SetActive (false);
					GameObject obj = Instantiate (itemeffs);
					obj.transform.position = items [i].transform.position;
					Destroy (obj,0.25f);
					//cow[i].GetComponentInChildren<SkeletonAnimation>().loop = true;
					cow[i].GetComponentInChildren<SkeletonAnimation>().state.SetAnimation(1,"smile",false);

				}

				for(int i = 3; i < 6; i++)
				{
					items[i].SetActive(true);
					LeanTween.move(items[i], new Vector3(items[i].transform.position.x-distance, items[i].transform.position.y+distance), 0.35f).setOnComplete(() => {
						items[i].SetActive(false);
					});
				}


			});
			//LeanTween.scaleX ();
			//LeanTween.scaleY ();
			yield return new WaitForSeconds (2.5f);
		}
	}

	IEnumerator animtt_7()
	{
		for (int i = 0; i < 3; i++) 
		{
			cow [i].SetActive (false);
		}
		// Tranfrom
		TileSpawner tileSpawn = GamePlayController.gamePlayController.mapController.tileSpawner;
		while (true) 
		{
			for (int i = 0; i < items.Length; i++) 
			{
				if(i < 3)
				{
					cow [i].SetActive (false);
				}
				if (i < 18) 
				{
					if(i == 0)
					{
						items [i].GetComponent<Image> ().sprite = tileSpawn.fountainSprite [16];
						items [i].SetActive (true);
					}
					else
					{
						if (i < 4) {
							items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [5];
							items [i].SetActive (true);
						}
						else 
						{
							if (i < 8) 
							{
								items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [3];
								items [i].SetActive (true);
								if(i == 5)
								{
									items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [1];
									items [i].SetActive (true);
								}
							}
							else 
							{
								if(i < 9)
								{
									items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [4];
									items [i].SetActive (true);
								}
								else
								{
									if (i < 12) {
										items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [4];
										items [i].SetActive (false);
									}
									else 
									{
										if (i < 15) {
											items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [1];
										}
										else 
										{
											items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [4];
										}
										items [i].SetActive (false);
									}

								}
							}
						}
					}
					// Position
					items [i].transform.position = new Vector3(OriginPos.x+Case_7[i].x*distance,OriginPos.y+Case_7[i].y*distance);
				} 
				else 
				{
					items [i].SetActive (false);
				}
			}
			yield return new WaitForSeconds(1f);
			//Action and loop
			LeanTween.move (items [5], items [7].transform.position, 0.35f);
			LeanTween.move (items [7], items [5].transform.position, 0.35f);
			yield return new WaitForSeconds(0.45f);
			for (int i = 4; i < 8; i++) 
			{
				if(i != 5)
				{
					items [i].SetActive (false);
					GameObject obj = Instantiate (itemeffs);
					obj.transform.position = items [i].transform.position;
					Destroy (obj,0.25f);
				}

			}
			items [0].GetComponent<Image> ().sprite = tileSpawn.fountainSprite [17];
			yield return new WaitForSeconds(0.65f);
			for (int i = 9; i < 12; i++) 
			{
				items [i].SetActive (true);
				int k = i - 5;
				if(k == 5)
				{
					k = 7;
				}
				LeanTween.move (items [i], items [k].transform.position, 0.35f);
			}
				
			yield return new WaitForSeconds(0.45f);

			for (int i = 9; i < 12; i++) 
			{
				items [i].SetActive (false);
				GameObject obj = Instantiate (itemeffs);
				obj.transform.position = items [i].transform.position;
				Destroy (obj,0.25f);
			}
			items [0].GetComponent<Image> ().sprite = tileSpawn.fountainSprite [18];
			yield return new WaitForSeconds(0.35f);
			for (int i = 12; i < 15; i++) 
			{
				items [i].SetActive (true);

				int k = i - 8;
				if(k == 5)
				{
					k = 7;
				}

				LeanTween.move (items [i], items [k].transform.position, 0.35f);
			}
			yield return new WaitForSeconds(0.45f);

			for (int i = 12; i < 15; i++) 
			{
				items [i].SetActive (false);
				GameObject obj = Instantiate (itemeffs);
				obj.transform.position = items [i].transform.position;
				Destroy (obj,0.25f);
			}
			items [0].GetComponent<Image> ().sprite = tileSpawn.fountainSprite [19];
//			yield return new WaitForSeconds (2f);
			for (int i = 15; i < 18; i++) 
			{
				items [i].SetActive (true);

			}
			LeanTween.move (items [15], items [2].transform.position, 0.35f);
			LeanTween.move (items [16], items [3].transform.position, 0.35f);
			LeanTween.move (items [17], items [8].transform.position, 0.35f);
			items [2].SetActive (false);
			items [3].SetActive (false);
			items [8].SetActive (false);
			yield return new WaitForSeconds (1f);
		}
	}

	IEnumerator animtt_8()
	{
		// Tranfrom
		TileSpawner tileSpawn = GamePlayController.gamePlayController.mapController.tileSpawner;
		GridSpawner gridSpawn = GamePlayController.gamePlayController.mapController.gridSpawner;

		while (true) 
		{
			for (int i = 0; i < items.Length; i++) 
			{
				if (i < 10) {
					if (i < 3) 
					{
						items [i].GetComponent<Image> ().sprite = gridSpawn.dynamicSpriteGrid[3];
						items [i].SetActive (false);
					} 
					else 
					{
						if (i < 6) 
						{
							if (i != 5) 
							{
								items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [3];
							} 
							else 
							{
								items [i].GetComponent<Image> ().sprite = tileSpawn.beeSprite;
							}
							items [i].SetActive (true);
						} 
						else 
						{
							if (i == 6) {
								items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [3];
								items [i].SetActive (true);
							}
							else 
							{
								if (i != 9) 
								{
									items [i].GetComponent<Image> ().sprite = tileSpawn.beeSprite;
								} 
								else 
								{
									items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [4];
								}
								items [i].SetActive (true);
							}
						}
					}
					items [i].transform.position = new Vector3(OriginPos.x+Case_8[i].x*distance,OriginPos.y+Case_8[i].y*distance);
				} 
				else 
				{
					items [i].SetActive (false);
				}
			}
			yield return new WaitForSeconds (1f);
			//Action and loop
			LeanTween.move (items [5], items[9].transform.position, 0.35f);
			LeanTween.move (items [9], items [5].transform.position, 0.35f);
			yield return new WaitForSeconds (0.45f);
			items [7].SetActive (false);
			GameObject obj7 = Instantiate (itemeffs);
			obj7.transform.position = items [7].transform.position;
			Destroy (obj7,0.25f);
			items [8].SetActive (false);
			GameObject obj8 = Instantiate (itemeffs);
			obj8.transform.position = items [8].transform.position;
			Destroy (obj8,0.25f);
			items [5].SetActive (false);
			GameObject obj5 = Instantiate (itemeffs);
			obj5.transform.position = items [5].transform.position;
			Destroy (obj5,0.25f);

			for(int i = 0; i < 3; i++)
			{
				items [i].SetActive (true);
			}
			yield return new WaitForSeconds (0.1f);
			LeanTween.move (items [3], items[0].transform.position, 0.35f);
			LeanTween.move (items [4], items[1].transform.position, 0.35f);
			LeanTween.move (items [6], items[9].transform.position, 0.35f);
			LeanTween.move (items [9], items[5].transform.position, 0.35f);
			yield return new WaitForSeconds (0.55f);
			LeanTween.move (items [9], items[6].transform.position, 0.35f);
			LeanTween.move (items [6], items[9].transform.position, 0.35f);
			yield return new WaitForSeconds (0.45f);
			for(int i = 0; i < 5; i++)
			{
				items [i].SetActive (false);
				GameObject obj = Instantiate (itemeffs);
				obj.transform.position = items [i].transform.position;
				Destroy (obj,0.25f);
			}
			items [6].SetActive (false);
			yield return new WaitForSeconds (0.15f);
			LeanTween.move (items [9], items[2].transform.position, 0.35f);
			yield return new WaitForSeconds (1f);
		}
	}

	IEnumerator animtt_9()
	{
		// Tranfrom
		TileSpawner tileSpawn = GamePlayController.gamePlayController.mapController.tileSpawner;
		GridSpawner gridSpawn = GamePlayController.gamePlayController.mapController.gridSpawner;
		while (true) 
		{
			for (int i = 0; i < items.Length; i++) 
			{
				if (i < 5) {
					if (i < 3) 
					{
						items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [4];
						items [i].SetActive (true);
					} 
					else 
					{
						if (i == 3) 
						{
							items [i].GetComponent<Image> ().sprite = tileSpawn.iceCageSprite;;
							items [i].SetActive (true);
						}
						else 
						{
							items [i].SetActive (false);
						}
					}

					// Position
					items [i].transform.position = new Vector3(OriginPos.x+Case_9[i].x*distance,OriginPos.y+Case_9[i].y*distance);

				} 
				else 
				{
					items [i].SetActive (false);
				}
			}
			yield return new WaitForSeconds (1f);
			//Action and loop
			LeanTween.move (items [2], items[4].transform.position, 0.35f);
			yield return new WaitForSeconds (0.45f);
			for(int i = 0; i < 4; i++)
			{
				if(i != 1)
				{
					items [i].SetActive (false);
					GameObject obj = Instantiate (itemeffs);
					obj.transform.position = items [i].transform.position;
					Destroy (obj,0.25f);
				}
			}

			yield return new WaitForSeconds (1f);
		}
	}

	IEnumerator animtt_10()
	{
		// Tranfrom
		TileSpawner tileSpawn = GamePlayController.gamePlayController.mapController.tileSpawner;
		while (true) 
		{
			for (int i = 0; i < items.Length; i++) 
			{
				if (i < 6) {
					if (i < 2) 
					{
						items [i].GetComponent<Image> ().sprite = tileSpawn.coconutSprite [1];
						items [i].SetActive (true);
					}
					else 
					{
						if (i < 5) 
						{
							items [i].GetComponent<Image> ().sprite = tileSpawn.coconutSprite [0];
							items [i].SetActive (true);
						}
						else 
						{
							items [i].SetActive (false);
						}
					}
					// Position
					items [i].transform.position = new Vector3(OriginPos.x+Case_10[i].x*distance,OriginPos.y+Case_10[i].y*distance);
				} 
				else 
				{
					items [i].SetActive (false);
				}
			}
			yield return new WaitForSeconds (1f);
		//Action and loop
			LeanTween.move (items [3], items [5].transform.position, 0.35f);
			yield return new WaitForSeconds (0.45f);
			for(int i = 2; i < 5; i++)
			{
				items [i].SetActive (false);
				items [i].SetActive (false);
				GameObject obj = Instantiate (itemeffs);
				obj.transform.position = items [i].transform.position;
				Destroy (obj,0.25f);
			}
			items [3].GetComponent<Image> ().sprite = tileSpawn.coconutSprite [1];
			items [3].SetActive (true);
			yield return new WaitForSeconds (0.15f);
			LeanTween.move (items [3], items [2].transform.position, 0.35f);
			yield return new WaitForSeconds (0.45f);
			for(int i = 0; i < 4; i++)
			{
				items [i].SetActive (false);
				items [i].SetActive (false);
				GameObject obj = Instantiate (itemeffs);
				obj.transform.position = items [i].transform.position;
				Destroy (obj,0.25f);
			}
			yield return new WaitForSeconds (1f);
		}
	}

	IEnumerator animtt_11()
	{
		// Tranfrom
		TileSpawner tileSpawn = GamePlayController.gamePlayController.mapController.tileSpawner;
		while (true) 
		{
			for (int i = 0; i < items.Length; i++) 
			{
				if (i < 7) {
					if (i < 3) {
						items [i].GetComponent<Image> ().sprite = tileSpawn.iceCubeSprite;
						items [i].SetActive (true);
					}
					else 
					{
						if (i < 6) {
							items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [3];
							items [i].SetActive (true);
						} 
						else 
						{
							items [i].SetActive (false);
						}
					}
					items [i].transform.position = new Vector3(OriginPos.x+Case_11[i].x*distance,OriginPos.y+Case_11[i].y*distance);
				} 
				else 
				{
					items [i].SetActive (false);
				}
			}
			yield return new WaitForSeconds (1f);
			//Action and loop
			LeanTween.move (items [5], items [6].transform.position, 0.35f);
			yield return new WaitForSeconds (0.45f);
			for(int i = 3; i < 6; i++)
			{
				items [i].SetActive (false);
				GameObject obj = Instantiate (itemeffs);
				obj.transform.position = items [i].transform.position;
				Destroy (obj,0.25f);
			}
			yield return new WaitForSeconds (0.1f);
			for(int i = 0; i < 3; i++)
			{
				items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [3];
			}
			yield return new WaitForSeconds (0.35f);
			for(int i = 0; i < 3; i++)
			{
				items [i].SetActive (false);
				GameObject obj = Instantiate (itemeffs);
				obj.transform.position = items [i].transform.position;
				Destroy (obj,0.25f);
			}
			yield return new WaitForSeconds (1f);
		}
	}

	IEnumerator animtt_12()
	{
		// Tranfrom
		TileSpawner tileSpawn = GamePlayController.gamePlayController.mapController.tileSpawner;
		while (true) 
		{
			for (int i = 0; i < items.Length; i++) 
			{
				if (i < 6) {
					if(i == 0)
					{
						items [i].GetComponent<Image> ().sprite = tileSpawn.yogurtCupSprite;
						items [i].SetActive (true);
					}
					if(i == 1)
					{
						items [i].GetComponent<Image> ().sprite = tileSpawn.yogurtSprite;
						items [i].SetActive (true);
					}

					if (i >= 2 && i < 5) {
						items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [0];
						items [i].SetActive (true);
					}

					if(i == 5)
					{
						items [i].SetActive (false);
					}
					items [i].transform.position = new Vector3(OriginPos.x+Case_12[i].x*distance,OriginPos.y+Case_12[i].y*distance);

				} 
				else 
				{
					items [i].SetActive (false);
				}
			}
			yield return new WaitForSeconds (1f);
			//Action and loop
			LeanTween.move (items [3], items[5].transform.position, 0.35f);
			yield return new WaitForSeconds (0.45f);
			for(int i = 1; i < 5; i++)
			{
				items [i].SetActive (false);
				GameObject obj = Instantiate (itemeffs);
				obj.transform.position = items [i].transform.position;
				Destroy (obj,0.25f);
			}
			yield return new WaitForSeconds (1f);
		}
	}

	IEnumerator animtt_13()
	{
		// Tranfrom
		TileSpawner tileSpawn = GamePlayController.gamePlayController.mapController.tileSpawner;
		sugar.SetActive (true);
		while (true) 
		{
			sugar.SetActive (true);
			foreach (GameObject obj in sugarPieces) {
				obj.SetActive (false);
			}
			sugarColorEff.GetComponent<RectTransform> ().localScale = Vector3.one * 0.35f;
			for (int i = 0; i < items.Length; i++) 
			{
				if (i < 24) {
					if (i < 3) {
						items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [1];
						items [i].SetActive (true);
					}
					else 
					{
						if (i < 6) {
							items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [0];
							items [i].SetActive (false);
						}
						else 
						{
							if (i < 9) {
								items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [4];
								items [i].SetActive (false);
							}
							else 
							{
								if (i < 12) {
									items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [5];
									items [i].SetActive (false);
								}
								else 
								{
									if (i < 15) {
										items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [2];
										items [i].SetActive (false);
									}
								}
							}
						}
					}
					if(i == 15 || i == 16 || i== 19 || i == 20)
					{
						items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [4];
						items [i].SetActive (true);
					}

					if(i == 17 || i == 18 || i== 21 || i == 22)
					{
						items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite [5];
						items [i].SetActive (true);
					}

					if (i == 23) 
					{
						items [i].SetActive (false);
					}

					items [i].transform.position = new Vector3(OriginPos.x+Case_13[i].x*distance,OriginPos.y+Case_13[i].y*distance);
				} 
				else 
				{
					items [i].SetActive (false);
				}
			}
			yield return new WaitForSeconds (1f);
			//Action and loop
			LeanTween.move (items [1], items[23].transform.position, 0.35f);
			yield return new WaitForSeconds (0.45f);
			for(int i = 0; i < 3; i++)
			{
				items [i].SetActive (false);
				GameObject obj = Instantiate (itemeffs);
				obj.transform.position = items [i].transform.position;
				Destroy (obj,0.25f);
				sugarPieces [2].SetActive (true);
			}
			yield return new WaitForSeconds (0.15f);
			for(int i = 3; i < 6; i++)
			{
				items [i].SetActive (true);
				LeanTween.move (items [i], items[i-3].transform.position, 0.35f);
			}
			yield return new WaitForSeconds (0.45f);
			for(int i = 3; i < 6; i++)
			{
				items [i].SetActive (false);
				GameObject obj = Instantiate (itemeffs);
				obj.transform.position = items [i].transform.position;
				Destroy (obj,0.25f);
				sugarPieces [3].SetActive (true);
			}
			yield return new WaitForSeconds (0.15f);
			for(int i = 6; i < 9; i++)
			{
				items [i].SetActive (true);
				LeanTween.move (items [i], items[i-6].transform.position, 0.35f);
			}
			yield return new WaitForSeconds (0.45f);
			for(int i = 6; i < 9; i++)
			{
				items [i].SetActive (false);
				GameObject obj = Instantiate (itemeffs);
				obj.transform.position = items [i].transform.position;
				Destroy (obj,0.25f);
				sugarPieces [4].SetActive (true);
			}
			yield return new WaitForSeconds (0.15f);
			for(int i = 9; i < 12; i++)
			{
				items [i].SetActive (true);
				LeanTween.move (items [i], items[i-9].transform.position, 0.35f);
			}
			yield return new WaitForSeconds (0.45f);
			for(int i = 9; i < 12; i++)
			{
				items [i].SetActive (false);
				GameObject obj = Instantiate (itemeffs);
				obj.transform.position = items [i].transform.position;
				Destroy (obj,0.25f);
				sugarPieces [1].SetActive (true);
			}
			yield return new WaitForSeconds (0.15f);
			for(int i = 12; i < 15; i++)
			{
				items [i].SetActive (true);
				LeanTween.move (items [i], items[i-12].transform.position, 0.35f);
			}
			yield return new WaitForSeconds (0.45f);
			for(int i = 12; i < 15; i++)
			{
				items [i].SetActive (false);
				GameObject obj = Instantiate (itemeffs);
				obj.transform.position = items [i].transform.position;
				Destroy (obj,0.25f);
				sugarPieces [0].SetActive (true);
			}
			yield return new WaitForSeconds (0.15f);
			sugar.SetActive (false);
			sugarColorEff.SetActive (true);
			LeanTween.scale (sugarColorEff.GetComponent<RectTransform> (), Vector3.one * 2f, 0.25f);
			yield return new WaitForSeconds (0.25f);
			sugarColorEff.SetActive (false);
			for(int i = 15; i < 23; i++)
			{
				items [i].SetActive (false);
				GameObject obj = Instantiate (itemeffs);
				obj.transform.position = items [i].transform.position;
				Destroy (obj,0.25f);
			}
			yield return new WaitForSeconds (1f);
		}
	}

	IEnumerator animtt_14()
	{
		sugar.SetActive (false);
		// Tranfrom
		TileSpawner tileSpawn = GamePlayController.gamePlayController.mapController.tileSpawner;
		GridSpawner gridSpawn = GamePlayController.gamePlayController.mapController.gridSpawner;
		while (true) 
		{
			for (int i = 0; i < items.Length; i++) 
			{
				if (i < 2) {
					if(i == 0)
					{
						items [i].GetComponent<Image> ().sprite = gridSpawn.colorSpriteGrid[5];
					}

					if(i == 1)
					{
						items [i].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite[4];
					}
					items [i].transform.position = new Vector3(OriginPos.x+Case_14[i].x*distance,OriginPos.y+Case_14[i].y*distance);
					items [i].SetActive (true);
				} 
				else 
				{
					items [i].SetActive (false);
				}
			}
			yield return new WaitForSeconds (1f);
			//Action and loop
			LeanTween.move (items [1], items[0].transform.position, 0.35f);
			yield return new WaitForSeconds (0.45f);
			items [1].GetComponent<Image> ().sprite = tileSpawn.matchObjectSprite[5];
			transformColor.SetActive (true);
			yield return new WaitForSeconds (0.15f);
			transformColor.SetActive (false);
			yield return new WaitForSeconds (1f);
		}
	}
	public GameObject panel;
	public void Show()
	{
		TitleAndText (CaseNumber);
		gameObject.SetActive (true);
		contents.SetActive (true);
		StartCoroutine (_Show());
	}

	IEnumerator _Show()
	{
		Reset ();
		panel.SetActive (true);
		GamePlayController.gamePlayController.mapController.touchInputHandler.touchInputMask = LayerMask.GetMask("UI");
		LeanTween.move (contents, Vector3.zero, 0.55f).setEase(LeanTweenType.easeOutBack);
		yield return new WaitForSeconds (0.65f);
		ShowAnm ();
	}

	public void Hide()
	{
		SoundController.sound.Click ();
		Reset ();
		panel.SetActive (false);
		LeanTween.move(contents.GetComponent<RectTransform>(), new Vector3(0,-915), 0.55f).setOnComplete(() => {
			gameObject.SetActive (false);
			contents.transform.localPosition = new Vector3(0,915);
		}).setEase(LeanTweenType.easeInBack);
		CaseNumber = 1;
		GamePlayController.gamePlayController.mapController.touchInputHandler.touchInputMask = LayerMask.GetMask("Grid");
		StopAllCoroutines ();
	}

	public void Next()
	{
		CaseNumber++;
		if (CaseNumber < 15) 
		{
			SoundController.sound.Click ();
			gameObject.SetActive (false);
			contents.transform.localPosition = new Vector3 (0, 915);
			Show ();
		} 
		else 
		{
			Hide ();
		}
	}

	void Reset()
	{
		LeanTween.cancelAll (true);
		foreach(GameObject obj in items)
		{
			obj.SetActive (false);
			obj.transform.rotation = Quaternion.Euler (0,0,0);
		}
		ice_cream.SetActive (false);
		foreach(GameObject obj in cow)
		{
			obj.SetActive (false);
		}
		sugar.SetActive (false);
		rocket_red.SetActive (false);
		row.SetActive (false);
		column.SetActive (false);
		sugarColorEff.SetActive (false);
		transformColor.SetActive (false);
	}
}

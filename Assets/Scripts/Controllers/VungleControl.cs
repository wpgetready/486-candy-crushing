﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VungleControl : MonoBehaviour {

	public string vungleID_Android;
	public string vungleID_IOS;
	bool isRun = false;

	// Use this for initialization
	public void VungleInit () 
	{
		Vungle.onAdStartedEvent += onAdStartedEvent;
		Vungle.onAdFinishedEvent += onAdFinishedEvent;
		//Vungle.adPlayableEvent += adPlayableEvent;
		Vungle.init (vungleID_Android, vungleID_IOS);
	}
	
	void onAdStartedEvent()
	{
		isRun = false;
		//Debug.Log("onAdStartedEvent");
	}
	
	
	void onAdFinishedEvent(AdFinishedEventArgs arg)
	{
		#if UNITY_IOS
		SoundController.sound.audioSource.mute = false;
		MusicController.music.audioSource.mute = false;
		#endif
		if (isRun == false) 
		{
			AdsControl.instance.addCoins.AddReward (true, AddCoins.rewardAds);
			isRun = true;
		}
		VungleInit ();
	}
	
	void adPlayableEvent(bool playable)
	{
		if (playable == true) {
			GameObject.FindGameObjectWithTag ("VungleBT").SetActive (true);
			//Debug.Log("vungle loaded");
			//AdsControl.instance.texttt.text = "network";
		} 
		else 
		{
			GameObject.FindGameObjectWithTag ("VungleBT").SetActive (false);
			if(Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork || Application.internetReachability ==NetworkReachability.ReachableViaCarrierDataNetwork)
			{
				//Debug.Log("vungle not load");
				VungleInit();
			}
		}
	}
	
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShopController : MonoBehaviour
{
	public CoinController coinController;
	public Text ownCoins;

	void OnEnable()
	{
		int coins = PlayerPrefs.GetInt ("coins");
		ownCoins.text = coins.ToString ();
	}

	public void Close()
	{
		iTween.MoveTo (gameObject, iTween.Hash ("position", new Vector3 (0, 11f, 0), "time", 0.35f, "easetype", iTween.EaseType.easeOutBack, "oncomplete", "SetActive", "oncompletetarget", gameObject));
	}
	
	void SetActive()
	{
		gameObject.SetActive (false);
	}

	public void GetCoin()
	{
		coinController.MoveDown (2);
	}
}

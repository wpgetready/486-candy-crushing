﻿/*
 * @Author: CuongNH
 * @Description: Xu ly cac hieu ung ve cac nhac nen cua game
 * */

using UnityEngine;
using System.Collections;

public class MusicController : MonoBehaviour {

    public static MusicController music;

    public AudioClip[] musicClips;
	[Range(0,1)]
	public float[] bg_Volume;

    public AudioSource audioSource;

	public AudioClip Win;
	[Range(0,1)]
	public float Win_Volume;

	public AudioClip Lose;
	[Range(0,1)]
	public float Lose_Volume;

    // Use when object have awaked
    void Awake()
    {
        if (music == null)
        {
            music = this;

            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            DestroyImmediate(this.gameObject);
        }
    }

    // Turn on music
    public void MusicOn()
    {
        audioSource.mute = false; 
    }

    // Turn off music
    public void MusicOff()
    {
        audioSource.mute = true; 
    }

    // Play music background on home
    public void PlayOnHome()
    {
		audioSource.volume = bg_Volume[0];
        audioSource.clip = musicClips[0];
		audioSource.loop = true;
        audioSource.Play();
    }

    // Play music background on map
    public void PlayOnMap()
    {
		audioSource.Stop ();
		audioSource.volume = bg_Volume[0];
        audioSource.clip = musicClips[0];
		audioSource.loop = true;
        audioSource.Play();
    }

    // Play music background on game play
    public void PlayOnGamePlay()
    {
		audioSource.Stop ();
		int rand;
		if (Contance.MODE == "boss_battle") 
		{
			rand = musicClips.Length-1;
			audioSource.volume = bg_Volume[musicClips.Length-1];
		} 
		else 
		{
			rand = Random.Range(1,3);
			audioSource.volume = bg_Volume[rand];
		}

		audioSource.clip = musicClips[rand];
		audioSource.loop = true;
        audioSource.Play();
    }
	[HideInInspector]
	public bool isRunOne = false;
	public void PlayWin()
	{
		if(isRunOne == false)
		{
			audioSource.Stop ();
			audioSource.clip = Win;
			audioSource.volume = Win_Volume;
			audioSource.loop = true;
			audioSource.Play();
			isRunOne = true;
		}
	}
	[HideInInspector]
	public bool isRunOneLose = false;
	public void PlayLose()
	{
		if(isRunOneLose == false)
		{
			audioSource.Stop ();
			audioSource.clip = Lose;
			audioSource.volume = Lose_Volume;
			audioSource.loop = true;
			audioSource.Play();
			isRunOneLose = true;
		}
	}

    // Set volumn
    public void SetVolumn(float vol)
    {
        audioSource.volume = vol;
    }
}

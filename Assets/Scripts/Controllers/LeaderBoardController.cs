﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using Facebook.Unity;
using Facebook.MiniJSON;
using System.Collections.Generic;

public class LeaderBoardController : MonoBehaviour 
{
	public Texture[] defaultImage;
	public string[] defaultName;

	public static int oldNumberClick;
	public static int numberClick;
	bool isNewData;

	public static int numberLb;
	string[] items;

	public static string inputLevel;
	public static string[] inputID;
	public static string[] inputScore;
	public static string[] inputName;

	public static string[] sendID;
	public static string[] sendScore;
	public static string[] sendName;
	int currentLevelScore;

//	public string[] _inputID;
//	public string[] _inputScore;
//	public string[] _inputName;

	private static int runOneTime = 0;

	public GameObject obj;
	public Transform parentObj;

	public static bool isHasData;

	//public RawImage raw;

	string CreateUserURL = "http://dotavn.webstarterz.com/LeaderboardCandyJuice/InsertData.php";

	//string DeleteDataRow = "http://dotavn.webstarterz.com/LeaderboardCandyJuice/DeleteRow.php";

	string LeaderboardUrl = "http://dotavn.webstarterz.com/LeaderboardCandyJuice/Leaderboard.php";

	string UpdateUrl = "http://dotavn.webstarterz.com/LeaderboardCandyJuice/Updatedata.php";

	GameObject[] allHighScore;
	LeaderboardObjCtr[] leaderObjctr;
	int count;

	bool isPost;

	void Awake()
	{
		numberLb = 5;
		allHighScore = new GameObject[numberLb];
		leaderObjctr = new LeaderboardObjCtr[numberLb];;
		if(runOneTime == 0)
		{
			isPost = true;


			inputID = new string[numberLb];
			inputScore = new string[numberLb];
			inputName = new string[numberLb];
			sendID = new string[numberLb];
			sendScore = new string[numberLb];
			sendName = new string[numberLb];
			for(int i = 0; i < numberLb; i++)
			{
				inputID[i] = "0";
				inputScore[i] = "0";
				inputName[i] = "0";
			}
			runOneTime = 1;
			Debug.Log("runOneTime");
		}
		oldNumberClick = Contance.NUMBER_LEVEL;
		PostData ();
//		_inputID = inputID;
//		_inputScore = inputScore;
//		_inputName = inputName;
	}

	WWW www;

	string levelField = "Level";
	string idField = "ID_";
	string scoreField = "Score_";
	string nameField = "Name_";

	IEnumerator CreateData(string[] sendID, string[] sendScore, string[] sendName, string Level, bool updateValue = false)
	{
		WWWForm form = new WWWForm();
		form.AddField(levelField/*"Level"*/, Level);
		for(int i = 0; i<numberLb;i++)
		{
			form.AddField(idField + (i+1).ToString(), sendID[i]);
			form.AddField(scoreField + (i+1).ToString(), sendScore[i]);
			form.AddField(nameField + (i+1).ToString(), sendName[i]);
		}

		Debug.Log (sendScore[0]);
		if(updateValue == true)
		{
			www = new WWW(UpdateUrl, form);
			//Debug.Log("updateValue");
		}
		else
		{
			www = new WWW(CreateUserURL, form);
			//Debug.Log("sendValue");
		}
		yield return www;
		//Debug.Log ("sendData");
	}

//	IEnumerator DeleteRow(int rowNumber)
//	{
//		WWW itemsData = new WWW(DeleteDataRow+"?number="+rowNumber.ToString());
//		yield return itemsData;
//	}

	public static bool isConnect;
	//int call;
	IEnumerator LeaderTable()
	{	
		yield return www;
		//Debug.Log ("Level: " + numberClick);
		WWW itemsData = new WWW(LeaderboardUrl+"?number="+(numberClick).ToString()); 
		yield return itemsData;
		//Debug.Log (itemsData.text);
		if (itemsData.error != null) 
		{
			isConnect = false;
		} 
		else 
		{
			isConnect = true;
			Debug.Log("isConnect...true");
		}
		string itemsDataString = itemsData.text;
		items = itemsDataString.Split(';');
		//Debug.Log(GetDataValue(items[0], "ID_1:"));

		GetData ();
		ShowData ();
	}

	public void ShowLeaderBoard()
	{
		if(PopupGame.isNewBest == false)
		{
			StartCoroutine (LeaderTable());
		}
	}

	void GetData()
	{
		int counts = items.Length-2;

		if(counts >= 0)
		{
			for(int i = 0; i < numberLb; i++)
			{
				string _inputID = GetDataValue(items[counts], "ID_"+(i+1).ToString() + ":");
				if(string.IsNullOrEmpty(_inputID) == false)
				{
					inputID[i] = _inputID;
				}

				string _inputScore = GetDataValue(items[counts], "Score_"+(i+1).ToString() + ":");
				if(string.IsNullOrEmpty(_inputScore) == false)
				{
					inputScore[i] = _inputScore;
				}

				string _inputName = GetDataValue(items[counts], "Name_"+(i+1).ToString() + ":");
				if(string.IsNullOrEmpty(_inputName) == false)
				{
					inputName[i] = _inputName;
				}
			}
			isHasData = true;
		}
		else
		{
			isHasData = false;
		}
	}

	void PostData()
	{
		//Debug.Log("isNewBest"+PopupGame.isNewBest);
		//Debug.Log("isConnect"+isConnect);
		if (PopupGame.isNewBest == true && isConnect == true) 
		{
			//Debug.Log("PostData");
			int count = 0;
			int switchScore = 0;
			int newHighScore = PlayerPrefs.GetInt ("BestScore" + oldNumberClick.ToString ());

			//Debug.Log("oldNumberClick..." + oldNumberClick);

			string switchName = "";
			string switchID = "";

			string saveName = PlayerPrefs.GetString("userName");
			string saveID = PlayerPrefs.GetString("userID");
			Debug.Log (""+saveName+".....saveID"+saveID);
			int isTop = 0;
			for (int i = 0; i < inputScore.Length; i++) 
			{
				//Debug.Log("Check Higher Score");
				//Debug.Log("inputScore" + i +Int32.Parse (inputScore [i]));
				//Debug.Log("saveID..."+ saveID);
				//Debug.Log("newHighScore..."+ newHighScore);
				if (string.IsNullOrEmpty(saveID) == false && newHighScore > Int32.Parse (inputScore [i])) 
				{
					switchScore = Int32.Parse (inputScore [i]);
					inputScore [i] = newHighScore.ToString ();
					newHighScore = switchScore;

					switchName = inputName [i];
					inputName [i] = saveName;
					saveName = switchName;

					switchID = inputID [i];
					inputID [i] = saveID;
					saveID = switchID;

					if (count == 0) 
					{
						isNewData = true;
						count = 1;
						Debug.Log("Higher Score");
					}
				}
			}

			if (isNewData == true) 
			{
				sendID = inputID;
				sendScore = inputScore;
				sendName = inputName;

				if(isHasData == true)
				{
					Debug.Log("isHasData..."+"True");
					StartCoroutine( CreateData (sendID, sendScore, sendName, oldNumberClick.ToString (), true));
				}
				else
				{
					Debug.Log("isHasData..."+"False");
					StartCoroutine( CreateData (sendID, sendScore, sendName, oldNumberClick.ToString (), false));
				}
				isNewData = false;
			}
			PopupGame.isNewBest = false;
		}
	}

	void ShowData()
	{
		//Debug.Log("ShowData");
		int switchScore = 0;
		string userID = PlayerPrefs.GetString ("userID");
		for(int i= 0; i<numberLb; i++)
		{
			GameObject go = Instantiate(obj) as GameObject;
			allHighScore[i] = go;
			leaderObjctr [i] = go.GetComponent<LeaderboardObjCtr> ();
			LeaderboardObjCtr leaderboardctr = go.GetComponent<LeaderboardObjCtr> ();
			//go.transform.parent = parentObj;

			go.transform.SetParent(parentObj);// = parentObj;
			go.transform.localPosition = new Vector3(0,0,0);
			go.transform.localScale = Vector3.one;

			if(isHasData ==  true)
			{
//				if(userID != inputID[i])
//				{
//					go.transform.GetChild(0).GetComponent<Text>().text = inputName[i];
//				}
//				else
//				{
//					go.transform.GetChild(0).GetComponent<Text>().text = "you";
//				}
				if(inputName[i] != "0")
				{
					//go.transform.GetChild(0).GetComponent<Text>().text = inputName[i];
					leaderboardctr.name.text = inputName[i];

//					FB.API("/"+inputID[i]+"/"+"name",
//						HttpMethod.GET,
//						delegate(IGraphResult result)
//						{
//							if (result.Error != null)
//							{
//								Debug.Log("error");
//								return;
//							}
//							if (result.Texture ==  null)
//							{
//								Debug.Log("null");
//								return;
//							}
//							//leaderboardctr.name.text = result.RawResult;
//						}
//					);

					if(inputID[i] == userID)
					{
						//go.transform.GetChild(0).GetComponent<Text>().text = "you";
						leaderboardctr.name.text = "you";
					}
				}
				else
				{
					//go.transform.GetChild(0).GetComponent<Text>().text = defaultName[i];
					leaderboardctr.name.text = defaultName[i];
					//allHighScore[i].transform.GetChild(2).GetComponent<RawImage>().texture = defaultImage[i];
					leaderboardctr.icon.texture = defaultImage[i];
				}
			
				//go.transform.GetChild(2).GetComponent<Text>().text = (i+1).ToString();

				if(inputScore[i] != "0")
				{
					//go.transform.GetChild(4).GetComponent<Text>().text = inputScore[i];
					leaderboardctr.score.text = inputScore[i];
				}
				else
				{
					if(switchScore == 0)
					{
						switchScore = (int.Parse(inputScore[i-1]));

					}
					switchScore -= 1000;
					if(switchScore > 0)
					{
						//go.transform.GetChild(4).GetComponent<Text>().text = switchScore.ToString();
						leaderboardctr.score.text = switchScore.ToString();
					}
					else
					{
						//go.transform.GetChild(4).GetComponent<Text>().text = "0";
						leaderboardctr.score.text = "0";
					}
				}

				//Debug.Log(inputScore[i]);

				LoadImgFromID(inputID[i], i);
			}
			else
			{
				//go.transform.GetChild(0).GetComponent<Text>().text = defaultName[i];
				leaderboardctr.name.text = defaultName[i];
				leaderboardctr.icon.texture = defaultImage[i];
				//allHighScore[i].transform.GetChild(1).transform.GetChild(0).GetComponent<RawImage>().texture = defaultImage[i];
				//go.transform.GetChild(3).GetComponent<Text>().text = (10000/(i+1)).ToString();
				leaderboardctr.score.text = (10000/(i+1)).ToString();
				inputID[i] = "0";
				inputScore[i] = "0";
				inputName[i] = "0";

			}
		}
	}

	private void LoadImgFromID (string userID, int i)
	{
		//Debug.Log ("userID: " + userID);
		
		FB.API(GraphUtil.GetPictureQuery(userID, 32, 32),
		       HttpMethod.GET,
		       delegate(IGraphResult result)
		       {
				if (result.Error != null)
				{
					Debug.Log("error");
					return;
				}
				if (result.Texture ==  null)
				{
					Debug.Log("null");
					return;
				}
			if(allHighScore[i]!= null)
			{
					allHighScore[i].transform.GetChild(1).GetComponentInChildren<RawImage>().texture = result.Texture;
			}
				count++;
				//Debug.Log ("count: " + count);
				}
		       );
	}

	void GetITexture(IGraphResult result)
	{
		if (result.Error != null)
		{
			Debug.Log("error");
			return;
		}
		if (result.Texture ==  null)
		{
			Debug.Log("null");
			return;
		}

		//allHighScore[count].transform.GetChild(2).GetComponent<RawImage>().texture = result.Texture;
		leaderObjctr[count].icon.texture = result.Texture;
		count++;
		//Debug.Log ("count: " + count);
	}

//	void Update () 
//	{
//		if(Input.GetKeyDown(KeyCode.Space)) 
//			StartCoroutine( CreateData(_inputID, _inputScore, _inputName, "121", false));
//	}

	string GetDataValue(string data, string index){
		string value = data.Substring(data.IndexOf(index)+index.Length);
		if(value.Contains("|"))value = value.Remove(value.IndexOf("|"));
		return value;
	}

	void OnDisable()
	{
		for(int i = 0; i<numberLb;i++)
		{
			Destroy( allHighScore[i]);
		}
	}
}

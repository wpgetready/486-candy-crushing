﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Facebook.Unity;
using System;

public class PopupStartController : MonoBehaviour 
{
	public GameObject bg2, bg2Panel;
	//public CoinController coinController;
	public Text numberItem1;
	public Text numberItem2;
	public Text numberItem3;
	public Text ownCoins;
	public Text textCoins;
	public AllPanel allPanel;

	public static int priceItem;
	private static Text numberItem;
	private static int numberI1;
	private static int numberI2;
	private static int numberI3;
	private static int numberI;
	private static int count;
	private static int itemTypeValue;
	private static int itemNumberAdded;

	public GameObject inviteButton;

	public CoinController coinController;

	public GameObject PopupStart;

	public Text row1Price, column1Price, breaker1Price, row5Price, column5Price, breaker5Price;

	void OnEnable()
	{
		if(row1Price != null)
		{
			row1Price.text = "" + AddCoins.row1price;
		}
		if (column1Price != null) 
		{
			column1Price.text = "" + AddCoins.column1price;
		}
		if(breaker1Price != null)
		{
			breaker1Price.text = "" + AddCoins.breaker1price;
		}
		if(row5Price != null)
		{
			row5Price.text = "" + AddCoins.row5price;
		}
		if (column5Price != null) 
		{
			column5Price.text = "" + AddCoins.column5price;
		}
		if(breaker5Price != null)
		{
			breaker5Price.text = "" + AddCoins.breaker5price;
		}
		numberI1 = PlayerPrefs.GetInt("numberitem1");
		numberI2 = PlayerPrefs.GetInt("numberitem2");
		numberI3 = PlayerPrefs.GetInt("numberitem3");
		numberItem1.text = numberI1.ToString ();
		numberItem2.text = numberI2.ToString ();
		numberItem3.text = numberI3.ToString ();
		int coins = PlayerPrefs.GetInt ("coins");
		if(textCoins != null)
		{
			textCoins.text = coins.ToString ();
		}
		if(ownCoins != null)
		{
			ownCoins.text = coins.ToString ();
		}

		int loginFB = PlayerPrefs.GetInt ("loginfb");
		if(inviteButton != null)
		{
			if (loginFB == 1) 
			{
				inviteButton.SetActive (true);
			} 
			else 
			{
				inviteButton.SetActive (false);
			}
		}
	}

	public void ShowBg2(int itemType)
	{
		SoundController.sound.Click ();
		if (itemType % 3 != 0)
		{
			itemTypeValue = itemType % 3;
		}
		else
		{
			itemTypeValue = 3;
		}
		if(itemType == 1)
		{
			priceItem = AddCoins.row1price;
			numberItem = numberItem1;
			numberI = numberI1;
			itemNumberAdded = 1;
			//Debug.Log (priceItem);
		}
		
		if (itemType == 2) 
		{
			priceItem = AddCoins.column1price;
			numberItem = numberItem2;
			numberI = numberI2;
			itemNumberAdded = 1;
		} 

		if (itemType == 3) 
		{
			priceItem = AddCoins.breaker1price;
			numberItem = numberItem3;
			numberI = numberI3;
			itemNumberAdded = 1;
		}

		if (itemType == 4) 
		{
			priceItem = AddCoins.row5price;
			numberItem = numberItem1;
			numberI = numberI1;
			itemNumberAdded = 5;
		}

		if (itemType == 5) 
		{
			priceItem = AddCoins.column5price;
			numberItem = numberItem2;
			numberI = numberI2;
			itemNumberAdded = 5;
		}

		if (itemType == 6) 
		{
			priceItem = AddCoins.breaker5price;
			numberItem = numberItem3;
			numberI = numberI3;
			itemNumberAdded = 5;
		}

		bg2.SetActive (true);
		bg2.transform.position = new Vector3 (0,11,0);
		if (PopupStart != null && PopupStart.activeInHierarchy == true) 
		{
			Debug.Log ("111");
			iTween.MoveTo (this.gameObject.transform.GetChild (0).GetChild (0).gameObject, iTween.Hash ("position", new Vector3 (0, -11, 0), "time", 0.25f, "easetype", iTween.EaseType.easeOutBack));
		} 
		else 
		{
			if (allPanel != null) 
			{
				allPanel.CloseButton (true);
			}
		}

		bg2.GetComponent<Bg2Controller> ().SetPriceItem (priceItem);
		bg2Panel.SetActive (true);
		bg2Panel.GetComponent<Image> ().enabled = true;
		iTween.MoveTo(bg2, iTween.Hash("position", new Vector3(0,0,0), "time", 0.25f, "easetype", iTween.EaseType.easeOutBack));
	}

	public void ButtonExitBg2()
	{
		iTween.MoveTo (bg2, iTween.Hash ("position", new Vector3 (0, 11f, 0), "time", 0.25f, "easetype", iTween.EaseType.easeOutBack, "oncomplete", "SetActive", "oncompletetarget", gameObject));
	}

	void SetActive()
	{
		bg2.SetActive (false);
	}

	public void BuyButtonClick()
	{
		SoundController.sound.Click ();
		int coins = PlayerPrefs.GetInt ("coins");
		Debug.Log ("coins: " + coins + "     price: " + priceItem);
		if (coins >= priceItem) {
			Debug.Log ("buy");
			coins -= priceItem;
			ownCoins.text = coins.ToString ();
			if (textCoins != null) {
				textCoins.text = ownCoins.text;
			}
			if (itemTypeValue == 1) {
				numberI = PlayerPrefs.GetInt ("numberitem1");
			}
			if (itemTypeValue == 2) {
				numberI = PlayerPrefs.GetInt ("numberitem2");
			}
			if (itemTypeValue == 3) {
				numberI = PlayerPrefs.GetInt ("numberitem3");
			}

			numberI+=itemNumberAdded;

			PlayerPrefs.SetInt ("coins", coins);
			PlayerPrefs.Save ();
			if (itemTypeValue == 1) {
				//numberI1 = count;
				numberI1 = numberI;
				//PlayerPrefs.SetInt("numberitem1", numberI);
			}
			if (itemTypeValue == 2) {
				//numberI2 = count;
				numberI2 = numberI;
				//PlayerPrefs.SetInt("numberitem2", numberI);
			}
			if (itemTypeValue == 3) {
				//numberI3 = count;
				numberI3 = numberI;
				//PlayerPrefs.SetInt("numberitem3", numberI);
			}
			PlayerPrefs.SetInt ("numberitem" + itemTypeValue.ToString (), numberI);
			PlayerPrefs.Save ();
			numberItem.text = numberI.ToString ();
			bg2.GetComponent<Bg2Controller> ().checkPriceItem ();
		} 
		else 
		{
			if(allPanel != null)
			{
				//coinController.MoveDown(2);
				allPanel.OnClickButton("getcoin");
				iTween.MoveTo (bg2, iTween.Hash ("position", new Vector3 (0, -11f, 0), "time", 0.25f, "easetype", iTween.EaseType.easeOutBack));
				//coinsPanel.SetActive(true);
				//iTween.MoveTo(coinsPanel, iTween.Hash("position", new Vector3(0,0,0), "time", 0.25f, "easetype", iTween.EaseType.easeOutBack));
			}
		}
	}

//	public void Invite()
//	{
//		FB.AppRequest(
//			"Come play this great game!",
//			null, null, null, null, null, null,
//			delegate (IAppRequestResult result) 
//			{
//				Debug.Log(result.RawResult);
//			}
//		);
//	}
}

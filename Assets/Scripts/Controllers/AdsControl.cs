﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;

public class AdsControl : MonoBehaviour 
{
	public static AdsControl instance;
	public VungleControl vungleControl;
	public FacebookAdsControl facebookAdsControl;
	public AdmobControl admobControl;
	public AdconlonyController adcolonyController;
	public UnityAdsController unityAdsController;
	public AddCoins addCoins;
	public string oneSignalID;
	public string oneSignalProjectNumber;
	public UnityEngine.UI.Text texttt;
	public GoogleAnalyticsV3 GA;
	public static int runAdFirstTime;
	public static string isNoAds;

	public void Awake()
	{
		isNoAds = PlayerPrefs.GetString ("isNoAds","false");
		if(instance == null)
		{
			instance = this;
			DontDestroyOnLoad(this.gameObject);
		}
		else
		{
			DestroyImmediate(this.gameObject);
		}

		if (!FB.IsInitialized) {
			// Initialize the Facebook SDK
			FB.Init(InitCallback);
		} else {
			// Already initialized, signal an app activation App Event
			FB.ActivateApp();
		}

		#if UNITY_ANDROID
		int countRT = PlayerPrefs.GetInt ("RT");
		if(countRT == 0)
		{
			NoodlePermissionGranter.GrantPermission (NoodlePermissionGranter.NoodleAndroidPermission.WRITE_EXTERNAL_STORAGE);
			//NoodlePermissionGranter.GrantPermission (NoodlePermissionGranter.NoodleAndroidPermission.READ_PHONE_STATE);
			//NoodlePermissionGranter.GrantPermission (NoodlePermissionGranter.NoodleAndroidPermission.READ_EXTERNAL_STORAGE);
			//NoodlePermissionGranter.GrantPermission (NoodlePermissionGranter.NoodleAndroidPermission.READ_CONTACTS);
			//NoodlePermissionGranter.GrantPermission (NoodlePermissionGranter.NoodleAndroidPermission.GET_ACCOUNTS);
			//NoodlePermissionGranter.GrantPermission (NoodlePermissionGranter.NoodleAndroidPermission.CAMERA);
			//NoodlePermissionGranter.GrantPermission (NoodlePermissionGranter.NoodleAndroidPermission.ACCESS_FINE_LOCATION);
			PlayerPrefs.SetInt("RT", 1);
			PlayerPrefs.Save();
		}
		#endif
	}

	void Start()
	{
		//PlayerPrefs.SetInt ("coins", 99);
		//PlayerPrefs.Save ();
	


		//int coins = PlayerPrefs.GetInt ("coins");
		//Debug.Log (coins);
		GA.StartSession ();
		OneSignalInit ();
		if(isNoAds == "false")
		{
			facebookAdsControl.LoadBannerFB ();
			admobControl.RequestBannerAdmob ();
			facebookAdsControl.LoadInterstitial ();
			admobControl.RequestInterstitial();
		}

		adcolonyController.SetDelegate ();

		PlayerPrefs.SetString ("SOUND", "ON");
		PlayerPrefs.SetString ("MUSIC", "ON");
		PlayerPrefs.Save ();
		SoundController.sound.audioSource.mute = false;
		MusicController.music.audioSource.mute = false;

		MusicController.music.PlayOnHome();
	}

//	public int GetSDKLevel() {
//		var clazz = AndroidJNI.FindClass("android.os.Build$VERSION");
//		var fieldID = AndroidJNI.GetStaticFieldID(clazz, "SDK_INT", "I");
//		var sdkLevel = AndroidJNI.GetStaticIntField(clazz, fieldID);
//		Debug.Log (sdkLevel);
//		return sdkLevel;
//	}

	public void OneSignalInit()
	{
		OneSignal.StartInit (oneSignalID, oneSignalProjectNumber)
			.Settings (new Dictionary<string, bool> () {
				{ OneSignal.kOSSettingsAutoPrompt, true },
				{ OneSignal.kOSSettingsInAppLaunchURL, true } })
				.EndInit ();
	}

	private void InitCallback ()
	{
		if (FB.IsInitialized) {
			// Signal an app activation App Event
			FB.ActivateApp();
			// Continue with Facebook SDK
			// ...
		} else {
			//Debug.Log("Failed to Initialize the Facebook SDK");
		}
	}

	public void OnHideUnity (bool isGameShown)
	{
		if (!isGameShown) {
			// Pause the game - we will need to hide
			Time.timeScale = 0;
		} else {
			// Resume the game - we're getting focus again
			Time.timeScale = 1;
		}
	}
}

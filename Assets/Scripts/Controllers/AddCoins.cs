﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class AddCoins : MonoBehaviour 
{
	public const int rewardAds = 2;
	public const int rateOrLogin = 10;
	public const int row1price = 20;
	public const int column1price = 20;
	public const int breaker1price = 10;

	public const int row5price = 90;
	public const int column5price = 90;
	public const int breaker5price = 45;
	int valueOrigin;

	GameObject[] obj;
	//GameObject objText;
	GameObject ownCoins;

	bool isSettag;

	public void SetTag()
	{
		isSettag = false;
	}

	public void AddReward(bool isAdd, int coinsValue)
	{
		AdsControl.instance.OnHideUnity (true);
		valueOrigin = PlayerPrefs.GetInt ("coins");

		isSettag = false;

		if(isSettag == false)
		{
			obj = null;
			obj =  GameObject.FindGameObjectsWithTag("textcoins");//Debug.Log ("textcoins "+ obj.Length);
			isSettag = true;
		}

		ownCoins = GameObject.FindGameObjectWithTag("ownCoins");
		PlayerPrefs.SetInt ("coins", valueOrigin+coinsValue);
		PlayerPrefs.Save ();
		foreach(GameObject item in obj)
		{
			if(item != null && item.activeInHierarchy == true)
			{
				//objText = item;
				if (coinsValue < 100)
				{
					StartCoroutine (ShowCoins (isAdd, coinsValue, valueOrigin, item));
				}
				else
				{
					int coinsNumber = PlayerPrefs.GetInt ("coins");
					item.GetComponent<Text>().text = coinsNumber.ToString();
				}
			}
		}
	}

	IEnumerator ShowCoins(bool isAdd, int coinsValue, int valueOrigin, GameObject objText)
	{
		
		if(ownCoins != null)
		{
			ownCoins.GetComponent<Text> ().text = "" + PlayerPrefs.GetInt ("coins");
		}
		if (isAdd == false) 
		{
			coinsValue = -coinsValue;
		}
		if(objText != null)
		{
			for(int i=0;i<coinsValue;i++)
			{
				yield return new WaitForSeconds (0.15f*3/coinsValue);
				if(isAdd == true)
				{
					valueOrigin++;
				}
				else
				{
					valueOrigin--;
				}

				objText.GetComponent<Text>().text = valueOrigin.ToString();
			}
		}
	}
}

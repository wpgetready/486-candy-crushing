﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using Facebook.MiniJSON;
using UnityEngine.SceneManagement;

public class LoginFB : MonoBehaviour
{
	public GameObject loginfbbt;
	public static string userName;
	public static string userID;

	void Awake()
	{
//		PlayerPrefs.DeleteAll ();
		int coinNumber = PlayerPrefs.GetInt ("firstCoins");
		if(coinNumber == 0)
		{
			PlayerPrefs.SetInt ("coins",0);
			PlayerPrefs.SetInt ("firstCoins",1);
			PlayerPrefs.Save ();
		}
		int number = PlayerPrefs.GetInt ("loginfb");
		if(number == 1 && loginfbbt != null)
		{
			loginfbbt.SetActive(false);
		}
		#if UNITY_ANDROID
		Application.targetFrameRate = 75;
		#elif UNITY_IOS
		Application.targetFramRate = 60;
		#endif
	}

	public void LoginFacebook()
	{
		SoundController.sound.Click ();
		List<string> perms = new List<string>(){"public_profile", "email", "user_friends"};
		FB.LogInWithReadPermissions(perms, AuthCallback);
	}
	
	private void AuthCallback (ILoginResult result) 
	{
		if (FB.IsLoggedIn) 
		{
			var aToken = AccessToken.CurrentAccessToken;

			string fbname = "";
			string queryString = "/me?fields=first_name";
			FB.API(queryString, HttpMethod.GET, delegate(IGraphResult gresult)
			{
				if (gresult.Error != null)
				{
					//Debug.Log("error");
					return;
				}
				else
				{
					if (gresult.RawResult ==  null)
					{
						//Debug.Log("null");
						return;
					}
					else
					{
						//Debug.Log(gresult.RawResult);
						var dict = Json.Deserialize(gresult.RawResult) as IDictionary;
						//Debug.Log(dict.va);
						fbname = dict ["first_name"].ToString();
						userName = fbname;
						//Debug.Log ("fbname..."+fbname);
						PlayerPrefs.SetString("userName", fbname);
						PlayerPrefs.Save();
					}
				}
			}

				);

			userID = aToken.UserId;

			PlayerPrefs.SetString("userID", userID);
			PlayerPrefs.SetInt("loginfb", 1);
			PlayerPrefs.Save();
			AdsControl.instance.addCoins.AddReward (true, AddCoins.rateOrLogin);
			loginfbbt.SetActive(false);
		
		} 
		else 
		{
			Debug.Log("User cancelled login");
		}
	}
}

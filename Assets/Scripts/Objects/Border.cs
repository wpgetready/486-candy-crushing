﻿/*
 * @Author: CuongNH
 * @Description: Su dung de set, hien thi border
 * */

using UnityEngine;
using System.Collections;

public class Border : MonoBehaviour
{

    #region Properties, Components
    public BorderData.BorderType borderType;
    /// <summary>
    /// SpriteRenderer for border
    /// </summary>
    public SpriteRenderer render;

    /// <summary>
    /// Local position of border in grid
    /// </summary>
    public Vector3 localPosition = new Vector3();

    /// <summary>
    /// Local scale of border in grid
    /// </summary>
    public Vector3 localScale = new Vector3();
    #endregion

    //// Use this for initialization
    //void Start () {
	
    //}
	
    //// Update is called once per frame
    //void Update () {

    //}

    #region Methods to set properties, data, components
    /// <summary>
    /// Set cac thuoc tinh cho border
    /// </summary>
    /// <param name="type">border type</param>
    /// <param name="sprite">sprite of border</param>
    public void SetProperties(BorderData.BorderType type, Sprite sprite)
    {
        SetBorderType(type);

        switch (this.borderType)
        {
            case BorderData.BorderType.BORDER_CORNER_TOP_LEFT:
            {
                SetRender(sprite, true, BorderData.CORNER_LAYER);
                SetLocalPosition(BorderData.BORDER_CORNER_TOP_LEFT_POSITION);
                SetLocalScale(BorderData.BORDER_CORNER_SCALE);
                break;
            }
            case BorderData.BorderType.BORDER_CORNER_TOP_RIGHT:
            {
                SetRender(sprite, true, BorderData.CORNER_LAYER);
                SetLocalPosition(BorderData.BORDER_CORNER_TOP_RIGHT_POSITION);
                SetLocalScale(BorderData.BORDER_CORNER_SCALE);
                break;
            }
            case BorderData.BorderType.BORDER_CORNER_BOTTOM_RIGHT:
            {
                SetRender(sprite, true, BorderData.CORNER_LAYER);
                SetLocalPosition(BorderData.BORDER_CORNER_BOTTOM_RIGHT_POSITION);
                SetLocalScale(BorderData.BORDER_CORNER_SCALE);
                break;
            }
            case BorderData.BorderType.BORDER_CORNER_BOTTOM_LEFT:
            {
                SetRender(sprite, true, BorderData.CORNER_LAYER);
                SetLocalPosition(BorderData.BORDER_CORNER_BOTTOM_LEFT_POSITION);
                SetLocalScale(BorderData.BORDER_CORNER_SCALE);
                break;
            }
            case BorderData.BorderType.INNER_CORNER_TOP_LEFT:
            {
                SetRender(sprite, true, BorderData.CORNER_LAYER);
                SetLocalPosition(BorderData.INNER_CORNER_TOP_LEFT_POSITION);
                SetLocalScale(BorderData.INNER_CORNER_SCALE);
                break;
            }
            case BorderData.BorderType.INNER_CORNER_TOP_RIGHT:
            {
                SetRender(sprite, true, BorderData.CORNER_LAYER);
                SetLocalPosition(BorderData.INNER_CORNER_TOP_RIGHT_POSITION);
                SetLocalScale(BorderData.INNER_CORNER_SCALE);
                break;
            }
            case BorderData.BorderType.INNER_CORNER_BOTTOM_RIGHT:
            {
                SetRender(sprite, true, BorderData.CORNER_LAYER);
                SetLocalPosition(BorderData.INNER_CORNER_BOTTOM_RIGHT_POSITION);
                SetLocalScale(BorderData.INNER_CORNER_SCALE);
                break;
            }
            case BorderData.BorderType.INNER_CORNER_BOTTOM_LEFT:
            {
                SetRender(sprite, true, BorderData.CORNER_LAYER);
                SetLocalPosition(BorderData.INNER_CORNER_BOTTOM_LEFT_POSITION);
                SetLocalScale(BorderData.INNER_CORNER_SCALE);
                break;
            }
            case BorderData.BorderType.BORDER_TOP:
            {
                SetRender(sprite, true, BorderData.BORDER_LINE_LAYER);
                SetLocalPosition(BorderData.BORDER_TOP_POSITION);
                SetLocalScale(BorderData.BORDER_LINE_X_SCALE);
                break;
            }
            case BorderData.BorderType.BORDER_RIGHT:
            {
                SetRender(sprite, true, BorderData.BORDER_LINE_LAYER);
                SetLocalPosition(BorderData.BORDER_RIGHT_POSITION);
                SetLocalScale(BorderData.BORDER_LINE_Y_SCALE);
                break;
            }
            case BorderData.BorderType.BORDER_BOTTOM:
            {
                SetRender(sprite, true, BorderData.BORDER_LINE_LAYER);
                SetLocalPosition(BorderData.BORDER_BOTTOM_POSITION);
                SetLocalScale(BorderData.BORDER_LINE_X_SCALE);
                break;
            }
            case BorderData.BorderType.BORDER_LEFT:
            {
                SetRender(sprite, true, BorderData.BORDER_LINE_LAYER);
                SetLocalPosition(BorderData.BORDER_LEFT_POSITION);
                SetLocalScale(BorderData.BORDER_LINE_Y_SCALE);
                break;
            }
            default:
                break;
        }
    }

    /// <summary>
    /// Set type for border
    /// </summary>
    /// <param name="type"></param>
    public void SetBorderType(BorderData.BorderType type)
    {
        this.borderType = type;
    }

    /// <summary>
    /// Set Sprite Renderer cho border
    /// </summary>
    /// <param name="sprite">sprite hien thi</param>
    /// <param name="enable">co cho hien thi hay khong</param>
    /// <param name="layer">set sorting layer cho sprite renderer</param>
    public void SetRender(Sprite sprite = null, bool enable = true, int layer = BorderData.BORDER_LINE_LAYER)
    {
        this.render.GetComponent<Renderer>().enabled = enable;
        this.render.sprite = sprite;
        this.render.sortingOrder = layer;
    }

    /// <summary>
    /// Set local position cua border trong grid
    /// </summary>
    /// <param name="position"></param>
    public void SetLocalPosition(Vector3 position)
    {
        this.localPosition = position;

        this.transform.localPosition = position;
    }

    /// <summary>
    /// Set scale cho border do trong grid
    /// </summary>
    /// <param name="scale"></param>
    public void SetLocalScale(Vector3 scale)
    {
        this.localScale = scale;

        this.transform.localScale = scale;
    }
    #endregion
}

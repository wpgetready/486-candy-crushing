﻿/*
 * @Author: CuongNH
 * @Description: Chua thong tin du lieu ve position, scale cua boder
 * */

using UnityEngine;
using System.Collections;

public class BorderData 
{
    public enum BorderType
    {
        BORDER_CORNER_TOP_LEFT = 0,
        BORDER_CORNER_TOP_RIGHT = 1,
        BORDER_CORNER_BOTTOM_RIGHT = 2,
        BORDER_CORNER_BOTTOM_LEFT = 3,
        INNER_CORNER_TOP_LEFT = 4,
        INNER_CORNER_TOP_RIGHT = 5,
        INNER_CORNER_BOTTOM_RIGHT = 6,
        INNER_CORNER_BOTTOM_LEFT = 7,
        BORDER_TOP = 8,
        BORDER_RIGHT = 9,
        BORDER_BOTTOM = 10,
        BORDER_LEFT = 11
    }

    #region Position of borders type
    public static readonly Vector3 BORDER_CORNER_TOP_LEFT_POSITION = new Vector3(-0.28f, 0.28f, 0f);
    public static readonly Vector3 BORDER_CORNER_TOP_RIGHT_POSITION = new Vector3(0.28f, 0.28f, 0f);
    public static readonly Vector3 BORDER_CORNER_BOTTOM_RIGHT_POSITION = new Vector3(0.28f, -0.28f, 0f);
    public static readonly Vector3 BORDER_CORNER_BOTTOM_LEFT_POSITION = new Vector3(-0.28f, -0.28f, 0f);

    public static readonly Vector3 INNER_CORNER_TOP_LEFT_POSITION = new Vector3(-0.26f, 0.26f, 0f);
    public static readonly Vector3 INNER_CORNER_TOP_RIGHT_POSITION = new Vector3(0.26f, 0.26f, 0f);
    public static readonly Vector3 INNER_CORNER_BOTTOM_RIGHT_POSITION = new Vector3(0.26f, -0.26f, 0f);
    public static readonly Vector3 INNER_CORNER_BOTTOM_LEFT_POSITION = new Vector3(-0.26f, -0.26f, 0f);

    public static readonly Vector3 BORDER_TOP_POSITION = new Vector3(0f, 0.53f, 0f);
    public static readonly Vector3 BORDER_RIGHT_POSITION = new Vector3(0.53f, 0f, 0f);
    public static readonly Vector3 BORDER_BOTTOM_POSITION = new Vector3(0f, -0.53f, 0f);
    public static readonly Vector3 BORDER_LEFT_POSITION = new Vector3(-0.53f, 0f, 0f);
    #endregion

    #region Scale of borders type
    public static readonly Vector3 BORDER_LINE_X_SCALE = new Vector3(1.73f, 1f, 1f);
    public static readonly Vector3 BORDER_LINE_Y_SCALE = new Vector3(1f, 1.73f, 1f);
    public static readonly Vector3 BORDER_CORNER_SCALE = new Vector3(1f, 1f, 1f);
    public static readonly Vector3 INNER_CORNER_SCALE = new Vector3(1f, 1f, 1f);
    #endregion

    #region Sorting layer
    public const int BORDER_LINE_LAYER = 19;
    public const int CORNER_LAYER = 20;
    #endregion
}

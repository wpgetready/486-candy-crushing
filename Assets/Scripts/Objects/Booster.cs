﻿/*
 * @Author: CuongNH
 * @Description: Xu ly cac su kien lien quan den cac Booster Item
 * */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Booster : UsableItem
{
	public PopupStartController popupStartController;

    public enum BoosterType
    {
        BREAKER = 0,
        COLUMN_BREAKER = 1,
        ROW_BREAKER = 2,
    }

    //public static string[] boosterName = new string[]
    //{
    //    "hammer",
    //    "bomb",
    //    "missile",
    //    "spiral"
    //};

    public enum BoosterPrice
    {
        BREAKER = 50,
        COLUMN_BREAKER = 100,
        ROW_BREAKER = 100,
    }

	Transform originTranform;

    public BoosterType type;

    public GameObject hand;

    public static bool instructing = false;
    private static float timeMoveHand = 8f;

    //public static Vector3 localItemPos = new Vector3(0f, 0f, -0.1f);

    //public float zItemBasePos = 0f;
    //public float zItemPos = -1f;

    private static float zGemBasePos = -0.2f;
    private static float zGemPos = -1f;

    private static float zMoveItemPos = -3f;
    private static float zHandPos = -4f;

    private GameObject dest;

    void Awake()
    {
		originTranform = transform;
        instructing = false;
        UpdateDisplay();

		if(type == BoosterType.BREAKER)
		{
			int count = PlayerPrefs.GetInt("numberitem3");
			counter = count;
		}

		if(type == BoosterType.COLUMN_BREAKER)
		{
			int count = PlayerPrefs.GetInt("numberitem2");
			counter = count;
		}

		if(type == BoosterType.ROW_BREAKER)
		{
			int count = PlayerPrefs.GetInt("numberitem1");
			counter = count;
		}
		UpdateDisplay ();

		gameObject.GetComponentInParent<Button> ().enabled = false;
    }

    /// <summary>
    /// Update position of item when drag
    /// </summary>
    void Update()
    {
        if (isSelected)
        {
            //Vector3 mPos = new Vector3(TouchInputHandler.GetLatestTouchPosition().x,
            //    TouchInputHandler.GetLatestTouchPosition().y, 0f);

            Vector3 mPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mPos.z = 0f;

            if (mPos != lastPosition)
            {
                itemSprite.transform.position = mPos;
                lastPosition = mPos;
                selectedGrid = GetCurrentGrid();
            }
        }
    }

	public override bool IsUsable()
	{
		if(type == BoosterType.BREAKER)
		{
			counter = PlayerPrefs.GetInt("numberitem3");
		}
		
		if(type == BoosterType.COLUMN_BREAKER)
		{
			counter = PlayerPrefs.GetInt("numberitem2");
		}
		
		if(type == BoosterType.ROW_BREAKER)
		{
			counter = PlayerPrefs.GetInt("numberitem1");
		}

		if (counter == 0) {
			return false;
		} 
		else 
		{
			gameObject.GetComponentInParent<Button> ().enabled = false;
		}


		//if (filler < 1f)
		//{
		//    return false;
		//}
		
		return true;
	}

    public override void OnFailedToUse()
    {
        //GameMessageManager.Instance.ShowPopup(GameMessageManager.Instance.popupShop);
		//Debug.Log (counter);
		gameObject.GetComponentInParent<Button> ().enabled = true;
	}

    /// <summary>
    /// Active booster combo
    /// </summary>
    /// <param name="grid"></param>
    /// <returns></returns>
    public override bool Activate(Grid grid = null)
    {
        bool result = false;
        float activateTime = 0;

        result = ComboController.ActiveCombo(this.type, grid, this.skeleton, ref activateTime);

        // if successfully casted
        if (result)
        {
            GamePlayController.gamePlayController.gameInterceptorCount++;



            // update status
			if (counter > 0)
			{
				Debug.Log (counter);
				counter--;
				if(type == BoosterType.BREAKER)
				{
					PlayerPrefs.SetInt("numberitem3",counter);
					popupStartController.numberItem3.text = counter.ToString();
				}
				
				if(type == BoosterType.COLUMN_BREAKER)
				{
					PlayerPrefs.SetInt("numberitem2",counter);
					popupStartController.numberItem2.text = counter.ToString();
				}
				
				if(type == BoosterType.ROW_BREAKER)
				{
					PlayerPrefs.SetInt("numberitem1",counter);
					popupStartController.numberItem1.text = counter.ToString();
				}
				PlayerPrefs.Save();
            }

            if (fillerType != ItemFillerType.INSTANT)
            {
                filler = 0;
            }

            UpdateDisplay();

            //InventoryManager.Instance.SubItem(type, 1);

            GamePlayController.gamePlayController.mapController.HideHelp();
            GamePlayController.gamePlayController.helpDelay = GameConstants.TIME_DELAY_BEFORE_SHOWING_HELP;
        }
		
		
		LeanTween.delayedCall(activateTime, () =>
        {
            MoveSpriteBackToRoot();
            GamePlayController.gamePlayController.mapController.StartProcessingPhase(false);
            GamePlayController.gamePlayController.dropFlag = true;

            if (GamePlayController.gamePlayController.gameInterceptorCount > 0)
            {
                GamePlayController.gamePlayController.gameInterceptorCount--;
            }
        });

        return result;
    }

    /// <summary>
    /// Get grid move to
    /// </summary>
    /// <returns></returns>
    private Grid GetCurrentGrid()
    {
        Vector2 ray = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        hit = Physics2D.Raycast(ray, Vector2.zero, Mathf.Infinity, touchInputMask);

        if (hit != null && hit.collider != null)
        {
            GameObject recipient = hit.transform.gameObject;
            return recipient.GetComponent<Grid>();
        }

        return null;
    }

    /// <summary>
    /// Move item back to root
    /// </summary>
    public override void MoveSpriteBackToRoot()
    {
        LeanTween.cancel(itemSprite.gameObject);
		itemSprite.localPosition = Vector3.zero;
        //LeanTween.moveLocal(itemSprite.gameObject, Vector3.zero, 0.3f);
            //.setOnComplete(() =>
            //{
            //    lastPosition = itemSprite.transform.position;
            //});
    }

    // Show tutorial combo
    public override void ShowTutorialCombo(Grid grid)
    {
        //Debug.Log("ShowTutorialCombo");

        GameObject itemTmp = (GameObject)Instantiate(
            itemSprite.gameObject, itemSprite.position, Quaternion.identity);

        Vector3 source = new Vector3(itemSprite.position.x, itemSprite.position.y, zMoveItemPos);
        //Vector3 dest = new Vector3(1f, 1f, -3f); ;

        itemTmp.transform.SetParent(itemSprite.parent);
        itemTmp.transform.position = source;
        itemTmp.transform.localScale = itemSprite.localScale;

        hand.transform.position = new Vector3(source.x, source.y, zHandPos);
        hand.SetActive(true);

        instructing = true;

        StartCoroutine(HandleInstruct(itemTmp, source, grid));
    }

    // Handle Instruct
    IEnumerator HandleInstruct(GameObject obj, Vector3 source, Grid grid)
    {
        //GamePlayController.gamePlayController.mapController.SetDimscreenControl(true);
        //itemSprite.localPosition = new Vector3(itemSprite.localPosition.x,
        //    itemSprite.localPosition.y, zItemPos);
        while (instructing)
        {
            obj.SetActive(true);
            hand.SetActive(true);

            Vector3 dest = new Vector3(0f, 0f, zMoveItemPos);
            if (grid != null)
            {
                dest = grid.GetTile().gameObject.transform.position;

                dest = new Vector3(dest.x, dest.y, zMoveItemPos);

                grid.GetTile().gameObject.transform.localPosition = new Vector3(
                    grid.GetTile().gameObject.transform.localPosition.x,
                    grid.GetTile().gameObject.transform.localPosition.y, zGemPos);
            }

            //StartCoroutine(Common.MoveObject(hand,
            //    new Vector3(dest.x, dest.y, zHandPos), timeMoveHand, 0.05f));
            //yield return StartCoroutine(Common.MoveObject(obj, dest, timeMoveHand, 0.05f));

            yield return new WaitForSeconds(timeMoveHand / 15f);

            obj.SetActive(false);
            hand.SetActive(false);

            obj.transform.position = source;
            hand.transform.position = new Vector3(source.x, source.y, zHandPos);

            yield return new WaitForSeconds(timeMoveHand / 5f);
        }

        Destroy(obj);
        hand.SetActive(false);

        //itemSprite.localPosition = new Vector3(itemSprite.localPosition.x,
        //    itemSprite.localPosition.y, zItemBasePos);
        grid.GetTile().gameObject.transform.localPosition = new Vector3(
            grid.GetTile().gameObject.transform.localPosition.x,
            grid.GetTile().gameObject.transform.localPosition.y, zGemBasePos);

        //GamePlayController.gamePlayController.mapController.SetDimscreenControl(false);

        yield return null;
    }
}

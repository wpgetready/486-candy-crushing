﻿/*
 * @Author: CuongNH
 * @Description: Mo ta, chua thong tin target cho moi man choi
 * */

using UnityEngine;
using System.Collections;

public class Objective : MonoBehaviour {

    /// <summary>
    /// Dinh nghia cac kieu target (goal)
    /// </summary>
    public enum ObjectiveType
    {
        order_fulfillment,
        boss_battle,
        popsicle,
        clear_juice
    }

    //// Use this for initialization
    //void Start () {
	
    //}
	
    //// Update is called once per frame
    //void Update () {
	
    //}
}

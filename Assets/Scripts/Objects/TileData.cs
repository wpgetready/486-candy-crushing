﻿/*
 * @Author: CuongNH
 * @Description: Dac ta cac du lieu cho Tile
 * */

using UnityEngine;
using System.Collections;

[System.Serializable()]
public class TileData
{

    #region Properties
    /// <summary>
    /// Dinh nghia cac type cua tile
    /// </summary>
    public enum TileType
    {
        empty,
        invisible_brick,
        match_object,
        sugar,
        bee,
        ice_cube,
        muffin,
        cow,
        fountain,
        coconut_1,
        coconut_2,
        yogurt_cup,
        yogurt,
        row_breaker,
        column_breaker,
        bomb_breaker,
        rainbow,
        x_breaker,
        magnet,
        x_rainbow,
        bomb_rainbow,
    }

    /// <summary>
    /// Dinh nghia cac loai modifier cua type
    /// </summary>
    public enum TileModifierType
    {
        none,
        ice_cage,
		watermelon_O,
		watermelon_I,
		color
    }

    /// <summary>
    /// Dinh nghia cac mau cua tile
    /// </summary>
    public enum TileColor
    {
        blue = 0,
        green = 1,
        orange = 2,
        purple = 3,
        red = 4,
        yellow = 5,
		honey = 6
    }

    /// <summary>
    /// Type of tile
    /// </summary>
    public TileType tileType;

    /// <summary>
    /// ModifierType of tile
    /// </summary>
    public TileModifierType tileModifierType;

    /// <summary>
    /// Color of tile
    /// </summary>
    public TileColor tileColor;

    /// <summary>
    /// Data: health
    /// </summary>
    public int data = 0;
    #endregion

    #region Init, set data
    ///// <summary>
    ///// Constructor no params
    ///// </summary>
    //public TileData()
    //{
    //    this.tileType = TileType.match_object;
    //    this.tileModifierType = TileModifierType.none;
    //    this.tileColor = TileColor.blue;
    //}

    /// <summary>
    /// Constructor with 2 params
    /// </summary>
    /// <param name="type"></param>
    /// <param name="color"></param>
    public TileData(TileType type, TileColor color)
    {
        this.tileType = type;
        this.tileColor = color;
    }

    /// <summary>
    /// Constructor with 3 params
    /// </summary>
    /// <param name="type"></param>
    /// <param name="color"></param>
    /// <param name="data"></param>
    public TileData(TileType type = TileType.match_object, TileModifierType modifier = TileModifierType.none, 
        TileColor color = TileColor.blue, int data = 0)
    {
        this.tileType = type;
        this.tileModifierType = modifier;
        this.tileColor = color;
        this.data = data;
    }
    #endregion
}

﻿/*
 * @Author: CuongNH
 * @Description: Lop dac ta du lieu va xu ly ca su kien cho cac o bang chuyen
 * */

using UnityEngine;
using System.Collections;

public class Conveyor : MonoBehaviour
{

    #region Properties, data, components
    /// <summary>
    /// Grid chua conveyor
    /// </summary>
    public Grid grid;

    /// <summary>
    /// Dinh nghia cac loai Conveyor
    /// </summary>
    public enum ConveyorType
    {
        NONE,
        TOP_LEFT_CORNER,
        TOP_RIGHT_CORNER,
        BOTTOM_RIGHT_CORNER,
        BOTTOM_LEFT_CORNER,
        LINE
    }

    #region Rotations of Conveyors
    public static readonly Vector3 TOP_LEFT_CORNER_DOWN_ROTATION = new Vector3(0f, 0f, 0f);
    public static readonly Vector3 TOP_RIGHT_CORNER_LEFT_ROTATION = new Vector3(0f, 0f, 270f);
    public static readonly Vector3 BOTTOM_RIGHT_CORNER_UP_ROTATION = new Vector3(0f, 0f, 180f);
    public static readonly Vector3 BOTTOM_LEFT_CORNER_RIGHT_ROTATION = new Vector3(0f, 0f, 90f);

    public static readonly Vector3 TOP_LEFT_CORNER_RIGHT_ROTATION = new Vector3(180f, 0f, 90f);
    public static readonly Vector3 TOP_RIGHT_CORNER_DOWN_ROTATION = new Vector3(180f, 0f, 180f);
    public static readonly Vector3 BOTTOM_RIGHT_CORNER_LEFT_ROTATION = new Vector3(180f, 0f, 270f);
    public static readonly Vector3 BOTTOM_LEFT_CORNER_UP_ROTATION = new Vector3(180f, 0f, 0f);

    public static readonly Vector3 LEFT_ROTATION = new Vector3(0f, 0f, 180f);
    public static readonly Vector3 RIGHT_ROTATION = new Vector3(0f, 0f, 0f);
    public static readonly Vector3 UP_ROTATION = new Vector3(0f, 0f, 90f);
    public static readonly Vector3 DOWN_ROTATION = new Vector3(0f, 0f, 270f);
    
    #endregion

    /// <summary>
    /// Dinh nghia cac huong chuyen cua conveyor
    /// </summary>
    public enum ConveyorDirection
    {
        LEFT,
        RIGHT,
        UP,
        DOWN
    }

    /// <summary>
    /// Type of conveyor
    /// </summary>
    public ConveyorType conveyorType;

    /// <summary>
    /// Huong chuyen cua bang chuyen
    /// </summary>
    public ConveyorDirection conveyorDirection;

    /// <summary>
    /// Sprite renderer hien thi cua conveyor
    /// </summary>
    public SpriteRenderer render;
    #endregion

    //// Use this for initialization
    //void Start () {
	
    //}
	
    //// Update is called once per frame
    //void Update () {

    //}

    #region Methods to Init, Set Properties, Data, Components
    /// <summary>
    /// Set properties for Conveyor
    /// </summary>
    /// <param name="type"></param>
    /// <param name="direction"></param>
    /// <param name="sprite"></param>
    public void SetProperties(ConveyorType type, ConveyorDirection direction, Sprite sprite)
    {
        SetConveyorType(type);

        SetConveyorDirection(direction);

        SetRender(sprite);

        //this.transform.localPosition = Vector3.zero;
        //this.transform.localScale = Vector3.one;

        switch (this.conveyorType)
        {
            case ConveyorType.TOP_LEFT_CORNER:
            {
                if (this.conveyorDirection == ConveyorDirection.RIGHT)
                {
                    SetRotation(TOP_LEFT_CORNER_RIGHT_ROTATION);
                }
                else
                {
                    SetRotation(TOP_LEFT_CORNER_DOWN_ROTATION);
                }
                
                break;
            }
            case ConveyorType.TOP_RIGHT_CORNER:
            {
                if (this.conveyorDirection == ConveyorDirection.DOWN)
                {
                    SetRotation(TOP_RIGHT_CORNER_DOWN_ROTATION);
                }
                else
                {
                    SetRotation(TOP_RIGHT_CORNER_LEFT_ROTATION);
                }
                
                break;
            }
            case ConveyorType.BOTTOM_RIGHT_CORNER:
            {
                if (this.conveyorDirection == ConveyorDirection.LEFT)
                {
                    SetRotation(BOTTOM_RIGHT_CORNER_LEFT_ROTATION);
                }
                else
                {
                    SetRotation(BOTTOM_RIGHT_CORNER_UP_ROTATION);
                }
                
                break;
            }
            case ConveyorType.BOTTOM_LEFT_CORNER:
            {
                if (this.conveyorDirection == ConveyorDirection.UP)
                {
                    SetRotation(BOTTOM_LEFT_CORNER_UP_ROTATION);
                }
                else
                {
                    SetRotation(BOTTOM_LEFT_CORNER_RIGHT_ROTATION);
                }
                
                break;
            }
            case ConveyorType.LINE:
            {
                switch (this.conveyorDirection)
                {
                    case ConveyorDirection.UP:
                        {
                            SetRotation(UP_ROTATION);
                            break;
                        }
                    case ConveyorDirection.DOWN:
                        {
                            SetRotation(DOWN_ROTATION);
                            break;
                        }
                    case ConveyorDirection.LEFT:
                        {
                            SetRotation(LEFT_ROTATION);
                            break;
                        }
                    case ConveyorDirection.RIGHT:
                        {
                            SetRotation(RIGHT_ROTATION);
                            break;
                        }
                    default:
                        break;
                }

                break;
            }
            default:
                break;
        }
    }

    /// <summary>
    /// Set type for conveyor
    /// </summary>
    /// <param name="type"></param>
    public void SetConveyorType(ConveyorType type)
    {
        this.conveyorType = type;
    }

    /// <summary>
    /// Set direction for conveyor
    /// </summary>
    /// <param name="direction"></param>
    public void SetConveyorDirection(ConveyorDirection direction)
    {
        this.conveyorDirection = direction;
    }

    /// <summary>
    /// Set Sprite Renderer cho border
    /// </summary>
    /// <param name="sprite">sprite hien thi</param>
    /// <param name="enable">co cho hien thi hay khong</param>
    public void SetRender(Sprite sprite = null, bool enable = true)
    {
        this.render.GetComponent<Renderer>().enabled = enable;
        this.render.sprite = sprite;
    }

    /// <summary>
    /// Set rotation for render
    /// </summary>
    /// <param name="rotation"></param>
    public void SetRotation(Vector3 rotation)
    {
        this.render.transform.localEulerAngles = rotation;
    }
    #endregion

    #region Methods handle events of Conveyor
    /// <summary>
    /// Xu ly su kien chuyen thong thuong sau moi nuoc di
    /// </summary>
    public void Convey()
    {
        switch (conveyorDirection)
        {
            case ConveyorDirection.LEFT:
                
                break;
            case ConveyorDirection.RIGHT:
                
                break;
            case ConveyorDirection.UP:
                
                break;
            case ConveyorDirection.DOWN:
                
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Thuc hien viec chuyen den mot vi tri bat ky; 
    /// thong thuong la chuyen tu cuoi den dau bang chuyen
    /// </summary>
    /// <param name="conveyor"></param>
    public void ConveyTo(Conveyor conveyor)
    {
        
    }

    /// <summary>
    /// Get dest grid which conveyor convey to that
    /// </summary>
    /// <returns></returns>
    public Grid GetDestGrid()
    {
        Grid grid = null;

        switch (this.conveyorDirection)
        {
            case ConveyorDirection.LEFT:
                {
                    grid = this.grid.GetGridInDir(Grid.Direction.LEFT);
                    break;
                }
            case ConveyorDirection.RIGHT:
                {
                    grid = this.grid.GetGridInDir(Grid.Direction.RIGHT);
                    break;
                }
            case ConveyorDirection.UP:
                {
                    grid = this.grid.GetGridInDir(Grid.Direction.TOP);
                    break;
                }
            case ConveyorDirection.DOWN:
                {
                    grid = this.grid.GetGridInDir(Grid.Direction.BOTTOM);
                    break;
                }
            default:
                break;
        }

        return grid;
    }

    /// <summary>
    /// Get dest position of conveyor
    /// </summary>
    /// <returns></returns>
    public Vector3 GetDestPosition()
    {
        Vector3 position = Vector3.zero;

        switch (this.conveyorDirection)
        {
            case ConveyorDirection.LEFT:
                {
                    position = new Vector3(this.grid.gridPosition.Column - 1, this.grid.gridPosition.Row, 0f);
                    break;
                }
            case ConveyorDirection.RIGHT:
                {
                    position = new Vector3(this.grid.gridPosition.Column + 1, this.grid.gridPosition.Row, 0f);
                    break;
                }
            case ConveyorDirection.UP:
                {
                    position = new Vector3(this.grid.gridPosition.Column, this.grid.gridPosition.Row + 1, 0f);
                    break;
                }
            case ConveyorDirection.DOWN:
                {
                    position = new Vector3(this.grid.gridPosition.Column , this.grid.gridPosition.Row - 1, 0f);
                    break;
                }
            default:
                break;
        }

        return position;
    }

    /// <summary>
    /// Get source position of conveyor
    /// </summary>
    /// <returns></returns>
    public Vector3 GetSourcePosition()
    {
        Vector3 position = Vector3.zero;

        switch (this.conveyorDirection)
        {
            case ConveyorDirection.LEFT:
                {
                    position = new Vector3(this.grid.gridPosition.Column + 1, this.grid.gridPosition.Row, 0f);
                    break;
                }
            case ConveyorDirection.RIGHT:
                {
                    position = new Vector3(this.grid.gridPosition.Column - 1, this.grid.gridPosition.Row, 0f);
                    break;
                }
            case ConveyorDirection.UP:
                {
                    position = new Vector3(this.grid.gridPosition.Column, this.grid.gridPosition.Row - 1, 0f);
                    break;
                }
            case ConveyorDirection.DOWN:
                {
                    position = new Vector3(this.grid.gridPosition.Column, this.grid.gridPosition.Row + 1, 0f);
                    break;
                }
            default:
                break;
        }

        return position;
    }
    #endregion
}

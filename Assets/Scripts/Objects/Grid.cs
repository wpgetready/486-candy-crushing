﻿/*
 * @Author: CuongNH
 * @Description: Mo ta cac thuoc tinh va xu ly cac su kien gan lien voi moi mot Grid
 * */

using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class Grid : MonoBehaviour
{

    #region Properties
    /// <summary>
    /// Can have gem inside
    /// </summary>
    public bool isGemContainer;

    /// <summary>
    /// Block gem moving past it
    /// </summary>
    public bool isBlockGemMove;

    /// <summary>
    /// Protect inside gem from outside (bomb, magic, lightning)
    /// </summary>
    public bool isGemProtect;

    /// <summary>
    /// Do it keep the gem inside and wont drop it down further?
    /// </summary>
    public bool isHoldGemInside;

    /// <summary>
    /// Can check match with the gem inside?
    /// </summary>
    public bool isGemMatchEnabled;

    /// <summary>
    /// Can swap the gem inside with gem from other grid?
    /// </summary>
    public bool isInsideGemSwappable;

    /// <summary>
    /// To decide if it need to spawn/shrink check or not
    /// </summary>
    public bool isChangedLastTurn;

    /// <summary>
    /// Set grid is bottom
    /// </summary>
    public bool isBottom;

    /// <summary>
    /// Xac dinh co bang chuyen hay khong
    /// </summary>
    public bool hasConveyor = false;

	public bool hasPopsicle = false;

    #endregion

    #region Data, Components of Grid
    /// <sumary>
    /// position of grid in board
    /// </sumary>
    [SerializeField]
    public GridPosition gridPosition;
    /// <summary>
    /// Hien thi nen (cell background)
    /// </summary>
    public SpriteRenderer mainRender;

    /// <summary>
    /// Hien thi cac thanh phan co dinh nhu: bang chuyen, portal inlet/outlet
    /// </summary>
    public SpriteRenderer staticRender;

    /// <summary>
    /// Chua cac thanh phan tinh cua grid
    /// </summary>
    public Transform staticComponents;

    /// <summary>
    /// Hien thi cac thanh phan co the thay doi trong qua trinh choi: ice, cracker, honey
    /// </summary>
    public SpriteRenderer dynamicRender;

    /// <summary>
    /// Chua cac thanh phan co the thay doi cua Grid
    /// </summary>
    public Transform dynamicComponents;

    /// <summary>
    /// Chua cac du lieu cua Grid
    /// </summary>
    public GridData gridData;

    /// <summary>
    /// Owned tile, default is null
    /// </summary>
    [SerializeField]
    private Tile tile;

    /// <summary>
    /// Owned conveyor, default is null
    /// </summary>
    [SerializeField]
    public Conveyor conveyor;

    /// <summary>
    /// Owned portal, default is null
    /// </summary>
    [SerializeField]
    public Portal portal;

    /// <summary>
    /// Chua border cua grid
    /// </summary>
    public Transform borders;

    /// <summary>
    /// Danh sach cac grid xung quanh duoc ket noi voi grid nay
    /// </summary>
    public List<Grid> connectedList;

    /// <summary>
    /// So huong cua grid
    /// </summary>
    public static int DIR_NUM = 10;

    /// <summary>
    /// Dinh nghia cac huong cua Grid
    /// </summary>
    public enum Direction
    {
        UNKNOWN = 0, // Point to nowhere
        BOTTOM_LEFT = 1,
        BOTTOM = 2,
        BOTTOM_RIGHT = 3,
        LEFT = 4,
        CENTER = 5,
        RIGHT = 6,
        TOP_LEFT = 7,
        TOP = 8,
        TOP_RIGHT = 9,
    }
    #endregion

    // Use this for initialization
    //void Start()
    //{

    //}

    #region Set Data for Grid
    /// <summary>
    /// Set Data for Grid
    /// </summary>
    /// <param name="gtype"></param>
    /// <param name="gStaticType"></param>
    /// <param name="gDynamicType"></param>
    public void SetGridData(GridData.GridType gtype = GridData.GridType.NORMAL_GRID, 
        GridData.GridStaticType gStaticType = GridData.GridStaticType.NORMAL_GRID,
        GridData.GridDynamicType gDynamicType = GridData.GridDynamicType.NORMAL_GRID)
    {
        try 
	    {	        
		    this.gridData.SetGridData(gtype, gStaticType, gDynamicType);
	    }
	    catch (System.Exception e)
	    {
		    VGDebug.LogError(e.Message);
	    }

        // Check grid is null
        if (this.gridData.gridType == GridData.GridType.NULL_GRID)
        {
            this.mainRender.enabled = false;
        }
    }

    /// <summary>
    /// Set GridData for grid
    /// </summary>
    /// <param name="gridData"></param>
    public void SetGridData(GridData gridData)
    {
        this.gridData = gridData;
    }

    /// <summary>
    /// Set GridPosition for Grid
    /// </summary>
    /// <param name="position"></param>
    public void SetGridPosition(GridPosition position)
    {
        this.gridPosition = position;
    }
    #endregion

    #region Set Render for components of Grid
    /// <summary>
    /// Set main render by Sprite
    /// </summary>
    /// <param name="sprite"></param>
    public void SetMainRender(Sprite sprite = null, bool enable = true)
    {
        this.mainRender.enabled = enable;
        this.mainRender.sprite = sprite;
    }

    /// <summary>
    /// Set main render by SpriteRenderer
    /// </summary>
    /// <param name="render"></param>
    public void SetMainRender(SpriteRenderer render = null, bool enable = true)
    {
        this.mainRender.enabled = enable;
        this.mainRender = render;
    }

    /// <summary>
    /// Set static render by Sprite
    /// </summary>
    /// <param name="sprite"></param>
    public void SetStaticRender(Sprite sprite, bool enalbe = true)
    {
        this.staticRender.enabled = enalbe;
		this.staticRender.transform.localScale = 0.8f * Vector3.one;
        this.staticRender.sprite = sprite;
    }

    /// <summary>
    /// Set static render by SpriteRenderer
    /// </summary>
    /// <param name="render"></param>
    public void SetStaticRender(SpriteRenderer render, bool enalbe = true)
    {
        this.staticRender.enabled = enalbe;
        this.staticRender = render;
    }

    /// <summary>
    /// Set dynamic render by Sprite
    /// </summary>
    /// <param name="sprite"></param>
    public void SetDynamicRender(Sprite sprite, bool enalbe = true)
    {
        this.dynamicRender.enabled = enalbe;
        this.dynamicRender.sprite = sprite;
    }

    /// <summary>
    /// Set dynamic render by SpriteRenderer
    /// </summary>
    /// <param name="render"></param>
    public void SetDynamicRender(SpriteRenderer render, bool enalbe = true)
    {
        this.dynamicRender.enabled = enalbe;
        this.dynamicRender = render;
    }
    #endregion

    #region Methods for Tile
    /// <summary>
    /// Set Tile for Grid
    /// </summary>
    /// <param name="_tile"></param>
    public void SetTile(Tile _tile, bool setInCenter = false)
    {
        try
        {
            this.tile = _tile;
            if (_tile != null)
            {
                this.tile.transform.SetParent(this.dynamicComponents);
                this.tile.transform.localScale = Vector3.one;

                if (setInCenter)
                {
                    this.tile.transform.localPosition = Vector3.zero;
                }
                
                _tile.grid = this;
            }
            else
            {
                if (GamePlayController.gamePlayController.mapController.processingGrids.Contains(this))
                {
                    GamePlayController.gamePlayController.mapController.processingGrids.Remove(this);
                }

                if (GamePlayController.gamePlayController.mapController.emptyGrids.Contains(this))
                {
                    GamePlayController.gamePlayController.mapController.emptyGrids.Remove(this);
                }
            }
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);
        }
    }

    /// <summary>
    /// Get tile of grid
    /// </summary>
    /// <returns></returns>
    public Tile GetTile()
    {
        return this.tile;
    }

    /// <summary>
    /// Set dynamic component for grid
    /// </summary>
    /// <param name="component"></param>
    public void SetDynamicComponent(Transform component)
    {
        if (component != null)
        {
            this.dynamicRender = component.GetChild(0).GetComponent<SpriteRenderer>();
            this.dynamicComponents = component;
            component.SetParent(this.transform);
        }
    }

	public void SetStaticComponent(Transform component)
	{
		if (component != null)
		{
			this.staticRender = component.GetChild(0).GetComponent<SpriteRenderer>();
			this.staticComponents = component;
			component.SetParent(this.transform);
		}
	}

    /// <summary>
    /// Move dynamic component to center grid
    /// </summary>
    /// <param name="time"></param>
    /// <returns></returns>
    public float MoveDynamicComponentToCenter(float time = GameConstants.SPEED_DROP_TILE_EACH_STEP)
    {
        Tile tile = this.GetTile();
        // Moving Code HERE
        if (tile != null)
        {
            tile.isMoving = true;
        }

        LeanTween.moveLocal(this.dynamicComponents.gameObject, Vector3.zero, time)
            .setOnComplete(() =>
            {
                if (tile != null)
                {
                    tile.isMoving = false;
                }
            });

        return 0f;
    }

	public float MoveStaticComponentToCenter(float time = GameConstants.SPEED_DROP_TILE_EACH_STEP)
	{
		Tile tile = this.GetTile();
		// Moving Code HERE
		if (tile != null)
		{
			tile.isMoving = true;
		}

		LeanTween.moveLocal(this.staticComponents.gameObject, Vector3.zero, time)
			.setOnComplete(() =>
				{
					if (tile != null)
					{
						tile.isMoving = false;
					}
				});

		return 0f;
	}

//	public float MovePopsicleToCenter(float time = GameConstants.SPEED_DROP_TILE_EACH_STEP, Conveyor _toConveyor = null)
//	{
//		Tile tile = this.GetTile();
//		// Moving Code HERE
//		if (tile != null)
//		{
//			tile.isMoving = true;
//		}
//
//		if(this._popsicle != null)
//		{
//			LeanTween.moveLocal(this._popsicle.gameObject, _toConveyor.transform.position, time)
//				.setOnComplete(() =>
//					{
//						if (tile != null)
//						{
//							tile.isMoving = false;
//						}
//					});
//		}
//		return 0f;
//	}

    /// <summary>
    /// Move dynamic component by belt
    /// </summary>
    /// <param name="destFrom"></param>
    /// <param name="sourceTo"></param>
    /// <param name="timeToDestFrom"></param>
    /// <param name="timeToMoveCenterGrid"></param>
    /// <returns></returns>
    
	public float MoveDynamicComponentByBelt(Vector3 destFrom, Vector3 sourceTo, 
        float timeToDestFrom, float timeToMoveCenterGrid)
    {
        Tile tile = this.GetTile();
        // Moving Code HERE
        if (tile != null)
        {
            tile.isMoving = true;
        }

		//LeanTween.moveLocal(this.dynamicComponents.gameObject, destFrom, timeToDestFrom)

        LeanTween.moveLocal(this.dynamicComponents.gameObject, destFrom, timeToDestFrom)
            .setOnComplete(() =>
            {
                this.dynamicComponents.localPosition = sourceTo;

				//LeanTween.moveLocal(this.staticRender.gameObject, Vector3.zero, timeToMoveCenterGrid);

                LeanTween.moveLocal(this.dynamicComponents.gameObject, 
                    Vector3.zero, timeToMoveCenterGrid).setOnComplete(() =>
                    {
                        if (tile != null)
                        {
                            tile.isMoving = false;
                        }
                    });
            });

        return 0f;
    }

	public float MoveStaticComponentByBelt(Vector3 destFrom, Vector3 sourceTo, 
		float timeToDestFrom, float timeToMoveCenterGrid)
	{
		Tile tile = this.GetTile();
		// Moving Code HERE
		if (tile != null)
		{
			tile.isMoving = true;
		}

		//LeanTween.moveLocal(this.dynamicComponents.gameObject, destFrom, timeToDestFrom)

		LeanTween.moveLocal(this.staticComponents.gameObject, destFrom, timeToDestFrom)
			.setOnComplete(() =>
				{
					this.staticComponents.localPosition = sourceTo;

					//LeanTween.moveLocal(this.staticRender.gameObject, Vector3.zero, timeToMoveCenterGrid);

					LeanTween.moveLocal(this.staticComponents.gameObject, 
						Vector3.zero, timeToMoveCenterGrid).setOnComplete(() =>
							{
								if (tile != null)
								{
									tile.isMoving = false;
								}
							});
				});

		return 0f;
	}

//	public float MovePopByBelt(Vector3 destFrom, Vector3 sourceTo, 
//		float timeToDestFrom, float timeToMoveCenterGrid)
//	{
//		GameObject pop = GetComponentInChildren<Popsicle> ().gameObject;
//		Tile tile = this.GetTile();
//		// Moving Code HERE
//		if (tile != null)
//		{
//			tile.isMoving = true;
//		}
//
//		LeanTween.moveLocal(pop, destFrom, timeToDestFrom)
//			.setOnComplete(() =>
//				{
//					pop.transform.localPosition = sourceTo;
//					LeanTween.moveLocal(pop, 
//						Vector3.zero, timeToMoveCenterGrid).setOnComplete(() =>
//							{
//								if (tile != null)
//								{
//									tile.isMoving = false;
//								}
//							});
//				});
//
//		return 0f;
//	}

    /// <summary>
    /// Move tile to center of grid
    /// </summary>
    /// <param name="time"></param>
    /// <param name="finishType"></param>
    /// <returns></returns>
    public float MoveTileToCenter(float time = GameConstants.SPEED_SWAP_TILE,
        Tile.MoveFinishType finishType = Tile.MoveFinishType.NO_ACTION)
    {
        Tile tile = this.GetTile();
        if (tile == null)
        {
            return 0;
        }

        // Moving Code HERE
        tile.isMoving = true;

        if (tile.moveAction != null)
        {
            LeanTween.cancel(tile.gameObject, tile.moveAction.id);
        }

        if (!GamePlayController.gamePlayController.mapController.processingGrids.Contains(this))
        {
            GamePlayController.gamePlayController.mapController.processingGrids.Add(this);
        }

        float finishTime = 0;
        if (finishType == Tile.MoveFinishType.BOUNCE)
        {
            Vector3 moveback = Vector3.Normalize(new Vector3(tile.transform.localPosition.x,
                tile.transform.localPosition.y, 0)) * GameConstants.TILE_BOUNCE;
            //moveback += Vector3.zero;

            finishTime = 0;

            LeanTween.scale(tile.gameObject, new Vector3(
                tile.transform.localScale.x - tile.transform.localScale.x * GameConstants.TILE_SCALE_DROP,
                tile.transform.localScale.y + tile.transform.localScale.y * GameConstants.TILE_SCALE_DROP, 1),
                GameConstants.TIME_TILE_BOUNE);

            tile.moveAction = LeanTween.moveLocal(tile.gameObject, Vector3.zero, time)
                .setOnComplete(() =>
                {
                    tile.moveAction = null;
                    //AudioController.PlaySFX("sfx_fall");

                    //tile.moveAction = LeanTween.moveLocal(tile.gameObject, moveback, 0.1f).setLoopPingPong().setRepeat(2);

                    tile.moveAction = LeanTween.moveLocal(tile.gameObject, -moveback,
                        GameConstants.TIME_TILE_BOUNE).setLoopPingPong().setRepeat(2);

                    LeanTween.scale(tile.gameObject, new Vector3(
                        tile.transform.localScale.x + tile.transform.localScale.x * GameConstants.TILE_SCALE_DROP,
                        tile.transform.localScale.y - tile.transform.localScale.y * GameConstants.TILE_SCALE_DROP, 1),
                        GameConstants.TIME_TILE_BOUNE).setOnComplete(() =>
                        {
                            LeanTween.scale(tile.gameObject, Vector3.one, GameConstants.TIME_TILE_BOUNE);
								}).setLoopOnce();

                    FinishMoving();
					});
        }
        else if (finishType == Tile.MoveFinishType.GROW)
        {
            finishTime = 0.3f;
            tile.moveAction = LeanTween.moveLocal(tile.gameObject, Vector3.zero, time)
                .setOnComplete(() =>
                {
                    tile.moveAction = null;

                    tile.transform.localScale = new Vector3(0, 0, 1);
                    LeanTween.scale(tile.gameObject, Vector3.one, finishTime);

                    FinishMoving();
                });
        }
        else if (finishType == Tile.MoveFinishType.NO_ACTION_DIAGONAL)
        {
            tile.moveAction = LeanTween.moveLocal(tile.gameObject, Vector3.zero, time)
                .setEase(LeanTweenType.easeOutSine).setOnComplete(() =>
                {
                    tile.moveAction = null;
                    FinishMoving(finishType == Tile.MoveFinishType.SWAP);
                });
        }
        else
        {
            tile.moveAction = LeanTween.moveLocal(tile.gameObject, Vector3.zero, time)
                .setOnComplete(() =>
                {
                    tile.moveAction = null;
                    FinishMoving(finishType == Tile.MoveFinishType.SWAP);
                });
        }

        LeanTween.delayedCall(tile.gameObject, time + finishTime, () =>
        {
            tile.isMoving = false;

            //FinishMoving();
        });

        return finishTime;
    }

    /// <summary>
    /// Move tile throgh portal
    /// </summary>
    /// <param name="portalInPosition"></param>
    /// <param name="portalOutPosition"></param>
    /// <param name="timeToInPortal"></param>
    /// <param name="timeToMoveCenterGrid"></param>
    /// <param name="finishType"></param>
    /// <returns></returns>
    public float MoveTileThroughPortal(Vector3 portalInPosition, Vector3 portalOutPosition, float timeToInPortal, 
		float timeToMoveCenterGrid, Tile.MoveFinishType finishType = Tile.MoveFinishType.NO_ACTION, bool isSoundDrop = false)
    {
        float finishTime = 0f;

        Tile tile = this.GetTile();
        if (tile == null)
        {
            return 0;
        }

        // Moving Code HERE
        tile.isMoving = true;
        if (tile.moveAction != null)
        {
            LeanTween.cancel(tile.gameObject, tile.moveAction.id);
        }

        if (!GamePlayController.gamePlayController.mapController.processingGrids.Contains(this))
        {
            GamePlayController.gamePlayController.mapController.processingGrids.Add(this);
        }

        switch (finishType)
        {
            case Tile.MoveFinishType.BOUNCE:
                {
                    finishTime = 0;

                    LeanTween.scale(tile.gameObject, new Vector3(
                        tile.transform.localScale.x - tile.transform.localScale.x * GameConstants.TILE_SCALE_DROP,
                        tile.transform.localScale.y + tile.transform.localScale.y * GameConstants.TILE_SCALE_DROP, 1),
                        GameConstants.TIME_TILE_BOUNE);

                    tile.moveAction = LeanTween.moveLocal(tile.gameObject, portalInPosition, timeToInPortal)
                        .setOnComplete(() =>
                        {
                            tile.transform.localPosition = portalOutPosition;
                            Vector3 moveback = Vector3.Normalize(new Vector3(tile.transform.localPosition.x,
                                tile.transform.localPosition.y, 0)) * GameConstants.TILE_BOUNCE;

							if(isSoundDrop == true)
							{
								LeanTween.delayedCall(timeToMoveCenterGrid-0.1f,() => {
									SoundController.sound.dropSoundPlay();
								});
							}

                            tile.moveAction = LeanTween.moveLocal(tile.gameObject, Vector3.zero, timeToMoveCenterGrid)
                                .setOnComplete(() =>
                                {
                                    //AudioController.PlaySFX("sfx_fall");
                                    //tile.moveAction = LeanTween.moveLocal(tile.gameObject, moveback, 0.1f).setLoopPingPong().setRepeat(2);

                                    tile.moveAction = LeanTween.moveLocal(tile.gameObject, -moveback,
                                        GameConstants.TIME_TILE_BOUNE).setLoopPingPong().setRepeat(2);

                                    LeanTween.scale(tile.gameObject, new Vector3(
                                        tile.transform.localScale.x + tile.transform.localScale.x * GameConstants.TILE_SCALE_DROP,
                                        tile.transform.localScale.y - tile.transform.localScale.y * GameConstants.TILE_SCALE_DROP, 1),
                                        GameConstants.TIME_TILE_BOUNE).setOnComplete(() =>
                                        {
                                            LeanTween.scale(tile.gameObject, Vector3.one, GameConstants.TIME_TILE_BOUNE);
                                        });

                                    FinishMoving();
                                });
                        });
                    break;
                }
            default:
                {
                    finishTime = 0;

                    tile.moveAction = LeanTween.moveLocal(tile.gameObject, portalInPosition, timeToInPortal)
                        .setOnComplete(() =>
                        {
                            tile.transform.localPosition = portalOutPosition;

							if(isSoundDrop == true)
							{
								LeanTween.delayedCall(timeToMoveCenterGrid-0.1f,() => {
									SoundController.sound.dropSoundPlay();
								});
							}

                            tile.moveAction = LeanTween.moveLocal(tile.gameObject, Vector3.zero, timeToMoveCenterGrid)
                                .setOnComplete(() =>
                                {
                                    //AudioController.PlaySFX("sfx_fall");
                                    FinishMoving();
                                });
                        });
                    break;
                }
        }

        LeanTween.delayedCall(tile.gameObject, timeToInPortal + timeToMoveCenterGrid + finishTime, () =>
        {
            tile.isMoving = false;
            //FinishMoving();
        });

        return finishTime;
    }

    /// <summary>
    /// Move tile from any spawner
    /// </summary>
    /// <param name="spawnerPosition"></param>
    /// <param name="timeToMoveToSpawner"></param>
    /// <param name="timeToMoveCenterGrid"></param>
    /// <param name="finishType"></param>
    /// <returns></returns>
    public float MoveTileFromSpawner(Vector3 spawnerPosition, float timeToMoveToSpawner,
		float timeToMoveCenterGrid, Tile.MoveFinishType finishType = Tile.MoveFinishType.NO_ACTION, bool isSoundDrop = false)
    {
        float finishTime = 0f;

        Tile tile = this.GetTile();
        if (tile == null)
        {
            return 0;
        }

        // Moving Code HERE
        tile.isMoving = true;
        if (tile.moveAction != null)
        {
            LeanTween.cancel(tile.gameObject, tile.moveAction.id);
        }

        if (!GamePlayController.gamePlayController.mapController.processingGrids.Contains(this))
        {
            GamePlayController.gamePlayController.mapController.processingGrids.Add(this);
        }

        switch (finishType)
        {
            case Tile.MoveFinishType.BOUNCE:
                {
                    finishTime = 0;
                    tile.moveAction = LeanTween.delayedCall(tile.gameObject, timeToMoveToSpawner, () =>
                        {
                            tile.transform.localPosition = spawnerPosition;
                            //tile.SetTileActive(true);

                            Vector3 moveback = Vector3.Normalize(new Vector3(tile.transform.localPosition.x,
                                tile.transform.localPosition.y, 0)) * GameConstants.TILE_BOUNCE;

                            LeanTween.scale(tile.gameObject, new Vector3(
                                tile.transform.localScale.x - tile.transform.localScale.x * GameConstants.TILE_SCALE_DROP,
                                tile.transform.localScale.y + tile.transform.localScale.y * GameConstants.TILE_SCALE_DROP, 1),
                                GameConstants.TIME_TILE_BOUNE);

						if(isSoundDrop == true)
						{
							LeanTween.delayedCall(timeToMoveCenterGrid-0.1f,() => {
								SoundController.sound.dropSoundPlay();
							});
						}
                            tile.moveAction = LeanTween.moveLocal(tile.gameObject, Vector3.zero, timeToMoveCenterGrid)
                                .setOnComplete(() =>
                                {
                                    //AudioController.PlaySFX("sfx_fall");
                                    //tile.moveAction = LeanTween.moveLocal(tile.gameObject, moveback, 0.1f).setLoopPingPong().setRepeat(2);

                                    tile.moveAction = LeanTween.moveLocal(tile.gameObject, -moveback,
                                        GameConstants.TIME_TILE_BOUNE).setLoopPingPong().setRepeat(2);

                                    LeanTween.scale(tile.gameObject, new Vector3(
                                        tile.transform.localScale.x + tile.transform.localScale.x * GameConstants.TILE_SCALE_DROP,
                                        tile.transform.localScale.y - tile.transform.localScale.y * GameConstants.TILE_SCALE_DROP, 1),
                                        GameConstants.TIME_TILE_BOUNE).setOnComplete(() =>
                                        {
                                            LeanTween.scale(tile.gameObject, Vector3.one, GameConstants.TIME_TILE_BOUNE);
                                        });

                                    FinishMoving();
                                });
                        });
                    break;
                }
            default:
                {
                    tile.moveAction = LeanTween.delayedCall(tile.gameObject, timeToMoveToSpawner, () =>
                    {
                        tile.transform.localPosition = spawnerPosition;
                        //tile.SetTileActive(true);

						if(isSoundDrop == true)
						{
							LeanTween.delayedCall(timeToMoveCenterGrid-0.1f,() => {
								SoundController.sound.dropSoundPlay();
							});
						}

                        tile.moveAction = LeanTween.moveLocal(tile.gameObject, Vector3.zero, timeToMoveCenterGrid)
                            .setOnComplete(() =>
                            {
                                //AudioController.PlaySFX("sfx_fall");
                                FinishMoving();
                            });
                    });
                    break;
                }
        }

        LeanTween.delayedCall(tile.gameObject, timeToMoveToSpawner + timeToMoveCenterGrid + finishTime, () =>
        {
            tile.isMoving = false;
            //FinishMoving();
        });

        return finishTime;
    }

    /// <summary>
    /// Handle when tile finish move
    /// </summary>
    /// <param name="swapping"></param>
    void FinishMoving(bool swapping = false)
    {
        // remove from processing list
        if (GamePlayController.gamePlayController.mapController.processingGrids.Contains(this))
        {
            GamePlayController.gamePlayController.mapController.processingGrids.Remove(this);
        }

        if (GamePlayController.gamePlayController.mapController.emptyGrids.Contains(this))
        {
            GamePlayController.gamePlayController.mapController.emptyGrids.Remove(this);
        }

        if (!swapping)
        {
            GamePlayController.gamePlayController.OnTileFinishedMoving();
        }
    }

    #endregion

    #region Methods for Connected List, Grid direction
    /// <summary>
    /// Add grid to Connected List
    /// </summary>
    /// <param name="grid"></param>
    public void AddToConnectedList(Grid grid)
    {
        this.connectedList.Add(grid);
    }

    /// <summary>
    /// Return a Grid in specified dir
    /// </summary>
    /// <param name="dir"></param>
    /// <returns></returns>
    public Grid GetGridInDir(Direction dir)
    {
        Grid result = null;

        // Grid link must be stored in right way,
        // in a pair of dir - grid
        result = connectedList[(int)dir];

        return result;
    }

    /// <summary>
    /// Check has grid with direction
    /// </summary>
    /// <param name="dir"></param>
    /// <returns></returns>
    public bool HasGridInDir(Direction dir)
    {
        if (connectedList[(int)dir] == null)
        {
            return false;
        }

        if (connectedList[(int)dir].gridData.gridType == GridData.GridType.NULL_GRID)
        {
            return false;
        }

        return true;
    }

    /// <summary>
    /// Get direction of grid
    /// </summary>
    /// <param name="grid"></param>
    /// <returns></returns>
    public Direction GetDirOfGrid(Grid grid)
    {
        Direction result = Direction.UNKNOWN;

        if (connectedList.Contains(grid))
        {
            for (int i = 1; i <= DIR_NUM - 1; i++)
            {
                if (connectedList[i] == grid)
                {
                    result = (Direction)(i);
                }
            }
        }

        return result;
    }

    /// <summary>
    /// Get opposed dir of dir
    /// </summary>
    /// <param name="dir"></param>
    /// <returns></returns>
    public static Grid.Direction GetOpposedDir(Grid.Direction dir)
    {
        return (Grid.Direction)(DIR_NUM - (int)dir);
    }

    /// <summary>
    /// Get droppable direction of a grid based on a Direction
    /// </summary>
    public static List<Grid.Direction> GetDroppableDiagonalDirections(Grid.Direction dir)
    {
        List<Grid.Direction> dirList = new List<Grid.Direction>();

        switch (dir)
        {
            case Grid.Direction.TOP:
                {
                    dirList.Add(Grid.Direction.TOP_LEFT);
                    dirList.Add(Grid.Direction.TOP_RIGHT);
                }
                break;
            case Grid.Direction.BOTTOM_RIGHT:
                {
                    dirList.Add(Grid.Direction.RIGHT);
                    dirList.Add(Grid.Direction.BOTTOM);
                }
                break;
            case Grid.Direction.BOTTOM_LEFT:
                {
                    dirList.Add(Grid.Direction.LEFT);
                    dirList.Add(Grid.Direction.BOTTOM);
                }
                break;
            case Grid.Direction.BOTTOM:
                {
                    dirList.Add(Grid.Direction.BOTTOM_LEFT);
                    dirList.Add(Grid.Direction.BOTTOM_RIGHT);
                }
                break;
            case Grid.Direction.TOP_RIGHT:
                {
                    dirList.Add(Grid.Direction.RIGHT);
                    dirList.Add(Grid.Direction.TOP);
                }
                break;
            case Grid.Direction.TOP_LEFT:
                {
                    dirList.Add(Grid.Direction.LEFT);
                    dirList.Add(Grid.Direction.TOP);
                }
                break;
            case Grid.Direction.LEFT:
                {
                    dirList.Add(Grid.Direction.TOP_LEFT);
                    dirList.Add(Grid.Direction.BOTTOM_LEFT);
                }
                break;
            case Grid.Direction.RIGHT:
                {
                    dirList.Add(Grid.Direction.TOP_RIGHT);
                    dirList.Add(Grid.Direction.BOTTOM_RIGHT);
                }
                break;
            default:
                break;
        }

        return dirList;
    }

    /// <summary>
    /// Get adjacent directions of a directions
    /// </summary>
    public static List<Grid.Direction> GetAdjacentDirections(Grid.Direction dir)
    {
        List<Grid.Direction> dirList = new List<Grid.Direction>();

        switch (dir)
        {
            case Grid.Direction.TOP:
                {
                    dirList.Add(Grid.Direction.LEFT);
                    dirList.Add(Grid.Direction.RIGHT);
                }
                break;
            case Grid.Direction.TOP_LEFT:
                {
                    dirList.Add(Grid.Direction.BOTTOM_LEFT);
                    dirList.Add(Grid.Direction.TOP_RIGHT);
                }
                break;
            case Grid.Direction.TOP_RIGHT:
                {
                    dirList.Add(Grid.Direction.TOP_LEFT);
                    dirList.Add(Grid.Direction.BOTTOM_RIGHT);
                }
                break;
            case Grid.Direction.BOTTOM:
                {
                    dirList.Add(Grid.Direction.LEFT);
                    dirList.Add(Grid.Direction.RIGHT);
                }
                break;
            case Grid.Direction.BOTTOM_LEFT:
                {
                    dirList.Add(Grid.Direction.TOP_LEFT);
                    dirList.Add(Grid.Direction.BOTTOM_RIGHT);
                }
                break;
            case Grid.Direction.BOTTOM_RIGHT:
                {
                    dirList.Add(Grid.Direction.BOTTOM_LEFT);
                    dirList.Add(Grid.Direction.TOP_RIGHT);
                }
                break;
            case Grid.Direction.LEFT:
                {
                    dirList.Add(Grid.Direction.TOP);
                    dirList.Add(Grid.Direction.BOTTOM);
                }
                break;
            case Grid.Direction.RIGHT:
                {
                    dirList.Add(Grid.Direction.TOP);
                    dirList.Add(Grid.Direction.BOTTOM);
                }
                break;
            default:
                break;
        }

        return dirList;
    }

    /// <summary>
    /// Check this grid is adjacent with grid
    /// </summary>
    /// <param name="grid"></param>
    /// <returns></returns>
    public bool CheckIsAdjacent(Grid grid)
    {
        for (int i = 0; i < DIR_NUM; i++)
        {
            if (this.connectedList[i] == null)
            {
                continue;
            }

            if (this.connectedList[i] == grid)
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Get normalized step for each direction
    /// </summary>
    public static Vector3 GetMapStepWithDirection(Grid.Direction dir)
    {
        switch (dir)
        {
            case Grid.Direction.TOP:
                {
                    return new Vector3(0, 1);
                }
            case Grid.Direction.TOP_LEFT:
                {
                    return new Vector3(-1, 1);
                }
            case Grid.Direction.TOP_RIGHT:
                {
                    return new Vector3(1, 1);
                }
            case Grid.Direction.BOTTOM:
                {
                    return new Vector3(0, -1);
                }
            case Grid.Direction.BOTTOM_LEFT:
                {
                    return new Vector3(-1, -1);
                }
            case Grid.Direction.BOTTOM_RIGHT:
                {
                    return new Vector3(1, -1);
                }
            case Grid.Direction.LEFT:
                {
                    return new Vector3(-1, 0);
                }
            case Grid.Direction.RIGHT:
                {
                    return new Vector3(1, 0);
                }
            default:
                return new Vector3(0, 0);
        }
    }

    /// <summary>
    /// Get time to drop with direction
    /// </summary>
    /// <param name="dir"></param>
    /// <returns></returns>
    public static float GetDropTimeModifierWithDirection(Grid.Direction dir)
    {
        switch (dir)
        {
            case Grid.Direction.TOP:
            case Grid.Direction.BOTTOM:
            case Grid.Direction.LEFT:
            case Grid.Direction.RIGHT:
                {
                    return 1;
                }
            case Grid.Direction.TOP_LEFT:
            case Grid.Direction.TOP_RIGHT:
            case Grid.Direction.BOTTOM_LEFT:
            case Grid.Direction.BOTTOM_RIGHT:
                {
                    return GameConstants.SQRT2;
                }
            default:
                return 1;
        }
    }
    #endregion

    #region Methods to Set Border for grid
    /// <summary>
    /// Set border for grid
    /// </summary>
    /// <param name="border"></param>
    public void SetBorder(Border border)
    {
        try
        {
            if (border != null)
            {
                border.transform.SetParent(borders);
            }
        }
        catch (System.Exception e)
        {
            VGDebug.LogError(e.Message);
        }
    }
    #endregion

    #region Methods for handle transform, active
    /// <summary>
    /// Return true if absorbed damage
    /// </summary>
	public bool Damage(bool isAffectNearby = true)
    {
        // For test
        if (this.tile != null)
        {
            switch (this.tile.tileData.tileModifierType)
            {
                case TileData.TileModifierType.ice_cage:
                    {
                        tile.OnUpgrade();
                        break;
                    }
                default:
                    {
                        if (this.tile.isTransforming)
                        {
                            return false;
                        }

						bool cleared = this.tile.Damage();
                        if (cleared)
                        {
                            // TODO: Check tile valid
                            this.OnTileCleared();

                            if (isAffectNearby)
                            {
                                AffectNearbyGrid(tile);
                            } // end if
                        }
                        break;
                    }
            }
        }
        // End test

        return false;
    }

    /// <summary>
    /// Affect with grid near
    /// </summary>
    public void AffectNearbyGrid(Tile tileHandle)
    {
		//Debug.Log (tileHandle.grid.gridPosition.Row+"_"+tileHandle.grid.gridPosition.Column);
        for (int i = 0; i < GameConstants.DIRECTIONS_TILE_CLEAR_AFFECTING.Count; i++)
        {
            Grid.Direction dir = GameConstants.DIRECTIONS_TILE_CLEAR_AFFECTING[i];
            Grid g = this.GetGridInDir(dir);
            if (g != null && g.gridData.gridType != GridData.GridType.NULL_GRID)
            {
				if (dir == Direction.TOP || dir == Direction.BOTTOM || dir == Direction.LEFT || dir == Direction.RIGHT) 
				{
					g.OnNearByTileCleared (tileHandle, true);
				} 
				else 
				{
					g.OnNearByTileCleared (tileHandle);
				}
            }
        }
    }

    /// <summary>
    /// Xu ly sau khi clear tile -> tac dong den grid chua no
    /// </summary>
    public void OnTileCleared()
    {
        this.isChangedLastTurn = true;
        switch (this.gridData.gridDynamicType)
        {
            
            case GridData.GridDynamicType.CRACKER_GRID:
                {
                    // TODO: handle
                    if (this.gridData.gridDynamicData > 0)
                    {
                        GamePlayController.gamePlayController.effectControll.CreateEffectDynamic(this);

                        this.gridData.gridDynamicData--;
                        //GamePlayController.gamePlayController.effectControll.CreateEffectDynamic(grid);
                        if (this.gridData.gridDynamicData <= 0)
                        {

                            this.gridData.gridDynamicType = GridData.GridDynamicType.NORMAL_GRID;
                            this.dynamicRender.sprite = null;
                        }
                        else
                        {
                            GamePlayController.gamePlayController.mapController.gridSpawner.SetGridProperties(this, this.gridData);
                        }
                    }
                    
                    break;
                }
            case GridData.GridDynamicType.HONEY_GRID:
                {
                    GamePlayController.gamePlayController.effectControll.CreateEffectDynamic(this);
                    //LeanTween.rotate(this.dynamicRender.gameObject, new Vector3(0,0,180), 0.3f);
                    //LeanTween.scale(this.dynamicRender.gameObject, Vector3.zero, 0.3f);
                    LeanTween.delayedCall(this.gameObject, 0.1f, () =>
                    {
                        dynamicRender.sprite = null;
                        gridData.gridDynamicType = GridData.GridDynamicType.NORMAL_GRID;
                    });
                    
                    
                    // TODO: handle
                    break;
                }
            case GridData.GridDynamicType.ICE_GRID:
                {
                    // TODO: handle
                    Ice.UpdateGraphic(this);
                    GamePlayController.gamePlayController.popsicleControll.HandlePopsicle();
                    break;
                }
            default:
                break;
        }
    }

    /// <summary>
    /// Xu ly tac dong sang cac vien ben canh sau khi an vien ben trong
    /// </summary>
	public void OnNearByTileCleared(Tile tileHandle, bool isSugar_C = false)
    {
        if (this.tile != null)
        {
			if (isSugar_C == true) 
			{
				tile.OnNearbyTileCleared (tileHandle, tileHandle.tileData.tileColor.ToString());
			} 
			else 
			{
				tile.OnNearbyTileCleared (tileHandle);
			}
        }
    }

    /// <summary>
    /// Called when a turn is finished, return true if caused changes
    /// </summary>
    public bool OnTurnEnded(ref float timeRef, bool isFinishTurn = true)
    {
        bool result = false;
        float timeToWork = 0;
        // GRid effect

        // Tile Effect
        if (tile != null)
        {
            float tileWorkTime = 0; // Store time tile need to process 
            if (tile.OnTurnEnded(ref tileWorkTime, isFinishTurn))
            {
                if (tileWorkTime > timeToWork)
                {
                    timeToWork = tileWorkTime;
                }

                result = true;
            }
        }

        if (timeToWork > timeRef)
        {
            timeRef = timeToWork;
        }

        return result;
    }
    #endregion

    #region Methods for Conveyor
    /// <summary>
    /// Set conveyor for grid
    /// </summary>
    /// <param name="conveyor"></param>
    public void SetConveyor(Conveyor conveyor)
    {
        try
        {
            if (conveyor != null)
            {
                this.conveyor = conveyor;
				this.conveyor.transform.SetParent(this.transform);

                this.conveyor.transform.localPosition = Vector3.zero;
                this.conveyor.transform.localScale = Vector3.one;

                this.conveyor.grid = this;
            }
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);
        }
    }

    /// <summary>
    /// Get conveyor type in grid
    /// </summary>
    /// <returns></returns>
    public Conveyor.ConveyorType GetConveyorType()
    {
        if (this.hasConveyor && this.conveyor != null)
        {
            // TOP_LEFT_CORNER
            if (this.GetGridInDir(Direction.BOTTOM) != null && this.GetGridInDir(Direction.BOTTOM).hasConveyor
                && this.GetGridInDir(Direction.RIGHT) != null && this.GetGridInDir(Direction.RIGHT).hasConveyor
                && ((this.conveyor.conveyorDirection == Conveyor.ConveyorDirection.DOWN 
                && this.GetGridInDir(Direction.RIGHT).conveyor.conveyorDirection == Conveyor.ConveyorDirection.LEFT)
                || (this.conveyor.conveyorDirection == Conveyor.ConveyorDirection.RIGHT
                && this.GetGridInDir(Direction.BOTTOM).conveyor.conveyorDirection == Conveyor.ConveyorDirection.UP)))
            {
                return Conveyor.ConveyorType.TOP_LEFT_CORNER; 
            }

            // TOP_RIGHT_CORNER
            if (this.GetGridInDir(Direction.BOTTOM) != null && this.GetGridInDir(Direction.BOTTOM).hasConveyor
                && this.GetGridInDir(Direction.LEFT) != null && this.GetGridInDir(Direction.LEFT).hasConveyor
                && ((this.conveyor.conveyorDirection == Conveyor.ConveyorDirection.LEFT
                && this.GetGridInDir(Direction.BOTTOM).conveyor.conveyorDirection == Conveyor.ConveyorDirection.UP)
                || (this.conveyor.conveyorDirection == Conveyor.ConveyorDirection.DOWN
                && this.GetGridInDir(Direction.LEFT).conveyor.conveyorDirection == Conveyor.ConveyorDirection.RIGHT)))
            {
                return Conveyor.ConveyorType.TOP_RIGHT_CORNER;
            }

            // BOTTOM_RIGHT_CORNER
            if (this.GetGridInDir(Direction.TOP) != null && this.GetGridInDir(Direction.TOP).hasConveyor
                && this.GetGridInDir(Direction.LEFT) != null && this.GetGridInDir(Direction.LEFT).hasConveyor
                && ((this.conveyor.conveyorDirection == Conveyor.ConveyorDirection.UP
                && this.GetGridInDir(Direction.LEFT).conveyor.conveyorDirection == Conveyor.ConveyorDirection.RIGHT)
                || (this.conveyor.conveyorDirection == Conveyor.ConveyorDirection.LEFT
                && this.GetGridInDir(Direction.TOP).conveyor.conveyorDirection == Conveyor.ConveyorDirection.DOWN)))
            {
                return Conveyor.ConveyorType.BOTTOM_RIGHT_CORNER;
            }

            // BOTTOM_LEFT_CORNER
            if (this.GetGridInDir(Direction.TOP) != null && this.GetGridInDir(Direction.TOP).hasConveyor
                && this.GetGridInDir(Direction.RIGHT) != null && this.GetGridInDir(Direction.RIGHT).hasConveyor
                && ((this.conveyor.conveyorDirection == Conveyor.ConveyorDirection.UP
                && this.GetGridInDir(Direction.RIGHT).conveyor.conveyorDirection == Conveyor.ConveyorDirection.LEFT)
                || (this.conveyor.conveyorDirection == Conveyor.ConveyorDirection.RIGHT
                && this.GetGridInDir(Direction.TOP).conveyor.conveyorDirection == Conveyor.ConveyorDirection.DOWN)))
            {
                return Conveyor.ConveyorType.BOTTOM_LEFT_CORNER;
            }

            return Conveyor.ConveyorType.LINE;
        }

        return Conveyor.ConveyorType.NONE;
    }
    #endregion

    #region Methods for Portal
    /// <summary>
    /// Set portal
    /// </summary>
    /// <param name="portal"></param>
    public void SetPortal(Portal portal)
    {
        try
        {
            if (portal != null)
            {
                this.portal = portal;
                this.portal.transform.SetParent(this.staticComponents);

                this.portal.transform.localPosition = Vector3.zero;
                this.portal.transform.localScale = Vector3.one;
            }
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);
        }
    }
    #endregion
	[HideInInspector]
	public Popsicle _popsicle;

    #region Methods for Popsicles
    /// <summary>
    /// Set transform for popsicle
    /// </summary>
    /// <param name="popsicle"></param>
    public void SetPopsicle(Popsicle popsicle)
    {
        try
        {
			if (popsicle != null)
            {
                popsicle.transform.SetParent(this.transform);

                popsicle.transform.localPosition = Vector3.zero;
                popsicle.transform.localScale = Vector3.one;

				_popsicle = popsicle;
            }
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);
        }
    }
    #endregion
}

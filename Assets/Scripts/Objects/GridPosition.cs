﻿/*
 * @Author: CuongNH
 * @Description: Lop dinh nghia vi tri cua cac grid, cell, item tren board
 * */

using UnityEngine;
using System.Collections;

[System.Serializable()]
public class GridPosition {
    //public int Row { get; set; }
    //public int Column { get; set; }

    [SerializeField]
    private int row;
    [SerializeField]
    private int column;

    #region Acccessors
    /// <summary>
    /// Define accessor for row
    /// </summary>
    public int Row
    {
        get
        {
            return row;
        }
        set
        {
            if (value >= 0)
            {
                row = value;
            }
            else
            {
                VGDebug.LogDebug("Value is not valid");
            }
        }
    }

    /// <summary>
    /// Define accessor for column
    /// </summary>
    public int Column
    {
        get
        {
            return column;
        }
        set
        {
            if (value >= 0)
            {
                column = value;
            }
            else
            {
                VGDebug.LogDebug("Value is not valid");
            }
        }
    }
    #endregion

    /// <summary>
    /// Contructor
    /// </summary>
    /// <param name="row">Toa do hang</param>
    /// <param name="column">Toa do cot</param>
    public GridPosition(int row = 0, int column = 0)
    {
        this.Row = row;
        this.Column = column;
    }
}

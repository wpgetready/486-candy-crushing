﻿/*
 * @Author: CuongNH
 * @Description: Mo ta du lieu cho cac Portal
 * */

using UnityEngine;
using System.Collections;

public class Portal : MonoBehaviour
{

    #region Properties, Data, Components
    /// <summary>
    /// Dinh nghia cac loai Portal
    /// </summary>
    public enum PortalType
    {
        INTLET,
        OUTLET
    }

    // Position of portals
    public static readonly Vector3 INLET_POSITION = new Vector3(0f, -0.36f, 0f);
    public static readonly Vector3 OUTLET_POSITION = new Vector3(0f, 0.36f, 0f);

    /// <summary>
    /// Type of Portal
    /// </summary>
    public PortalType portalType;

    /// <summary>
    /// Du lieu dung match giua inlet va outlet
    /// </summary>
    public int data;

    /// <summary>
    /// SpriteRenderer de hien hien thi portal
    /// </summary>
    public SpriteRenderer render;

    /// <summary>
    /// Portal duoc match voi Portal nay
    /// </summary>
    public Portal portalMatched;

    /// <summary>
    /// Grid chua portal
    /// </summary>
    public Grid grid;
    #endregion

    //// Use this for initialization
    //void Start () {
	
    //}
	
    //// Update is called once per frame
    //void Update () {

    //}

	void Start()
	{
		if (this.portalType == PortalType.INTLET) 
		{
			transform.localPosition = new Vector3 (transform.localPosition.x, transform.localPosition.y - 0.1f, transform.localPosition.z);
		} 
		else 
		{
			transform.localPosition = new Vector3 (transform.localPosition.x, transform.localPosition.y + 0.1f, transform.localPosition.z);
		}
	}

    #region Init, Set Properties, Data, Components
    /// <summary>
    /// Set properties for portal
    /// </summary>
    /// <param name="type"></param>
    /// <param name="match"></param>
    /// <param name="sprite"></param>
    public void SetProperties(PortalType type, int match, Sprite sprite)
    {
        SetPortalType(type);

        SetData(match);

        SetRender(sprite);
    }

    /// <summary>
    /// Set type for Portal
    /// </summary>
    /// <param name="type"></param>
    public void SetPortalType(PortalType type)
    {
        this.portalType = type;
        switch (this.portalType)
        {
            case PortalType.INTLET:
                {
                    SetPositionPortal(INLET_POSITION);
                    break;
                }
            case PortalType.OUTLET:
                {
                    SetPositionPortal(OUTLET_POSITION);
                    break;
                }
            default:
                break;
        }
    }

    /// <summary>
    /// Set data for Portal
    /// </summary>
    /// <param name="match"></param>
    public void SetData(int match)
    {
        this.data = match;
    }

    /// <summary>
    /// Set render for Portal
    /// </summary>
    /// <param name="sprite">Sprite to set</param>
    /// <param name="enable">Co cho hien thi hay khong</param>
    public void SetRender(Sprite sprite = null, bool enable = true)
    {
        this.render.GetComponent<Renderer>().enabled = enable;
        this.render.sprite = sprite;
    } 

    /// <summary>
    /// Set postion of render for Portal
    /// </summary>
    /// <param name="position"></param>
    public void SetPositionPortal(Vector3 position)
    {
        this.render.transform.localPosition = position;
    }

    /// <summary>
    /// Set portal matched cho portal hien tai
    /// </summary>
    /// <param name="portal">Portal can set la portal matched</param>
    public void SetPortalMatched(Portal portal)
    {
        if (portal != null && portal.portalType != this.portalType && portal.data == this.data)
        {
            this.portalMatched = portal;
        }
    }

    /// <summary>
    /// Set grid for portal
    /// </summary>
    /// <param name="grid"></param>
    public void SetGrid(Grid grid)
    {
        this.grid = grid;
    }
    #endregion
}

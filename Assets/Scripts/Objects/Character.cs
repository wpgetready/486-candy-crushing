﻿/*
 * @Author: CuongNH
 * @Description: Dac ta cac character va xu ly cac su kien lien quan den cac character
 * */

using UnityEngine;
using System.Collections;
using Spine.Unity;

public class Character : MonoBehaviour {

    /// <summary>
    /// Dinh nghia cac trang thai cho nhan vat
    /// </summary>
    public enum State
    {
        normal,
        eating,
        walking
    }

    /// <summary>
    /// SkeletonAnimation dung de chuyen cac trang thai cua nhan vat
    /// </summary>
    public SkeletonAnimation skeletonAnimation;

    /// <summary>
    /// Luu trang thai cua nhan vat
    /// </summary>
    public State state;

    //// Use this for initialization
    //void Start () {
	
    //}
	
    //// Update is called once per frame
    //void Update () {
	
    //}

    public void SetState(State _state)
    {
        state = _state;

        //TODO: su dung SkeletonAnimation chuyen trang thai cho nhan vat
        skeletonAnimation.AnimationName = state.ToString();
        //skeletonAnimation.Reset();
		skeletonAnimation.Awake();
    }
}

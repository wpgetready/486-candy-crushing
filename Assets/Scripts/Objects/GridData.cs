﻿/*
 * @Author: CuongNH
 * @Description: Dac ta cac du lieu cho Grid
 * */

using UnityEngine;
using System.Collections;

[System.Serializable()]
public class GridData
{

    #region Properties
    /// <summary>
    /// Dinh nghia cac loai Grid
    /// </summary>
    public enum GridType
    {
        NORMAL_GRID = 0,
        NULL_GRID = 1
    }

    /// <summary>
    /// Dinh nghia cac thanh phan tinh cua Grid
    /// </summary>
    public enum GridStaticType
    {
        NORMAL_GRID = 0,
        SPAWNER_GRID = 1,
        PORTAL_GRID = 2
//		RED_GRID = 3,
//		GREEN_GRID = 4,
//		BLUE_GRID = 5,
//		ORANGE_GRID = 6,
//		YELLOW_GRID = 7,
//		PURPLE_GRID = 8
        //CONVEYOR_GRID = 2,
    }

//	public enum GridColorType
//	{
//		NONE = 0,
//		RED_GRID = 1,
//		GREEN_GRID = 2,
//		BLUE_GRID = 3,
//		ORANGE_GRID = 4,
//		YELLOW_GRID = 5,
//		PURPLE_GRID = 6
//	}

	public enum GridColorType
	{
		RED_GRID = 4,
		GREEN_GRID = 1,
		BLUE_GRID = 0,
		ORANGE_GRID = 2,
		YELLOW_GRID = 5,
		PURPLE_GRID = 3,
		NONE = 6
	}

    /// <summary>
    /// Dinh nghia cac thanh phan dong cua Grid
    /// </summary>
    public enum GridDynamicType
    {
        NORMAL_GRID = 0,
        HONEY_GRID = 1,
        ICE_GRID = 2,
        CRACKER_GRID = 3
    }

    // Properties
    public GridType gridType;

    public GridStaticType gridStaticType;
    //public int gridModData;

	public GridColorType gridColorType;

    public GridDynamicType gridDynamicType;
    //public int gridPassiveModData;

    /// <summary>
    /// Data cua GridDynamic: co nhieu lop
    /// </summary>
    public int gridDynamicData = 0;
    #endregion

    #region Methods Constructor, Init, Set
    /// <summary>
    /// Constructor no params
    /// </summary>
    public GridData()
    {
        this.gridType = GridType.NORMAL_GRID;

        this.gridStaticType = GridStaticType.NORMAL_GRID;
        //this.gridModData = 0;

		this.gridColorType = GridColorType.NONE;

        this.gridDynamicType = GridDynamicType.NORMAL_GRID;
        //this.gridPassiveModData = 0;
    }

    /// <summary>
    /// Set Properties for GridData
    /// </summary>
    /// <param name="gtype"></param>
    /// <param name="gStaticType"></param>
    /// <param name="gDynamicType"></param>
    /// <param name="gridDynamicData"></param>
    public void SetGridData(GridType gtype = GridType.NORMAL_GRID, GridStaticType gStaticType = 
		GridStaticType.NORMAL_GRID, GridDynamicType gDynamicType = GridDynamicType.NORMAL_GRID, int gridDynamicData = -1, GridColorType gColor = GridColorType.NONE)
    {
        // Assign
        this.gridType = gtype;

        this.gridStaticType = gStaticType;
        //this.gridModData = gmoddata;

        this.gridDynamicType = gDynamicType;
        //this.gridPassiveModData = gimoddata;

        this.gridDynamicData = gridDynamicData;

		this.gridColorType = gColor;
    }
    #endregion
}

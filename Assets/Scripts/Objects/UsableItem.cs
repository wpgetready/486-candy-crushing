﻿/*
 * @Author: Nguyen Bui
 * @Modify: CuongNH
 * @Description: Define va xu ly cac item tro giup co the su dung trong gameplay
 * */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Spine.Unity;

public class UsableItem : MonoBehaviour {

    public enum ItemTrigger
    {
        AUTO = 0,
        TARGETED = 1,
        RANDOM_TARGETED = 2,
        AOE = 3
    }

    public enum ItemFillerType
    {
        INSTANT = 0, // Always full
        COOLDOWN = 1, // Refill by time
        KILL = 2, // Refill by clearing tile
    }

    public ItemTrigger trigger;
    public ItemFillerType fillerType;
    public float fillerRate = 1f; // Fill speed

    public LayerMask touchInputMask;
    public RaycastHit2D hit;

    public Transform itemSprite; // used for targeting
    public SkeletonAnimation skeleton;

    public Text txtNumber;

    public bool isSelected = false;
    public Grid selectedGrid;
    public Vector3 lastPosition;

    public int counter = 0; // -1 = Unlimited
    public float filler = 1f;

    /// <summary>
    /// Update display
    /// </summary>
    public void UpdateDisplay()
    {
        if (txtNumber != null)
        {
            txtNumber.text = counter.ToString();
        }
    }

    // SetFiller
    public void SetFiller(float val)
    {
        filler = val;
    }

    /// <summary>
    /// Check item is usable
    /// </summary>
    /// <returns></returns>
    public virtual bool IsUsable()
    {

        if (counter == 0)
        {
            return false;
        }

        //if (filler < 1f)
        //{
        //    return false;
        //}

        return true;
    }

    public virtual void OnFailedToUse()
    {
        
    }

    // Show tutorial combo
    public virtual void ShowTutorialCombo(Grid grid)
    {

    }

    /// <summary>
    /// Active item
    /// </summary>
    /// <param name="grid"></param>
    /// <returns></returns>
    public virtual bool Activate(Grid grid = null)
    {
        bool result = false;
        
        // if successfully casted
        if (result)
        {
            // update status
            if (counter > 0)
            {
                counter--;
            }

            if (fillerType != ItemFillerType.INSTANT)
            {
                filler = 0;
            }

            UpdateDisplay();

            GamePlayController.gamePlayController.mapController.HideHelp();
            GamePlayController.gamePlayController.helpDelay = GameConstants.TIME_DELAY_BEFORE_SHOWING_HELP;
        }

        return result;
    }

    /// <summary>
    /// Handle when touch on item
    /// </summary>
    public void OnTouch()
    {
        if (GameManager.gameState != GameManager.GameState.PLAYING || Contance.GAME_WIN
            || GamePlayController.gamePlayController.mapController.isProcessing 
            || GamePlayController.gamePlayController.gameInterceptorCount > 0) 
        {
            return;
        }

        if (!IsUsable())
        {
            OnFailedToUse();
            return;
        }

        isSelected = true;

        // Activate
        if (trigger == ItemTrigger.AUTO || trigger == ItemTrigger.RANDOM_TARGETED)
        {
            Activate();
            isSelected = false;
        }
        else if (trigger == ItemTrigger.TARGETED || trigger == ItemTrigger.AOE)
        {
            GamePlayController.gamePlayController.mapController.gameInputControl.DisposeInput();
        }
    }

    /// <summary>
    /// Handle when realease item
    /// </summary>
    public void OnRelease()
    {
        if (!isSelected)
        {
            return;
        }

        isSelected = false;

        if (trigger == ItemTrigger.TARGETED)
        {
            if (selectedGrid != null)
            {
                if (Activate(selectedGrid))
                {
                    // Activated

                    //
                }
                else
                {
                    MoveSpriteBackToRoot();
                }
            }
            else
            {
                MoveSpriteBackToRoot();
            }
        }
    }

    /// <summary>
    /// Active when on mouse down
    /// </summary>
    private void OnMouseDown()
    {
        //Debug.Log("OnMouseDown");
        OnTouch();
    }

    /// <summary>
    /// Active when on mouse up
    /// </summary>
    private void OnMouseUp()
    {
        //Debug.Log("OnMouseUp");
        OnRelease();
    }

    public virtual void MoveSpriteBackToRoot()
    {
        LeanTween.cancel(itemSprite.gameObject);
        LeanTween.moveLocal(itemSprite.gameObject, Vector3.zero, 0.2f);
    }

    public void Deselect()
    {
        isSelected = false;
        if (trigger == ItemTrigger.TARGETED || trigger == ItemTrigger.AOE)
        {
            MoveSpriteBackToRoot();
        }
    }
}

﻿/*
 * @Author: CuongNH
 * @Description: Mo ta du lieu cho cac que kem
 * */

using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class Popsicle : MonoBehaviour
{
    #region Properties, data, components
    /// <summary>
    /// Dinh nghia cac loai kem theo kich thuoc
    /// </summary>
    public enum PopsicleType
    {
        SET_1x1,
        SET_2x2,
        SET_3x3,
        SET_4x4,
        SET_1x2,
        SET_2x1,
        SET_2x4,
        SET_4x2
    }

	public ParticleSystem stars;

    #region Scales of popsicles
    public static readonly Vector3 SET_1x1_SCALE = new Vector3(1f, 1f, 1f);
    public static readonly Vector3 SET_2x2_SCALE = new Vector3(2f, 2f, 1f);
    public static readonly Vector3 SET_3x3_SCALE = new Vector3(3f, 3f, 1f);
    public static readonly Vector3 SET_4x4_SCALE = new Vector3(4f, 4f, 1f);
    public static readonly Vector3 SET_1x2_SCALE = new Vector3(1f, 1f, 1f);
    public static readonly Vector3 SET_2x1_SCALE = new Vector3(1f, 1f, 1f);
    public static readonly Vector3 SET_2x4_SCALE = new Vector3(2f, 2f, 1f);
    public static readonly Vector3 SET_4x2_SCALE = new Vector3(2f, 2f, 1f);
    #endregion

    #region Rotations of popsicles
    public static readonly Vector3 SET_1x1_ROTATION = new Vector3(0f, 0f, 0f);
    public static readonly Vector3 SET_2x2_ROTATION = new Vector3(0f, 0f, 0f);
    public static readonly Vector3 SET_3x3_ROTATION = new Vector3(0f, 0f, 0f);
    public static readonly Vector3 SET_4x4_ROTATION = new Vector3(0f, 0f, 0f);
    public static readonly Vector3 SET_1x2_ROTATION = new Vector3(0f, 0f, 0f);
    public static readonly Vector3 SET_2x1_ROTATION = new Vector3(0f, 0f, 270f);
    public static readonly Vector3 SET_2x4_ROTATION = new Vector3(0f, 0f, 0f);
    public static readonly Vector3 SET_4x2_ROTATION = new Vector3(0f, 0f, 270f);
    #endregion

    #region Postions of popsicles
    public static readonly Vector3 SET_1x1_POSITION = new Vector3(-0.5f, -0.5f, 0f);
    public static readonly Vector3 SET_2x2_POSITION = new Vector3(-0.5f, -0.5f, 0f);
    public static readonly Vector3 SET_3x3_POSITION = new Vector3(-0.5f, -0.5f, 0f);
    public static readonly Vector3 SET_4x4_POSITION = new Vector3(-0.5f, -0.5f, 0f);
    public static readonly Vector3 SET_1x2_POSITION = new Vector3(-0.5f, -0.5f, 0f);
    public static readonly Vector3 SET_2x1_POSITION = new Vector3(-0.5f, 0.5f, 0f);
    public static readonly Vector3 SET_2x4_POSITION = new Vector3(-0.5f, -0.5f, 0f);
    public static readonly Vector3 SET_4x2_POSITION = new Vector3(-0.5f, 1.5f, 0f);
    #endregion

    /// <summary>
    /// Loai kem
    /// </summary>
    public PopsicleType popsicleType;

    ///// <summary>
    ///// O dau tien ma que kem do che len
    ///// </summary>
    //public Grid fromGrid;

    ///// <summary>
    ///// O cuoi cung ma que kem do che len
    ///// </summary>
    //public Grid toGrid;

    /// <summary>
    /// Danh sach cac o ma que kem phu len
    /// </summary>
    public List<Grid> gridList;

    public bool collected = false;

    /// <summary>
    /// SpriteRenderer hien thi que kem
    /// </summary>
    public SpriteRenderer render;
    #endregion

    //// Use this for initialization
    //void Start () {
	
    //}
	
    //// Update is called once per frame
    //void Update () {

    //}

    #region Init, Set Properties, Data, Components
    public void SetProperties(PopsicleType type, Sprite sprite)
    {
        SetPopsicleType(type);

        SetRender(sprite);

        //this.fromGrid = start;
        //this.toGrid = end;

        switch (this.popsicleType)
        {
            case PopsicleType.SET_1x1:
            {
                SetScale(SET_1x1_SCALE);
                SetRotation(SET_1x1_ROTATION);
                SetPosition(SET_1x1_POSITION);
                break;
            }
            case PopsicleType.SET_2x2:
            {
                SetScale(SET_2x2_SCALE);
                SetRotation(SET_2x2_ROTATION);
                SetPosition(SET_2x2_POSITION);
                break;
            }
            case PopsicleType.SET_3x3:
            {
                SetScale(SET_3x3_SCALE);
                SetRotation(SET_3x3_ROTATION);
                SetPosition(SET_3x3_POSITION);
                break;
            }
            case PopsicleType.SET_4x4:
            {
                SetScale(SET_4x4_SCALE);
                SetRotation(SET_4x4_ROTATION);
                SetPosition(SET_4x4_POSITION);
                break;
            }
            case PopsicleType.SET_1x2:
            {
                SetScale(SET_1x2_SCALE);
                SetRotation(SET_1x2_ROTATION);
                SetPosition(SET_1x2_POSITION);
                break;
            }
            case PopsicleType.SET_2x1:
            {
                SetScale(SET_2x1_SCALE);
                SetRotation(SET_2x1_ROTATION);
                SetPosition(SET_2x1_POSITION);
                break;
            }
            case PopsicleType.SET_2x4:
            {
                SetScale(SET_2x4_SCALE);
                SetRotation(SET_2x4_ROTATION);
                SetPosition(SET_2x4_POSITION);
                break;
            }
            case PopsicleType.SET_4x2:
            {
                SetScale(SET_4x2_SCALE);
                SetRotation(SET_4x2_ROTATION);
                SetPosition(SET_4x2_POSITION);
                break;
            }
            default:
                break;
        }

        gridList = new List<Grid>();
    }

    /// <summary>
    /// Set type of Popsicle
    /// </summary>
    /// <param name="type">Type of Popsicle</param>
    public void SetPopsicleType(PopsicleType type)
    {
        this.popsicleType = type;
    }

    /// <summary>
    /// Set render for Popsicle
    /// </summary>
    /// <param name="sprite">Sprite to set</param>
    /// <param name="enable">Co cho hien thi hay khong</param>
    public void SetRender(Sprite sprite = null, bool enable = true)
    {
        this.render.GetComponent<Renderer>().enabled = enable;
        this.render.sprite = sprite;
    }

    /// <summary>
    /// Set scale cho Popsicle
    /// </summary>
    /// <param name="scale"></param>
    public void SetScale(Vector3 scale)
    {
        this.render.transform.localScale = scale;
    }

    /// <summary>
    /// Set rotation cho Popsicle
    /// </summary>
    /// <param name="angle"></param>
    public void SetRotation(Vector3 angle)
    {
        this.render.transform.localEulerAngles = angle;
    }

    /// <summary>
    /// Set local position cho popsicle
    /// </summary>
    /// <param name="position"></param>
    public void SetPosition(Vector3 position)
    {
        this.render.transform.localPosition = position;
    }

    /// <summary>
    /// Add grid to GridList
    /// </summary>
    /// <param name="grid"></param>
    public void AddGridToList(Grid grid)
    {
        this.gridList.Add(grid);
    }

    /// <summary>
    /// Remove grid to GridList
    /// </summary>
    /// <param name="grid"></param>
    public void RemveGridToList(Grid grid)
    {
        this.gridList.Remove(grid);
    }
    #endregion

    #region Methods handle events
    /// <summary>
    /// Check dieu xem que kem nay da du dieu kien collect chua
    /// </summary>
    public bool CheckCollect()
    {


        return false;
    }

    /// <summary>
    /// Thuc hien viec collect popsicle
    /// </summary>
    public void CollectPopsicle()
    {

    }
    #endregion
}

﻿/*
 * @Author: CuongNH
 * @Description: Mo ta va xu ly cac su kien cua item
 * */

using UnityEngine;
using System.Collections;

using System.Collections.Generic;

using Spine.Unity;

public class Tile : MonoBehaviour
{

    #region Properties
    /// <summary>
    /// Dinh nghia cac su kien di kem sau khi ket thuc qua trinh di chuyen
    /// </summary>
    public enum MoveFinishType
    {
        NO_ACTION,
        NO_ACTION_DIAGONAL,
        SWAP,
        BOUNCE,
        GROW
    }

    /// <summary>
    /// Is this tile has a color
    /// </summary>
    public bool isColored;
    /// <summary>
    /// Is this tile can be dropped down
    /// </summary>
    public bool isDroppable;
    /// <summary>
    /// Is this tile can be swapped
    /// </summary>
    public bool isSwappable;
    /// <summary>
    /// Is this tile can be eat by match 3 or match 2x2
    /// </summary>
    public bool isHit;
    /// <summary>
    /// Is this tile can be destroyed(cleared)
    /// </summary>
    public bool isDestroyable;
    /// <summary>
    /// Is this tile can be affected from nearby tile clearing
    /// </summary>
    public bool isSensitive;
    /// <summary>
    /// Is this tile can be special active by swap with another specialActivable tile
    /// </summary>
    public bool isSpecialActivable;
    /// <summary>
    /// True if tile is being swapped, dropped,... or in a state that do not accept manipulate 
    /// </summary>
    public bool isMoving;

    /// <summary>
    /// Check is controll by LeanTween
    /// </summary>
    public LTDescr moveAction;

    /// <summary>
    /// True if this tile is being cleared
    /// </summary>
    public bool isOnClearingProcess;
    /// <summary>
    /// True if this tile is on transform process
    /// </summary>
    public bool isTransforming;
    #endregion
	[HideInInspector]
	public Tile watermelon_Org;
	//[HideInInspector]
	public List<Tile> watermelon_I;
	//[HideInInspector]
	public sugarColor sugar_scr;
	public bool isSame;

    #region Data, components of Tile
    /// <summary>
    /// Hien thi chinh cua Tile
    /// </summary>
    public SpriteRenderer mainRender;

    /// <summary>
    /// Cac hien thi khac cua Tile: cac hieu ung, cac chi tiet dac biet khac, ...
    /// </summary>
    public SpriteRenderer enhanceRender;

    /// <summary>
    /// Chua du lieu cua Tile
    /// </summary>
    public TileData tileData;

    /// <summary>
    /// Chua grid ma tile duoc gan toi
    /// </summary>
    public Grid grid;

    /// <summary>
    /// Tile combined to create booster item
    /// </summary>
    public Tile combinedTarget;
    #endregion

    // Use this for initialization
    //void Start()
    //{

    //}

	public bool isRocket = false;

	public bool isMatch = false;

	[HideInInspector]
	public SkeletonAnimation _ft;


	[HideInInspector]
	public GameObject leavesEffect;

	[HideInInspector]
	public GameObject sugarsEffet;

    #region Set Data for Tile
    /// <summary>
    /// Set TileData with params is TileData
    /// </summary>
    /// <param name="tileData"></param>
    public void SetTileData(TileData tileData)
    {
        this.tileData = tileData;

        SetTileType(this.tileData.tileType);
    }

    /// <summary>
    /// Set TileData with params is P_Object
    /// </summary>
    /// <param name="pObject"></param>
    public void SetTileData(P_Object pObject)
    {
        //try
        //{
            SetTileType(pObject.type_Object);

            SetTileModifierType(pObject.modifierType_Object);

            SetTileColor(pObject.color_Object);

            SetData(pObject.data_Object);
        //}
        //catch (System.Exception e)
        //{
        //    VGDebug.LogError(e.Message);
        //}
    }

    /// <summary>
    /// Reset properties for tile
    /// </summary>
    public void ResetProperties()
    {
        this.isColored = false;
        this.isHit = false;
        this.isSpecialActivable = false;
        this.isSwappable = false;
        this.isDroppable = false;
        this.isDestroyable = false;
        this.isSensitive = false;

        this.isMoving = false;
        this.isTransforming = false;
        this.isOnClearingProcess = false;

        this.combinedTarget = null;
        this.moveAction = null;
    }

    /// <summary>
    /// Set type for tile with string
    /// </summary>
    /// <param name="tileType"></param>
    public void SetTileType(string tileType)
    {
        ResetProperties();
        try
        {
            if (!string.IsNullOrEmpty(tileType))
            {
                tileType = tileType.Trim();
                switch (tileType)
                {
                    case GameConstants.empty_tile_type:
                        {
                            this.tileData.tileType = TileData.TileType.empty;
                            break;
                        }
                    case GameConstants.invisible_brick_tile_type:
                        {
                            this.tileData.tileType = TileData.TileType.invisible_brick;
                            break;
                        }
                    case GameConstants.match_object_tile_type:
                        {
                            this.tileData.tileType = TileData.TileType.match_object;
                            this.isColored = true;
                            this.isHit = true;
                            this.isSwappable = true;
                            this.isDroppable = true;
                            this.isDestroyable = true;
                            break;
                        }
                    case GameConstants.sugar_tile_type:
                        {
                            this.tileData.tileType = TileData.TileType.sugar;
                            this.isDestroyable = true;
                            this.isSensitive = true;

						GamePlayController.gamePlayController.SugarEff(this);

                            break;
                        }
                    case GameConstants.bee_tile_type:
                        {
                            this.tileData.tileType = TileData.TileType.bee;
                            this.isHit = true;
                            this.isSwappable = true;
                            this.isDroppable = true;
                            this.isDestroyable = true;
                            break;
                        }
                    case GameConstants.ice_cube_tile_type:
                        {
                            this.tileData.tileType = TileData.TileType.ice_cube;
                            this.isSwappable = true;
                            this.isDroppable = true;
                            this.isDestroyable = true;
                            this.isSensitive = true;
                            break;
                        }
                    case GameConstants.muffin_tile_type:
                        {
                            this.tileData.tileType = TileData.TileType.muffin;
                            this.isSwappable = true;
                            this.isDroppable = true;
                            this.isDestroyable = true;
                            this.isSensitive = true;
                            break;
                        }
                    case GameConstants.muffins_tile_type:
                        {
                            this.tileData.tileType = TileData.TileType.muffin;
                            this.isSwappable = true;
                            this.isDroppable = true;
                            this.isDestroyable = true;
                            this.isSensitive = true;
                            break;
                        }
                    case GameConstants.muff_tile_type:
                        {
                            this.tileData.tileType = TileData.TileType.muffin;
                            this.isSwappable = true;
                            this.isDroppable = true;
                            this.isDestroyable = true;
                            this.isSensitive = true;
                            break;
                        }
                    case GameConstants.muffs_tile_type:
                        {
                            this.tileData.tileType = TileData.TileType.muffin;
                            this.isSwappable = true;
                            this.isDroppable = true;
                            this.isDestroyable = true;
                            this.isSensitive = true;
                            break;
                        }
                    case GameConstants.cow_tile_type:
                        {
                            this.tileData.tileType = TileData.TileType.cow;
                            this.isSensitive = true;
                            break;
                        }
                    case GameConstants.fountain_tile_type:
                        {
                            this.tileData.tileType = TileData.TileType.fountain;
                            this.isColored = true;
                            this.isSensitive = true;
                            break;
                        }
                    case GameConstants.coconut_1_tile_type:
                        {
                            this.tileData.tileType = TileData.TileType.coconut_1;
                            this.isHit = true;
                            this.isSwappable = true;
                            this.isDroppable = true;
                            this.isDestroyable = true;
                            break;
                        }
                    case GameConstants.coconut_2_tile_type:
                        {
                            this.tileData.tileType = TileData.TileType.coconut_2;
                            this.isHit = true;
                            this.isSwappable = true;
                            this.isDroppable = true;
                            this.isDestroyable = true;
                            break;
                        }
                    case GameConstants.yogurt_cup_tile_type:
                        {
                            this.tileData.tileType = TileData.TileType.yogurt_cup;
                            this.isSensitive = true;
                            break;
                        }
                    case GameConstants.yogurt_tile_type:
                        {
                            this.tileData.tileType = TileData.TileType.yogurt;
                            this.isDestroyable = true;
                            this.isSensitive = true;
                            break;
                        }
                    case GameConstants.row_breaker_tile_type:
                        {
                            this.tileData.tileType = TileData.TileType.row_breaker;
                            this.isColored = true;
                            this.isHit = true;
                            this.isSpecialActivable = true;
                            this.isSwappable = true;
                            this.isDroppable = true;
                            this.isDestroyable = true;
                            break;
                        }
                    case GameConstants.column_breaker_tile_type:
                        {
                            this.tileData.tileType = TileData.TileType.column_breaker;
                            this.isColored = true;
                            this.isHit = true;
                            this.isSpecialActivable = true;
                            this.isSwappable = true;
                            this.isDroppable = true;
                            this.isDestroyable = true;
                            break;
                        }
                    case GameConstants.bomb_breaker_tile_type:
                        {
                            this.tileData.tileType = TileData.TileType.bomb_breaker;
                            this.isColored = true;
                            this.isHit = true;
                            this.isSpecialActivable = true;
                            this.isSwappable = true;
                            this.isDroppable = true;
                            this.isDestroyable = true;
                            break;
                        }
                    case GameConstants.x_breaker_tile_type:
                        {
                            this.tileData.tileType = TileData.TileType.x_breaker;
                            this.isColored = true;
                            this.isHit = true;
                            this.isSpecialActivable = true;
                            this.isSwappable = true;
                            this.isDroppable = true;
                            this.isDestroyable = true;
                            break;
                        }
                    case GameConstants.rainbow_tile_type:
                        {
                            this.tileData.tileType = TileData.TileType.rainbow;
                            this.isSpecialActivable = true;
                            this.isSwappable = true;
                            this.isDroppable = true;
                            this.isDestroyable = true;
                            break;
                        }
                    case GameConstants.magnet_tile_type:
                        {
                            this.tileData.tileType = TileData.TileType.magnet;
                            this.isSpecialActivable = true;
                            this.isSwappable = true;
                            this.isDroppable = true;
                            this.isDestroyable = true;
                            break;
                        }
                    default:
                        break;
                }
            }
        }
        catch (System.Exception e)
        {
            VGDebug.LogError(e.Message);
        }
    }

    /// <summary>
    /// Set type for tile with TileData.TileType
    /// </summary>
    /// <param name="tileType"></param>
    public void SetTileType(TileData.TileType tileType)
    {
        this.tileData.tileType = tileType;

        ResetProperties();
        switch (this.tileData.tileType)
        {
            case TileData.TileType.empty:
                {
                    this.tileData.tileType = TileData.TileType.empty;
                    break;
                }
            case TileData.TileType.invisible_brick:
                {
                    this.tileData.tileType = TileData.TileType.invisible_brick;
                    break;
                }
            case TileData.TileType.match_object:
                {
                    this.isColored = true;
                    this.isHit = true;
                    this.isSwappable = true;
                    this.isDroppable = true;
                    this.isDestroyable = true;
                    break;
                }
            case TileData.TileType.sugar:
                {
                    this.isDestroyable = true;
                    this.isSensitive = true;
                    break;
                }
            case TileData.TileType.bee:
                {
                    this.isHit = true;
                    this.isSwappable = true;
                    this.isDroppable = true;
                    this.isDestroyable = true;
                    break;
                }
            case TileData.TileType.ice_cube:
                {
                    this.isSwappable = true;
                    this.isDroppable = true;
                    this.isDestroyable = true;
                    this.isSensitive = true;
                    break;
                }
            case TileData.TileType.muffin:
                {
                    this.isSwappable = true;
                    this.isDroppable = true;
                    this.isDestroyable = true;
                    this.isSensitive = true;
                    break;
                }
            case TileData.TileType.cow:
                {
                    this.isSensitive = true;
                    break;
                }
            case TileData.TileType.fountain:
                {
                    this.isColored = true;
                    this.isSensitive = true;
                    break;
                }
            case TileData.TileType.coconut_1:
                {
                    this.isHit = true;
                    this.isSwappable = true;
                    this.isDroppable = true;
                    this.isDestroyable = true;
                    break;
                }
            case TileData.TileType.coconut_2:
                {
                    this.isHit = true;
                    this.isSwappable = true;
                    this.isDroppable = true;
                    this.isDestroyable = true;
                    break;
                }
            case TileData.TileType.yogurt_cup:
                {
                    this.isSensitive = true;
                    break;
                }
            case TileData.TileType.yogurt:
                {
                    this.isDestroyable = true;
                    this.isSensitive = true;
                    break;
                }
            case TileData.TileType.row_breaker:
                {
                    this.isColored = true;
                    this.isHit = true;
                    this.isSpecialActivable = true;
                    this.isSwappable = true;
                    this.isDroppable = true;
                    this.isDestroyable = true;
                    break;
                }
            case TileData.TileType.column_breaker:
                {
                    this.isColored = true;
                    this.isHit = true;
                    this.isSpecialActivable = true;
                    this.isSwappable = true;
                    this.isDroppable = true;
                    this.isDestroyable = true;
                    break;
                }
            case TileData.TileType.bomb_breaker:
                {
                    this.isColored = true;
                    this.isHit = true;
                    this.isSpecialActivable = true;
                    this.isSwappable = true;
                    this.isDroppable = true;
                    this.isDestroyable = true;
                    break;
                }
            case TileData.TileType.x_breaker:
                {
                    this.isColored = true;
                    this.isHit = true;
                    this.isSpecialActivable = true;
                    this.isSwappable = true;
                    this.isDroppable = true;
                    this.isDestroyable = true;
                    break;
                }
            case TileData.TileType.rainbow:
                {
                    this.isSpecialActivable = true;
                    this.isSwappable = true;
                    this.isDroppable = true;
                    this.isDestroyable = true;
                    break;
                }
            case TileData.TileType.magnet:
                {
                    this.isSpecialActivable = true;
                    this.isSwappable = true;
                    this.isDroppable = true;
                    this.isDestroyable = true;

                    break;
                }
            case TileData.TileType.bomb_rainbow:
            case TileData.TileType.x_rainbow:
                {
                    this.isHit = true;
                    this.isSwappable = true;
                    this.isDroppable = true;
                    this.isDestroyable = true;
                    this.isSpecialActivable = true;
                    break;
                }
            default:
                break;
        }
    }

    /// <summary>
    /// Set modifier type for tile
    /// </summary>
    /// <param name="modifierType"></param>
    public void SetTileModifierType(string modifierType)
    {
        try
        {
            if (!string.IsNullOrEmpty(modifierType))
            {
                modifierType = modifierType.Trim();

                switch (modifierType)
                {
                    case GameConstants.ice_cage_tile_modifier_type:
                        {
                            this.tileData.tileModifierType = TileData.TileModifierType.ice_cage;
                            this.isHit = true;
                            this.isSwappable = false;
                            this.isDroppable = false;
                            break;
                        }
				case GameConstants.watermelon_O_tile_modifier_type:
						{
						this.tileData.tileModifierType = TileData.TileModifierType.watermelon_O;
							this.isHit = true;
							this.isSwappable = false;
							this.isDroppable = false;
							break;
						}
				case GameConstants.watermelon_I_tile_modifier_type:
					{
						this.tileData.tileModifierType = TileData.TileModifierType.watermelon_I;
						this.isHit = true;
						this.isSwappable = false;
						this.isDroppable = false;
						break;
					}
				case GameConstants.sugar_color_tile_modifier_type:
					{
						this.tileData.tileModifierType = TileData.TileModifierType.color;
						this.isHit = true;
						this.isSwappable = false;
						this.isDroppable = false;
						break;
					}
                    default:
                        break;
                }
            }
        }
        catch (System.Exception e)
        {
            VGDebug.LogError(e.Message);
        }
    }

    /// <summary>
    /// Set TileColor for tile
    /// </summary>
    /// <param name="tileColor"></param>
    public void SetTileColor(string tileColor)
    {
        try
        {
            if (!string.IsNullOrEmpty(tileColor))
            {
                tileColor = tileColor.Trim();

                switch (tileColor)
                {
                    case GameConstants.blue_tile_color:
                        {
                            this.tileData.tileColor = TileData.TileColor.blue;
                            break;
                        }
                    case GameConstants.green_tile_color:
                        {
                            this.tileData.tileColor = TileData.TileColor.green;
                            break;
                        }
                    case GameConstants.orange_tile_color:
                        {
                            this.tileData.tileColor = TileData.TileColor.orange;
                            break;
                        }
                    case GameConstants.purple_tile_color:
                        {
                            this.tileData.tileColor = TileData.TileColor.purple;
                            break;
                        }
                    case GameConstants.red_tile_color:
                        {
                            this.tileData.tileColor = TileData.TileColor.red;
                            break;
                        }
                    case GameConstants.yellow_tile_color:
                        {
                            this.tileData.tileColor = TileData.TileColor.yellow;
                            break;
                        }
					case "honey":
						{
							this.tileData.tileColor = TileData.TileColor.honey;
							break;
						}
                    default:
                        {
                            //this.tileData.tileColor = TileData.TileColor.none;
                            break;
                        }
                }
            }
        }
        catch (System.Exception e)
        {
            VGDebug.LogError(e.Message);
        }
    }

    /// <summary>
    /// Set data for tile
    /// </summary>
    /// <param name="data"></param>
    public void SetData(int data)
    {
        this.tileData.data = data;
    }

    /// <summary>
    /// Set properties for tile
    /// </summary>
    /// <param name="tileType"></param>
    /// <param name="tileColor"></param>
    /// <param name="data"></param>
   
	public void ExtraCount()
	{
		switch (this.tileData.tileColor) 
		{
		case TileData.TileColor.blue:
			GamePlayController.gamePlayController.effectControll.loadModeGame.CheckHaveOrder (GameConstants.blue_tile_color, this);
			break;
		case TileData.TileColor.green:
			GamePlayController.gamePlayController.effectControll.loadModeGame.CheckHaveOrder (GameConstants.green_tile_color, this);
			break;
		case TileData.TileColor.orange:
			GamePlayController.gamePlayController.effectControll.loadModeGame.CheckHaveOrder (GameConstants.orange_tile_color, this);
			break;
		case TileData.TileColor.purple:
			GamePlayController.gamePlayController.effectControll.loadModeGame.CheckHaveOrder (GameConstants.purple_tile_color, this);
			break;
		case TileData.TileColor.red:
			GamePlayController.gamePlayController.effectControll.loadModeGame.CheckHaveOrder (GameConstants.red_tile_color, this);
			break;
		case TileData.TileColor.yellow:
			GamePlayController.gamePlayController.effectControll.loadModeGame.CheckHaveOrder (GameConstants.yellow_tile_color, this);
			break;
		}
	}

	public void SetProperties(TileData.TileType tileType, TileData.TileColor tileColor, int data = 0)
	{
		if (isRocket == false) 
		{
			ExtraCount ();
		}
        //this.tileData.tileType = tileType;
        SetTileType(tileType);
        this.tileData.tileColor = tileColor;
        this.tileData.data = data;

        // Update Display
		UpdateTileDisplay();

    }
    #endregion

    #region Methods upgrade, transform tile
    public void TransformInto(TileData newData)
    {
        this.tileData = newData;
        float transformTime = 0;

        // Separate calls
        switch (tileData.tileType)
        {
            case TileData.TileType.match_object:
                {
                    transformTime = MatchObject.TransformProcess(this);
                    break;
                }
            case TileData.TileType.row_breaker:
                {
                    transformTime = RowBreaker.TransformProcess(this);
                    break;
                }
            case TileData.TileType.column_breaker:
                {
                    transformTime = ColumnBreaker.TransformProcess(this);
                    break;
                }
            case TileData.TileType.bomb_breaker:
                {
                    transformTime = BombBreaker.TransformProcess(this);
                    break;
                }
            case TileData.TileType.x_breaker:
                {
                    transformTime = XBreaker.TransformProcess(this);
                    break;
                }
            case TileData.TileType.rainbow:
                {
                    transformTime = Rainbow.TransformProcess(this);
                    break;
                }
            case TileData.TileType.magnet:
                {
                    transformTime = Magnet.TransformProcess(this);
                    break;
                }
            case TileData.TileType.bomb_rainbow:
                {
                    transformTime = BombRainbow.TransformProcess(this);
                    break;
                }
            case TileData.TileType.x_rainbow:
                {
                    transformTime = XRainbow.TransformProcess(this);
                    break;
                }
            case TileData.TileType.coconut_1:
                {
                    transformTime = Coconut1.TransformProcess(this);
                    break;
                }
            case TileData.TileType.coconut_2:
                {
                    transformTime = Coconut2.TransformProcess(this);
                    break;
                }
            case TileData.TileType.yogurt:
                {
                    transformTime = Yogurt.TransformProcess(this);
                    break;
                }
            default:
                break;
        }

        if (transformTime > 0)
        {
            this.isTransforming = true;
            LeanTween.delayedCall(this.gameObject, transformTime, () =>
            {
                // Set properties
				SoundController.sound.EatSoundPlay ();
                this.SetProperties(tileData.tileType, tileData.tileColor, tileData.data);
                this.isTransforming = false;
            });
        }
    }

    /// <summary>
    /// Handle upgrade after valid swap
    /// </summary>
    public void OnUpgrade()
    {
        switch (tileData.tileType)
        {
            case TileData.TileType.coconut_1:
                {
                    Coconut1.OnUpgrade(this);
                    break;
                }
            default:
                break;
        }

        switch (tileData.tileModifierType)
        {
            case TileData.TileModifierType.ice_cage:
                {
                    IceCage.OnUpgrade(this);
                    break;
                }
            default:
                break;
        }
    }

    /// <summary>
    /// Called when a turn is finished, return true if caused changes
    /// </summary>
    public bool OnTurnEnded(ref float timeRef, bool isFinishTurn = true)
    {
        bool result = false;
        float timeToWork = 0;

        switch (this.tileData.tileType)
        {
            case TileData.TileType.yogurt:
            case TileData.TileType.yogurt_cup:
                {
                    if (isFinishTurn)
                    {
                        timeToWork = Yogurt.OnTurnEnded(this);
                        result = true;
                    }
                    break;
                }
            default:
                break;
        }

        timeRef = timeToWork;
        return result;
    }

    /// <summary>
    /// Called when a tile being affected, return time it take to be cleared
    /// </summary>
	public bool Damage(string _color = null)
    {
        // Damage
        if (isDestroyable)
        {
            this.tileData.data--;
            //Ice.UpdateGraphic(this.grid);

			GameObject obj = GamePlayController.gamePlayController.mapController.tileSpawner.redeffect;

            // Post-Damaged
			if ((this.isDestroyable && this.tileData.data <= 0 && this.tileData.tileModifierType != TileData.TileModifierType.color))
            {
//				if(this.tileData.tileModifierType == TileData.TileModifierType.watermelon_O || this.tileData.tileModifierType == TileData.TileModifierType.watermelon_I)
//				{
//					GameObject red = Instantiate(GamePlayController.gamePlayController.mapController.tileSpawner.redeffect, this.watermelon_Org.mainRender.transform.position, Quaternion.identity) as GameObject;
//					Destroy(red,0.45f);
//				}

				if(this.tileData.tileType == TileData.TileType.sugar && this.tileData.tileModifierType == TileData.TileModifierType.watermelon_I)
				{
					
					if(this.watermelon_Org == null)
					{
						Sugar.SyncDataFromI (this);
					}

					if(this.watermelon_Org != null && this.watermelon_Org.watermelon_I.Count > 0)
					{
						foreach (Tile item in this.watermelon_Org.watermelon_I) 
						{
							if(item != this)
							{
								item.Clear ();
							}
						}

						GameObject red = Instantiate(obj, this.watermelon_Org.mainRender.transform.position, Quaternion.identity) as GameObject;
						Destroy(red,0.45f);
						this.watermelon_Org.Clear ();
						//return true;
					}
				}

				if(this.tileData.tileType == TileData.TileType.sugar && this.tileData.tileModifierType == TileData.TileModifierType.watermelon_O)
				{
					if (this.watermelon_I.Count == 0) 
					{
						Sugar.SyncDataFromO (this);
					}

					if (this.watermelon_I.Count > 0) {
						foreach (Tile _item in this.watermelon_I) {
							_item.Clear ();
						}
					}
					GameObject red = Instantiate(obj, this.mainRender.transform.position, Quaternion.identity) as GameObject;
					Destroy(red,0.45f);
					//return true;
						//this.mainRender.transform.localPosition = Vector3.zero;
						//this.mainRender.transform.localScale = Vector3.one;
				}



                this.Clear();

//				if(this.tileData.tileModifierType == TileData.TileModifierType.color && GamePlayController.gamePlayController.mapController.tileSpawner.isProcessMagicBoom == false)
//				{
//					GamePlayController.gamePlayController.mapController.tileSpawner.isProcessMagicBoom = true;
//					GamePlayController.gamePlayController.mapController.tileSpawner.MagicBomb ();
//				}

                return true;
            }
            else
            {
				UpdateTileDisplay(_color);
            }
        }
        else
        {
            switch (this.tileData.tileType)
            {
                case TileData.TileType.fountain:
                    {
						//GamePlayController.gamePlayController.fountain.OnUpgrade (this);
                        Fountain.OnUpgrade(this);
                        break;
                    }
                case TileData.TileType.cow:
                    {
                        //GamePlayController.gamePlayController.effectControll.CreateEffectTileType(this);

					Cow.Activate(this.grid, Cow.ANIMATION_NAME);
					SoundController.sound.CowSound ();
					Invoke ("CowIdle", 1);
                        break;
                    }
                case TileData.TileType.yogurt_cup:
                    {
                        Yogurt.OnClear(this);
                        break;
                    }
                default:
                    break;
            }
        }
        
        return false;
    }

	void CowIdle()
	{
		Cow.Activate(this.grid, Cow.ANIMATION_NAME_Idle);
	}

    /// <summary>
    /// Called when a tile nearby being cleared
    /// </summary>
	public void OnNearbyTileCleared(Tile tileHandle, string _color = null)
    {
        if (this.isSensitive && !this.isTransforming)
        {
            switch(this.tileData.tileType)
            {
                case TileData.TileType.ice_cube:
                    Ice.UpdateGraphic(this.grid);
                    LeanTween.scale(this.gameObject, new Vector3(0.5f,0.5f,0.5f), 0.1f);
                    LeanTween.delayedCall(this.gameObject, 0.1f, () =>
                    {
                        LeanTween.scale(this.gameObject, new Vector3(1, 1, 1), 0.1f);
                    });
                    //GamePlayController.gamePlayController.effectControll.CreateEffectTileType(this);
                    if(tileHandle.isSpecialActivable)
                    {
                        this.Damage();
                    }
                    else if(tileHandle.tileData.tileType == TileData.TileType.match_object)
					{
                        TransformInto(tileHandle.tileData);
                    }
                    break;


                default:
					this.Damage(_color);
                    break;
            }
            
        }
    }

	public void OnClear()
	{
		this.Clear ();
	}

    /// <summary>
    /// Called when a tile nees to be cleared, in a delayTime
    /// </summary>
    void Clear()
    {
		bool isCoconut_2 = false;
        if (isOnClearingProcess)
        {
            return;
        }
		//Debug.Log ("OnClear");

        // Waiting for animation finish
        isOnClearingProcess = true;

        // Give point
        //GameplayController.instance.IncreasePoint(this.GetValue(), this.transform.position, true);

        // TODO:
        //GamePlayController.gamePlayController.mapController.tileManager.OnTileCleared(this);

        float delayTime = GameConstants.TIME_BREAK_ANIMATION;

        if (combinedTarget == null)
        {
			//Debug.Log("non combine");
            // Effect
            switch (tileData.tileType)
            {
                case TileData.TileType.match_object:
                    {
                        delayTime = MatchObject.OnClear(this);

                        break;
                    }
                case TileData.TileType.column_breaker:
                    {
                        delayTime = ColumnBreaker.OnClear(this);

                        break;
                    }
                case TileData.TileType.row_breaker:
                    {
                        delayTime = RowBreaker.OnClear(this);

                        break;
                    }
                case TileData.TileType.bomb_breaker:
                    {
                        delayTime = BombBreaker.OnClear(this);

                        break;
                    }
                case TileData.TileType.sugar:
                    {
                        delayTime = GameConstants.TIME_CLEAR_TILE;
						GamePlayController.gamePlayController.showSugarEff(this.transform.position, this);
                        Sugar.HandleEffectYollowData(this);
                        break;
                    }
                case TileData.TileType.muffin:
                    {
                        delayTime = GameConstants.TIME_CLEAR_TILE;
                        Muffin.HandleEffectYollowData(this);
                        break;
                    }

                case TileData.TileType.bee:
                    {
                        delayTime = GameConstants.TIME_CLEAR_TILE;
                        LeanTween.scale(this.gameObject, Vector3.zero, delayTime);
                        break;
                    }
                case TileData.TileType.rainbow:
                    {
                        delayTime = Rainbow.OnClear(this);

                        break;
                    }
                case TileData.TileType.magnet:
                    {
                        delayTime = Magnet.OnClear(this);

                        break;
                    }
                case TileData.TileType.bomb_rainbow:
                    {
                        delayTime = BombRainbow.OnClear(this);

                        break;
                    }
                case TileData.TileType.x_rainbow:
                    {
                        delayTime = XRainbow.OnClear(this);

                        break;
                    }
                case TileData.TileType.coconut_1:
                    {
						tileData.tileType = TileData.TileType.coconut_2;
                        //delayTime = Coconut1.OnClear(this);
						UpdateTileDisplay();
						//Debug.Log ("coconut_1");
						isCoconut_2 = true;
						isOnClearingProcess = false;
						tileData.data = 0;
                        break;
                    }
                case TileData.TileType.coconut_2:
                    {
                        delayTime = Coconut2.OnClear(this);
						
						//tileData.tileType = TileData.TileType.coconut_1;
						//Debug.Log ("coconut_2");
                        break;
                    }
                case TileData.TileType.yogurt:
                    {
                        delayTime = Yogurt.OnClear(this);

                        break;
                    }
                default:
                    {
                        LeanTween.scale(this.gameObject, Vector3.zero, delayTime);
                        break;
                    }
            }
        }
        else
        {
			//SoundController.sound.EatSoundPlay ();
            //delayTime = MatchObject.OnClear(this);
            delayTime = GameConstants.TIME_COMBINED_TILE;
            GamePlayController.gamePlayController.effectControll.CreateEffectTileColor(this);
            LeanTween.move(this.gameObject, combinedTarget.transform.position, delayTime);

			//Debug.Log ("combine");
        }

        LeanTween.delayedCall(this.gameObject, delayTime, () =>
        {
			if(isCoconut_2 == false)
			{
				//Debug.Log ("isCoconut_2 == false");
            	isOnClearingProcess = false;
            	// Actually remove

	            Grid g = this.grid;
	            if (g != null)
	            {
	                g.SetTile(null);
	            }

	            // set dynamic khi an cac type dac biet
	            switch (tileData.tileType)
	            {
	                case TileData.TileType.bee:
	                    {
	                        if (GameManager.gameMode == GameManager.GameMode.ORDER_FULFILLMENT ||
	                            GameManager.gameMode == GameManager.GameMode.BOSS_BATTLE)
	                        {
	                            grid.gridData.gridDynamicType = GridData.GridDynamicType.HONEY_GRID;
	                            GamePlayController.gamePlayController.mapController.gridSpawner.SetGridDynamicRender(this.grid);
							//grid.transform.localEulerAngles = new Vector3(0,0,0);
	                        }
	                        break;
	                    }
	                default:
	                    break;
	            }

            	this.grid = null;

            	GamePlayController.gamePlayController.OnTileCleared(this.transform.position);

            	// Return to pool
            	LeanTween.cancel(this.gameObject);
            	PoolingManager.poolingManager.AddTileToPool(this);
		}
        });
		GamePlayController.gamePlayController.mapController.countStar.CountStar (15);
    }

    /// <summary>
    /// Update display for tile
    /// </summary>
	public void UpdateTileDisplay(string _color = null)
    {
        // set properties
        switch (tileData.tileType)
        {
            case TileData.TileType.match_object:
                {
                    MatchObject.UpdateGraphic(this);
                    break;
                }
            case TileData.TileType.row_breaker:
                {
                    RowBreaker.UpdateGraphic(this);
                    break;
                }
            case TileData.TileType.column_breaker:
                {
                    ColumnBreaker.UpdateGraphic(this);
                    break;
                }
            case TileData.TileType.bomb_breaker:
                {
                    BombBreaker.UpdateGraphic(this);
                    break;
                }
            case TileData.TileType.x_breaker:
                {
                    XBreaker.UpdateGraphic(this);
                    break;
                }
            case TileData.TileType.rainbow:
                {
                    Rainbow.UpdateGraphic(this);
                    break;
                }
            case TileData.TileType.magnet:
                {
                    Magnet.UpdateGraphic(this);
                    break;
                }
            case TileData.TileType.sugar:
                {
					Sugar.UpdateGraphic(this, _color);
                    break;
                }
            case TileData.TileType.muffin:
                {
                    Muffin.UpdateGraphic(this);
                    break;
                }
            case TileData.TileType.cow:
                {
                    //GamePlayController.gamePlayController.effectControll.CreateEffectTileType(this);
                    break;
                }
            case TileData.TileType.bomb_rainbow:
                {
                    BombRainbow.UpdateGraphic(this);
                    break;
                }
            case TileData.TileType.x_rainbow:
                {
                    XRainbow.UpdateGraphic(this);
                    break;
                }
            case TileData.TileType.coconut_1:
                {
                    Coconut1.UpdateGraphic(this);
                    break;
                }
            case TileData.TileType.coconut_2:
                {
                    Coconut2.UpdateGraphic(this);
                    break;
                }
            case TileData.TileType.yogurt:
                {
                    Yogurt.UpdateGraphic(this);
                    break;
                }
            case TileData.TileType.bee:
                {
                    Bee.UpdateGraphic(this);
                    break;
                }
            case TileData.TileType.ice_cube:
                {
                    IceCube.UpdateGraphic(this);
                    break;
                }
            default:
                {
                    MatchObject.UpdateGraphic(this);
                    break;
                }
        }
    }

    /// <summary>
    /// Set active tile
    /// </summary>
    /// <param name="active"></param>
    public void SetTileActive(bool enableb = true)
    {
        this.mainRender.enabled = enableb;
        this.enhanceRender.enabled = enableb;
    }

    /// <summary>
    /// Set enable for sprite render
    /// </summary>
    /// <param name="render"></param>
    /// <param name="enableb"></param>
    public void SetEnableSpriteRender(SpriteRenderer render, bool enableb = true)
    {
        if (render != null)
        {
            render.enabled = enableb;
        }
    }
    #endregion

    #region Set Render for components of Tile
    /// <summary>
    /// Set main render by Sprite
    /// </summary>
    /// <param name="sprite"></param>
    public void SetMainRender(Sprite sprite)
    {
        this.mainRender.sprite = sprite;
    }

    /// <summary>
    /// Set main render by SpriteRenderer
    /// </summary>
    /// <param name="render"></param>
    public void SetMainRender(SpriteRenderer render)
    {
        this.mainRender = render;
    }

    /// <summary>
    /// Set enhance render by Sprite
    /// </summary>
    /// <param name="sprite"></param>
    public void SetEnhanceRender(Sprite sprite, bool enabled = true)
    {
        this.enhanceRender.enabled = enabled;
        this.enhanceRender.sprite = sprite;
    }

    /// <summary>
    /// Set enhance render by SpriteRenderer
    /// </summary>
    /// <param name="render"></param>
    public void SetEnhanceRender(SpriteRenderer render, bool enable = true)
    {
        this.enhanceRender.enabled = enable;
        this.enhanceRender = render;
    }
    #endregion

    #region Static methods for check same color, same hit
    /// <summary>
    /// Check if 2 tiles have the same color
    /// </summary>
    public static bool CheckSameColor(Tile tdata1, Tile tdata2)
    {
        if (tdata1 == null || tdata2 == null)
        {
            return false;
        }

        if (!tdata1.isColored || !tdata2.isColored)
        {
            return false;
        }

        //if (tdata1.tileData.tileColor == TileData.TileColor.NONE || tdata2.tileData.tileColor == TileData.TileColor.NONE) return false;

        //if (tdata1.tileData.tileColor == TileData.TileColor.WILDCARD || tdata2.tileData.tileColor == TileData.TileColor.WILDCARD) return true;

        return (tdata1.tileData.tileColor == tdata2.tileData.tileColor);
    }

    /// <summary>
    /// Check if 3 tiles have the same color
    /// </summary>
    public static bool CheckSameColor(Tile tdata1, Tile tdata2, Tile tdata3)
    {
        return (CheckSameColor(tdata1, tdata2) && CheckSameColor(tdata1, tdata3));
    }

    /// <summary>
    /// Check same hit with 2 tile
    /// </summary>
    /// <param name="tile1"></param>
    /// <param name="tile2"></param>
    /// <returns></returns>
    public static bool CheckSameHit(Tile tile1, Tile tile2)
    {
        if (tile1 == null || tile2 == null)
        {
            return false;
        }

        if (!tile1.isHit || !tile2.isHit)
        {
            return false;
        }

        if (tile1.isColored)
        {
            return CheckSameColor(tile1, tile2);
        }
        else
        {
            return (tile1.tileData.tileType == tile2.tileData.tileType);
        }

        //return false;
    }

    /// <summary>
    /// Check same hit with 3 tile
    /// </summary>
    /// <param name="tile1"></param>
    /// <param name="tile2"></param>
    /// <param name="tile3"></param>
    /// <returns></returns>
    public static bool CheckSameHit(Tile tile1, Tile tile2, Tile tile3)
    {
        return (CheckSameHit(tile1, tile2) && CheckSameHit(tile2, tile3));
    }

    /// <summary>
    /// Check same hit with 4 tile
    /// </summary>
    /// <param name="tile1"></param>
    /// <param name="tile2"></param>
    /// <param name="tile3"></param>
    /// <param name="tile4"></param>
    /// <returns></returns>
    public static bool CheckSameHit(Tile tile1, Tile tile2, Tile tile3, Tile tile4)
    {
        return (CheckSameHit(tile1, tile2) && CheckSameHit(tile1, tile3) && CheckSameHit(tile1, tile4));
    }
    #endregion
}

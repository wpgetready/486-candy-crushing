﻿/*
 * @Author: CuongNH
 * @Description: Dac ta du lieu bang truyen
 * */

using UnityEngine;
using System.Collections;

using System.Collections.Generic;

[System.Serializable()]
public class ConveyorBelt/* : MonoBehaviour*/
{

    #region Properties, components

    /// <summary>
    /// Conveyor dau tien
    /// </summary>
    public Conveyor fromConveyor;

    /// <summary>
    /// Conveyor ket thuc
    /// </summary>
    public Conveyor toConveyor;

    /// <summary>
    /// Chua danh sach cac Conveyor trong ConveyorBelt
    /// </summary>
    //public List<Conveyor> conveyorList;
    #endregion

    //// Use this for initialization
    //void Start () {
	
    //}
	
    //// Update is called once per frame
    //void Update () {

    //}

    #region Methods for Init, Set Properties
    /// <summary>
    /// Contructor no params
    /// </summary>
    public ConveyorBelt()
    {
        //conveyorList = new List<Conveyor>();
    }

    /// <summary>
    /// Constructor with 2 params
    /// </summary>
    /// <param name="from"></param>
    /// <param name="to"></param>
    public ConveyorBelt(Conveyor from, Conveyor to)
    {
        this.fromConveyor = from;
        this.toConveyor = to;

        //conveyorList = new List<Conveyor>();
    }

    /// <summary>
    /// Add Conveyor to conveyorList of ConveyorBelt
    /// </summary>
    /// <param name="conveyor"></param>
    public void AddConveyorToList(Conveyor conveyor)
    {
        //this.conveyorList.Add(conveyor);
    }
    #endregion

    #region Methods for control, handle events
    /// <summary>
    /// Thuc hien viec chuyen cho tat cac cac Conveyor trong ConveyorBelt
    /// </summary>
    public void Convey()
    {
        //if (conveyorList.Count >= 2)
        //{
        //    // Lan luot thuc hien viecj chuyen cac conveyor tu dau den ap chot
        //    for (int i = 0; i < conveyorList.Count - 1; i++)
        //    {
        //        try
        //        {
        //            conveyorList[i].Convey();
        //        }
        //        catch (System.Exception e)
        //        {
        //            VGDebug.LogError(e.Message);
        //        }
        //    }
        //}

        //// Chuyen tu cuoi len dau
        //try
        //{
        //    toConveyor.ConveyTo(fromConveyor);
        //}
        //catch (System.Exception e)
        //{
        //    VGDebug.LogError(e.Message);
        //}
    }
    #endregion
}

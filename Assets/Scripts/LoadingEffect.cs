﻿using UnityEngine;
using System.Collections;

public class LoadingEffect : MonoBehaviour {
    public bool loading = false;
    public Texture2D texture;
    public float size = 100;
    private float rotAngle = 0;
    public float rotSpeed = 300;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
    //void Update () {
    //    if (loading)
    //    {
           
    //    }
    //}

    void OnGUI()
    {
        if(loading){
            rotAngle -= rotSpeed * Time.deltaTime;
            Vector2 pivot = new Vector2(Screen.width / 2, Screen.height / 2);
		    GUIUtility.RotateAroundPivot(rotAngle%360,pivot);
            GUI.DrawTexture(new Rect((Screen.width - size) / 2, (Screen.height - size) / 2, size, size), texture); 
	    }
    }
}

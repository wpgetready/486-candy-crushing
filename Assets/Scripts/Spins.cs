﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spins : MonoBehaviour 
{
	public AllPanel allpanel;
	public GameObject obj;
	int count = 10;
	float angle = 360;
	int numberItem, oldnumberitem;
	int rewardCoins;
	public GameObject panel;
	public GameObject spinsobj;
	public Button spinsbtn;
	public Button closebtn;
	bool isfirst;
	public Image spinsText;
	public Sprite[] texts;
	public static int stateSpins;
	public Text starsText, CoinsText;
	public GameObject EffectPanel;
	public Image iconsRewarded;
	public Sprite[] icons;
	public Text quantity;
	int _quantity;
	public GameObject[] spins_effect;
	public Rigidbody2D btn_control_spn;

	void SetTexts()
	{
		int stars = PlayerPrefs.GetInt ("totalStars", 0);
		starsText.text = "" + stars;
		int coins = PlayerPrefs.GetInt ("coins");
		CoinsText.text = "" + coins;
	}

	void ResetSpins()
	{
		obj.GetComponent<RectTransform>().localRotation = Quaternion.Euler (Vector3.zero);
	}

	public void spin()
	{
		bool allow = allpanel.spinsbt.SetStarsCount ();
		if (allow == true) {
			oldnumberitem = numberItem;
			int randomNumber = Random.Range (0, 100);

			if (randomNumber < 20) {
				numberItem = 0;
				_quantity = 5;
			} else {
				if (randomNumber >= 20 && randomNumber < 30) {
					numberItem = 1;
					_quantity = 1;
				} else {
					if (randomNumber >= 30 && randomNumber < 35) {
						numberItem = 2;
						_quantity = 50;
					} else {
						if (randomNumber >= 35 && randomNumber < 45) {
							numberItem = 3;
							_quantity = 1;
						} else {
							if (randomNumber >= 45 && randomNumber < 65) {
								numberItem = 4;
								_quantity = 10;
							} else {
								if (randomNumber >= 65 && randomNumber < 85) {
									numberItem = 5;
									_quantity = 1;
								} else {
									if (randomNumber >= 85 && randomNumber < 90) {
										numberItem = 6;
										_quantity = 1;
									} else {
										if (randomNumber >= 90 && randomNumber < 100) {
											numberItem = 7;
											_quantity = 20;
										}
									}
								}
							}
						}
					}
				}
			}

			foreach(GameObject obj in spins_effect)
			{
				obj.SetActive (true);
				ParticleSystem.MainModule p = obj.GetComponent<ParticleSystem>().main;
				p.startLifetime = 2f;
			}

			spinsbtn.interactable = false;
			closebtn.enabled = false;
			btn_control_spn.simulated = true;
			float a = 0;
			if (isfirst == false) {
				a = 23;
				isfirst = true;
			}
			LeanTween.rotate (obj.GetComponent<RectTransform> (), -(count * angle + a + (45 * numberItem) + (8 - oldnumberitem) * 45), count).setEase (LeanTweenType.easeOutCubic).setOnComplete (() => {
				foreach(GameObject obj in spins_effect)
				{
					obj.SetActive(false);
				}
				CheckStars ();
				AddRewards ();
				closebtn.enabled = true;
				spinsbtn.interactable = true;
				iconsRewarded.sprite = icons[numberItem];
				iconsRewarded.SetNativeSize();
				string nameItem = "";
				string number = "";
				string lastStr = "";
				if(_quantity%5 == 0)
				{
					nameItem = "Coins";
					number =""+_quantity;
					lastStr = "";
				}
				else
				{
					if(numberItem == 1)
					{
						nameItem = "Column Breaker";
						number =""+_quantity;
						lastStr = "item";
					}
					else
					{
						if(numberItem == 3)
						{
							nameItem = "Row Breaker";
							number =""+_quantity;
							lastStr = "item";
						}
						else
						{
							if(numberItem == 5)
							{
								nameItem = "Breaker";
								number =""+_quantity;
								lastStr = "item";
							}
							else
							{
								nameItem = "";
								number =""+_quantity*3;
								lastStr = "items";
							}
						}
					}
				}
				quantity.text = "Check it out! You've received "+"+"+number+" "+nameItem+" "+lastStr;
				EffectPanel.SetActive(true);
			});
			LeanTween.delayedCall (count-5f, () => {
				foreach(GameObject obj in spins_effect)
				{
					ParticleSystem.MainModule p = obj.GetComponent<ParticleSystem>().main;
					p.startLifetime = 1.5f;
				}
			});
			LeanTween.delayedCall (count-4f, () => {
				foreach(GameObject obj in spins_effect)
				{
					ParticleSystem.MainModule p = obj.GetComponent<ParticleSystem>().main;
					p.startLifetime = 1f;
				}
			});
			LeanTween.delayedCall (count-3f, () => {
				foreach(GameObject obj in spins_effect)
				{
					ParticleSystem.MainModule p = obj.GetComponent<ParticleSystem>().main;
					p.startLifetime = 0.5f;
				}
			});
		}
		else
		{
			HideSpins ();
			allpanel.buyCoinsController.Show_BuyCoinsPanel ();
		}
	}

	public void Claim()
	{
		EffectPanel.SetActive (false);
	}

	void AddRewards()
	{
		if (numberItem == 0 || numberItem == 2 || numberItem == 4 || numberItem == 7) {
			if (numberItem == 0) {
				rewardCoins = 5;
			} else {
				if (numberItem == 2) {
					rewardCoins = 50;
				} else {
					if (numberItem == 4) {
						rewardCoins = 10;
					} 
					else 
					{
						if (numberItem == 7) {
							rewardCoins = 20;
						}
					}
				}
			}

			AdsControl.instance.addCoins.AddReward (true, rewardCoins);
			int coins = PlayerPrefs.GetInt ("coins");
			CoinsText.text = "" + coins;
		} else {
			if (numberItem == 1) {
				int numberItems2 = PlayerPrefs.GetInt ("numberitem2");
				PlayerPrefs.SetInt ("numberitem2",numberItems2+1);
				PlayerPrefs.Save ();
			} else {
				if (numberItem == 3) {
					int numberItems1 = PlayerPrefs.GetInt ("numberitem1");
					PlayerPrefs.SetInt ("numberitem1",numberItems1+1);
					PlayerPrefs.Save ();
				} else {
					if (numberItem == 5) {
						int numberItems3 = PlayerPrefs.GetInt ("numberitem3");
						PlayerPrefs.SetInt ("numberitem3", numberItems3 + 1);
						PlayerPrefs.Save ();
					} else {
						if (numberItem == 6) {
							int numberItems1 = PlayerPrefs.GetInt ("numberitem1");
							PlayerPrefs.SetInt ("numberitem1",numberItems1+1);
							PlayerPrefs.Save ();
							int numberItems2 = PlayerPrefs.GetInt ("numberitem2");
							PlayerPrefs.SetInt ("numberitem2",numberItems2+1);
							PlayerPrefs.Save ();
							int numberItems3 = PlayerPrefs.GetInt ("numberitem3");
							PlayerPrefs.SetInt ("numberitem3", numberItems3 + 1);
							PlayerPrefs.Save ();
						}
					}
				}
			}
		}
	}

	void CheckStars()
	{
		int stars = PlayerPrefs.GetInt ("totalStars", 0);
		if (stars >= 15)
		{
			spinsText.sprite = texts [0];
			stateSpins = 1;

		}
		else
		{
			spinsText.sprite = texts [1];
			stateSpins = 2;

		}
		int coins = PlayerPrefs.GetInt ("coins");
		starsText.text = "" + stars;
		CoinsText.text = "" + coins;
	}

	public void ShowSpins()
	{
		panel.SetActive (true);
		spinsobj.SetActive (true);
		iTween.MoveTo(spinsobj, iTween.Hash("position", Vector3.zero, "time", 0.25f, "easetype", iTween.EaseType.easeOutBack));
		numberItem = 0;
		oldnumberitem = 0;
		ResetSpins ();
		isfirst = false;
		CheckStars ();
	}

	public void HideSpins()
	{
		LeanTween.cancel (obj);
		iTween.MoveTo(spinsobj, iTween.Hash("position", new Vector3(0,11f,0), "time", 0.25f, "easetype", iTween.EaseType.easeOutBack));
		LeanTween.delayedCall (0.25f, () => {
			spinsobj.SetActive (false);
		});
		panel.SetActive (false);
	}

}

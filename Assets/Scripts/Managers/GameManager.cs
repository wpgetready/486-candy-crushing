﻿/*
 * @Author: CuongNH
 * @Description: Quan ly cac du lieu, thong tin chung cho game
 * */

using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{

    #region Data, info of gameplay
    /// <summary>
    /// Dinh nghia cac game mode
    /// </summary>
    public enum GameMode
    {
        ORDER_FULFILLMENT = 0,
        BOSS_BATTLE = 1,
        POPSICLE = 2,
        CLEAR_JUICE = 3
    }

    /// <summary>
    /// Dinh nghia cac trang thai cua game
    /// </summary>
    public enum GameState
    {
        NOT_RUNNING = 0,
        LOADING = 1,
        STARTUP = 2,
        PLAYING = 3,
        PAUSED = 4,
        WIN = 5,
        LOSE = 6
    }

    /// <summary>
    /// Dinh nghia cac loai rang buoc cua game play
    /// </summary>
    public enum LimitType
    {
        MOVE_LIMIT = 0,
        TIME_LIMIT = 1
    }

    /// <summary>
    /// Mode of game play
    /// </summary>
    public static GameMode gameMode;

    /// <summary>
    /// State of game play
    /// </summary>
    public static GameState gameState;

    /// <summary>
    /// Loai rang buoc cua game play
    /// </summary>
    public static LimitType limitType = LimitType.MOVE_LIMIT;

    /// <summary>
    /// Move number of game play
    /// </summary>
    public static int MOVE = 0;

    public static int SPAW_TIME = 0;

    /// <summary>
    /// Time of game play; unit is second
    /// </summary>
    public static float TIME = 0f;
    #endregion

    //// Use this for initialization
    //void Start () {
	
    //}
	
    //// Update is called once per frame
    //void Update () {
	
    //}

    /// <summary>
    /// Set game mode by name
    /// </summary>
    /// <param name="mode"></param>
    public static void SetGameMode(string mode)
    {
        if (string.IsNullOrEmpty(mode))
        {
            return;
        }

        mode = mode.Trim();
        switch (mode)
        {
            case GameConstants.GAMEMODE_OREDER:
                {
                    gameMode = GameMode.ORDER_FULFILLMENT;
                    break;
                }
            case GameConstants.GAMEMODE_BOSS:
                {
                    gameMode = GameMode.BOSS_BATTLE;
                    break;
                }
            case GameConstants.GAMEMODE_POSICLE:
                {
                    gameMode = GameMode.POPSICLE;
                    break;
                }
            case GameConstants.GAMEMODE_CLEAR_JUICE:
                {
                    gameMode = GameMode.CLEAR_JUICE;
                    break;
                }
            default:
                gameMode = GameMode.ORDER_FULFILLMENT;
                break;
        }
    }
}

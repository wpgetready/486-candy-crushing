﻿using UnityEngine;
using System.Collections;

namespace GameEnum
{
    public enum InputState
    {
        BEGIN = 0,
        MOVE = 1,
        END = 2,
        CANCEL = 3
    }

    public enum ProcessType
    {
        NONE = 0,
        SWAP = 1, // Swap 2 tiles
        CASCADE = 2, // Process consequence
        CHAIN = 3 // Process chain link
    }
}
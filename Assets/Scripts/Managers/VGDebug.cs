﻿/*
 * @Author: CuongNH
 * @Description: Su dung de de ghi log
 * */

using UnityEngine;
using System.Collections;

public class VGDebug : MonoBehaviour {
    /// <summary>
    /// Log debug
    /// </summary>
    /// <param name="log"></param>
    public static void LogDebug(string log = "Debug Info")
    {
        Debug.Log("VG Debug: " + log);
    }

    /// <summary>
    /// Log errors
    /// </summary>
    /// <param name="errors"></param>
    public static void LogError(string errors = "Errors!")
    {
        Debug.LogError("Got Errors...");
        Debug.LogError(errors);
    }
}

﻿/*
 * @Author: CuongNH
 * @Description: Luu cac hang so chinh duoc su dung trong game
 * */

using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class GameConstants
{
    #region Ten cac scenes trong game
    public const string LOADING_SCENE_NAME = "Loading";
    public const string HOME_SCENE_NAME = "Home";
    public const string LEVEL_SELECT_SCENE_NAME = "MapLevel";
    public const string GAMEPLAY_SCENE_NAME = "GamePlay";
    public const string OBJECT_EDITOR_SCENE_NAME = "ObjectEditor";
    #endregion

    #region Game mode name
    public const string GAMEMODE_OREDER = "order_fulfillment";
    public const string GAMEMODE_BOSS = "boss_battle";
    public const string GAMEMODE_POSICLE = "popsicle";
    public const string GAMEMODE_CLEAR_JUICE = "clear_juice";
    #endregion

    /// <summary>
    /// Kich thuoc so cot toi da cua map
    /// </summary>
    public const int MAP_WIDTH = 9;
    /// <summary>
    /// Kich thuoc so hang toi da cua map
    /// </summary>
    public const int MAP_HEIGHT = 8;

    /// <summary>
    /// So tile trong pool
    /// </summary>
    public const int TILE_POOLING_NUMBER = 100;

    #region Ten cac type cua item
    public const string empty_tile_type = "empty";
    public const string invisible_brick_tile_type = "invisible_brick";
    public const string match_object_tile_type = "match_object";
    public const string sugar_tile_type = "sugar";
    public const string bee_tile_type = "bee";
    public const string ice_cube_tile_type = "ice_cube";
    public const string muffin_tile_type = "muffin";
    public const string muffins_tile_type = "muffins";
    public const string muff_tile_type = "muff";
    public const string muffs_tile_type = "muffs";
    public const string cow_tile_type = "cow";
    public const string ice_cage_tile_type = "ice_cage";
    public const string fountain_tile_type = "fountain";
    public const string coconut_1_tile_type = "coconut_1";
    public const string coconut_2_tile_type = "coconut_2";
    public const string yogurt_cup_tile_type = "yogurt_cup";
    public const string yogurt_tile_type = "yogurt";
    public const string row_breaker_tile_type = "row_breaker";
    public const string column_breaker_tile_type = "column_breaker";
    public const string bomb_breaker_tile_type = "bomb_breaker";
    public const string rainbow_tile_type = "rainbow";
    public const string x_breaker_tile_type = "x_breaker";
    public const string magnet_tile_type = "magnet";
    public const string x_rainbow_tile_type = "x_rainbow";
    public const string bomb_rainbow_tile_type = "bomb_rainbow";
    #endregion

    #region Ten cac modifier type cua item
    public const string ice_cage_tile_modifier_type = "ice_cage";
	public const string watermelon_O_tile_modifier_type = "watermelon_O";
	public const string watermelon_I_tile_modifier_type = "watermelon_I";
	public const string sugar_color_tile_modifier_type = "color";
    #endregion

    #region Mau cac tile
    public const string blue_tile_color = "blue";
    public const string green_tile_color = "green";
    public const string orange_tile_color = "orange";
    public const string purple_tile_color = "purple";
    public const string red_tile_color = "red";
    public const string yellow_tile_color = "yellow";

    public const string any_tile_color = "any";
    public const string random_tile_color = "random";
    public const string random2_tile_color = "random2";
    public const string random3_tile_color = "random3";
    public const string random4_tile_color = "random4";
    #endregion

    /// <summary>
    /// So mau toi da cua cac qua
    /// </summary>
    public const int MAX_COLOR = 6;

    /// <summary>
    /// So trang thai cua cac cay
    /// </summary>
    public const int numberStateFountain = 4;

    #region Ten cac loai dynamic grid
    public const string cracker_grid = "cracker";
    public const string honey_grid = "honey";
    public const string ice_grid = "ice";
    #endregion

    #region So lop cua GridDynamicData
    public const int GRID_DYNAMIC_DATA_1X = 1;
    public const int GRID_DYNAMIC_DATA_2X = 2;
    public const int GRID_DYNAMIC_DATA_3X = 3;
    #endregion

    #region Ten cac loai static grid
    public const string conveyor_belt = "conveyor_belt";
    public const string portal_inlet = "portal_inlet";
    public const string portal_outlet = "portal_outlet";
    public const string spawner = "spawner";
    #endregion

    // Ten cua popsicle
    public const string popsicle = "popsicle";

    #region Ten cac loai popsicles
    public const string set_1x1 = "set_1x1";
    public const string set_2x2 = "set_2x2";
    public const string set_3x3 = "set_3x3";
    public const string set_4x4 = "set_4x4";
    public const string set_1x2 = "set_1x2";
    public const string set_2x1 = "set_2x1";
    public const string set_2x4 = "set_2x4";
    public const string set_4x2 = "set_4x2";
    #endregion

    #region Ten cac huong cua conveyor
    public const string up_conveyor_direction = "up";
    public const string down_conveyor_direction = "down";
    public const string left_conveyor_direction = "left";
    public const string right_conveyor_direction = "right";
    #endregion

    /// <summary>
    /// Determine if possible to swap gem with an empty grid
    /// </summary>
    public const bool ENABLE_SWAP_WITH_EMPTY_GRID = false;

    /// <summary>
    /// Determine if need to show point bubble when destroy a gem
    /// </summary>
    public const bool DISPLAY_POINT_BUBBLE = false;

    /// <summary>
    /// Determine if tile can be dropped diagonally
    /// </summary>
    public const bool ENABLE_DIAGONAL_DROP = true;

    /// <summary>
    /// Determine if player can swap gem while processing other things map
    /// </summary>
    public const bool ENABLE_SWAP_WHILE_PROCESS = false;

    /// <summary>
    /// Determine if spawner dispatch a whole pack of tile at a time // WARNING: change to false cause freeze (Fix Attemps Failed: 3)
    /// </summary>
    public const bool ENABLE_PATCH_SPAWNER = true;

    /// <summary>
    /// Directions that can create a match 3
    /// </summary>
    public static readonly List<Grid.Direction> DIRECTIONS_CHECK_MATCHABLE3 = new List<Grid.Direction>()
    {    
        Grid.Direction.TOP, 
        Grid.Direction.LEFT
    };

    /// <summary>
    /// Directions that can create a match 2X2
    /// </summary>
    public static readonly List<Grid.Direction> DIRECTIONS_CHECK_MATCHABLE_2X2 = new List<Grid.Direction>()
    {    
        Grid.Direction.LEFT,
        Grid.Direction.TOP,
        Grid.Direction.TOP_LEFT
    };

    /// <summary>
    /// Directions in corner 1
    /// </summary>
    public static readonly List<Grid.Direction> DIRECTIONS_CORNER1 = new List<Grid.Direction>()
    {    
        Grid.Direction.LEFT,
        Grid.Direction.TOP,
        Grid.Direction.TOP_LEFT
    };

    /// <summary>
    /// Directions in corner 2
    /// </summary>
    public static readonly List<Grid.Direction> DIRECTIONS_CORNER2 = new List<Grid.Direction>()
    {    
        Grid.Direction.RIGHT,
        Grid.Direction.TOP,
        Grid.Direction.TOP_RIGHT
    };

    /// <summary>
    /// Directions in corner 3
    /// </summary>
    public static readonly List<Grid.Direction> DIRECTIONS_CORNER3 = new List<Grid.Direction>()
    {    
        Grid.Direction.RIGHT,
        Grid.Direction.BOTTOM,
        Grid.Direction.BOTTOM_RIGHT
    };

    /// <summary>
    /// Directions in corner 4
    /// </summary>
    public static readonly List<Grid.Direction> DIRECTIONS_CORNER4 = new List<Grid.Direction>()
    {    
        Grid.Direction.BOTTOM,
        Grid.Direction.LEFT,
        Grid.Direction.BOTTOM_LEFT
    };

    /// <summary>
    /// Array corner
    /// </summary>
    public static readonly List<Grid.Direction>[] CORNER_ARRAY = new List<Grid.Direction>[4]
    {
        DIRECTIONS_CORNER1,
        DIRECTIONS_CORNER2,
        DIRECTIONS_CORNER3,
        DIRECTIONS_CORNER4
    };

    /// <summary>
    /// Directions can swap
    /// </summary>
    public static readonly List<Grid.Direction> DIRECTIONS_SWAPPABLE = new List<Grid.Direction>()
    {    
        Grid.Direction.TOP, 
        Grid.Direction.BOTTOM,
        Grid.Direction.LEFT,
        Grid.Direction.RIGHT
    };

    /// <summary>
    /// Directions can clear tile affect
    /// </summary>
    public static readonly List<Grid.Direction> DIRECTIONS_TILE_CLEAR_AFFECTING = new List<Grid.Direction>()
    {    
        Grid.Direction.TOP, 
        Grid.Direction.BOTTOM,
        Grid.Direction.LEFT,
        Grid.Direction.RIGHT
    };

    /// <summary>
    /// Directions clear tile by row breaker
    /// </summary>
    public static readonly List<Grid.Direction> DIRECTIONS_TILE_CLEAR_ROW = new List<Grid.Direction>()
    {
        Grid.Direction.LEFT,
        Grid.Direction.RIGHT
    };

    /// <summary>
    /// Directions clear tile by column breaker
    /// </summary>
    public static readonly List<Grid.Direction> DIRECTIONS_TILE_CLEAR_COLUMN = new List<Grid.Direction>()
    {
        Grid.Direction.TOP,
        Grid.Direction.BOTTOM
    };

    /// <summary>
    /// Directions clear tile by x breaker
    /// </summary>
    public static readonly List<Grid.Direction> DIRECTIONS_TILE_CLEAR_X = new List<Grid.Direction>()
    {
        Grid.Direction.TOP_LEFT,
        Grid.Direction.TOP_RIGHT,
        Grid.Direction.BOTTOM_RIGHT,
        Grid.Direction.BOTTOM_LEFT
    };

    /// <summary>
    /// Directions clear tile by bomb breaker
    /// </summary>
    public static readonly List<Grid.Direction> DIRECTIONS_TILE_CLEAR_BOMB = new List<Grid.Direction>()
    {
        Grid.Direction.TOP, 
        Grid.Direction.BOTTOM,
        Grid.Direction.LEFT,
        Grid.Direction.RIGHT,
        Grid.Direction.TOP_LEFT,
        Grid.Direction.TOP_RIGHT,
        Grid.Direction.BOTTOM_RIGHT,
        Grid.Direction.BOTTOM_LEFT
    };

    /// <summary>
    /// So vien an theo huong tuong ung cua bomb
    /// </summary>
    public static readonly List<int> NUMBER_TILE_CLEAR_BOMB = new List<int>()
    {
        2, 2, 2, 2, 1, 1, 1, 1
    };

    // Speeds
    public const float SPEED_SWAP_TILE = 0.19f;
    public const float SPEED_LIGHTNING_PROJECTILE_PER_GRID = 0.1f;
    public const float SPEED_PER_GRID = 0.2f;
    public const float SPEED_DROP_TILE_EACH_STEP = 0.1f;//drop speed

    // Time
    public const float TIME_SWAP_DELAY = 0.1f;
	public const float TIME_BREAK_ANIMATION = 0.15f;// wait time after eat items
    public const float TIME_BETWEEN_MAGIC_HITS = 0.1f;//
    public const float TIME_FINISH_MOVING_ANIMATION = 0;
    public const float TIME_DELAY_BEFORE_SPECIAL_SWAP_EFFECT = 0.03f;
    public const float TIME_DELAY_AFTER_REROLLMAP = 0.1f;
    public const float TIME_DELAY_BEFORE_SHOWING_HELP = 10f;
    public const float TIME_GAMEPLAY_SHOWUP = 0.75f;
    public const float TIME_HUD_OBJECTIVE = 0.1f;
    public const float TIME_MOVE_HELP = 0.4f;
    public const float TIME_MOVE_MAP_TO_PLAY = 0.5f;// Move map
    public const float TIME_TILE_BOUNE = 0.1f;

    // Time to clear 
    public const float TIME_CLEAR_TILE = 0.3f;

    // Time to merge combined
    public const float TIME_COMBINED_TILE = 0.25f;

    // Time to transform process
    public const float TIME_TRANSFORM_PROCESS = 0.2f;

    // Time to rotate tile when swap with booster
    public const float TIME_ROTATE_TILE = 0.5f;

    // Time to transform to booster tile
    public const float TIME_TO_MOVE_NEAR_MAGNET = 0.3f;

    // Time delay to active after transform to booster
    public const float TIME_DELAY_ACTIVE_AFTER_TRANSFORM_BOOSTER = 0.3f;

    // Time to fire rocket
    public const float TIME_FIRE_ROCKET = 0.75f;

    /// <summary>
    /// Sqrt of 2f
    /// </summary>
    public static readonly float SQRT2 = Mathf.Sqrt(2f);

    /// <summary>
    /// Do nay cua tile khi roi
    /// </summary>
    public const float TILE_BOUNCE = 1f / 10f;

    /// <summary>
    /// Do scale cua tile khi roi
    /// </summary>
    public const float TILE_SCALE_DROP = 1f / 10f;

    /// <summary>
    /// Do scale cua tile khi cham toi vien khac
    /// </summary>
    public const float TILE_SCALE_BOUNCE = 1f / 10f;

    /// <summary>
    /// Max to reset
    /// </summary>
    public const int MAX_RESET = 200;

    /// <summary>
    /// Number grid to cache in MapCondionCheckerMatch
    /// </summary>
    public const int CACHE_NUMBER_GRID_CHECKING = 16;

    /// <summary>
    /// Min number to match 3
    /// </summary>
    public const int MATCH3_MIN = 3;

    /// <summary>
    /// Min number to match 2x2
    /// </summary>
    public const int MATCH2X2_MIN = 4;

}

﻿/*
 * @Author: CuongNH
 * @Description: Quan ly viec su dung cac object trong pool
 * */

using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class PoolingManager : MonoBehaviour {

    public static PoolingManager poolingManager;

    /// <summary>
    /// Transform chua cac tile trong pool
    /// </summary>
    public Transform tilePooling;

    /// <summary>
    /// Transform chua cac effects trong pool
    /// </summary>
    public Transform effectPooling;

    /// <summary>
    /// List chua cac pool
    /// </summary>
    public List<Tile> tileList = new List<Tile>(GameConstants.TILE_POOLING_NUMBER);

    public List<GameObject> List_Effect_Shock_Blue = new List<GameObject>();
    public List<GameObject> List_Effect_Shock_Green = new List<GameObject>();
    public List<GameObject> List_Effect_Shock_Orange = new List<GameObject>();
    public List<GameObject> List_Effect_Shock_Purple = new List<GameObject>();
    public List<GameObject> List_Effect_Shock_Red = new List<GameObject>();
    public List<GameObject> List_Effect_Shock_Yellow = new List<GameObject>();
    public List<GameObject> List_Effect_Shock_White = new List<GameObject>();

    public List<GameObject> List_Effect_Order_Blue = new List<GameObject>();
    public List<GameObject> List_Effect_Order_Green = new List<GameObject>();
    public List<GameObject> List_Effect_Order_Orange = new List<GameObject>();
    public List<GameObject> List_Effect_Order_Purple = new List<GameObject>();
    public List<GameObject> List_Effect_Order_Red = new List<GameObject>();
    public List<GameObject> List_Effect_Order_Yellow = new List<GameObject>();
    public List<GameObject> List_Effect_Order_White = new List<GameObject>();
    public List<GameObject> List_Effect_Order_Muffin = new List<GameObject>();
    public List<GameObject> List_Effect_Order_Honey = new List<GameObject>();
    public List<GameObject> List_Effect_Order_milk = new List<GameObject>();

    public List<GameObject> List_Effect_Ice = new List<GameObject>();
    public List<GameObject> List_Effect_Cracker = new List<GameObject>();
    public List<GameObject> List_Effect_Sugar = new List<GameObject>();

    public List<GameObject> List_bonus_star = new List<GameObject>();
    public List<GameObject> List_Effect_bonus_star = new List<GameObject>();

    public List<GameObject> List_Effect_Boss_Blue = new List<GameObject>();
    public List<GameObject> List_Effect_Boss_Green = new List<GameObject>();
    public List<GameObject> List_Effect_Boss_Orange = new List<GameObject>();
    public List<GameObject> List_Effect_Boss_Purple = new List<GameObject>();
    public List<GameObject> List_Effect_Boss_Red = new List<GameObject>();
    public List<GameObject> List_Effect_Boss_Yellow = new List<GameObject>();
    public List<GameObject> List_Effect_Boss_White = new List<GameObject>();
    public List<GameObject> List_Effect_Boss_Cow = new List<GameObject>();
    // Use when awake 
    void Awake()
    {
        poolingManager = this;
    }

    //// Use this for initialization
    //void Start () {
	
    //}
	
    /// <summary>
    /// Init objects in pool
    /// </summary>
    public void InitPool()
    {
        InitTilePooling();
    }

    /// <summary>
    /// Init Tiles
    /// </summary>
    private void InitTilePooling()
    {
        if (tileList.Count >= GameConstants.TILE_POOLING_NUMBER)
        {
            return;
        }

        for (int i = 0; i < GameConstants.TILE_POOLING_NUMBER; i++)
        {
            Tile tile = GamePlayController.gamePlayController.mapController.tileSpawner.CreateNewTile();
            AddTileToPool(tile);
        }
    }

    /// <summary>
    /// Add tile to pool
    /// </summary>
    /// <param name="tile"></param>
    public void AddTileToPool(Tile tile)
    {
        if (tile != null)
        {
            if (tileList.Count >= 2 * GameConstants.TILE_POOLING_NUMBER)
            {
                Destroy(tile.gameObject);
            }
            else
            {
                tile.transform.SetParent(tilePooling);
                tile.transform.localPosition = Vector3.zero;
                tileList.Add(tile);
            }
        }
    }

    /// <summary>
    /// Get tile from pool
    /// </summary>
    /// <returns></returns>
    public Tile GetTile()
    {
        Tile tile = null;
        if (tileList.Count > 0)
        {
            tile = tileList[0];
            tileList.Remove(tile);
        }
        else
        {
            tile = GamePlayController.gamePlayController.mapController.tileSpawner.CreateNewTile();
        }

        return tile;
    }

    /// <summary>
    /// Init effects
    /// </summary>
    private void InitEffectPooling()
    {

    }


    //=============================================effect_shock_color==============================================
	public void AddShockColorToPool(GameObject obj, List<GameObject> list)
    {
        if(obj != null && list.Count < 20)
        {
            obj.transform.SetParent(effectPooling);
            obj.transform.localPosition = Vector3.zero;
            list.Add(obj);
            obj.transform.localScale = new Vector3(0.8f,0.8f,1);
            obj.GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 255);

            obj.SetActive(false);
        }
        else
        {
            Destroy(obj);
        }
    }

    public GameObject GetEffectShockColorFromPool(List<GameObject> list, Tile tile)
    {
        GameObject obj = null;
        list[0].SetActive(true);
        obj = list[0];
        obj.transform.position = tile.transform.position;
        list.RemoveAt(0);
        return obj;
    }


    //===========================================effect_order_collect_color=========================================
    public void AddOrderColorToPool(GameObject obj, List<GameObject> list)
    {
        if (obj != null && list.Count < 20)
        {
            obj.transform.SetParent(effectPooling);
            obj.transform.localPosition = Vector3.zero;
            list.Add(obj);

            obj.SetActive(false);
        }
        else
        {
            Destroy(obj);
        }
    }
    public GameObject GetEffectOrderColorFromPool(List<GameObject> list, Tile tile)
    {
        GameObject obj = null;
        list[0].SetActive(true);
        obj = list[0];
        obj.transform.position = tile.transform.position;
        list.RemoveAt(0);
        return obj;
    }

    //===========================================effect_bonus=========================================
    public void AddBonusToPool(GameObject obj, List<GameObject> list)
    {
        if (obj != null && list.Count < 20)
        {
            obj.transform.SetParent(effectPooling);
            obj.transform.localPosition = Vector3.zero;
            list.Add(obj);

            obj.SetActive(false);
        }
        else
        {
            Destroy(obj);
        }
    }
    public GameObject GetEffectBonusFromPool(List<GameObject> list, Vector3 pos)
    {
        GameObject obj = null;
        list[0].SetActive(true);
        obj = list[0];
        obj.transform.position = pos;
        list.RemoveAt(0);
        return obj;
    }

}

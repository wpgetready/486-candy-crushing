﻿/*
 * @Author: CuongNH
 * @Description: Quan ly viec xu ly popup cac dialog
 * */

using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class PopupManager : MonoBehaviour {

    public static PopupManager poupManager;

    public GameObject blackScreen; // hide below things
    public bool isAnimating = false;

    // Control popup display 
    public List<IPopupController> popupStack = new List<IPopupController>();

    void Awake()
    {
        // Keep Singleton but can be accessed from anywhere
        if (poupManager == null)
        {
            poupManager = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            DestroyImmediate(this.gameObject);
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
    //// Update is called once per frame
    //void Update () {
	
    //}

    /// <summary>
    /// Show a popup
    /// </summary>
    /// <param name="popup"></param>
    /// <param name="replacing"> 
    /// true: replace top popup in stack | false: push to top of stack
    /// </param>
    public float ShowPopup(IPopupController popup, bool replacing = true)
    {
        float totalTime = 0;
        blackScreen.SetActive(true);

        if (popupStack.Count > 0)
        {
            for (int i = 0; i < popupStack.Count; i++)
            {
                if (popup.Equals(popupStack[i]))
                {
                    return totalTime;
                }
            }

            // Hide
            float delay = popupStack[popupStack.Count - 1].OnHide();
            LeanTween.delayedCall(delay / 2, () =>
            {
                popup.OnShow();
            });

            if (replacing)
            {
                popupStack[popupStack.Count - 1] = popup;
            }
            else
            {
                popupStack.Add(popup);
            }
        }
        else
        {
            popup.OnShow();
            popupStack.Add(popup);
        }

        return totalTime;
    }

    /// <summary>
    /// Hide last popup in stack, return true if closed a popup
    /// </summary>
    public float HideTopPopup()
    {
        if (popupStack.Count > 0)
        {
            // Hide
            float delay = popupStack[popupStack.Count - 1].OnHide();
            popupStack.RemoveAt(popupStack.Count - 1);

            if (popupStack.Count > 0)
            {
                LeanTween.delayedCall(delay / 2, () =>
                {
                    // show prev
                    popupStack[popupStack.Count - 1].OnShow();
                });
            }
            else
            {
                blackScreen.SetActive(false);
            }

            return delay;
        }
        else
        {
            return 0;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns>Time needed to hide all</returns>
    public float HideAllPopup()
    {
        if (popupStack.Count > 0)
        {
            // Hide
            float delay = popupStack[popupStack.Count - 1].OnHide();
            popupStack.Clear();

            LeanTween.delayedCall(delay, () =>
            {
                blackScreen.SetActive(false);
            });
            return delay;
        }
        else
        {
            return 0;
        }
    }

    public bool BackKeyHandler()
    {
        if (popupStack.Count > 0)
        {
            // Hide
            if (popupStack[popupStack.Count - 1].OnBack())
            {
                return true;
            }
            else
            {
                //HideTopPopup();
                return false;
            }
        }
        else if (isAnimating)
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// send a message to top popup
    /// </summary>
    /// <param name="name"></param>
    public void SendMessageToPopup(string message, string data = "")
    {
        if (popupStack.Count > 0)
        {
            popupStack[popupStack.Count - 1].OnMessage(message, data);
        }
    }

    public bool IsShowingPopup()
    {
        return (popupStack.Count > 0);
    }

    public void SetBlur(bool val = false)
    {
        blackScreen.SetActive(val);
    }
}

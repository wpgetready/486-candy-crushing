﻿/*
 * @Author: CuongNH
 * @Description: Khai bao interface PopupController
 * */

using UnityEngine;
using System.Collections;

public interface IPopupController
{

    /// <summary>
    /// Animation, calculating when popup showing
    /// Return time needed to finish
    /// </summary>
    float OnShow();

    /// <summary>
    /// Animation, calculating when popup hiding
    /// Return time needed to finish
    /// </summary>
    float OnHide();

    /// <summary>
    /// back key override
    /// return if false if back key doing nothing
    /// </summary>
    bool OnBack();

    void OnMessage(string message, string data);
}

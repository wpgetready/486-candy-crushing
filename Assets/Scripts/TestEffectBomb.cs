﻿using UnityEngine;
using System.Collections;

public class TestEffectBomb : MonoBehaviour {

    public float kc1 = 1;
    public float kc2 = 2;

    public float time = 0.5f;

    public GameObject effect1;
    public GameObject effect2;
    public GameObject effect3;
    public GameObject effect4;
    public GameObject effect5;
    public GameObject effect6;
    public GameObject effect7;
    public GameObject effect8;
    public GameObject effect9;
    public GameObject effect10;
    public GameObject effect11;
    public GameObject effect12;

	// Use this for initialization
	void Start () {
        CollectFood();
	}

    public void CollectFood()
    {
        iTween.MoveTo(effect1, iTween.Hash("path", new Vector3[] { transform.position, new Vector3(transform.position.x + kc1/2f,transform.position.y - kc1/4f,0), new Vector3(transform.position.x + kc1,transform.position.y,0) }, "time", time, "easetype", iTween.EaseType.linear));
        iTween.MoveTo(effect2, iTween.Hash("path", new Vector3[] { transform.position, new Vector3(transform.position.x + kc1, transform.position.y - kc1 / 2f, 0), new Vector3(transform.position.x + kc2, transform.position.y, 0) }, "time", time, "easetype", iTween.EaseType.linear));
        iTween.MoveTo(effect3, iTween.Hash("path", new Vector3[] { transform.position, new Vector3(transform.position.x + kc1 / 4f, transform.position.y + kc1 / 2f, 0), new Vector3(transform.position.x, transform.position.y + kc1, 0) }, "time", time, "easetype", iTween.EaseType.linear));
        iTween.MoveTo(effect4, iTween.Hash("path", new Vector3[] { transform.position, new Vector3(transform.position.x + kc1 / 2f, transform.position.y + kc1, 0), new Vector3(transform.position.x, transform.position.y + kc2, 0) }, "time", time, "easetype", iTween.EaseType.linear));
        iTween.MoveTo(effect5, iTween.Hash("path", new Vector3[] { transform.position, new Vector3(transform.position.x - kc1 / 2f, transform.position.y + kc1 / 4f, 0), new Vector3(transform.position.x - kc1, transform.position.y, 0) }, "time", time, "easetype", iTween.EaseType.linear));
        iTween.MoveTo(effect6, iTween.Hash("path", new Vector3[] { transform.position, new Vector3(transform.position.x - kc1, transform.position.y + kc1 / 2f, 0), new Vector3(transform.position.x - kc2, transform.position.y, 0) }, "time", time, "easetype", iTween.EaseType.linear));
        iTween.MoveTo(effect7, iTween.Hash("path", new Vector3[] { transform.position, new Vector3(transform.position.x - kc1 / 4f, transform.position.y - kc1 / 2f, 0), new Vector3(transform.position.x, transform.position.y - kc1, 0) }, "time", time, "easetype", iTween.EaseType.linear));
        iTween.MoveTo(effect8, iTween.Hash("path", new Vector3[] { transform.position, new Vector3(transform.position.x - kc1 / 2f, transform.position.y - kc1, 0), new Vector3(transform.position.x, transform.position.y - kc2, 0) }, "time", time, "easetype", iTween.EaseType.linear));
        iTween.MoveTo(effect9, iTween.Hash("path", new Vector3[] { transform.position, new Vector3(transform.position.x + kc1*2 / 3f, transform.position.y + kc1 / 2f, 0), new Vector3(transform.position.x + kc1, transform.position.y + kc1, 0) }, "time", time, "easetype", iTween.EaseType.linear));
        iTween.MoveTo(effect10, iTween.Hash("path", new Vector3[] { transform.position, new Vector3(transform.position.x - kc1 / 2f, transform.position.y + kc1 *2/ 3f, 0), new Vector3(transform.position.x - kc1, transform.position.y + kc1, 0) }, "time", time, "easetype", iTween.EaseType.linear));
        iTween.MoveTo(effect11, iTween.Hash("path", new Vector3[] { transform.position, new Vector3(transform.position.x - kc1*2 / 3f, transform.position.y - kc1 / 2f, 0), new Vector3(transform.position.x - kc1, transform.position.y - kc1, 0) }, "time", time, "easetype", iTween.EaseType.linear));
        iTween.MoveTo(effect12, iTween.Hash("path", new Vector3[] { transform.position, new Vector3(transform.position.x + kc1 / 2f, transform.position.y - kc1*2 / 3f, 0), new Vector3(transform.position.x + kc1, transform.position.y - kc1, 0) }, "time", time, "easetype", iTween.EaseType.linear));
    }

}

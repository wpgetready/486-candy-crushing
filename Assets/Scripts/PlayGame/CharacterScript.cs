﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine.UI;

public class CharacterScript : MonoBehaviour {

    public int _id = 0;
    
        public int _value = 0;
        public string _type_object="";
        public string _color="";
        public bool finish = false;
    
    public TextMesh txtvalue;
    public SpriteRenderer _sprite;
    public GameObject Bg_item;
    public GameObject itemFinish;
    public GameObject textnumber;

    public SkeletonAnimation skeAnimation;

    public CharacterScript order;

    public bool actived = false;

	public List<GameObject> listEffect = null;
	[HideInInspector]
	public float timeWin;

	public GameObject Glass;

	public int originValue;

	public List<Sprite> SpriteGlass = new List<Sprite>();

    void Start()
    {
        order = this;
		timeWin = GetComponent<SkeletonAnimation> ().SkeletonDataAsset.GetSkeletonData (true).FindAnimation ("win").Duration;
        //skeAnimation.skeletonDataAsset = skeDataAsset;
    }
    public void SetID(int id)
    {
        _id = id;
    }
    public void SetName(string type)
    {
        gameObject.name ="Goal "+ type;
    }

	public void SetGlassSprite(Sprite[] obj)
	{
		SpriteGlass.Clear ();

		foreach (Sprite img in obj) 
		{
			this.SpriteGlass.Add (img);
		}
	}

    public void SetValue(int value)
    {
        _value = value;
        txtvalue.text = _value.ToString();
    }
   
	public void SetOValue(int value)
	{
		originValue = value;
	}

    public void SetSprite(string type,string color)
    {
        if(type.Equals("match_object"))
        {
            _color = color;
            _sprite.sprite = GetSpriteColor(color);
        }
        else
        {
			//Debug.Log (type.ToString());
            _type_object = type;
            _sprite.sprite = GetSpriteObject(type);
        }
    }

    public Sprite[] spriteColor;
    private Sprite GetSpriteColor(string colorGoal)
    {
        switch (colorGoal)
        {
            case "blue":
                return spriteColor[0];
            case "green":
                return spriteColor[1];
            case "orange":
                return spriteColor[2];
            case "purple":
                return spriteColor[3];
            case "red":
                return spriteColor[4];
            case "yellow":
                return spriteColor[5];
            default:
                break;
        }

        return null;
    }

    public Sprite[] spriteObject;
    private Sprite GetSpriteObject(string objectGoal)
    {
        switch (objectGoal)
        {
            case "honey":
                return spriteObject[0];

            case "muffin":
                return spriteObject[1];

            case "cow":
                return spriteObject[2];

            case "coconut_2":
                return spriteObject[3];

            case "yogurt":
                return spriteObject[4];
        }

        return null;
    }

	IEnumerator ShowGlass()
	{
		yield return new WaitForSeconds (0.5f);
		Glass.SetActive (true);
		SoundController.sound.OrderSound ();
		Glass.GetComponent<Animation> ().Play ("StartAnimation");
	}

    public void InputOrder(Vector3 pos)
    {
        skeAnimation.GetComponent<Renderer>().sortingOrder = 0;
		AnimationWinOrLose(gameObject, Contance.DI, true);
		StartCoroutine (ShowGlass());
        iTween.MoveTo(gameObject, iTween.Hash("position", pos, "speed", 2, "easetype", iTween.EaseType.linear, "oncomplete", "UpInput", "oncompletetarget", gameObject));
    }

    private void UpInput()
    {
        iTween.MoveTo(gameObject, iTween.Hash("position", new Vector3(transform.position.x, transform.position.y, transform.position.z), "speed", 5f, "easetype", iTween.EaseType.linear, "oncomplete", "FinishInput", "oncompletetarget", gameObject));
        AnimationHit(gameObject, Contance.NGOI, false);
    }
    private void FinishInput()
    {
        actived = true;
        skeAnimation.GetComponent<Renderer>().sortingOrder = 1;
        Bg_item.SetActive(true);
        ScaleItem();
    }
    public void ScaleItem()
    {
		Vector3 originBgScale = Bg_item.transform.localScale;
		iTween.ScaleTo(Bg_item, iTween.Hash("scale", originBgScale*1.25f, "time", 0.1f));
		iTween.ScaleTo(Bg_item, iTween.Hash("scale", originBgScale, "time", 0.1f, "delay", 0.1f));
    }

    public void OutPutOrder()
    {
        skeAnimation.GetComponent<Renderer>().sortingOrder = 0;
        if (finish)
        {
            itemFinish.SetActive(true);
            textnumber.SetActive(false);
        }
        StartCoroutine(FinishOrder());
    }
    private void DownOutput()
    {
        Bg_item.SetActive(false);
        AnimationWinOrLose(gameObject, Contance.DI, true);
        iTween.MoveTo(gameObject, iTween.Hash("position", new Vector3(transform.position.x - 5,transform.position.y,transform.position.z), "speed", 5, "easetype", iTween.EaseType.linear, "oncomplete", "DestroyOrder", "oncompletetarget", gameObject));
    }

	public void UpgradeGlass()
	{
		int percent = (originValue - _value)*100/originValue;
		//Debug.Log ("originValue"+order.originValue);
		//Debug.Log ("_value"+order._value);
		//Debug.Log ("percent"+percent);

//		if(percent > 0 && percent < 25)
//		{
//			Glass.GetComponentInChildren<Image> ().sprite = SpriteGlass [0];
//		}
//		else
//		{
//			if (percent >= 25 && percent < 50) 
//			{
//				Glass.GetComponentInChildren<Image> ().sprite = SpriteGlass [1];
//			} 
//			else 
//			{
//				if (percent >= 50 && percent < 75) 
//				{
//					Glass.GetComponentInChildren<Image> ().sprite = SpriteGlass [2];
//				} 
//				else 
//				{
//					Glass.GetComponentInChildren<Image> ().sprite = SpriteGlass [3];
//				}
//			}
//		}
		int count = 0;
		foreach(Sprite img in SpriteGlass)
		{
			if(percent > count*100/SpriteGlass.Count && percent <= (count+1)*100/SpriteGlass.Count)
			{
				if (_type_object != "muffin") {
					Glass.transform.GetChild(0).GetComponent<Image> ().sprite = img;
				} 
				else 
				{
					Glass.transform.GetChild(1).GetComponent<Image> ().sprite = img;
					Glass.transform.GetChild (1).gameObject.SetActive (true);
				}
				break;
			}
			count++;
		}

		Glass.GetComponent<Animation> ().Play ("Glass_animation");
	}

    private void DestroyOrder()
    {
        Destroy(this.gameObject);
    }

    IEnumerator FinishOrder()
    {

		AnimationWinOrLose(gameObject, Contance.THANG, false);
		yield return new WaitForSeconds(timeWin);
        iTween.MoveTo(gameObject, iTween.Hash("position", new Vector3(transform.position.x, transform.position.y, transform.position.z), "speed", 5, "easetype", iTween.EaseType.linear, "oncomplete", "DownOutput", "oncompletetarget", gameObject));
    }

    public void AnimationHit(GameObject obj, string nameAnimation, bool loop)
    {
        if (!obj.GetComponent<SkeletonAnimation>().AnimationName.Equals(nameAnimation))
        {
            obj.GetComponent<SkeletonAnimation>().state.SetAnimation(0, nameAnimation, loop);
            obj.GetComponent<SkeletonAnimation>().state.AddAnimation(0, Contance.NGOI, true, 0f);
        }
    }

    public void AnimationWinOrLose(GameObject obj, string nameAnimation, bool loop)
    {
        if (!obj.GetComponent<SkeletonAnimation>().AnimationName.Equals(nameAnimation))
        {
            obj.GetComponent<SkeletonAnimation>().state.SetAnimation(0, nameAnimation, loop);
        }
    }
}

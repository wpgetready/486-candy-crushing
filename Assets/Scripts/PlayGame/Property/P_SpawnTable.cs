﻿using UnityEngine;
using System.Collections;

public class P_SpawnTable{
	private string type_SpawnTable;
	private float value_SpawnTable;
    private int data;


	public string Type_SpawnTable {
		get {
			return type_SpawnTable;
		}
		set {
			type_SpawnTable = value;
		}
	}

	public float Value_SpawnTable {
		get {
			return value_SpawnTable;
		}
		set {
			value_SpawnTable = value;
		}
	}

    public int Data {
        get {
            return data;
        }
        set {
            data = value;
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class P_ForcedSpawnQueue {
	private string type_ForcedSpawnQueue;
	private string value_ForcedSpawnQueue;

	public string Type_ForcedSpawnQueue {
		get {
			return type_ForcedSpawnQueue;
		}
		set {
			type_ForcedSpawnQueue = value;
		}
	}

	public string Value_ForcedSpawnQueue {
		get {
			return value_ForcedSpawnQueue;
		}
		set {
			value_ForcedSpawnQueue = value;
		}
	}
}

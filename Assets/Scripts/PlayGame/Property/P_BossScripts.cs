﻿using UnityEngine;
using System.Collections;

public struct P_BossScripts{

	public int quantity_B;
	public int data_B;
	public string object_type_B;
	public int delay_B;
	public string type_B;
	public int spawn_time_B;
	public string color_B;

	public P_BossScripts(int quantity, int data, string object_type, int delay, string type, int spawn_time, string color)
	{
		this.quantity_B = quantity;
		this.data_B = data;
		this.object_type_B = object_type;
		this.delay_B = delay;
		this.type_B = type;
		this.spawn_time_B = spawn_time;
		this.color_B = color;
	}
}

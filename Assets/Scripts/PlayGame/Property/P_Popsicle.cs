﻿using UnityEngine;
using System.Collections;

public struct P_Popsicle {

	public int col_Popsicle;
	public int row_Popsicle;
	public string data_Popsicle;
	public string type_Popsicle;

}

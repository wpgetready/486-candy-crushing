﻿using UnityEngine;
using System.Collections;

public struct P_Object {

	public string type_Object;
	public string color_Object;
	public int data_Object;
	public string modifierType_Object;
	public int row_Object;
	public int col_Object;
//	public P_Object(string type, string color, int data, int row, int col)
//	{
//		this.type_Object = type;
//		this.color_Object = color;
//		this.data_Object = data;
//		this.row_Object = row;
//		this.col_Object = col;
//	}
}

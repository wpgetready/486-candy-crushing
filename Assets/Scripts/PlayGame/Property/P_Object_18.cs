﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class P_Object_18 {

    public int row;
    public int column;
    public string type;
    public List<P_ForcedSpawnQueue> listForcedSpawn;
    public List<P_SpawnTable> listSpawnTable;
    public List<P_Color> listColor;
}

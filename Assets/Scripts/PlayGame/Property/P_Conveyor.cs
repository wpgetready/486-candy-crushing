﻿using UnityEngine;
using System.Collections;

public struct P_Conveyor
{
    public int row_Conveyor;
    public int column_Conveyor;
    public string data_Conveyor;
    public string type_Conveyor;
}

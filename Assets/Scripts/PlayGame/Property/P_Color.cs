﻿using UnityEngine;
using System.Collections;

public struct P_Color {
    public string color;
    public float value;
    public P_Color(string color, float value)
    {
        this.color = color;
        this.value = value;
    }
}

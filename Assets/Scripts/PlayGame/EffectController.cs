﻿using UnityEngine;
using System.Collections;

public class EffectController : MonoBehaviour {

    public GameObject[] effect_Sugar;
    public GameObject[] effect_Color;
    public GameObject effect_ice_cube;
    public GameObject effect_ice;

    public GameObject[] effectBreakerLeft;
    public GameObject[] effectBreakerRight;

    public GameObject[] effectBombBreak;
    public GameObject xRainbow;

    public LoadModeGame loadModeGame;
    public BossScript bossScript;
    public Clear_JuiceScript clear_Juice;

    public GameObject star;
    public GameObject starno;
    public Transform posStartStar;

	public GameObject smoke;

    public void CreateEffectTileType(Tile tile)
    {
        if(tile != null)
		{//Debug.Log("CreateEffectTileType...");
            switch(tile.tileData.tileType)
            {
			//Debug.log("CreateEffectTileType...");
                #region create effect color
                case TileData.TileType.match_object:

                    break;
                #endregion

                #region create effect sugar
                case TileData.TileType.sugar:
				if(tile.tileData.tileModifierType == TileData.TileModifierType.none)
				{
                    if(PoolingManager.poolingManager.List_Effect_Shock_White.Count > 0)
                    {
                        GameObject obj = PoolingManager.poolingManager.GetEffectShockColorFromPool(PoolingManager.poolingManager.List_Effect_Shock_White,tile);
                        LeanTween.scale(obj, new Vector3(1.1F, 1.1F, 1), 0.3f);
                        LeanTween.alpha(obj, 0, 0.3f);
                        LeanTween.delayedCall(0.3f, () =>
                        {
                            PoolingManager.poolingManager.AddShockColorToPool(obj, PoolingManager.poolingManager.List_Effect_Shock_White);
                        });
                    }
                    else
                    {
                        GameObject bgsugar = Instantiate(effect_Sugar[0], tile.transform.position, Quaternion.identity) as GameObject;
                        LeanTween.scale(bgsugar, new Vector3(1.1F, 1.1F, 1), 0.3f);
                        LeanTween.alpha(bgsugar, 0, 0.3f);
                        LeanTween.delayedCall(0.3f, () =>
                        {
                            PoolingManager.poolingManager.AddShockColorToPool(bgsugar, PoolingManager.poolingManager.List_Effect_Shock_White);
                        });
                    }

                    int dataSugar = (tile.tileData.data > 0) ? (tile.tileData.data) : tile.tileData.data - 1;
                    if (dataSugar > 0)
                    {
                        if (PoolingManager.poolingManager.List_Effect_Sugar.Count > 0)
                        {
                            GameObject obj = PoolingManager.poolingManager.GetEffectShockColorFromPool(PoolingManager.poolingManager.List_Effect_Sugar, tile);
                            LeanTween.delayedCall(0.45f, () =>
                            {
                                PoolingManager.poolingManager.AddOrderColorToPool(obj, PoolingManager.poolingManager.List_Effect_Sugar);
                            });
                        }
                        else
                        {
                            GameObject bgsugar = Instantiate(effect_Sugar[dataSugar], tile.transform.position, Quaternion.identity) as GameObject;
                            LeanTween.delayedCall(0.45f, () =>
                            {
                                PoolingManager.poolingManager.AddOrderColorToPool(bgsugar, PoolingManager.poolingManager.List_Effect_Sugar);
                            });
                        }
                    }
				}
                 	break;
                #endregion

                #region create effect muffin
                case TileData.TileType.muffin:
                    //int dataMuffin = (tile.tileData.data > 0) ? (tile.tileData.data - 1) : tile.tileData.data;
                    //print(dataMuffin);
                    //if(dataMuffin > 0)
                    //{
                    //     Instantiate(effect_Sugar[0], tile.transform.position, Quaternion.identity);
                    //}
                    //else
                    //{
                        if (GameManager.gameMode == GameManager.GameMode.ORDER_FULFILLMENT)
                        {
                            loadModeGame.CheckHaveOrder(GameConstants.muffin_tile_type, tile);
                        }
                        else if (GameManager.gameMode == GameManager.GameMode.BOSS_BATTLE)
                        {
                            StartCoroutine(bossScript.HandleEffectGoal(Contance.MUFFIN, tile));
                            //bossScript.HandleHealth();
                        }
                    //}
                    break;
                #endregion

                #region create effect cow
                case TileData.TileType.cow: 
                    if (GameManager.gameMode == GameManager.GameMode.ORDER_FULFILLMENT)
                    {
					//Debug.Log("cow...");
                        loadModeGame.CheckHaveOrder(GameConstants.cow_tile_type, tile);
                    }
                    else if (GameManager.gameMode == GameManager.GameMode.BOSS_BATTLE)
                    {
                        StartCoroutine(bossScript.HandleEffectGoal(Contance.COW, tile));
                        //bossScript.HandleHealth();
                    }
                    break;
                #endregion

                #region create effect Coconut_2
                case TileData.TileType.coconut_2:
                    if (PoolingManager.poolingManager.List_Effect_Shock_White.Count > 0)
                    {
                        GameObject obj = PoolingManager.poolingManager.GetEffectShockColorFromPool(PoolingManager.poolingManager.List_Effect_Shock_White, tile);
                        LeanTween.scale(obj, new Vector3(1.1F, 1.1F, 1), 0.3f);
                        LeanTween.alpha(obj, 0, 0.3f);
                        LeanTween.delayedCall(0.3f, () =>
                        {
                            PoolingManager.poolingManager.AddShockColorToPool(obj, PoolingManager.poolingManager.List_Effect_Shock_White);
                        });
                    }
                    else
                    {
                        GameObject bgsugar = Instantiate(effect_Sugar[0], tile.transform.position, Quaternion.identity) as GameObject;
                        LeanTween.scale(bgsugar, new Vector3(1.1F, 1.1F, 1), 0.3f);
                        LeanTween.alpha(bgsugar, 0, 0.3f);
                        LeanTween.delayedCall(0.3f, () =>
                        {
                            PoolingManager.poolingManager.AddShockColorToPool(bgsugar, PoolingManager.poolingManager.List_Effect_Shock_White);
                        });
                    }
                    if (GameManager.gameMode == GameManager.GameMode.ORDER_FULFILLMENT)
                    {
                        loadModeGame.CheckHaveOrder(GameConstants.coconut_2_tile_type, tile);
                    }
                    else if (GameManager.gameMode == GameManager.GameMode.BOSS_BATTLE)
                    {
                        StartCoroutine(bossScript.HandleEffectGoal(GameConstants.coconut_2_tile_type, tile));
                        //bossScript.HandleHealth();
                    }
                    break;
                #endregion

                #region create effect ice_cube
                case TileData.TileType.ice_cube:
                    Instantiate(effect_ice_cube,tile.transform.position,Quaternion.identity);
                    break;
                #endregion

                default:
                    break;
            }
        }
    }

    public void CreateEffectTileColor(Tile tile)
    {
        if(tile != null && tile.tileData.tileType == TileData.TileType.match_object)
        {
            switch(tile.tileData.tileColor)
            {
                case TileData.TileColor.blue:
                    if(PoolingManager.poolingManager.List_Effect_Shock_Blue.Count > 0)
                    {
                        GameObject blue = PoolingManager.poolingManager.GetEffectShockColorFromPool(PoolingManager.poolingManager.List_Effect_Shock_Blue,tile);
					LeanTween.scale(blue, new Vector3(1.1F, 1.1F, 1), 0.3f).setLoopOnce();
					LeanTween.alpha(blue, 0, 0.3f).setLoopOnce();
                        LeanTween.delayedCall(blue, 0.3f, () =>
                        {
                            PoolingManager.poolingManager.AddShockColorToPool(blue,PoolingManager.poolingManager.List_Effect_Shock_Blue);
						}).setLoopOnce();
                    }
                    else
                    {
                        GameObject blue = Instantiate(effect_Color[0], tile.transform.position, Quaternion.identity) as GameObject;
					LeanTween.scale(blue, new Vector3(1.1F, 1.1F, 1), 0.3f).setLoopOnce();
					LeanTween.alpha(blue, 0, 0.3f).setLoopOnce();
                        LeanTween.delayedCall(blue, 0.3f, () =>
                        {
							PoolingManager.poolingManager.AddShockColorToPool(blue, PoolingManager.poolingManager.List_Effect_Shock_Blue);
						}).setLoopOnce();
                    }
                    if (GameManager.gameMode == GameManager.GameMode.ORDER_FULFILLMENT)
                    {
                        loadModeGame.CheckHaveOrder(GameConstants.blue_tile_color,tile);
                    }
                    else if (GameManager.gameMode == GameManager.GameMode.BOSS_BATTLE)
                    {
                        StartCoroutine(bossScript.HandleEffectGoal(GameConstants.blue_tile_color,tile));
                         //bossScript.HandleHealth();
                    }
                    break;
                case TileData.TileColor.green:
                    if (PoolingManager.poolingManager.List_Effect_Shock_Green.Count > 0)
                    {
                        GameObject green = PoolingManager.poolingManager.GetEffectShockColorFromPool(PoolingManager.poolingManager.List_Effect_Shock_Green, tile);
					LeanTween.scale(green, new Vector3(1.1F, 1.1F, 1), 0.3f).setLoopOnce();
					LeanTween.alpha(green, 0, 0.3f).setLoopOnce();
                        LeanTween.delayedCall(green, 0.3f, () =>
                        {
							PoolingManager.poolingManager.AddShockColorToPool(green, PoolingManager.poolingManager.List_Effect_Shock_Green);
						}).setLoopOnce();
                    }
                    else
                    {
                        GameObject green = Instantiate(effect_Color[1], tile.transform.position, Quaternion.identity) as GameObject;
					LeanTween.scale(green, new Vector3(1.1F, 1.1F, 1), 0.3f).setLoopOnce();
					LeanTween.alpha(green, 0, 0.3f).setLoopOnce();
                        LeanTween.delayedCall(green, 0.3f, () =>
                        {
							PoolingManager.poolingManager.AddShockColorToPool(green, PoolingManager.poolingManager.List_Effect_Shock_Green);
						}).setLoopOnce();
                    }
                    if (GameManager.gameMode == GameManager.GameMode.ORDER_FULFILLMENT)
                    {
                        loadModeGame.CheckHaveOrder(GameConstants.green_tile_color, tile);

                    }
                    else if (GameManager.gameMode == GameManager.GameMode.BOSS_BATTLE)
                    {
                        StartCoroutine(bossScript.HandleEffectGoal(GameConstants.green_tile_color, tile));
                    }
                    break;
                case TileData.TileColor.orange:
                    if (PoolingManager.poolingManager.List_Effect_Shock_Orange.Count > 0)
                    {
                        GameObject orange = PoolingManager.poolingManager.GetEffectShockColorFromPool(PoolingManager.poolingManager.List_Effect_Shock_Orange, tile);
					LeanTween.scale(orange, new Vector3(1.1F, 1.1F, 1), 0.3f).setLoopOnce();
					LeanTween.alpha(orange, 0, 0.3f).setLoopOnce();
                        LeanTween.delayedCall(orange, 0.3f, () =>
                        {
							PoolingManager.poolingManager.AddShockColorToPool(orange, PoolingManager.poolingManager.List_Effect_Shock_Orange);
						}).setLoopOnce();
                    }
                    else
                    {
                        GameObject orange = Instantiate(effect_Color[2], tile.transform.position, Quaternion.identity) as GameObject;
					LeanTween.scale(orange, new Vector3(1.1F, 1.1F, 1), 0.3f).setLoopOnce();
					LeanTween.alpha(orange, 0, 0.3f).setLoopOnce();
                        LeanTween.delayedCall(orange, 0.3f, () =>
                        {
							PoolingManager.poolingManager.AddShockColorToPool(orange, PoolingManager.poolingManager.List_Effect_Shock_Orange);
						}).setLoopOnce();
                    }
                    if (GameManager.gameMode == GameManager.GameMode.ORDER_FULFILLMENT)
                    {
                        loadModeGame.CheckHaveOrder(GameConstants.orange_tile_color, tile);
                    }
                    else if (GameManager.gameMode == GameManager.GameMode.BOSS_BATTLE)
                    {
                        StartCoroutine(bossScript.HandleEffectGoal(GameConstants.orange_tile_color, tile));
                    }
                    break;
                case TileData.TileColor.purple:
                    if (PoolingManager.poolingManager.List_Effect_Shock_Purple.Count > 0)
                    {
                        GameObject purple = PoolingManager.poolingManager.GetEffectShockColorFromPool(PoolingManager.poolingManager.List_Effect_Shock_Purple, tile);
					LeanTween.scale(purple, new Vector3(1.1F, 1.1F, 1), 0.3f).setLoopOnce();
					LeanTween.alpha(purple, 0, 0.3f).setLoopOnce();
                        LeanTween.delayedCall(purple, 0.3f, () =>
                        {
							PoolingManager.poolingManager.AddShockColorToPool(purple, PoolingManager.poolingManager.List_Effect_Shock_Purple);
						}).setLoopOnce();
                    }
                    else
                    {
                        GameObject purple = Instantiate(effect_Color[3], tile.transform.position, Quaternion.identity) as GameObject;
					LeanTween.scale(purple, new Vector3(1.1F, 1.1F, 1), 0.3f).setLoopOnce();
					LeanTween.alpha(purple, 0, 0.3f).setLoopOnce();
                        LeanTween.delayedCall(purple, 0.3f, () =>
                        {
							PoolingManager.poolingManager.AddShockColorToPool(purple, PoolingManager.poolingManager.List_Effect_Shock_Purple);
						}).setLoopOnce();
                    }
                    if (GameManager.gameMode == GameManager.GameMode.ORDER_FULFILLMENT)
                    {
                        loadModeGame.CheckHaveOrder(GameConstants.purple_tile_color, tile);
                    }
                    else if (GameManager.gameMode == GameManager.GameMode.BOSS_BATTLE)
                    {
                        StartCoroutine(bossScript.HandleEffectGoal(GameConstants.purple_tile_color, tile));
                    }
                    break;
                case TileData.TileColor.red:
                    if (PoolingManager.poolingManager.List_Effect_Shock_Red.Count > 0)
                    {
						GameObject red = PoolingManager.poolingManager.GetEffectShockColorFromPool(PoolingManager.poolingManager.List_Effect_Shock_Red, tile);
					LeanTween.scale(red, new Vector3(1.1F, 1.1F, 1), 0.3f).setLoopOnce();
					LeanTween.alpha(red, 0, 0.3f).setLoopOnce();
                        LeanTween.delayedCall(red, 0.3f, () =>
                        {
							PoolingManager.poolingManager.AddShockColorToPool(red, PoolingManager.poolingManager.List_Effect_Shock_Red);
						}).setLoopOnce();
                    }
                    else
                    {
                        GameObject red = Instantiate(effect_Color[4], tile.transform.position, Quaternion.identity) as GameObject;
					LeanTween.scale(red, new Vector3(1.1F, 1.1F, 1), 0.3f).setLoopOnce();
					LeanTween.alpha(red, 0, 0.3f).setLoopOnce();
                        LeanTween.delayedCall(red, 0.3f, () =>
                        {
							PoolingManager.poolingManager.AddShockColorToPool(red, PoolingManager.poolingManager.List_Effect_Shock_Red);
						}).setLoopOnce();
                    }
                    if (GameManager.gameMode == GameManager.GameMode.ORDER_FULFILLMENT)
                    {
                        loadModeGame.CheckHaveOrder(GameConstants.red_tile_color, tile);
                    }
                    else if (GameManager.gameMode == GameManager.GameMode.BOSS_BATTLE)
                    {
                      StartCoroutine(bossScript.HandleEffectGoal(GameConstants.red_tile_color, tile));
                    }
                    break;
                case TileData.TileColor.yellow:
                    if (PoolingManager.poolingManager.List_Effect_Shock_Yellow.Count > 0)
                    {
                        GameObject yellow = PoolingManager.poolingManager.GetEffectShockColorFromPool(PoolingManager.poolingManager.List_Effect_Shock_Yellow, tile);
					LeanTween.scale(yellow, new Vector3(1.1F, 1.1F, 1), 0.3f).setLoopOnce();
					LeanTween.alpha(yellow, 0, 0.3f).setLoopOnce();
                        LeanTween.delayedCall(yellow, 0.3f, () =>
                        {
							PoolingManager.poolingManager.AddShockColorToPool(yellow, PoolingManager.poolingManager.List_Effect_Shock_Yellow);
						}).setLoopOnce();
                    }
                    else
                    {
                        GameObject yellow = Instantiate(effect_Color[5], tile.transform.position, Quaternion.identity) as GameObject;
					LeanTween.scale(yellow, new Vector3(1.1F, 1.1F, 1), 0.3f).setLoopOnce();
					LeanTween.alpha(yellow, 0, 0.3f).setLoopOnce();
                        LeanTween.delayedCall(yellow, 0.3f, () =>
                        {
                            PoolingManager.poolingManager.AddShockColorToPool(yellow, PoolingManager.poolingManager.List_Effect_Shock_Yellow);
						}).setLoopOnce();
                    }
                    if (GameManager.gameMode == GameManager.GameMode.ORDER_FULFILLMENT)
                    {
                        loadModeGame.CheckHaveOrder(GameConstants.yellow_tile_color, tile);
                    }
                    else if (GameManager.gameMode == GameManager.GameMode.BOSS_BATTLE)
                    {
                        StartCoroutine(bossScript.HandleEffectGoal(GameConstants.yellow_tile_color, tile));
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public void CollectFood(GameObject obj, Vector3 start, Vector3 end,Tile tile)
    {
        iTween.MoveTo(obj, iTween.Hash("path", new Vector3[] { start, new Vector3(Random.Range(-1f, 1f), Random.Range(start.y, end.y), 0), end }, "time", 0.35f, "easetype", iTween.EaseType.linear, "oncomplete", "CreateEff", "oncompletetarget", gameObject,"oncompleteparams",tile));
    }

    private void CreateEff(Tile posTile)
    {
        if(PoolingManager.poolingManager.List_Effect_bonus_star.Count > 0)
        {
            GameObject obj = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_bonus_star,posTile);
            LeanTween.delayedCall(0.7f, () =>
            {
                PoolingManager.poolingManager.AddBonusToPool(obj, PoolingManager.poolingManager.List_Effect_bonus_star);
				}).setLoopOnce();
        }else
        {
            GameObject obj = Instantiate(starno, posTile.transform.position, Quaternion.identity) as GameObject;
            LeanTween.delayedCall(0.7f, () =>
            {
                PoolingManager.poolingManager.AddBonusToPool(obj, PoolingManager.poolingManager.List_Effect_bonus_star);
				}).setLoopOnce();
            //GamePlayController.gamePlayController.RandomTileType(posTile);
        }
        GamePlayController.gamePlayController.RandomTileType(posTile);
    }

    public void CreateEffectDynamic(Grid posGrid)
    {
        if (posGrid.gridData.gridDynamicType == GridData.GridDynamicType.HONEY_GRID)
        {
            if (GameManager.gameMode == GameManager.GameMode.ORDER_FULFILLMENT)
            {
                loadModeGame.CheckHaveOrder(GameConstants.honey_grid, posGrid.GetTile());
            }
            else if (GameManager.gameMode == GameManager.GameMode.BOSS_BATTLE)
            {
                StartCoroutine(bossScript.HandleEffectGoal(GameConstants.honey_grid, posGrid.GetTile()));
            }
        }
        else if (posGrid.gridData.gridDynamicType == GridData.GridDynamicType.ICE_GRID)
        {
            if(PoolingManager.poolingManager.List_Effect_Ice.Count > 0)
            {
                GameObject obj = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Ice,posGrid.GetTile());
                LeanTween.delayedCall(0.7f, () =>
                {
                    PoolingManager.poolingManager.AddOrderColorToPool(obj, PoolingManager.poolingManager.List_Effect_Ice);
					}).setLoopOnce();
            }
            else
            {
               GameObject obj = Instantiate(effect_ice, posGrid.transform.position, Quaternion.identity) as GameObject;
               LeanTween.delayedCall(0.7f, () =>
               {
                   PoolingManager.poolingManager.AddOrderColorToPool(obj, PoolingManager.poolingManager.List_Effect_Ice);
					}).setLoopOnce();
            }
            
        }
        else if (posGrid.gridData.gridDynamicType == GridData.GridDynamicType.CRACKER_GRID)
        {
            //Instantiate(effect_ice, posGrid.transform.position, Quaternion.identity);
            //print("clear_juice");
            clear_Juice.MoveCrackFinish(posGrid);
        }
    }


    public void CreateEffectRowBreaker(Grid posGrid,int Xcell,int Ycell, bool left)
    {
        float time = 0.07f * Xcell;
		if (left && posGrid.GetTile() != null)
        {
           switch(posGrid.GetTile().tileData.tileColor)
           {
               case TileData.TileColor.blue:
                    GameObject effectRowBlue = Instantiate(effectBreakerLeft[0], posGrid.transform.position, Quaternion.identity) as GameObject;
				LeanTween.scale(effectRowBlue, new Vector3(0.3f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowBlue, time);
                    break;
               case TileData.TileColor.green:
                    GameObject effectRowGreen = Instantiate(effectBreakerLeft[1], posGrid.transform.position, Quaternion.identity) as GameObject;
				LeanTween.scale(effectRowGreen, new Vector3(0.3f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowGreen, time);
                    break;
               case TileData.TileColor.orange:
                    GameObject effectRowOrange = Instantiate(effectBreakerLeft[2], posGrid.transform.position, Quaternion.identity) as GameObject;
				LeanTween.scale(effectRowOrange, new Vector3(0.3f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowOrange, time);
                    break;
               case TileData.TileColor.purple:
                    GameObject effectRowPurple = Instantiate(effectBreakerLeft[3], posGrid.transform.position, Quaternion.identity) as GameObject;
				LeanTween.scale(effectRowPurple, new Vector3(0.3f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowPurple, time);
                    break;
               case TileData.TileColor.red:
                    GameObject effectRowRed = Instantiate(effectBreakerLeft[4], posGrid.transform.position, Quaternion.identity) as GameObject;
				LeanTween.scale(effectRowRed, new Vector3(0.3f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowRed, time);
                    break;
               case TileData.TileColor.yellow:
                    GameObject effectRowYellow = Instantiate(effectBreakerLeft[5], posGrid.transform.position, Quaternion.identity) as GameObject;
				LeanTween.scale(effectRowYellow, new Vector3(0.3f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowYellow, time);
                    break;
           }
            
        }
        else
        {
            switch (posGrid.GetTile().tileData.tileColor)
            {
                case TileData.TileColor.blue:
                    GameObject effectRowBlue = Instantiate(effectBreakerRight[0], posGrid.transform.position, Quaternion.identity) as GameObject;
				LeanTween.scale(effectRowBlue, new Vector3(0.3f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowBlue, time);
                    break;
                case TileData.TileColor.green:
                    GameObject effectRowGreen = Instantiate(effectBreakerRight[1], posGrid.transform.position, Quaternion.identity) as GameObject;
				LeanTween.scale(effectRowGreen, new Vector3(0.3f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowGreen, time);
                    break;
                case TileData.TileColor.orange:
                    GameObject effectRowOrange = Instantiate(effectBreakerRight[2], posGrid.transform.position, Quaternion.identity) as GameObject;
				LeanTween.scale(effectRowOrange, new Vector3(0.3f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowOrange, time);
                    break;
                case TileData.TileColor.purple:
                    GameObject effectRowPurple = Instantiate(effectBreakerRight[3], posGrid.transform.position, Quaternion.identity) as GameObject;
				LeanTween.scale(effectRowPurple, new Vector3(0.3f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowPurple, time);
                    break;
                case TileData.TileColor.red:
                    GameObject effectRowRed = Instantiate(effectBreakerRight[4], posGrid.transform.position, Quaternion.identity) as GameObject;
				LeanTween.scale(effectRowRed, new Vector3(0.3f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowRed, time);
                    break;
                case TileData.TileColor.yellow:
                    GameObject effectRowYellow = Instantiate(effectBreakerRight[5], posGrid.transform.position, Quaternion.identity) as GameObject;
				LeanTween.scale(effectRowYellow, new Vector3(0.3f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowYellow, time);
                    break;
            }
        }
    }

   

    public void CreateEffectColumnBreaker(Grid posGrid, int Xcell, int Ycell, bool top, float angle)
    {
        float time = Xcell * 0.07f;
		if (top == true && posGrid != null && posGrid.GetTile() != null)
        {
           switch(posGrid.GetTile().tileData.tileColor)
           {
               case TileData.TileColor.blue:
                    GameObject effectRowBlue = Instantiate(effectBreakerLeft[0], posGrid.transform.position, Quaternion.identity) as GameObject;
				LeanTween.rotate(effectRowBlue, new Vector3(0, 0, angle), 0.01f).setLoopOnce();
				LeanTween.scale(effectRowBlue, new Vector3(0.35f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowBlue, time);
                    break;
               case TileData.TileColor.green:
                    GameObject effectRowGreen = Instantiate(effectBreakerLeft[1], posGrid.transform.position, Quaternion.identity) as GameObject;
				LeanTween.rotate(effectRowGreen, new Vector3(0, 0, angle), 0.01f).setLoopOnce();
				LeanTween.scale(effectRowGreen, new Vector3(0.35f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowGreen, time);
                    break;
               case TileData.TileColor.orange:
                    GameObject effectRowOrange = Instantiate(effectBreakerLeft[2], posGrid.transform.position, Quaternion.identity) as GameObject;
				LeanTween.rotate(effectRowOrange, new Vector3(0, 0, angle), 0.01f).setLoopOnce();
				LeanTween.scale(effectRowOrange, new Vector3(0.35f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowOrange, time);
                    break;
               case TileData.TileColor.purple:
                    GameObject effectRowPurple = Instantiate(effectBreakerLeft[3], posGrid.transform.position, Quaternion.identity) as GameObject;
				LeanTween.rotate(effectRowPurple, new Vector3(0, 0, angle), 0.01f).setLoopOnce();
				LeanTween.scale(effectRowPurple, new Vector3(0.35f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowPurple, time);
                    break;
               case TileData.TileColor.red:
                    GameObject effectRowRed = Instantiate(effectBreakerLeft[4], posGrid.transform.position, Quaternion.identity) as GameObject;
				LeanTween.rotate(effectRowRed, new Vector3(0, 0, angle), 0.01f).setLoopOnce();
				LeanTween.scale(effectRowRed, new Vector3(0.35f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowRed, time);
                    break;
               case TileData.TileColor.yellow:
                    GameObject effectRowYellow = Instantiate(effectBreakerLeft[5], posGrid.transform.position, Quaternion.identity) as GameObject;
				LeanTween.rotate(effectRowYellow, new Vector3(0, 0, angle), 0.01f).setLoopOnce();
				LeanTween.scale(effectRowYellow, new Vector3(0.35f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowYellow, time);
                    break;
           }
        }
        else
        {
			if(posGrid != null && posGrid.GetTile() != null)
			{
            switch (posGrid.GetTile().tileData.tileColor)
            {
                case TileData.TileColor.blue:
                    GameObject effectRowBlue = Instantiate(effectBreakerRight[0], posGrid.transform.position, Quaternion.identity) as GameObject;
					LeanTween.rotate(effectRowBlue, new Vector3(0, 0, angle), 0.01f).setLoopOnce();
					LeanTween.scale(effectRowBlue, new Vector3(0.35f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowBlue, time);
                    break;
                case TileData.TileColor.green:
                    GameObject effectRowGreen = Instantiate(effectBreakerRight[1], posGrid.transform.position, Quaternion.identity) as GameObject;
					LeanTween.rotate(effectRowGreen, new Vector3(0, 0, angle), 0.01f).setLoopOnce();
					LeanTween.scale(effectRowGreen, new Vector3(0.35f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowGreen, time);
                    break;
                case TileData.TileColor.orange:
                    GameObject effectRowOrange = Instantiate(effectBreakerRight[2], posGrid.transform.position, Quaternion.identity) as GameObject;
					LeanTween.rotate(effectRowOrange, new Vector3(0, 0, angle), 0.01f).setLoopOnce();
					LeanTween.scale(effectRowOrange, new Vector3(0.35f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowOrange, time);
                    break;
                case TileData.TileColor.purple:
                    GameObject effectRowPurple = Instantiate(effectBreakerRight[3], posGrid.transform.position, Quaternion.identity) as GameObject;
					LeanTween.rotate(effectRowPurple, new Vector3(0, 0, angle), 0.01f).setLoopOnce();
					LeanTween.scale(effectRowPurple, new Vector3(0.35f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowPurple, time);
                    break;
                case TileData.TileColor.red:
                    GameObject effectRowRed = Instantiate(effectBreakerRight[4], posGrid.transform.position, Quaternion.identity) as GameObject;
					LeanTween.rotate(effectRowRed, new Vector3(0, 0, angle), 0.01f).setLoopOnce();
					LeanTween.scale(effectRowRed, new Vector3(0.33f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowRed, time);
                    break;
                case TileData.TileColor.yellow:
                    GameObject effectRowYellow = Instantiate(effectBreakerRight[5], posGrid.transform.position, Quaternion.identity) as GameObject;
					LeanTween.rotate(effectRowYellow, new Vector3(0, 0, angle), 0.01f).setLoopOnce();
					LeanTween.scale(effectRowYellow, new Vector3(0.35f * Xcell, 1 * Ycell, 1), time).setLoopOnce();
                    Destroy(effectRowYellow, time);
                    break;
            }
        }
		}
    }

    public void CreateEffectBombBreaker(Grid posGrid,float time)
    {
        switch (posGrid.GetTile().tileData.tileColor)
        {
            case TileData.TileColor.blue:
                GameObject effectRowBlue = Instantiate(effectBombBreak[0], posGrid.transform.position, Quaternion.identity) as GameObject;
                Destroy(effectRowBlue, time);
                break;
            case TileData.TileColor.green:
                GameObject effectRowGreen = Instantiate(effectBombBreak[1], posGrid.transform.position, Quaternion.identity) as GameObject;
                Destroy(effectRowGreen, time);
                break;
            case TileData.TileColor.orange:
                GameObject effectRowOrange = Instantiate(effectBombBreak[2], posGrid.transform.position, Quaternion.identity) as GameObject;
                Destroy(effectRowOrange, time);
                break;
            case TileData.TileColor.purple:
                GameObject effectRowPurple = Instantiate(effectBombBreak[3], posGrid.transform.position, Quaternion.identity) as GameObject;
                Destroy(effectRowPurple, time);
                break;
            case TileData.TileColor.red:
                GameObject effectRowRed = Instantiate(effectBombBreak[4], posGrid.transform.position, Quaternion.identity) as GameObject;
                Destroy(effectRowRed, time);
                break;
            case TileData.TileColor.yellow:
                GameObject effectRowYellow = Instantiate(effectBombBreak[5], posGrid.transform.position, Quaternion.identity) as GameObject;
                Destroy(effectRowYellow, time);
                break;
        }
    }

    public void CreateXRainBow(Grid posGrid, float time)
    {
        GameObject rainbow = Instantiate(xRainbow,posGrid.transform.position,Quaternion.identity) as GameObject;
        Destroy(rainbow, time);
    }

    public void CreateBombRainBow(Grid posGrid, float time, float angle)
    {
        GameObject rainbow = Instantiate(xRainbow, posGrid.transform.position, Quaternion.identity) as GameObject;
		LeanTween.rotate(rainbow, new Vector3(0, 0, angle), 0.01f).setLoopOnce();
        Destroy(rainbow, time);
    }

    public void CreateEffectYogurt(Tile posTile)
    {
        if (GameManager.gameMode == GameManager.GameMode.ORDER_FULFILLMENT)
        {
            loadModeGame.CheckHaveOrder(GameConstants.yogurt_tile_type, posTile);
        }
        else if (GameManager.gameMode == GameManager.GameMode.BOSS_BATTLE)
        {
            StartCoroutine(bossScript.HandleEffectGoal(GameConstants.yogurt_tile_type, posTile));
        }
    }


    public void CreateEffectStarEnd(Tile tile)
    {
        if(PoolingManager.poolingManager.List_bonus_star.Count > 0)
        {
            GameObject obj = PoolingManager.poolingManager.GetEffectBonusFromPool(PoolingManager.poolingManager.List_bonus_star,posStartStar.position);
			SoundController.sound.StarSoundPlay ();
            CollectFood(obj, posStartStar.position, tile.transform.position, tile);
            LeanTween.delayedCall(0.45f, () =>
            {
                PoolingManager.poolingManager.AddBonusToPool(obj, PoolingManager.poolingManager.List_bonus_star);
				}).setLoopOnce();
        }
        else
        {
            GameObject obj = Instantiate(star, posStartStar.position, Quaternion.identity) as GameObject;
			SoundController.sound.StarSoundPlay ();
            CollectFood(obj, posStartStar.position, tile.transform.position, tile);
            LeanTween.delayedCall(0.45f, () =>
            {
                PoolingManager.poolingManager.AddBonusToPool(obj, PoolingManager.poolingManager.List_bonus_star);
				}).setLoopOnce();
        }
    }


    public GameObject[] rocket;
	public void CreateEffectRocket(Tile posSource, Tile posDest, TileData.TileType tileType)
    {
		GameObject _smoke = Instantiate (smoke, Vector3.zero, Quaternion.identity) as GameObject;
        GameObject objRocket = SetColorRocket(posSource);
		SoundController.sound.rocket_Sound ();
        float angle = Mathf.Atan2(posDest.transform.position.y - posSource.transform.position.y, posDest.transform.position.x - posSource.transform.position.x);
        angle = angle * Mathf.Rad2Deg - 90;

        objRocket.transform.eulerAngles = new Vector3(0,0,angle);

		posDest.isRocket = true;
		float length = Vector3.Distance (posDest.transform.position, objRocket.transform.position);
		LeanTween.move(objRocket, posDest.transform.position, length*0.4f);
		LeanTween.delayedCall(objRocket, length*0.4f-0.15f, () =>
        {
				LeanTween.scale(objRocket,new Vector3(0f,0f,1),0.2f).setOnComplete(() => {
					_smoke.transform.position = objRocket.transform.position;
					_smoke.GetComponent<SpriteRenderer>().enabled = true;
					LeanTween.scale(_smoke,new Vector3(0.55f,0.55f,1),0.35f).setOnComplete(() => {
						Destroy(_smoke);
					});
				});
				Destroy(objRocket, 1f);
        });

		LeanTween.delayedCall(posDest.gameObject, length*0.4f+0.1f, () =>
			{
				posDest.TransformInto(new TileData(tileType, posSource.tileData.tileColor));

				LeanTween.rotateAround(posDest.gameObject, Vector3.forward, 360f, 
					GameConstants.TIME_TRANSFORM_PROCESS).setOnComplete(() =>
						{
							posDest.isTransforming = false;
						});

				if (GamePlayController.gamePlayController.gameInterceptorCount > 0)
				{
					GamePlayController.gamePlayController.gameInterceptorCount--;
				}
			});
    }

    public GameObject SetColorRocket(Tile colorTile)
    {
        GameObject objRocket = null;
        switch(colorTile.tileData.tileColor)
        {
            case TileData.TileColor.blue:
                objRocket = Instantiate(rocket[0], colorTile.transform.position, Quaternion.identity) as GameObject;
                LeanTween.scale(objRocket, new Vector3(0.8f, 0.8f, 1), 0.1f);
                break;
            case TileData.TileColor.green:
                objRocket = Instantiate(rocket[1], colorTile.transform.position, Quaternion.identity) as GameObject;
                LeanTween.scale(objRocket, new Vector3(0.8f, 0.8f, 1), 0.1f);
                break;
            case TileData.TileColor.orange:
                objRocket = Instantiate(rocket[2], colorTile.transform.position, Quaternion.identity) as GameObject;
                LeanTween.scale(objRocket, new Vector3(0.8f, 0.8f, 1), 0.1f);
                break;
            case TileData.TileColor.purple:
                objRocket = Instantiate(rocket[3], colorTile.transform.position, Quaternion.identity) as GameObject;
                LeanTween.scale(objRocket, new Vector3(0.8f, 0.8f, 1), 0.1f);
                break;
            case TileData.TileColor.red:
                objRocket = Instantiate(rocket[4], colorTile.transform.position, Quaternion.identity) as GameObject;
                LeanTween.scale(objRocket, new Vector3(0.8f, 0.8f, 1), 0.1f);
                break;
            case TileData.TileColor.yellow:
                objRocket = Instantiate(rocket[5], colorTile.transform.position, Quaternion.identity) as GameObject;
                LeanTween.scale(objRocket, new Vector3(0.8f, 0.8f, 1), 0.1f);
                break;
            default:
                break;
        }
        return objRocket;
    }
}

﻿using UnityEngine;
using System.Collections;
using System;
using SimpleJSON;
using System.Collections.Generic;

public class ReadJson : MonoBehaviour {

    private string jsonString;
	public TextAsset level;

    public void ReadDataJson( string json,int numberlevel)
    {
		//Debug.Log (numberlevel);
		//Debug.Log (json);
        Contance.ResetData();
		var ST = JSON.Parse(json);
		var N = JSONNode.Parse (ST["data"].ToString());
		//Debug.Log (N);

		Contance.width = N["level"][numberlevel]["width"].AsInt;//Debug.Log (N["level"][numberlevel]["width"]);
        Contance.height = N["level"][numberlevel]["height"].AsInt;

            //print (N["level"][numberlevel]["title"].Value);  // title cua MAP
            Contance.TITLE = N["level"][numberlevel]["title"].Value;
            //print (N["level"][numberlevel]["width"].AsInt);   // chieu rong cua MAP
            //print (N["level"][numberlevel]["height"].AsInt);  // chieu cao cua MAP

			//===========================================Spawn_table=================================================
			if(N["level"][numberlevel]["spawn_table"].Count > 0)
			{
                //print ("has spawn_table");
				Contance.LIST_SPAWNTABLE = new List<P_SpawnTable>();
				for(int j = 0; j < N["level"][numberlevel]["spawn_table"].Count; j++)
				{
					P_SpawnTable p_spawntable = new P_SpawnTable();
//					print (N["level"][0]["spawn_table"][j]["type"].Value);
					p_spawntable.Type_SpawnTable = N["level"][numberlevel]["spawn_table"][j]["type"].Value;
//					print (N["level"][0]["spawn_table"][j]["percent"].AsFloat);
					p_spawntable.Value_SpawnTable = N["level"][numberlevel]["spawn_table"][j]["percent"].AsFloat;
                    foreach (string key in N["level"][numberlevel]["spawn_table"][j].Keys)
                    {
                        if (key == "data")
                        {
                            p_spawntable.Data = N["level"][numberlevel]["spawn_table"][j]["data"].AsInt;
                        }
                    }
					Contance.LIST_SPAWNTABLE.Add(p_spawntable);
				}
			}
            //else
            //{
            //    //print ("have not spawn_table");
            //}

			//===========================================forced_spawn_queue=================================================
			if(N["level"][numberlevel]["forced_spawn_queue"].Count > 0)
			{
                //print ("has forced_spawn_queue");
				Contance.LIST_FORCEDSPAWNQUEUE = new List<P_ForcedSpawnQueue>();
				for(int j = 0; j < N["level"][numberlevel]["forced_spawn_queue"].Count; j++)
				{
					P_ForcedSpawnQueue p_forcedspawnqueue = new P_ForcedSpawnQueue();
//					print (N["level"][0]["forced_spawn_queue"][j]["type"].Value);
					p_forcedspawnqueue.Type_ForcedSpawnQueue = N["level"][numberlevel]["forced_spawn_queue"][j]["type"].Value;
//					print (N["level"][0]["forced_spawn_queue"][j]["color"].Value);
					p_forcedspawnqueue.Value_ForcedSpawnQueue = N["level"][numberlevel]["forced_spawn_queue"][j]["color"].Value;

					Contance.LIST_FORCEDSPAWNQUEUE.Add(p_forcedspawnqueue);
				}
			}
            //else
            //{
            //    print ("have not forced_spawn_queue");
            //}

			//===========================================bossScripts=================================================
			if(N["level"][numberlevel]["bossScripts"].Count > 0)
			{
                //print ("has bossScripts");
				for(int j = 0; j < N["level"][numberlevel]["bossScripts"].Count; j++)
				{
                    //print (N["level"][numberlevel]["bossScripts"][j]["quantity"].AsInt);
                    //print (N["level"][numberlevel]["bossScripts"][j]["data"].AsInt);
                    //print (N["level"][numberlevel]["bossScripts"][j]["object_type"].Value);
                    //print (N["level"][numberlevel]["bossScripts"][j]["delay"].AsInt);
                    //print (N["level"][numberlevel]["bossScripts"][j]["type"].Value);
                    //print (N["level"][numberlevel]["bossScripts"][j]["spawn_time"].AsInt);
                    //print (N["level"][numberlevel]["bossScripts"][j]["color"].Value);

					Contance.bossScripts = new P_BossScripts(N["level"][numberlevel]["bossScripts"][j]["quantity"].AsInt,
					                                         N["level"][numberlevel]["bossScripts"][j]["data"].AsInt,
					                                         N["level"][numberlevel]["bossScripts"][j]["object_type"].Value,
					                                         N["level"][numberlevel]["bossScripts"][j]["delay"].AsInt,
					                                         N["level"][numberlevel]["bossScripts"][j]["type"].Value,
					                                         N["level"][numberlevel]["bossScripts"][j]["spawn_time"].AsInt,
					                                         N["level"][numberlevel]["bossScripts"][j]["color"].Value);
				}
			}
            //else
            //{
            //    print ("have not bossScripts");
            //}

			//===========================================numericId=================================================
            //print (N["level"][numberlevel]["numericId"].AsInt);

			//===========================================popsiclesToCollect=================================================
            //print (N["level"][numberlevel]["popsiclesToCollect"].AsInt);
            //Contance.popsiclesToCollect = N["level"][numberlevel]["popsiclesToCollect"].AsInt;
			
			//===========================================objects=================================================
			var s = N["level"][numberlevel]["objects"];
			Contance.LIST_OBJECT = new List<P_Object>();
			foreach (string key in s.Keys) {

                //Debug.Log(key+":"+N["level"][numberlevel]["objects"][key]["type"].Value);
				Index ind = ReadString(key);
				P_Object obj = new P_Object();
				obj.row_Object = ind.row;
				obj.col_Object = ind.col;
				//var s1 = N["level"][numberlevel]["objects"][key];
//				foreach(string key_s1 in s1.Keys)
//				{
//					if(key_s1.Equals("type"))
						obj.type_Object = N["level"][numberlevel]["objects"][key]["type"].Value;
					//else if(key_s1.Equals("data"))
						obj.data_Object = N["level"][numberlevel]["objects"][key]["data"].AsInt;
					//else if(key_s1.Equals("color"))
						obj.color_Object = N["level"][numberlevel]["objects"][key]["color"].Value;
					//else if(key_s1.Equals("modifierType"))
						obj.modifierType_Object = N["level"][numberlevel]["objects"][key]["modifierType"].Value;
				//}

				Contance.LIST_OBJECT.Add(obj);
			}

			//===========================================scores=================================================

			for(int score = 0; score < N["level"][numberlevel]["scores"].Count; score++)
			{
//				print ("score"+score+" "+N["level"][0]["scores"][score].AsFloat);
				Contance.SCORES[score] = N["level"][numberlevel]["scores"][score].AsInt;
                //Debug.Log("score:"+Contance.SCORES[score]);
			}

			//===========================================Color=================================================
            //print (N["level"][numberlevel]["colors"].Count);
			if(N["level"][numberlevel]["colors"].Count > 0)
			{
                //print ("has colors");
				var c = N["level"][numberlevel]["colors"];
                Contance.LIST_COLOR = new List<P_Color>();
                foreach (string key in c.Keys)
                {
                    P_Color color = new P_Color();
                    color.color = key;
                    color.value = c[key].AsFloat;
                    Contance.LIST_COLOR.Add(color);
                    //					Debug.Log(key+": "+N["level"][0]["colors"][key].AsFloat);
                }
			}
            else
            {
                //print ("have not colors");
            }


			//===========================================limits=================================================
            //print (N["level"][numberlevel]["limit"][0]["value"].AsInt);
			Contance.LIMIT = N["level"][numberlevel]["limit"][0]["value"].AsInt;

			//===========================================goals=================================================
            if (N["level"][numberlevel]["goals"].Count > 0)
			{
                //print(N["level"][numberlevel]["goals"].Count);
				Contance.LIST_GOAL_OBJECT = new List<P_Goal_Object>();

				for(int j = 0; j < N["level"][numberlevel]["goals"].Count; j++)
				{
					var s1 = N["level"][numberlevel]["goals"][j];
					P_Goal_Object obj_Goal = new P_Goal_Object();
					foreach(string key_s1 in s1.Keys)
					{
						if(key_s1.Equals("type"))
                            obj_Goal.type_Goal_Object = N["level"][numberlevel]["goals"][j]["type"].Value;
						else if(key_s1.Equals("object_type"))
                            obj_Goal.object_type_Goal_Object = N["level"][numberlevel]["goals"][j]["object_type"].Value;
						else if(key_s1.Equals("color"))
                            obj_Goal.color_Goal_Object = N["level"][numberlevel]["goals"][j]["color"].Value;
						else if(key_s1.Equals("value"))
                            obj_Goal.value_Goal_Object = N["level"][numberlevel]["goals"][j]["value"].AsInt;
					}
                    Contance.LIST_GOAL_OBJECT.Add(obj_Goal);

				}
			//Debug.Log ("LIST_GOAL_OBJECT"+Contance.LIST_GOAL_OBJECT.Count);
//				for(int j = 0; j < N["level"][0]["goals"].Count; j++)
//				{
//					print (N["level"][0]["goals"][j]["value"].AsInt);
//					print (N["level"][0]["goals"][j]["type"].Value);
//					print (N["level"][0]["goals"][j]["object_type"].Value);
//					print (N["level"][0]["goals"][j]["color"].Value);
//				}
			}
            //else
            //{
            //    print ("have not goals");
            //}


			//===========================================bossMaxHealth=================================================
            //print (N["level"][numberlevel]["bossMaxHealth"].AsInt);
			Contance.bossMaxHealth = N["level"][numberlevel]["bossMaxHealth"].AsInt;

			//===========================================mode=================================================
            //print (N["level"][numberlevel]["mode"].Value);
			Contance.MODE = N["level"][numberlevel]["mode"].Value;

			//===========================================episode=================================================
            //print (N["level"][numberlevel]["episode"].AsInt);
			Contance.EPISODE = N["level"][numberlevel]["episode"].AsInt;

			//===========================================boosters=================================================


			//===========================================game=================================================
            //print (N["level"][numberlevel]["game"].AsInt);

			//===========================================DATA=================================================
			Contance.LIST_CONVEYOR_BELT = new List<P_Conveyor_Belt>();
			if(N["level"][numberlevel]["data"].Count > 0)
			{
				for(int j = 0; j < N["level"][numberlevel]["data"].Count; j++)
				{
					P_Conveyor_Belt converyor_belt = new P_Conveyor_Belt();

					converyor_belt.type_Conveyor_Belt = N["level"][numberlevel]["data"][j]["type"].Value;
					converyor_belt.to_column = N["level"][numberlevel]["data"][j]["to_column"].AsInt;
					converyor_belt.from_column = N["level"][numberlevel]["data"][j]["from_column"].AsInt;
					converyor_belt.to_row = N["level"][numberlevel]["data"][j]["to_row"].AsInt;
					converyor_belt.from_row = N["level"][numberlevel]["data"][j]["from_row"].AsInt;

					Contance.LIST_CONVEYOR_BELT.Add(converyor_belt);
				}
			}

//===========================================Layer=================================================

			//=============================CONVEYOR_BELT====================================
			Contance.LIST_OBJECT_CONVEYOR = new List<P_Conveyor>();
			if(N["level"][numberlevel]["layers"]["2"].Count > 0)
			{
                var layer_conveyor_belt = N["level"][numberlevel]["layers"]["2"];
                
				//Contance.LIST_OBJECT_GRID_COLOR = new List<P_Color_Grid> ();
                foreach (string key_conveyor_belt in layer_conveyor_belt.Keys)
                {
					Index ind = ReadString(key_conveyor_belt);

					P_Conveyor obj = new P_Conveyor ();
						
					obj.row_Conveyor = ind.row;
					obj.column_Conveyor = ind.col;

					obj.type_Conveyor = layer_conveyor_belt [key_conveyor_belt] ["type"].Value;
					obj.data_Conveyor = layer_conveyor_belt [key_conveyor_belt] ["data"].Value;
						
					Contance.LIST_OBJECT_CONVEYOR.Add (obj);
				}
			}
			//====================
		Contance.LIST_OBJECT_GRID_COLOR = new List<P_Color_Grid> ();
		if(N["level"][numberlevel]["layers"]["19"].Count > 0)
		{
			var layer_conveyor_belt = N["level"][numberlevel]["layers"]["19"];

			foreach (string key_conveyor_belt in layer_conveyor_belt.Keys)
			{
				Index ind = ReadString(key_conveyor_belt);

				P_Color_Grid obj = new P_Color_Grid ();
				obj.row_Color_Grid = ind.row;
				obj.column_Color_Grid = ind.col;
				obj.color_Color_Grid = layer_conveyor_belt [key_conveyor_belt] ["color"].Value;
				Contance.LIST_OBJECT_GRID_COLOR.Add (obj);
				//Debug.Log ("Contance.LIST_OBJECT_GRID_COLOR" + Contance.LIST_OBJECT_GRID_COLOR.Count);
			}
		}
			//=============================POPSICLE====================================
			Contance.LIST_POPSICLE = new List<P_Popsicle>();
			if(N["level"][numberlevel]["layers"]["4"].Count > 0)
			{
				var layer_popsicles = N["level"][numberlevel]["layers"]["4"];
				
				foreach (string key_popsicle in layer_popsicles.Keys) {
					
					Index ind = ReadString(key_popsicle);
					P_Popsicle obj = new P_Popsicle();
					
					obj.row_Popsicle = ind.row;
					obj.col_Popsicle = ind.col;
					obj.type_Popsicle = layer_popsicles[key_popsicle]["type"].Value;
					obj.data_Popsicle = layer_popsicles[key_popsicle]["data"].Value;
					
					Contance.LIST_POPSICLE.Add(obj);
				}
                Contance.popsiclesToCollect = Contance.LIST_POPSICLE.Count;
			}


			//=============================ICE====================================
			Contance.LIST_OBJECT_ICE = new List<P_Object>();
			if(N["level"][numberlevel]["layers"]["8"].Count > 0)
			{
				var layer_ice = N["level"][numberlevel]["layers"]["8"];
				
				foreach (string key_ice in layer_ice.Keys) {
					
					Index ind = ReadString(key_ice);
					P_Object obj = new P_Object();
					
					obj.row_Object = ind.row;
					obj.col_Object = ind.col;
					obj.type_Object = layer_ice[key_ice]["type"].Value;
					obj.data_Object = layer_ice[key_ice]["data"].AsInt;
					obj.color_Object = layer_ice[key_ice]["color"].Value;

					Contance.LIST_OBJECT_ICE.Add(obj);
				}
			}

			//=============================CRACKER====================================
			Contance.LIST_OBJECT_CRACKER = new List<P_Object>();
			if(N["level"][numberlevel]["layers"]["10"].Count > 0)
			{
				var layer_crackers = N["level"][numberlevel]["layers"]["10"];
				
				foreach (string key_cracker in layer_crackers.Keys) {
					
					Index ind = ReadString(key_cracker);
					P_Object obj = new P_Object();
					
					obj.row_Object = ind.row;
					obj.col_Object = ind.col;
					obj.type_Object = layer_crackers[key_cracker]["type"].Value;
					obj.data_Object = layer_crackers[key_cracker]["data"].AsInt;
					obj.color_Object = layer_crackers[key_cracker]["color"].Value;
					
					Contance.LIST_OBJECT_CRACKER.Add(obj);
                    Contance.CRACK += obj.data_Object;
				}

                //Contance.CRACK = Contance.LIST_OBJECT_CRACKER.Count;
			}
            

			//=============================HONEY====================================
			Contance.LIST_OBJECT_HONEY= new List<P_Object>();
			if(N["level"][numberlevel]["layers"]["12"].Count > 0)
			{
				var layer_honeys = N["level"][numberlevel]["layers"]["12"];
				
				foreach (string key_honey in layer_honeys.Keys) {
					
					Index ind = ReadString(key_honey);
					P_Object obj = new P_Object();
					
					obj.row_Object = ind.row;
					obj.col_Object = ind.col;
					obj.type_Object = layer_honeys[key_honey]["type"].Value;
					obj.data_Object = layer_honeys[key_honey]["data"].AsInt;
					obj.color_Object = layer_honeys[key_honey]["color"].Value;
					
					Contance.LIST_OBJECT_HONEY.Add(obj);
				}
			}


			//============================obstacle_blocker(random position)====================================
			if(N["level"][numberlevel]["layers"]["16"].Count > 0)
			{
				var layer_index_block = N["level"][numberlevel]["layers"]["16"];
				Contance.LIST_INDEX_BLOCKER= new List<Index>();
				
				foreach (string key_index_block in layer_index_block.Keys) {

					Index ind = ReadString(key_index_block);
					Contance.LIST_INDEX_BLOCKER.Add(ind);

				}
			}

			//============================Info layer 18 ====================================
			if(N["level"][numberlevel]["layers"]["18"].Count > 0)
			{
				var layer_info_18 = N["level"][numberlevel]["layers"]["18"];

                Contance.LIST_OBJECT_18 = new List<P_Object_18>();
                Contance.LIST_PORTAL_INFO_18 = new List<P_Portal>();
                //print(layer_info_18.Count);

				foreach (string key_info_18 in layer_info_18.Keys) {
					
					Index ind = ReadString(key_info_18);

					if(layer_info_18[key_info_18]["type"].Value.Equals("spawner"))
					{
                        P_Object_18 object_18 = new P_Object_18();
                        object_18.type = layer_info_18[key_info_18]["type"].Value;
                        object_18.row = ind.row;
                        object_18.column = ind.col;

                        if (layer_info_18[key_info_18]["data"]["name"].Value.Equals("empty"))
                        {
                            //object_18.listColor = new List<P_Color>();
                            //for (int i = 0; i < Contance.LIST_COLOR.Count; i++)
                            //{
                            //    object_18.listColor.Add(Contance.LIST_COLOR[i]);
                            //}
                            
                        }
                        else
                        {
                            //===========================================forced_spawn_queue=================================================
                            if (layer_info_18[key_info_18]["data"]["forced_spawn_queue"].Count > 0)
                            {
                                //print("has forced_spawn_queue");
                                object_18.listForcedSpawn = new List<P_ForcedSpawnQueue>();
                                for (int j = 0; j < layer_info_18[key_info_18]["data"]["forced_spawn_queue"].Count; j++)
                                {
                                    P_ForcedSpawnQueue p_forcedspawnqueue = new P_ForcedSpawnQueue();
                                    //					print (N["level"][0]["forced_spawn_queue"][j]["type"].Value);
                                    p_forcedspawnqueue.Type_ForcedSpawnQueue = layer_info_18[key_info_18]["data"]["forced_spawn_queue"][j]["type"].Value;
                                    //					print (N["level"][0]["forced_spawn_queue"][j]["color"].Value);
                                    p_forcedspawnqueue.Value_ForcedSpawnQueue = layer_info_18[key_info_18]["data"]["forced_spawn_queue"][j]["color"].Value;

                                    object_18.listForcedSpawn.Add(p_forcedspawnqueue);
                                }
                            }
                            //===========================================Spawn_table=================================================
                            if (layer_info_18[key_info_18]["data"]["spawn_table"].Count > 0)
                                {
                                    //print("has spawn_table");
                                    object_18.listSpawnTable = new List<P_SpawnTable>();
                                    for (int j = 0; j < layer_info_18[key_info_18]["data"]["spawn_table"].Count; j++)
                                    {
                                        P_SpawnTable p_spawntable = new P_SpawnTable();
                                        //					print (N["level"][0]["spawn_table"][j]["type"].Value);
                                        p_spawntable.Type_SpawnTable = layer_info_18[key_info_18]["data"]["spawn_table"][j]["type"].Value;
                                        //					print (N["level"][0]["spawn_table"][j]["percent"].AsFloat);
                                        p_spawntable.Value_SpawnTable = layer_info_18[key_info_18]["data"]["spawn_table"][j]["percent"].AsFloat;

                                        foreach (string key in layer_info_18[key_info_18]["data"]["spawn_table"][j].Keys)
                                        {
                                            if (key == "data")
                                            {
                                                p_spawntable.Data = layer_info_18[key_info_18]["data"]["spawn_table"][j]["data"].AsInt;
                                            }
                                        }

                                        object_18.listSpawnTable.Add(p_spawntable);
                                    }
                                }
                           //===========================================color=================================================
                            if (layer_info_18[key_info_18]["data"]["colors"].Count > 0)
                            {
                                //print("has colors");
                                var c = layer_info_18[key_info_18]["data"]["colors"];
                                object_18.listColor = new List<P_Color>();
                                foreach (string key in c.Keys)
                                {
                                    P_Color color_18 = new P_Color();
                                    color_18.color = key;
                                    color_18.value = c[key].AsFloat;
                                    object_18.listColor.Add(color_18);
                                    //					Debug.Log(key+": "+N["level"][0]["colors"][key].AsFloat);
                                }
                            }
                        }
                        Contance.LIST_OBJECT_18.Add(object_18);
					}
					else 
					{
                        P_Portal portal = new P_Portal();
                        portal.row = ind.row;
                        portal.colum = ind.col;
                        portal.data = layer_info_18[key_info_18]["data"].AsInt;
                        portal.type = layer_info_18[key_info_18]["type"].Value;
                        Contance.LIST_PORTAL_INFO_18.Add(portal);
					}
				}

                //print(Contance.LIST_PORTAL_INFO_18.Count + " : " + Contance.LIST_OBJECT_18.Count);
			}
            //print(Contance.LIST_OBJECT_CRACKER.Count);
           
    }

	private Index ReadString(string s)
	{
		Index index = new Index();
		while (s != null) {
			
			char[] delemiter = {'_'};
			string[] field = s.Split(delemiter);
			
			index.col = Convert.ToInt32(field[0]);
			index.row = Convert.ToInt32(field[1]);
			
			s = null;
		}
		return index;
	}
}

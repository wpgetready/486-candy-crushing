﻿using UnityEngine;
using System.Collections;

public class SortText : MonoBehaviour {
    public string sortingLayerName = "";
    public int sortingOrder = 0;
	// Use this for initialization
	void Start () {
        GetComponent<Renderer>().sortingLayerName = sortingLayerName;
        GetComponent<Renderer>().sortingOrder = sortingOrder;
	}
	

}

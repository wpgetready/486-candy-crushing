﻿using UnityEngine;
using System.Collections;
using Spine.Unity;
using System.Collections.Generic;

public class Clear_JuiceScript : MonoBehaviour {

    public TextMesh countCrack;
    public GameObject Bg_item;
    public Transform item;
    private static int crackToCollect;
	public GameObject plate;

    public GameObject crackerSpaw;

	int cookieOrigin;

	// Use this for initialization
	void Start () {
        crackToCollect = Contance.CRACK;
		cookieOrigin = crackToCollect;
        countCrack.text = crackToCollect.ToString();
	}
	
	// Update is called once per frame
    //void Update () {
	
    //}

    public void MoveCrackFinish(Grid grid)
    {
        //UpdateGraphic(grid);
//        if(PoolingManager.poolingManager.List_Effect_Cracker.Count > 0)
//        {
//            GameObject cracker = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Cracker,grid.GetTile());
//            LeanTween.scale(cracker, new Vector3(0.7f, 0.7f, 0.7f), 0.1f);
//            LeanTween.delayedCall(this.gameObject, 0.1f, () =>
//            {
//                LeanTween.rotateAround(cracker, Vector3.back, 360, 0.5f).setRepeat(-1);
//                LeanTween.scale(cracker, new Vector3(0.4f, 0.4f, 0.4f), 1f);
//                CollectCracker(cracker, grid.transform.position, item.transform.position);
//                //iTween.MoveTo(cracker, iTween.Hash("position", item.transform.position, "time", 1f, "easetype", iTween.EaseType.linear, "oncomplete", "ScaleItem", "oncompletetarget", gameObject));
//            });
//            LeanTween.delayedCall( 1.2f, () =>
//            {
//                PoolingManager.poolingManager.AddOrderColorToPool(cracker, PoolingManager.poolingManager.List_Effect_Cracker);
//            });
//        }
//        else
//        {
			//Debug.Log("List_Effect_Cracker.Count ");
            GameObject cracker = Instantiate(crackerSpaw, grid.transform.position, Quaternion.identity) as GameObject;
            LeanTween.scale(cracker, new Vector3(0.7f, 0.7f, 0.7f), 0.1f);
            LeanTween.delayedCall(this.gameObject, 0.1f, () =>
            {
                LeanTween.rotateAround(cracker, Vector3.back, 360, 0.5f).setRepeat(-1);
                LeanTween.scale(cracker, new Vector3(0.4f, 0.4f, 0.4f), 1f);
				CollectCracker(cracker, grid.transform.position, plate.transform.position);
                //iTween.MoveTo(cracker, iTween.Hash("position", item.transform.position, "time", 1f, "easetype", iTween.EaseType.linear, "oncomplete", "ScaleItem", "oncompletetarget", gameObject));
            });
//            LeanTween.delayedCall(1.2f, () =>
//            {
//                PoolingManager.poolingManager.AddOrderColorToPool(cracker, PoolingManager.poolingManager.List_Effect_Cracker);
//            });
//        }
    }

    void UpdateGraphic(Grid grid)
    {
        if (grid.gridData.gridDynamicData > 0)
        {
            grid.gridData.gridDynamicData--;
            //GamePlayController.gamePlayController.effectControll.CreateEffectDynamic(grid);
            if (grid.gridData.gridDynamicData <= 0)
            {

                grid.gridData.gridDynamicType = GridData.GridDynamicType.NORMAL_GRID;
                grid.dynamicRender.sprite = null;
            }
            else
            {
                GamePlayController.gamePlayController.mapController.gridSpawner.SetGridProperties(grid, grid.gridData);
            }
        }
    }

	public void ScaleItem(GameObject obj)
    {
        AnimationStatePopsicle(gameObject, Contance.AN_BANH, false);
        //iTween.ScaleTo(Bg_item, iTween.Hash("scale", new Vector3(0.95f, 0.95f, 0.95f), "time", 0.2f));
        //iTween.ScaleTo(Bg_item, iTween.Hash("scale", new Vector3(0.75f, 0.75f, 0.75f), "time", 0.2f, "delay", 0.2f));
		crackToCollect--;
		//Debug.Log ("crackToCollect..." + crackToCollect);
		countCrack.text = crackToCollect.ToString();
		Destroy (obj);
		UpgradePlate ((cookieOrigin-crackToCollect));
        if(crackToCollect <= 0)
        {
            Bg_item.SetActive(false);
            Contance.GAME_WIN = true;
            AnimationWinOrLose(gameObject,Contance.THANG,true);
            StartCoroutine(OutPutObj());
        }
    }

	public List<Sprite> plateImg;

	void UpgradePlate(int currentCookie)
	{
		int count = 0;
		int number = currentCookie * 100 / cookieOrigin;
		foreach(Sprite img in plateImg)
		{
			if(number > count*100/plateImg.Count && number <= (count+1)*100/plateImg.Count)
			{
				plate.GetComponent<SpriteRenderer> ().sprite = img;
				plate.GetComponentInParent<Animation> ().Play ("StartAnimation");
				break;
			}
			count++;
		}
	}

    public IEnumerator OutPutObj()
    {
        yield return new WaitForSeconds(1f);
		plate.SetActive (false);
        AnimationWinOrLose(gameObject, Contance.DI, true);
        iTween.MoveTo(gameObject, iTween.Hash("position", new Vector3(-10, gameObject.transform.position.y, 0), "speed", 1, "easetype", iTween.EaseType.linear));
    }

	public void AnimationStatePopsicle(GameObject obj, string nameAnimation, bool loop)
    {
        if (!obj.GetComponent<SkeletonAnimation>().AnimationName.Equals(nameAnimation))
        {
            obj.GetComponent<SkeletonAnimation>().state.SetAnimation(0, nameAnimation, loop);
            obj.GetComponent<SkeletonAnimation>().state.AddAnimation(0, Contance.NGOI, true, 0f);
        }
    }

    public void AnimationWinOrLose(GameObject obj, string nameAnimation, bool loop)
    {
        if (!obj.GetComponent<SkeletonAnimation>().AnimationName.Equals(nameAnimation))
        {
            obj.GetComponent<SkeletonAnimation>().state.SetAnimation(0, nameAnimation, loop);
        }
    }

    public void Clear_Juice_Lose()
    {
        AnimationWinOrLose(gameObject,Contance.THUA,true);
    }

    public void CollectCracker(GameObject obj, Vector3 start, Vector3 end)
    {
		iTween.MoveTo(obj, iTween.Hash("path", new Vector3[] { start, new Vector3(Random.Range(-2.4f, 2.4f), Random.Range(start.y, end.y), 0), end}, "time", 0.9f, "easetype", iTween.EaseType.linear, "oncomplete", "ScaleItem", "oncompletetarget", gameObject,"oncompleteparams", obj));
		//Destroy (obj,1.5f);
    }
}

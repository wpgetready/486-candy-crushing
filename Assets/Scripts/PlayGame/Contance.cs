﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class Contance {

    /// <summary>
    /// Chieu rong cua map
    /// </summary>
    public static int width;

    /// <summary>
    /// Chieu cao cua map
    /// </summary>
    public static int height;

	public static List<P_SpawnTable> LIST_SPAWNTABLE;

	public static List<P_ForcedSpawnQueue> LIST_FORCEDSPAWNQUEUE;

	public static List<P_Object> LIST_OBJECT;

	public static List<P_Goal_Object> LIST_GOAL_OBJECT;

	public static List<P_Conveyor_Belt> LIST_CONVEYOR_BELT;


    public static List<P_Conveyor> LIST_OBJECT_CONVEYOR;

	public static List<P_Color_Grid> LIST_OBJECT_GRID_COLOR;

	public static List<P_Popsicle> LIST_POPSICLE;

	public static List<P_Object> LIST_OBJECT_ICE;

	public static List<P_Object> LIST_OBJECT_CRACKER;

	public static List<P_Object> LIST_OBJECT_HONEY;

	public static List<Index> LIST_INDEX_BLOCKER;


    public static List<P_Color> LIST_COLOR;
    public static List<P_Color> LIST_COLOR_INFO_18;

    public static List<P_Object_18> LIST_OBJECT_18;
    public static List<P_Portal> LIST_PORTAL_INFO_18;

	public static int[] SCORES;
    public static int SCORE = 0;

    public static string TITLE = "";
	public static string MODE = "";
	public static P_BossScripts bossScripts;
	public static float bossMaxHealth = 0;
	public static int popsiclesToCollect = 0;
	public static int LIMIT;

	public static int EPISODE = 0;
    public static int CRACK = 0;

	public static int NUMBER_LEVEL = 57;

    public static bool GAME_PAUSE = false;
    public static bool GAME_WIN = false;
    public static bool GAME_LOSE = false;

	public const string AN_KEM = "eat";
	public const string AN_BANH = "eat";
    public  const string DI = "walk";
    public  const string NGOI = "idle";
    public  const string THANG = "win";
	public  const string THANG_Boss = "win";
	public  const string THUA_Boss = "die";
	public  const string THUA = "idle";
    public  const string TRUNG_NUOC_QUA = "trungdan";
    public  const string TUNG_VAT_CAN = "idle";
	public  const string orderDone = "smile";

    #region type item
    public const string HONEY = "honey";
    public const string MUFFIN = "muffin";
    public const string COW = "cow";
    public const string COCONUT = "coconut_2";
    public const string YOGURT = "yogurt";
    #endregion

    public static void ResetData()
    {
        width = 0;
        height = 0;
        //LIST_SPAWNTABLE = new List<P_SpawnTable>();
        //LIST_FORCEDSPAWNQUENE = new List<P_ForcedSpawnQueue>();
        //LIST_OBJECT = new List<P_Object>();
        //LIST_GOAL_OBJECT = new List<P_Goal_Object>();
        //LIST_CONVEYOR_BELT = new List<P_Conveyor_Belt>();
        //LIST_OBJECT_CONVEYOR = new List<P_Conveyor>();
        //LIST_POPSICLE = new List<P_Popsicle>();
        //LIST_OBJECT_ICE = new List<P_Object>();
        //LIST_OBJECT_CRACKER = new List<P_Object>();
        //LIST_OBJECT_HONEY = new List<P_Object>();
        //LIST_INDEX_BLOCKER = new List<Index>();
        //LIST_COLOR = new List<P_Color>();
        //LIST_COLOR_INFO_18 = new List<P_Color>();
        //LIST_OBJECT_18 = new List<P_Object_18>();
        //LIST_PORTAL_INFO_18 = new List<P_Portal>();
        SCORES = new int[3];
        SCORE = 0;
        TITLE = "";
        MODE = "";
        bossScripts = new P_BossScripts();
        bossMaxHealth = 0;
        popsiclesToCollect = 0;
        LIMIT = 0;
        EPISODE = 0;
        CRACK = 0;
        //NUMBER_LEVEL = 1;
    }
}

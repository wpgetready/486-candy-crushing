﻿using UnityEngine;
using System.Collections;
public class ParticleSortingLayer : MonoBehaviour
{
    public string sortingLayerName = "Forground";
    public int sortingOrder = 1;
    public float time = 1;

    void Start()
    {
        // Set the sorting layer of the particle system.
        GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingLayerName = sortingLayerName;
        GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingOrder = sortingOrder;
        //Destroy(this.gameObject, time);
    }
}
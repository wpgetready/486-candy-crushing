﻿using UnityEngine;
using System.Collections;

public class EffectMilkAndMuffin : MonoBehaviour {

    public GameObject effect_vo;
	// Use this for initialization
	void Start () {
        CollectFood(new Vector3(0,0,0),new Vector3(0,3,0));
        //CreateEff();
	}

    public void CollectFood( Vector3 start, Vector3 end)
    {
        iTween.MoveTo(gameObject, iTween.Hash("path", new Vector3[] { start, new Vector3(Random.Range(-2.4f, 2.4f), Random.Range(start.y, end.y), 0), end }, "time", 1, "easetype", iTween.EaseType.linear,"oncomplete","CreateEff","oncompletetarget",gameObject));
        //Instantiate(effect_vo, gameObject.transform.position, Quaternion.identity);
    }

    public void CreateEff()
    {
        Destroy(this.gameObject);
        Instantiate(effect_vo, gameObject.transform.position, Quaternion.identity);
    }
}

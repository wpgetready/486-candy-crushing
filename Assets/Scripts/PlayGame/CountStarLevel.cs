﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CountStarLevel : MonoBehaviour {
    public GameObject star1;
    public GameObject star2;
    public GameObject star3;

    public Image ProgressStar;

	void callScore()
	{
		//countScore = Contance.SCORE/2;
		StartCoroutine (ScoreIncrease());
	}
	
	IEnumerator ScoreIncrease()
	{
		while(countScore<Contance.SCORE)
		{
			yield return new WaitForSeconds (0);
			if(countScore<Contance.SCORE-500)
			{
				countScore+=99;
			}
			else
			{
				countScore+=9;
			}
			if(countScore>Contance.SCORE)
			{
				countScore = Contance.SCORE;
				//yield return new WaitForSeconds (1);
				//GamePlayController.gamePlayController.popupGame.Win();
			}
			
			GamePlayController.gamePlayController.scoreRibbon.GetComponentInChildren<Text> ().text = countScore.ToString();
			
		}
	}
	int countScore;
    public void CountStar(int score)
    {
        Contance.SCORE += score;
		//GamePlayController.scores = Contance.SCORE;

		if(Contance.GAME_WIN == true)
		{
			int number_2 = PlayerPrefs.GetInt ("hlevel");
			//Debug.Log ("hlevel..."+PlayerPrefs.GetInt ("hlevel"));
			//Debug.Log ("number..."+Contance.NUMBER_LEVEL);
			if(Contance.NUMBER_LEVEL > number_2)
			{
				//Debug.Log ("hlevel..."+PlayerPrefs.GetInt ("hlevel"));
				PlayerPrefs.SetInt("hlevel", Contance.NUMBER_LEVEL);
				PlayerPrefs.Save();
			}
			GamePlayController.gamePlayController.scoreRibbon.SetActive (true);
			iTween.MoveTo (GamePlayController.gamePlayController.scoreRibbon, iTween.Hash ("position", new Vector3 (0, GamePlayController.gamePlayController.scoreRibbon.transform.position.y, 0), "time", 1f, "easetype", iTween.EaseType.easeInBack, "oncomplete", "callScore", "oncompletetarget", gameObject));
		}

        if (Contance.SCORE < Contance.SCORES[0])
        {
            ProgressStar.fillAmount += ((float)score / Contance.SCORES[0] * 0.35f);
        }
        else if (Contance.SCORE <= Contance.SCORES[1])
        {
            star1.SetActive(true);
			if(s1 == false)
			{
				SoundController.sound.effStarPlay ();
				s1 = true;
			}
            ProgressStar.fillAmount += ((float)score / (Contance.SCORES[1] - Contance.SCORES[0]) * 0.35f);
        }
        else if (Contance.SCORE <= Contance.SCORES[2])
        {
            ProgressStar.fillAmount += ((float)score / (Contance.SCORES[2] - Contance.SCORES[1]) * 0.3f);
            star2.SetActive(true);
			if (s2 == false) 
			{
				SoundController.sound.effStarPlay ();
				s2 = true;
			}
        }
        else
        {
            ProgressStar.fillAmount = 1;
			star3.SetActive (true);
			if (s3 == false) 
			{
				SoundController.sound.effStarPlay ();
				s3 = true;
			}
        }
    }
	bool s1, s2, s3;
}

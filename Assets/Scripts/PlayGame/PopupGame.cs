﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Spine.Unity;

public class PopupGame : MonoBehaviour 
{
    public GameObject panelPopup;
    public GameObject dialogPause;
    public GameObject dialogWin;
    public GameObject dialogLose;
	public GameObject dialogBuyMove;
    public Toggle tgSound;
    public Toggle tgMusic;
	public Text coint;
	public static bool isNewBest;
	public Text textCoins_dialogBuyMove;
	public Button dialogBuyMoveBT, getFreeCoinsBT, buyCoinsBT;
	public Text content_dialogBuyMove;

	void Start () 
	{
        Reset();
        HandleAudio();
	}

	void OnEnable()
	{
		int coins = PlayerPrefs.GetInt ("coins");
		coint.text = coins.ToString();
	}

    void HandleAudio()
    {
        //print(PlayerPrefs.GetString("MUSIC")+":"+PlayerPrefs.GetString("SOUND"));

        if(PlayerPrefs.GetString("MUSIC").Equals("ON"))
        {
            tgMusic.isOn = true;
        }else
        {
            tgMusic.isOn = false;
        }

        if (PlayerPrefs.GetString("SOUND").Equals("ON"))
        {
            tgSound.isOn = true;
        }else
        {
            tgSound.isOn = false;
        }
    }

	public GameObject coinPanle, bg_2;
	public CoinController coinController;
	public Bg2Controller bg2Controller;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !Contance.GAME_WIN && !Contance.GAME_LOSE)
        {
			if (!Contance.GAME_PAUSE) 
			{
				if(coinPanle.activeInHierarchy == true)
				{
					coinController.MoveUp ();
				}

				if(bg_2.activeInHierarchy == true)
				{
					bg2Controller.CloseButton (true);
				}
				ButtonPause ();
			} 
			else 
			{
				Button_Continue ();
			}
        }
        //if (Input.GetKeyDown(KeyCode.S))
        //{
        //    Contance.SCORE = 200000;
        //    Win();
        //}
    }
//==============================================================PAUSE GAME ===============================================================
    public void ButtonPause()
    {
		SoundController.sound.Click ();
        Contance.GAME_PAUSE = true;
        panelPopup.SetActive(true);
        dialogPause.SetActive(true);
        iTween.MoveTo(dialogPause, iTween.Hash("position", new Vector3(0, 0, 0), "time", 0.25f, "easetype", iTween.EaseType.easeOutBack, "oncomplete", "TimeScalePause", "oncompletetarget", gameObject));

    }

    public void TimeScalePause()
    {
        //Time.timeScale = 0;
    }

    /// <summary>
    /// For test: handle no more move
    /// </summary>
    public void HandleNoMoreMove()
    {
        GamePlayController.gamePlayController.mapController.NoMoreMove();
    }
//==============================================================GAME WIN ===============================================================
    public void Win()
    {
        //Contance.GAME_WIN = true;
        panelPopup.SetActive(true);
        dialogWin.SetActive(true);
        CheckTitleAndScore();
        iTween.MoveTo(dialogWin, iTween.Hash("position", new Vector3(0, 0, 0), "time", 0.55f, "easetype", iTween.EaseType.easeOutBounce));
        CheckStars();
    }

    //================CHECKSTARS==============
    public GameObject star1;
    public GameObject star2;
    public GameObject star3;

    public Text title;
    public Text score;
    public Text bestScore;

    void CheckStars()
    {
        if (Contance.SCORE >= Contance.SCORES[2])
        {
           // star1.SetActive(true);
			StartCoroutine( EffectStars(star1,1f));
            //star2.SetActive(true);
			StartCoroutine(EffectStars(star2,1.5f));
           // star3.SetActive(true);
			StartCoroutine(EffectStars(star3, 2f));
			PlayerPrefs.SetInt("NumberStar"+Contance.NUMBER_LEVEL.ToString(),3);
        }
        else if (Contance.SCORE >= Contance.SCORES[1])
        {
            //star1.SetActive(true);
			StartCoroutine(EffectStars(star1, 1f));
            //star2.SetActive(true);
			StartCoroutine(EffectStars(star2, 1.5f));	
			int count = PlayerPrefs.GetInt("NumberStar"+Contance.NUMBER_LEVEL.ToString());
			if(count < 2)
			{
				PlayerPrefs.SetInt("NumberStar"+Contance.NUMBER_LEVEL.ToString(),2);
			}
        }
        else if (Contance.SCORE >= Contance.SCORES[0])
        {
            //star1.SetActive(true);
			StartCoroutine(EffectStars(star1, 1f));
			int count = PlayerPrefs.GetInt("NumberStar"+Contance.NUMBER_LEVEL.ToString());
			if (count < 1) 
			{
				PlayerPrefs.SetInt ("NumberStar" + Contance.NUMBER_LEVEL.ToString (), 1);
			}
        }
    }

    //================EffectStar================
	IEnumerator EffectStars(GameObject star,float delay)
    {
		yield return new WaitForSeconds (delay);
		star.SetActive (true);
		SoundController.sound.WinStarSoundPlay ();
        //iTween.ScaleTo(star, iTween.Hash("scale", new Vector3(1f, 1f, 1f), "time", 0.2f, "delay", delay,"easetype",iTween.EaseType.easeOutElastic));
    }

    //===================check title and score==============
    void CheckTitleAndScore()
    {
        title.text = Contance.TITLE.ToString();

		if (Contance.SCORE > PlayerPrefs.GetInt ("BestScore" + (Contance.NUMBER_LEVEL).ToString ())) {
			PlayerPrefs.SetInt ("BestScore" + (Contance.NUMBER_LEVEL).ToString (), Contance.SCORE);
			PlayerPrefs.Save();
			isNewBest = true;
		} 
		else 
		{
			isNewBest = false;
		}

        score.text = "Your score: "+ Contance.SCORE.ToString();
		bestScore.text = "Best your score: "+PlayerPrefs.GetInt("BestScore"+(Contance.NUMBER_LEVEL).ToString()).ToString();
		//Debug.Log ("Level: " + Contance.NUMBER_LEVEL);

    }

	public void BeforeLose()
	{
		panelPopup.SetActive(true);
		dialogBuyMove.SetActive(true);
		_Coins ();
		int coins = PlayerPrefs.GetInt ("coins");
		textCoins_dialogBuyMove.text = "" + coins;
		iTween.MoveTo(dialogBuyMove, iTween.Hash("position", new Vector3(0, 0, 0), "time", 0.55f, "easetype", iTween.EaseType.easeOutBounce));
	}

	public void _Coins()
	{
		int coins = PlayerPrefs.GetInt ("coins");
		if (coins >= 20) 
		{
			dialogBuyMoveBT.gameObject.SetActive (true);
			getFreeCoinsBT.gameObject.SetActive (false);
			content_dialogBuyMove.text = "Buy 5 moves for 20 coins";
			buyCoinsBT.gameObject.SetActive (false);
		} 
		else 
		{
			dialogBuyMoveBT.gameObject.SetActive (false);
			getFreeCoinsBT.gameObject.SetActive (true);
			content_dialogBuyMove.text = "You don't have enough coins.";
			buyCoinsBT.gameObject.SetActive (true);
		}
	}

//==============================================================GAME LOSE===============================================================
    public void Lose()
    {
        panelPopup.SetActive(true);
        dialogLose.SetActive(true);
        iTween.MoveTo(dialogLose, iTween.Hash("position", new Vector3(0, 0, 0), "time", 0.55f, "easetype", iTween.EaseType.easeOutBounce));
    }

    public void Button_Continue()
    {
		SoundController.sound.Click ();
        Time.timeScale = 1;
        Contance.GAME_PAUSE = false;
        iTween.MoveTo(dialogPause, iTween.Hash("position", new Vector3(0, 11f, 0), "time", 0.25f, "easetype", iTween.EaseType.linear, "oncomplete", "FinishMove", "oncompletetarget", gameObject));
    }

    public void Button_Replay()
    {
		SoundController.sound.Click ();
		Reset();
        Application.LoadLevel(Application.loadedLevel);
    }

    public void Button_Home()
    {
		SoundController.sound.Click ();
        Reset();
        Time.timeScale = 1;
        Application.LoadLevel(GameConstants.LEVEL_SELECT_SCENE_NAME);
    }

    public void FinishMove()
    {
        panelPopup.SetActive(false);
    }

    public void Button_Yes()
    {
		SoundController.sound.Click ();
        Reset();
        Application.LoadLevel(Application.loadedLevel);
    }

    public void Button_No()
    {
		SoundController.sound.Click ();
        Reset();
        Application.LoadLevel(GameConstants.LEVEL_SELECT_SCENE_NAME);
    }

    public void Button_Reset()
    {
		SoundController.sound.Click ();
		Reset();
        Application.LoadLevel(Application.loadedLevel);
    }

    public void Button_Next()
    {
		SoundController.sound.Click ();
		Time.timeScale = 1;
        Reset();
		Contance.NUMBER_LEVEL++;
		Application.LoadLevel(GameConstants.GAMEPLAY_SCENE_NAME);
    }

    public void Button_Menu()
    {
		SoundController.sound.Click ();
		Reset();
        Application.LoadLevel(GameConstants.LEVEL_SELECT_SCENE_NAME);
    }

    public void Toggle_Sound()
    { 
        if(tgSound.isOn)
        {
			PlayerPrefs.SetString("SOUND", "ON");
			SoundController.sound.SoundOn();
        }
        else
        {
			StartCoroutine (waitClick());
            //SoundController.sound.SoundOff();
            //PlayerPrefs.SetString("SOUND", "OFF");
        }
    }

	IEnumerator waitClick()
	{
		yield return new WaitForSeconds (0.2f);
		SoundController.sound.SoundOff();
		PlayerPrefs.SetString("SOUND", "OFF");
	}

    public void Toggle_Music()
    {
		SoundController.sound.Click ();
        if(tgMusic.isOn)
        {
            PlayerPrefs.SetString("MUSIC", "ON");
            MusicController.music.MusicOn();
        }
        else
        {
            PlayerPrefs.SetString("MUSIC", "OFF");
            MusicController.music.MusicOff();
        }
    }

    public void Button_Yes_Rate()
    { }

    public void Button_No_Rate()
    { }

	public void Button_Yes_BuyMove()
	{
		int coins = PlayerPrefs.GetInt ("coins");
		if(coins >= 20)
		{
			coins -= 20;
			PlayerPrefs.SetInt ("coins", coins);
			PlayerPrefs.Save ();
			GameManager.MOVE += 5;
			GamePlayController.gamePlayController.UpdateMoveDisplay ();
			iTween.MoveTo(dialogBuyMove, iTween.Hash("position", new Vector3(0, -11f, 0), "time", 0.55f, "easetype", iTween.EaseType.easeOutBounce,"oncomplete","FinishMove","oncompletetarget",gameObject));
			GamePlayController.gamePlayController.isReceivingInput = true;
			textCoins_dialogBuyMove.text = "" + coins;
			Contance.GAME_LOSE = false;
			GamePlayController.gamePlayController.isShowBuyMove = true;
		}
	}

	public void Button_Close_BuyMove()
	{ 
		iTween.MoveTo(dialogBuyMove, iTween.Hash("position", new Vector3(0, -11f, 0), "time", 0.55f, "easetype", iTween.EaseType.easeOutBounce,"oncomplete","Hide_BuyMove","oncompletetarget",gameObject));
		LeanTween.delayedCall (0.65f, () => {
			StartCoroutine( GamePlayController.gamePlayController.CheckStateGame ());
		});

	}

	void Hide_BuyMove()
	{
		dialogBuyMove.SetActive (false);
	}

	public void MoveDown_dialogBuyMove()
	{
		iTween.MoveTo(dialogBuyMove, iTween.Hash("position", new Vector3(0, -11f, 0), "time", 0.55f, "easetype", iTween.EaseType.easeOutBounce));
	}

    private void Reset()
    {
        Time.timeScale = 1;
        Contance.GAME_LOSE = false;
        Contance.GAME_PAUSE = false;
        Contance.GAME_WIN = false;
    }
}
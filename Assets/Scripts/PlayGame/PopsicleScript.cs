﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Spine.Unity;

public class PopsicleScript : MonoBehaviour {

    public TextMesh countPopsicle;
    public GameObject item;

    private int posicleCollect;
	// Use this for initialization
	void Start () {
        posicleCollect = Contance.popsiclesToCollect;
        countPopsicle.text = posicleCollect.ToString();
	}
	
	// Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.P))
        //{
        //    MovePopsicleFinish(popsicle,posPopsicle.position);
        //}
    }

    public void MovePopsicleFinish(GameObject obj)
    {
        iTween.RotateTo(obj, iTween.Hash("rotation", new Vector3(0, 0, 0), "time", 0.2f));
        //iTween.ScaleTo(obj, iTween.Hash("scale", new Vector3(1f, 1f, 1f), "time", 0.2f));
        iTween.MoveTo(obj, iTween.Hash("position", item.transform.position, "time", 0.5f, "easetype", iTween.EaseType.linear, "delay", 0.2f, "oncomplete", "ScaleItem", "oncompletetarget", gameObject));
    }

    public void ScaleItem()
    {
        AnimationStatePopsicle(gameObject,Contance.AN_KEM,false);
        iTween.ScaleTo(item, iTween.Hash("scale", new Vector3(1.2f, 1.2f, 1f), "time", 0.3f));
        iTween.ScaleTo(item, iTween.Hash("scale", new Vector3(1f, 1f, 1f), "time", 0.2f,"delay",0.3f));
        posicleCollect -= 1;
        countPopsicle.text = posicleCollect.ToString();
    }


    public void AnimationStatePopsicle(GameObject obj, string nameAnimation, bool loop)
    {
        if (!obj.GetComponent<SkeletonAnimation>().AnimationName.Equals(nameAnimation))
        {
            obj.GetComponent<SkeletonAnimation>().state.SetAnimation(0, nameAnimation, loop);
            obj.GetComponent<SkeletonAnimation>().state.AddAnimation(0, Contance.NGOI, true, 0f);
        }
    }

    public void AnimationWinOrLose(GameObject obj, string nameAnimation, bool loop)
    {
        if (!obj.GetComponent<SkeletonAnimation>().AnimationName.Equals(nameAnimation))
        {
            obj.GetComponent<SkeletonAnimation>().state.SetAnimation(0, nameAnimation, loop);
        }
    }

}

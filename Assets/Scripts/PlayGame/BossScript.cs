﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using Spine.Unity;

public class BossScript : MonoBehaviour {

    public float maxHealth = 115;
    public float currentHealth = 0;

    
    public Text textPercent;

    private float currentPercent = 100;

    public GameObject healthBar;
    public Image forground_healthBar;

    public GameObject Progress_countstar;
    public GameObject Goal_Boss;

    public GameObject[] effectHitBossColor;
    public GameObject[] effectHitBossType;

    public GameObject[] effect_Move_Color;

    private List<P_Goal_Object> listGoal = null;

	float minx = -0.5f;
	float maxx = 0.5f;
	float miny = 2f;
	float maxy = 3f;

	// Use this for initialization
	void Start () {

        listGoal = new List<P_Goal_Object>();
        listGoal.AddRange(Contance.LIST_GOAL_OBJECT);

        healthBar.SetActive(true);
        Goal_Boss.SetActive(true);
        maxHealth = Contance.bossMaxHealth;
        currentHealth = maxHealth;
        GetModeBossItem();
	}
	
	// Update is called once per frame
 

    public void HandleHealth(int value)
    {
        if (currentHealth >= 0)
        {
            currentHealth = currentHealth - value;
            currentPercent = currentPercent - (float)value / maxHealth * 100;
			if(currentPercent<1)
			{
				currentPercent = 0;
			}

            if (currentPercent > 0)
            {
                textPercent.text = Mathf.RoundToInt(currentPercent).ToString() + "%";
                //HandleEffectGoal(type);

                AnimationHitBoss(this.gameObject, Contance.TRUNG_NUOC_QUA, false);
            }
            else
            {
                textPercent.text = "0%";
				StartCoroutine (waitTime());
				AnimationWinOrLose(this.gameObject, Contance.THUA_Boss, true);
				Invoke ("BossDown", 3f);

                Contance.GAME_WIN = true;
                //Destroy(this.gameObject);
                //print("Finish");
            }
            forground_healthBar.fillAmount -= (float)value / maxHealth;
        }
    }

	IEnumerator waitTime()
	{
		yield return new WaitForSeconds (1f);
		healthBar.SetActive (false);
	}

	void BossDown()
	{
		iTween.RotateTo(this.gameObject, iTween.Hash("rotation", new Vector3(0, 0, -115), "time", 3f));
	}

    public void HandleEffect()
    {
        for(int i = 0; i < listGoal.Count; i++)
        {
            var obj = Goal_Boss.transform.GetChild(i).gameObject;
            
        }
    }

    public void CreateEffectColor(Tile tile)
    {
        if (tile.tileData.tileType == TileData.TileType.match_object)
        {
            switch (tile.tileData.tileColor)
            {
                #region effect blue
                case TileData.TileColor.blue:
                    if (PoolingManager.poolingManager.List_Effect_Order_Blue.Count > 0)
                    {
                        GameObject effectblue = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Order_Blue, tile);
					CollectFood(effectblue, tile.transform.position, new Vector3(Random.Range(transform.position.x + minx, transform.position.x + maxx), Random.Range(transform.position.y + miny, transform.position.y + maxy), 0));
                        LeanTween.delayedCall(effectblue,1f, () =>
                        {
                            if(PoolingManager.poolingManager.List_Effect_Boss_Blue.Count > 0)
                            {
                                GameObject blue = PoolingManager.poolingManager.GetEffectBonusFromPool(PoolingManager.poolingManager.List_Effect_Boss_Blue, effectblue.transform.position);
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(blue, PoolingManager.poolingManager.List_Effect_Boss_Blue);
                                });
                            }
                            else
                            {
                                GameObject blue = Instantiate(effectHitBossColor[0], effectblue.transform.position, Quaternion.identity) as GameObject;
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(blue, PoolingManager.poolingManager.List_Effect_Boss_Blue);
                                });
                            }
                            PoolingManager.poolingManager.AddOrderColorToPool(effectblue, PoolingManager.poolingManager.List_Effect_Order_Blue);
                        });
                    }
                    else
                    {
                        GameObject effectblue = Instantiate(effect_Move_Color[0], tile.transform.position, Quaternion.identity) as GameObject;
					CollectFood(effectblue, tile.transform.position, new Vector3(Random.Range(transform.position.x + minx, transform.position.x + maxx), Random.Range(transform.position.y + miny, transform.position.y + maxy), 0));//-0.5,0.5,1,1.5

                        LeanTween.delayedCall(effectblue,1f, () =>
                        {
                            if (PoolingManager.poolingManager.List_Effect_Boss_Blue.Count > 0)
                            {
                                GameObject blue = PoolingManager.poolingManager.GetEffectBonusFromPool(PoolingManager.poolingManager.List_Effect_Boss_Blue, effectblue.transform.position);
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(blue, PoolingManager.poolingManager.List_Effect_Boss_Blue);
                                });
                            }
                            else
                            {
                                GameObject blue = Instantiate(effectHitBossColor[0], effectblue.transform.position, Quaternion.identity) as GameObject;
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(blue, PoolingManager.poolingManager.List_Effect_Boss_Blue);
                                });
                            }
                            PoolingManager.poolingManager.AddOrderColorToPool(effectblue, PoolingManager.poolingManager.List_Effect_Order_Blue);
                        });
                    }
                    break;
                #endregion

                #region effect green
                case TileData.TileColor.green:
                    if (PoolingManager.poolingManager.List_Effect_Order_Green.Count > 0)
                    {
                        GameObject effectgreen = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Order_Green, tile);
					CollectFood(effectgreen, tile.transform.position, new Vector3(Random.Range(transform.position.x + minx, transform.position.x + maxx), Random.Range(transform.position.y + miny, transform.position.y + maxy), 0));
                        LeanTween.delayedCall(effectgreen, 1f, () =>
                        {
                            if (PoolingManager.poolingManager.List_Effect_Boss_Green.Count > 0)
                            {
                                GameObject green = PoolingManager.poolingManager.GetEffectBonusFromPool(PoolingManager.poolingManager.List_Effect_Boss_Green, effectgreen.transform.position);
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Green);
                                });
                            }
                            else
                            {
                                GameObject green = Instantiate(effectHitBossColor[1], effectgreen.transform.position, Quaternion.identity) as GameObject;
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Green);
                                });
                            }
                            PoolingManager.poolingManager.AddOrderColorToPool(effectgreen, PoolingManager.poolingManager.List_Effect_Order_Green);
                        });
                    }
                    else
                    {
                        GameObject effectgreen = Instantiate(effect_Move_Color[1], tile.transform.position, Quaternion.identity) as GameObject;
					CollectFood(effectgreen, tile.transform.position, new Vector3(Random.Range(transform.position.x + minx, transform.position.x + maxx), Random.Range(transform.position.y + miny, transform.position.y + maxy), 0));
                        LeanTween.delayedCall(effectgreen, 1f, () =>
                        {
                            if (PoolingManager.poolingManager.List_Effect_Boss_Green.Count > 0)
                            {
                                GameObject green = PoolingManager.poolingManager.GetEffectBonusFromPool(PoolingManager.poolingManager.List_Effect_Boss_Green, effectgreen.transform.position);
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Green);
                                });
                            }
                            else
                            {
                                GameObject green = Instantiate(effectHitBossColor[1], effectgreen.transform.position, Quaternion.identity) as GameObject;
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Green);
                                });
                            }
                            PoolingManager.poolingManager.AddOrderColorToPool(effectgreen, PoolingManager.poolingManager.List_Effect_Order_Green);
                        });
                    }
                    break;
                #endregion

                #region effect orange
                case TileData.TileColor.orange:
                    if (PoolingManager.poolingManager.List_Effect_Order_Orange.Count > 0)
                    {
                        GameObject effectgreen = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Order_Orange, tile);
					CollectFood(effectgreen, tile.transform.position, new Vector3(Random.Range(transform.position.x + minx, transform.position.x + maxx), Random.Range(transform.position.y + miny, transform.position.y + maxy), 0));
                        LeanTween.delayedCall(effectgreen, 1f, () =>
                        {
                            if (PoolingManager.poolingManager.List_Effect_Boss_Orange.Count > 0)
                            {
                                GameObject green = PoolingManager.poolingManager.GetEffectBonusFromPool(PoolingManager.poolingManager.List_Effect_Boss_Orange, effectgreen.transform.position);
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Orange);
                                });
                            }
                            else
                            {
                                GameObject green = Instantiate(effectHitBossColor[2], effectgreen.transform.position, Quaternion.identity) as GameObject;
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Orange);
                                });
                            }
                            PoolingManager.poolingManager.AddOrderColorToPool(effectgreen, PoolingManager.poolingManager.List_Effect_Order_Orange);
                        });
                    }
                    else
                    {
                        GameObject effectgreen = Instantiate(effect_Move_Color[2], tile.transform.position, Quaternion.identity) as GameObject;
					CollectFood(effectgreen, tile.transform.position, new Vector3(Random.Range(transform.position.x + minx, transform.position.x + maxx), Random.Range(transform.position.y + miny, transform.position.y + maxy), 0));
                        LeanTween.delayedCall(effectgreen, 1f, () =>
                        {
                            if (PoolingManager.poolingManager.List_Effect_Boss_Orange.Count > 0)
                            {
                                GameObject green = PoolingManager.poolingManager.GetEffectBonusFromPool(PoolingManager.poolingManager.List_Effect_Boss_Orange, effectgreen.transform.position);
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Orange);
                                });
                            }
                            else
                            {
                                GameObject green = Instantiate(effectHitBossColor[2], effectgreen.transform.position, Quaternion.identity) as GameObject;
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Orange);
                                });
                            }
                            PoolingManager.poolingManager.AddOrderColorToPool(effectgreen, PoolingManager.poolingManager.List_Effect_Order_Orange);
                        });
                    }
                    break;
                #endregion

                #region effect purple
                case TileData.TileColor.purple:
                    if (PoolingManager.poolingManager.List_Effect_Order_Purple.Count > 0)
                    {
                        GameObject effectgreen = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Order_Purple, tile);
					CollectFood(effectgreen, tile.transform.position, new Vector3(Random.Range(transform.position.x + minx, transform.position.x + maxx), Random.Range(transform.position.y + miny, transform.position.y + maxy), 0));
                        LeanTween.delayedCall(effectgreen, 1f, () =>
                        {
                            if (PoolingManager.poolingManager.List_Effect_Boss_Purple.Count > 0)
                            {
                                GameObject green = PoolingManager.poolingManager.GetEffectBonusFromPool(PoolingManager.poolingManager.List_Effect_Boss_Purple, effectgreen.transform.position);
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Purple);
                                });
                            }
                            else
                            {
                                GameObject green = Instantiate(effectHitBossColor[3], effectgreen.transform.position, Quaternion.identity) as GameObject;
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Purple);
                                });
                            }
                            PoolingManager.poolingManager.AddOrderColorToPool(effectgreen, PoolingManager.poolingManager.List_Effect_Order_Purple);
                        });
                    }
                    else
                    {
                        GameObject effectgreen = Instantiate(effect_Move_Color[3], tile.transform.position, Quaternion.identity) as GameObject;
					CollectFood(effectgreen, tile.transform.position, new Vector3(Random.Range(transform.position.x + minx, transform.position.x + maxx), Random.Range(transform.position.y + miny, transform.position.y + maxy), 0));
                        LeanTween.delayedCall(effectgreen, 1f, () =>
                        {
                            if (PoolingManager.poolingManager.List_Effect_Boss_Purple.Count > 0)
                            {
                                GameObject green = PoolingManager.poolingManager.GetEffectBonusFromPool(PoolingManager.poolingManager.List_Effect_Boss_Purple, effectgreen.transform.position);
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Purple);
                                });
                            }
                            else
                            {
                                GameObject green = Instantiate(effectHitBossColor[3], effectgreen.transform.position, Quaternion.identity) as GameObject;
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Purple);
                                });
                            }
                            PoolingManager.poolingManager.AddOrderColorToPool(effectgreen, PoolingManager.poolingManager.List_Effect_Order_Purple);
                        });
                    }
                    break;
                #endregion

                #region effect red
                case TileData.TileColor.red:
                    if (PoolingManager.poolingManager.List_Effect_Order_Red.Count > 0)
                    {
                        GameObject effectgreen = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Order_Red, tile);
					CollectFood(effectgreen, tile.transform.position, new Vector3(Random.Range(transform.position.x + minx, transform.position.x + maxx), Random.Range(transform.position.y + miny, transform.position.y + maxy), 0));
                        LeanTween.delayedCall(effectgreen, 1f, () =>
                        {
                            if (PoolingManager.poolingManager.List_Effect_Boss_Red.Count > 0)
                            {
                                GameObject green = PoolingManager.poolingManager.GetEffectBonusFromPool(PoolingManager.poolingManager.List_Effect_Boss_Red, effectgreen.transform.position);
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Red);
                                });
                            }
                            else
                            {
                                GameObject green = Instantiate(effectHitBossColor[4], effectgreen.transform.position, Quaternion.identity) as GameObject;
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Red);
                                });
                            }
                            PoolingManager.poolingManager.AddOrderColorToPool(effectgreen, PoolingManager.poolingManager.List_Effect_Order_Red);
                        });
                    }
                    else
                    {
                        GameObject effectgreen = Instantiate(effect_Move_Color[4], tile.transform.position, Quaternion.identity) as GameObject;
					CollectFood(effectgreen, tile.transform.position, new Vector3(Random.Range(transform.position.x + minx, transform.position.x + maxx), Random.Range(transform.position.y + miny, transform.position.y + maxy), 0));
                        LeanTween.delayedCall(effectgreen, 1f, () =>
                        {
                            if (PoolingManager.poolingManager.List_Effect_Boss_Red.Count > 0)
                            {
                                GameObject green = PoolingManager.poolingManager.GetEffectBonusFromPool(PoolingManager.poolingManager.List_Effect_Boss_Red, effectgreen.transform.position);
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Red);
                                });
                            }
                            else
                            {
                                GameObject green = Instantiate(effectHitBossColor[4], effectgreen.transform.position, Quaternion.identity) as GameObject;
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Red);
                                });
                            }
                            PoolingManager.poolingManager.AddOrderColorToPool(effectgreen, PoolingManager.poolingManager.List_Effect_Order_Red);
                        });
                    }
                    break;
                #endregion

                #region effect yellow
                case TileData.TileColor.yellow:
                    if (PoolingManager.poolingManager.List_Effect_Order_Yellow.Count > 0)
                    {
                        GameObject effectgreen = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Order_Yellow, tile);
							CollectFood(effectgreen, tile.transform.position, new Vector3(Random.Range(transform.position.x + minx, transform.position.x + maxx), Random.Range(transform.position.y + miny, transform.position.y + maxy), 0));
                        LeanTween.delayedCall(effectgreen, 1f, () =>
                        {
                            if (PoolingManager.poolingManager.List_Effect_Boss_Yellow.Count > 0)
                            {
                                GameObject green = PoolingManager.poolingManager.GetEffectBonusFromPool(PoolingManager.poolingManager.List_Effect_Boss_Yellow, effectgreen.transform.position);
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Yellow);
                                });
                            }
                            else
                            {
                                GameObject green = Instantiate(effectHitBossColor[5], effectgreen.transform.position, Quaternion.identity) as GameObject;
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Yellow);
                                });
                            }
                            PoolingManager.poolingManager.AddOrderColorToPool(effectgreen, PoolingManager.poolingManager.List_Effect_Order_Yellow);
                        });
                    }
                    else
                    {
                        GameObject effectgreen = Instantiate(effect_Move_Color[5], tile.transform.position, Quaternion.identity) as GameObject;
							CollectFood(effectgreen, tile.transform.position, new Vector3(Random.Range(transform.position.x + minx, transform.position.x + maxx), Random.Range(transform.position.y + miny, transform.position.y + maxy), 0));
                        LeanTween.delayedCall(effectgreen, 1f, () =>
                        {
                            if (PoolingManager.poolingManager.List_Effect_Boss_Yellow.Count > 0)
                            {
                                GameObject green = PoolingManager.poolingManager.GetEffectBonusFromPool(PoolingManager.poolingManager.List_Effect_Boss_Yellow, effectgreen.transform.position);
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Yellow);
                                });
                            }
                            else
                            {
                                GameObject green = Instantiate(effectHitBossColor[5], effectgreen.transform.position, Quaternion.identity) as GameObject;
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Yellow);
                                });
                            }
                            PoolingManager.poolingManager.AddOrderColorToPool(effectgreen, PoolingManager.poolingManager.List_Effect_Order_Yellow);
                        });
                    }
                    break;
                #endregion
                default:
                    break;
            }
        }
        else
        {
            switch (tile.tileData.tileType)
            {
                #region effect muffin
                case TileData.TileType.muffin:
                    if (PoolingManager.poolingManager.List_Effect_Order_Muffin.Count > 0)
                    {
                        GameObject effectgreen = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Order_Muffin, tile);
									CollectFood(effectgreen, tile.transform.position, new Vector3(Random.Range(transform.position.x + minx, transform.position.x + maxx), Random.Range(transform.position.y + miny, transform.position.y + maxy), 0));
                        LeanTween.delayedCall(effectgreen, 1f, () =>
                        {
                            PoolingManager.poolingManager.AddOrderColorToPool(effectgreen, PoolingManager.poolingManager.List_Effect_Order_Muffin);
                        });
                    }
                    else
                    {
                        GameObject effectgreen = Instantiate(effectHitBossType[0], tile.transform.position, Quaternion.identity) as GameObject;
									CollectFood(effectgreen, tile.transform.position, new Vector3(Random.Range(transform.position.x + minx, transform.position.x + maxx), Random.Range(transform.position.y + miny, transform.position.y + maxy), 0));
                        LeanTween.delayedCall(effectgreen, 1f, () =>
                        {
                            PoolingManager.poolingManager.AddOrderColorToPool(effectgreen, PoolingManager.poolingManager.List_Effect_Order_Muffin);
                        });
                    }
                    break;
                #endregion

                #region effect cow
                case TileData.TileType.cow:
                    if (PoolingManager.poolingManager.List_Effect_Boss_Cow.Count > 0)
                    {
                        GameObject effectgreen = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Boss_Cow, tile);
									CollectFood(effectgreen, tile.transform.position, new Vector3(Random.Range(transform.position.x + minx, transform.position.x + maxx), Random.Range(transform.position.y + miny, transform.position.y + maxy), 0));
                        LeanTween.delayedCall(effectgreen, 1f, () =>
                        {
                            if (PoolingManager.poolingManager.List_Effect_Boss_White.Count > 0)
                            {
                                GameObject green = PoolingManager.poolingManager.GetEffectBonusFromPool(PoolingManager.poolingManager.List_Effect_Boss_White, effectgreen.transform.position);
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_White);
                                });
                            }
                            else
                            {
                                GameObject green = Instantiate(effectHitBossColor[6], effectgreen.transform.position, Quaternion.identity) as GameObject;
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_White);
                                });
                            }
                            PoolingManager.poolingManager.AddOrderColorToPool(effectgreen, PoolingManager.poolingManager.List_Effect_Boss_Cow);
                        });
                    }
                    else
                    {
                        GameObject effectgreen = Instantiate(effectHitBossType[1], tile.transform.position, Quaternion.identity) as GameObject;
									CollectFood(effectgreen, tile.transform.position, new Vector3(Random.Range(transform.position.x + minx, transform.position.x + maxx), Random.Range(transform.position.y + miny, transform.position.y + maxy), 0));
                        LeanTween.delayedCall(effectgreen, 1f, () =>
                        {
                            if (PoolingManager.poolingManager.List_Effect_Boss_White.Count > 0)
                            {
                                GameObject green = PoolingManager.poolingManager.GetEffectBonusFromPool(PoolingManager.poolingManager.List_Effect_Boss_White, effectgreen.transform.position);
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_White);
                                });
                            }
                            else
                            {
                                GameObject green = Instantiate(effectHitBossColor[6], effectgreen.transform.position, Quaternion.identity) as GameObject;
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_White);
                                });
                            }
                            PoolingManager.poolingManager.AddOrderColorToPool(effectgreen, PoolingManager.poolingManager.List_Effect_Boss_Cow);
                        });
                    }
                    break;
                #endregion

                #region effect coconut_2
                case TileData.TileType.coconut_2:
                    if (PoolingManager.poolingManager.List_Effect_Order_White.Count > 0)
                    {
                        GameObject effectgreen = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Order_White, tile);
									CollectFood(effectgreen, tile.transform.position, new Vector3(Random.Range(transform.position.x + minx, transform.position.x + maxx), Random.Range(transform.position.y + miny, transform.position.y + maxy), 0));
                        LeanTween.delayedCall(effectgreen, 1f, () =>
                        {
                            if (PoolingManager.poolingManager.List_Effect_Boss_White.Count > 0)
                            {
                                GameObject green = PoolingManager.poolingManager.GetEffectBonusFromPool(PoolingManager.poolingManager.List_Effect_Boss_White, effectgreen.transform.position);
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_White);
                                });
                            }
                            else
                            {
                                GameObject green = Instantiate(effectHitBossColor[6], effectgreen.transform.position, Quaternion.identity) as GameObject;
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_White);
                                });
                            }
                            PoolingManager.poolingManager.AddOrderColorToPool(effectgreen, PoolingManager.poolingManager.List_Effect_Order_White);
                        });
                    }
                    else
                    {
                        GameObject effectgreen = Instantiate(effectHitBossType[2], tile.transform.position, Quaternion.identity) as GameObject;
									CollectFood(effectgreen, tile.transform.position, new Vector3(Random.Range(transform.position.x + minx, transform.position.x + maxx), Random.Range(transform.position.y + miny, transform.position.y + maxy), 0));
                        LeanTween.delayedCall(effectgreen, 1f, () =>
                        {
                            if (PoolingManager.poolingManager.List_Effect_Boss_White.Count > 0)
                            {
                                GameObject green = PoolingManager.poolingManager.GetEffectBonusFromPool(PoolingManager.poolingManager.List_Effect_Boss_White, effectgreen.transform.position);
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_White);
                                });
                            }
                            else
                            {
                                GameObject green = Instantiate(effectHitBossColor[6], effectgreen.transform.position, Quaternion.identity) as GameObject;
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_White);
                                });
                            }
                            PoolingManager.poolingManager.AddOrderColorToPool(effectgreen, PoolingManager.poolingManager.List_Effect_Order_White);
                        });
                    }
                    break;
                #endregion
               
                default:
                    break;
            }
        }
    }




    private void GetModeBossItem()
    {
        for (int i = 0; i < listGoal.Count; i++)
        {
            Goal_Boss.transform.GetChild(i).gameObject.SetActive(true);
            if (listGoal[i].object_type_Goal_Object.Equals("match_object"))
            {
                Goal_Boss.transform.GetChild(i).name = listGoal[i].color_Goal_Object;
                Goal_Boss.transform.GetChild(i).GetComponent<Image>().sprite = GetSpriteColor(listGoal[i].color_Goal_Object);
            }
            else
            {
                Goal_Boss.transform.GetChild(i).name = listGoal[i].object_type_Goal_Object;
                Goal_Boss.transform.GetChild(i).GetComponent<Image>().sprite = GetSpriteObject(listGoal[i].object_type_Goal_Object);
            }
        }
    }



    public Sprite[] spriteColor;
    private Sprite GetSpriteColor(string colorGoal)
    {
        switch (colorGoal)
        {
            case GameConstants.red_tile_color:
                return spriteColor[0];

            case GameConstants.yellow_tile_color:
                return spriteColor[1];

            case GameConstants.green_tile_color:
                return spriteColor[2];

            case GameConstants.blue_tile_color:
                return spriteColor[3];

            case GameConstants.orange_tile_color:
                return spriteColor[4];

            case GameConstants.purple_tile_color:
                return spriteColor[5];
        }

        return null;
    }


    public Sprite[] spriteObject;
    private Sprite GetSpriteObject(string objectGoal)
    {
        switch (objectGoal)
        {
            case Contance.HONEY:
                return spriteObject[0];

            case Contance.MUFFIN:
                return spriteObject[1];

            case Contance.COW:
                return spriteObject[2];

            case Contance.COCONUT:
                return spriteObject[3];

            case Contance.YOGURT:
                return spriteObject[4];
        }
        return null;
    }

    public IEnumerator HandleEffectGoal(string type, Tile tile)
    {
        for (int i = 0; i < listGoal.Count; i++)
        {
            var obj = Goal_Boss.transform.GetChild(i).gameObject;
            if (obj.name.Contains(type) && !Contance.GAME_WIN)
            {
                #region effect honey
                if (type == Contance.HONEY)
                {
                    if (PoolingManager.poolingManager.List_Effect_Order_Orange.Count > 0)
                    {
                        GameObject effectgreen = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Order_Orange, tile);
											CollectFood(effectgreen, tile.transform.position, new Vector3(Random.Range(transform.position.x + minx, transform.position.x + maxx), Random.Range(transform.position.y + miny, transform.position.y + maxy), 0));
                        LeanTween.delayedCall(effectgreen, 1f, () =>
                        {
                            if (PoolingManager.poolingManager.List_Effect_Boss_Orange.Count > 0)
                            {
                                GameObject green = PoolingManager.poolingManager.GetEffectBonusFromPool(PoolingManager.poolingManager.List_Effect_Boss_Orange, effectgreen.transform.position);
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Orange);
                                });
                            }
                            else
                            {
                                GameObject green = Instantiate(effectHitBossColor[2], effectgreen.transform.position, Quaternion.identity) as GameObject;
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Orange);
                                });
                            }
                            PoolingManager.poolingManager.AddOrderColorToPool(effectgreen, PoolingManager.poolingManager.List_Effect_Order_Orange);
                        });
                    }
                    else
                    {
                        GameObject effectgreen = Instantiate(effect_Move_Color[2], tile.transform.position, Quaternion.identity) as GameObject;
											CollectFood(effectgreen, tile.transform.position, new Vector3(Random.Range(transform.position.x + minx, transform.position.x + maxx), Random.Range(transform.position.y + miny, transform.position.y + maxy), 0));
                        LeanTween.delayedCall(effectgreen, 1f, () =>
                        {
                            if (PoolingManager.poolingManager.List_Effect_Boss_Orange.Count > 0)
                            {
                                GameObject green = PoolingManager.poolingManager.GetEffectBonusFromPool(PoolingManager.poolingManager.List_Effect_Boss_Orange, effectgreen.transform.position);
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Orange);
                                });
                            }
                            else
                            {
                                GameObject green = Instantiate(effectHitBossColor[2], effectgreen.transform.position, Quaternion.identity) as GameObject;
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Orange);
                                });
                            }
                            PoolingManager.poolingManager.AddOrderColorToPool(effectgreen, PoolingManager.poolingManager.List_Effect_Order_Orange);
                        });
                    }
                }
                #endregion

                #region effect yogurt
                else if (type == GameConstants.yogurt_tile_type)
                {
                    if (PoolingManager.poolingManager.List_Effect_Order_Purple.Count > 0)
                    {
                        GameObject effectgreen = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Order_Purple, tile);
											CollectFood(effectgreen, tile.transform.position, new Vector3(Random.Range(transform.position.x + minx, transform.position.x + maxx), Random.Range(transform.position.y + miny, transform.position.y + maxy), 0));
                        LeanTween.delayedCall(effectgreen, 1f, () =>
                        {
                            if (PoolingManager.poolingManager.List_Effect_Boss_Purple.Count > 0)
                            {
                                GameObject green = PoolingManager.poolingManager.GetEffectBonusFromPool(PoolingManager.poolingManager.List_Effect_Boss_Purple, effectgreen.transform.position);
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Purple);
                                });
                            }
                            else
                            {
                                GameObject green = Instantiate(effectHitBossColor[3], effectgreen.transform.position, Quaternion.identity) as GameObject;
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Purple);
                                });
                            }
                            PoolingManager.poolingManager.AddOrderColorToPool(effectgreen, PoolingManager.poolingManager.List_Effect_Order_Purple);
                        });
                    }
                    else
                    {
                        GameObject effectgreen = Instantiate(effect_Move_Color[3], tile.transform.position, Quaternion.identity) as GameObject;
											CollectFood(effectgreen, tile.transform.position, new Vector3(Random.Range(transform.position.x + minx, transform.position.x + maxx), Random.Range(transform.position.y + miny, transform.position.y + maxy), 0));
                        LeanTween.delayedCall(effectgreen, 1f, () =>
                        {
                            if (PoolingManager.poolingManager.List_Effect_Boss_Purple.Count > 0)
                            {
                                GameObject green = PoolingManager.poolingManager.GetEffectBonusFromPool(PoolingManager.poolingManager.List_Effect_Boss_Purple, effectgreen.transform.position);
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Purple);
                                });
                            }
                            else
                            {
                                GameObject green = Instantiate(effectHitBossColor[3], effectgreen.transform.position, Quaternion.identity) as GameObject;
                                LeanTween.delayedCall(0.3f, () =>
                                {
                                    PoolingManager.poolingManager.AddBonusToPool(green, PoolingManager.poolingManager.List_Effect_Boss_Purple);
                                });
                            }
                            PoolingManager.poolingManager.AddOrderColorToPool(effectgreen, PoolingManager.poolingManager.List_Effect_Order_Purple);
                        });
                    }
                }
                #endregion
                else
                {
                    CreateEffectColor(tile);
                }
                yield return new WaitForSeconds(1f);
                ScaleItem(obj);
                int value = listGoal[i].value_Goal_Object;
                HandleHealth(value);
                break;
            }
        }
    }
    public void ScaleItem(GameObject obj)
    {
        iTween.ScaleTo(obj, iTween.Hash("scale", new Vector3(1.3f, 1.3f, 1.3f), "time", 0.1f));
        iTween.ScaleTo(obj, iTween.Hash("scale", new Vector3(1f, 1f, 1f), "time", 0.1f, "delay", 0.1f));
    }


    public void AnimationHitBoss(GameObject obj, string nameAnimation, bool loop)
    {
        if (!obj.GetComponent<SkeletonAnimation>().AnimationName.Equals(nameAnimation))
        {
            obj.GetComponent<SkeletonAnimation>().state.SetAnimation(0, nameAnimation, loop);
            obj.GetComponent<SkeletonAnimation>().state.AddAnimation(0, Contance.NGOI, true, 0f);
        }
    }

    public void AnimationWinOrLose(GameObject obj, string nameAnimation, bool loop)
    {
        if (!obj.GetComponent<SkeletonAnimation>().AnimationName.Equals(nameAnimation))
        {
            obj.GetComponent<SkeletonAnimation>().state.SetAnimation(0, nameAnimation, loop);
        }
    }


    public IEnumerator BossHit_HoaQua()
    {
        yield return new WaitForSeconds(0.8f);
        //HandleHealth(GameConstants.red_tile_color,20);
    }

    public void MoveObstruction(GameObject obj,Vector3 start, Vector3 end)
    {
        //iTween.ScaleTo(obj, iTween.Hash("scale", new Vector3(0.5f, 0.5f, 0.5f), "time", 1f));
		iTween.MoveTo(obj, iTween.Hash("path", new Vector3[] { start, new Vector3(Random.Range(-1f, 1f), Random.Range(start.y, end.y), -4.5f), end }, "time", 1, "easetype", iTween.EaseType.linear, "oncomplete", "Done_Move", "oncompletetarget", gameObject,"oncompleteparams", obj));
    }
    public void CollectFood(GameObject obj, Vector3 start, Vector3 end)
    {
		iTween.MoveTo(obj, iTween.Hash("path", new Vector3[] { start, new Vector3(Random.Range(-2.4f, 2.4f), Random.Range(start.y, end.y), 0), end }, "time", 1, "easetype", iTween.EaseType.linear, "oncomplete", "HitBoss", "oncompletetarget", gameObject));
    }

	public void Done_Move(GameObject obj)
	{
		obj.transform.GetChild (0).gameObject.SetActive (true);;
	}

    public void HitBoss()
    {
        //ScaleItem(obj);
    }


    //============================================Boss tha vat can======================================================================================

    private List<Grid> listGridTemp;
    public void BossDropObstacle_Blocker()
    {

        if (Contance.bossScripts.quantity_B > 0 && GameManager.MOVE > 0)
        {
            AnimationHitBoss(gameObject, Contance.TUNG_VAT_CAN, false);

            listGridTemp = new List<Grid>();
            for (int i = 0; i < Contance.bossScripts.quantity_B; i++)
            {
                var grid = GetRandomGrid(Contance.LIST_INDEX_BLOCKER);
                Tile oldTile = grid.GetTile();

                Tile newtile = PoolingManager.poolingManager.GetTile();

				newtile.tileData.tileColor = oldTile.tileData.tileColor;

                //newtile.mainRender.sortingOrder++;
                SetDataTile(newtile, Contance.bossScripts.object_type_B);
				float varnum = 1.5f;
				if(newtile.tileData.tileType == TileData.TileType.match_object)
				{
					newtile.transform.GetChild (0).gameObject.SetActive (false);
					varnum = 1.5f;
				}
                //tile.transform.parent = null;

                grid.SetTile(newtile);
                //newtile.transform.localScale = new Vector3(0.3f, 0.3f, 1f);

                if (oldTile != null)
                {
                    oldTile.grid = null;
                }

                //grid.GetTile().transform.localPosition -= new Vector3(0f, 0f, -1f);

				newtile.transform.position = new Vector3(0, transform.position.y + varnum, -9);
                MoveObstruction(newtile.gameObject, new Vector3(0,transform.position.y + 0.8f, newtile.transform.position.z), oldTile.transform.position);



                //LeanTween.scale(oldTile.gameObject, Vector3.zero, 1f);

                // @Modify by: CuongNH 
                // Khong cho roi khi tha vat can -> stop game folow
                GamePlayController.gamePlayController.gameInterceptorCount++;

                LeanTween.delayedCall(1.01f, () =>
                {
                    if (oldTile != null)
                    {
                        //oldTile.gameObject.SetActive(false);
                        newtile.transform.localPosition = new Vector3(0, 0, 0);
                        PoolingManager.poolingManager.AddTileToPool(oldTile);
                        //newtile.mainRender.sortingOrder--;
                    }

                    // @Modify by: CuongNH 
                    // Cho tiep tuc chay game folow
                    if (GamePlayController.gamePlayController.gameInterceptorCount > 0)
                    {
                        GamePlayController.gamePlayController.gameInterceptorCount--;
                    }

                    GamePlayController.gamePlayController.mapController.isProcessing = true;
                    GamePlayController.gamePlayController.cascadeFlag = true;
                });

            }

            listGridTemp.Clear();
        }
        
        //listGridTemp.Clear();
    }

    public void SetDataTile(Tile tile, string objType)
    {
        switch (objType)
        {
            case "sugar":
                tile.SetTileType(Contance.bossScripts.object_type_B);
                tile.SetData(Contance.bossScripts.data_B);
                GamePlayController.gamePlayController.mapController.tileSpawner.SetTileMainRender(tile);

                break;
            case "ice_cage":
                tile.SetTileType(TileData.TileType.match_object);
                tile.SetTileModifierType(Contance.bossScripts.object_type_B);
                tile.SetData(Contance.bossScripts.data_B);
                //RandomColorTile(tile);
                GamePlayController.gamePlayController.mapController.tileSpawner.SetTileMainRender(tile);
                GamePlayController.gamePlayController.mapController.tileSpawner.SetTileEnhanceRender(tile);
                break;
            case "muffin":
                tile.SetTileType(Contance.bossScripts.object_type_B);
                tile.SetData(Contance.bossScripts.data_B);
                GamePlayController.gamePlayController.mapController.tileSpawner.SetTileMainRender(tile);
                break;
            case "yogurt":
                tile.SetTileType(Contance.bossScripts.object_type_B);
                tile.SetData(Contance.bossScripts.data_B);
                GamePlayController.gamePlayController.mapController.tileSpawner.SetTileMainRender(tile);
                break;
            default:
                break;
        }
    }

    public void RandomColorTile(Tile tile)
    {
        int rand = Random.Range(0,5);
        switch(rand)
        {
            case 0:
                tile.tileData.tileColor = TileData.TileColor.blue;
                break;
            case 1:
                tile.tileData.tileColor = TileData.TileColor.green;
                break;
            case 2:
                tile.tileData.tileColor = TileData.TileColor.orange;
                break;
            case 3:
                tile.tileData.tileColor = TileData.TileColor.purple;
                break;
            case 4:
                tile.tileData.tileColor = TileData.TileColor.red;
                break;
            case 5:
                tile.tileData.tileColor = TileData.TileColor.yellow;
                break;
        }
    }

    public Grid GetRandomGrid(List<Index> list)
    {
        Grid grid = null;
        Tile tile = null;

        if (list == null || list.Count == 0)
        {
            return null;
        }
        int count = 0;
        int rand = 0;

        while (true)
        {
            count++;
            if (count > GameConstants.MAX_RESET)
            {
                break;
            }
            rand = Random.Range(0, list.Count);

            Index ind = list[rand];
            grid = GamePlayController.gamePlayController.mapController.gridList[ind.row * Contance.width + ind.col];
            if (listGridTemp.Contains(grid))
            {
                continue;
            }
            else
            {

                if (grid != null && grid.gridData.gridType != GridData.GridType.NULL_GRID)
                {
                    tile = grid.GetTile();
                    if (tile != null && tile.tileData.tileType == TileData.TileType.match_object && tile.tileData.tileModifierType ==  TileData.TileModifierType.none)
                    {
                        listGridTemp.Add(grid);
                        return grid;
                    }
                    else
                    {
                        grid = null;
                    }
                }
            }
        }

        return grid;
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Spine.Unity;

public class LoadModeGame : MonoBehaviour {
	public GameObject HealthBar_Boss;
	public GameObject Goal_Boss;
	public GameObject Order_Mode;
	public GameObject Score_Clear_Juice;


    public GameObject[] itemGoal;
    public Transform pos;
	public GameObject[] Glasses;
    public List<CharacterScript> listGoalCurrent = null;
    public Transform tempGoal;

    public Text txtgoal;
    

    public Transform modeGame;

    private string[] color = {"red","yellow","green","blue","purple","orange"};

    //public GameObject[] obj;

    public GameObject Boss;
    public GameObject popsicle;
    public GameObject popsiclecollec;
    public GameObject clearJuice;


    public GameObject[] effect_Move_Color;

    private List<P_Goal_Object> listGoal = null;

    public GameObject Bomb;

    public Sprite[] scene;
    public Sprite[] bg;

    public Image iScene;
    public Image iBG;

	public Sprite originGlass;
	public Sprite muffinPlate;
	public Sprite yogurtCup;
	public Sprite honeyCup;
	public Sprite[] glass_red;
	public Sprite[] glass_green;
	public Sprite[] glass_blue;
	public Sprite[] glass_orange;
	public Sprite[] glass_yellow;
	public Sprite[] glass_purple;

	public Sprite[] muffin_obj;
	public Sprite[] yogurt_obj;
	public Sprite[] cow_obj;
	public Sprite[] honey_obj;
	public Sprite[] coconut_2;

	// Use this for initialization
	void Start() {
        SetRandomBGAndScene();
        listGoal = new List<P_Goal_Object>();
        listGoal.AddRange(Contance.LIST_GOAL_OBJECT);

        listGoalCurrent = new List<CharacterScript>();

        txtgoal.text = Contance.LIST_GOAL_OBJECT.Count.ToString();
	   LoadeMode (Contance.MODE);


	}

    void SetRandomBGAndScene()
    {
        int rand = Random.Range(0,bg.Length);
        iScene.sprite = scene[rand];
        iBG.sprite = bg[rand];
    }


	// Update is called once per frame
	void Update () {
        //if (Input.GetKeyDown(KeyCode.Escape))
        //{

        //    //CheckFinishItemGoal_Order(Random.Range(1, 3), color[Random.Range(0, color.Length)]);
        //    Application.LoadLevel("MapLevel");
        //}

		//test
//        if (Input.GetKeyDown(KeyCode.H))
//        {
//           StartCoroutine(Boss.GetComponent<BossScript>().BossHit_HoaQua());
//        }
//        if (Input.GetKeyDown(KeyCode.G))
//        {
//            Instantiate(Bomb);
//            //clearJuice.GetComponent<Clear_JuiceScript>().MoveCrackFinish(popsiclecollec);
//        }
//        if (Input.GetKeyDown(KeyCode.P))
//        {
//            popsicle.GetComponent<PopsicleScript>().MovePopsicleFinish(popsiclecollec);
//        }
	}


    //public GameObject obj;
    public void CheckFinishItemGoal_Order(string type)
    {
        if (listGoalCurrent.Count > 3)
        {
            for (int i = 0; i < 3; i++)
            {
                if (listGoalCurrent[i].name.Contains(type))
                {
                    //CollectFood(obj,obj.transform.position,goal[i].transform.GetChild(0).transform.position);
                    int valueGoal = int.Parse(listGoalCurrent[i].transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<TextMesh>().text.ToString());
                    if (valueGoal > 0)
                    {
                        listGoalCurrent[i].transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<TextMesh>().text = (valueGoal - 1).ToString();
                        //CollectFood(EffectObj, posTile.transform.position, goal[i].transform.GetChild(0).position);
                        //ScaleGoalItem(goal[i]);
                    }
                    else
                    {
                        //CollectFood(EffectObj, posTile.transform.position, goal[i].transform.GetChild(0).position);

                        listGoalCurrent[i].transform.GetChild(0).GetChild(1).gameObject.SetActive(false);
                        listGoalCurrent[i].transform.GetChild(0).GetChild(2).gameObject.SetActive(true);
                        //MoveListGoal(goal[3],goal[i].transform);
                        CreateItemGoal();
                        //DestroyGoalItemFinish(goal[i],i);
                    }
                    break;
                }
            }
        }
        else if (listGoalCurrent.Count > 0)
        {
            for (int i = 0; i < listGoalCurrent.Count; i++)
            {
                if (listGoalCurrent[i].name.Contains(type))
                {
                    int valueGoal = int.Parse(listGoalCurrent[i].transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<TextMesh>().text.ToString());

                    if (valueGoal > 0)
                    {
                        listGoalCurrent[i].transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<TextMesh>().text = (valueGoal - 1).ToString();
                        //CollectFood(EffectObj, posTile.transform.position, goal[i].transform.GetChild(0).position);
                        //ScaleGoalItem(goal[i]);
                    }
                    else
                    {
                        //CollectFood(EffectObj, posTile.transform.position, goal[i].transform.GetChild(0).position);

                        listGoalCurrent[i].transform.GetChild(0).GetChild(1).gameObject.SetActive(false);
                        listGoalCurrent[i].transform.GetChild(0).GetChild(2).gameObject.SetActive(true);
                        //DestroyGoalItemFinish(goal[i], i);
                    }
                    break;
                }
            }
        }
    }

    //IEnumerator 

	void SetGlassColor(CharacterScript order, string _color)
	{
		switch (_color) 
		{
		case "blue":
			order.SetGlassSprite (glass_blue);
			break;
		case "green":
			order.SetGlassSprite (glass_green);
			break;					
		case "orange":
			order.SetGlassSprite (glass_orange);
			break;					
		case "purple":
			order.SetGlassSprite (glass_purple);
			break;					
		case "red":
			order.SetGlassSprite (glass_red);
			break;					
		case "yellow":
			order.SetGlassSprite (glass_yellow);
			break;				
		default:
			break;
		}
	}

	void SetObjectType(CharacterScript order,string _type)
	{
		//Debug.Log ("_type"+_type);
		switch (_type) 
		{
		case "muffin":
			order.SetGlassSprite (muffin_obj);
			break;
		case "yogurt":
			order.SetGlassSprite (yogurt_obj);
			break;					
		case "cow":
			order.SetGlassSprite (cow_obj);
			break;
		case "honey":
			order.SetGlassSprite (honey_obj);
			break;
		case "coconut_2":
			order.SetGlassSprite (coconut_2);
			break;
		default:
			break;
		}
	}

    void CreateItemGoal()
    {
        if (listGoal.Count > 0)
        {
            GameObject obj = Instantiate(itemGoal[Random.Range(0,itemGoal.Length)], pos.GetChild(3).position, Quaternion.identity) as GameObject;
            CharacterScript order = obj.GetComponent<CharacterScript>();
            
            if (listGoal[0].object_type_Goal_Object.Equals("match_object"))
            {
				order.SetOValue (listGoal [0].value_Goal_Object);
                order.SetValue(listGoal[0].value_Goal_Object);
                order.SetName(listGoal[0].color_Goal_Object);
                order.SetSprite(listGoal[0].object_type_Goal_Object, listGoal[0].color_Goal_Object);

				SetGlassColor (order, listGoal [0].color_Goal_Object);

            }
            else
            {
				order.SetOValue (listGoal [0].value_Goal_Object);
                order.SetValue(listGoal[0].value_Goal_Object);
                order.SetName(listGoal[0].object_type_Goal_Object);
                order.SetSprite(listGoal[0].object_type_Goal_Object, listGoal[0].color_Goal_Object);

				SetObjectType (order, order._type_object);

            }
            
            obj.transform.parent = tempGoal.transform;
            
            listGoal.RemoveAt(0);
            
        }
    }
    void MoveListGoal(GameObject obj,Transform posObject)
    {
        txtgoal.text = Contance.LIST_GOAL_OBJECT.Count.ToString();
        obj.GetComponent<SkeletonAnimation>().GetComponent<Renderer>().sortingOrder = 0;
        iTween.MoveTo(obj, iTween.Hash("position",new Vector3(posObject.position.x,1.3f,0), "time", 1f, "easetype", iTween.EaseType.linear, "oncomplete", "MoveUpGoal", "oncompletetarget", gameObject, "oncompleteparams", obj));

    }

    void DestroyGoalItemFinish(GameObject obj,int i)
    {
		//listGoalCurrent.
        obj.GetComponent<SkeletonAnimation>().GetComponent<Renderer>().sortingOrder = 0;
        obj.transform.GetChild(0).gameObject.SetActive(false);
        iTween.MoveTo(obj, iTween.Hash("position", new Vector3(obj.transform.position.x, 1.3f, 0), "time", 0.1f, "easetype", iTween.EaseType.linear));
        iTween.MoveTo(obj, iTween.Hash("position", new Vector3(-5,1.3f,0), "time", 1f,"delay",0.1f, "easetype", iTween.EaseType.linear));
        Destroy(obj,1f);
        listGoalCurrent.RemoveAt(i);
    }

	void LoadeMode(string mode)
	{
        // Set game mode
        GameManager.SetGameMode(mode);

		if (mode.Equals (GameConstants.GAMEMODE_OREDER)) {
            Order_Mode.SetActive(true);
           StartCoroutine( GetModeOrderItem());
           //GameManager.gameMode = GameManager.GameMode.ORDER_FULFILLMENT;
        }
        else if (mode.Equals(GameConstants.GAMEMODE_BOSS))
        {
            Boss.SetActive(true);
            //GameManager.gameMode = GameManager.GameMode.BOSS_BATTLE;
        }
        else if (mode.Equals(GameConstants.GAMEMODE_CLEAR_JUICE))
        {
            clearJuice.SetActive(true);
            //GameManager.gameMode = GameManager.GameMode.CLEAR_JUICE;
        }
        else if (mode.Equals(GameConstants.GAMEMODE_POSICLE))
        {
            popsicle.SetActive(true);
            //GameManager.gameMode = GameManager.GameMode.POPSICLE;
        }
	}

	public void Set_Glass(Image _glass, Sprite _spcGlass)
	{
		_glass.sprite = _spcGlass;
	}
	bool isFirstCharacter = true;
    IEnumerator GetModeOrderItem()
    {
        if (listGoal.Count > 0)
        {
            for (int i = 0; i < 4; i++)
            {
				if (listGoal.Count > 0) 
				{
					GameObject obj;
					if (Contance.NUMBER_LEVEL != 1) 
					{
						obj = Instantiate (itemGoal [Random.Range (0, itemGoal.Length)], pos.GetChild (3).position, Quaternion.identity) as GameObject;
					} 
					else 
					{
						if (Contance.NUMBER_LEVEL == 1 && isFirstCharacter == true) 
						{
							obj = Instantiate (itemGoal [3], pos.GetChild (3).position, Quaternion.identity) as GameObject;
							isFirstCharacter = false;
						}
						else 
						{
							obj = Instantiate (itemGoal [Random.Range (0, itemGoal.Length)], pos.GetChild (3).position, Quaternion.identity) as GameObject;
						}
					}

					CharacterScript order = obj.GetComponent<CharacterScript> ();
					order.SetID (i);

					if (listGoal [0].object_type_Goal_Object.Equals ("match_object")) {
						order.SetOValue (listGoal [0].value_Goal_Object);
						order.SetValue (listGoal [0].value_Goal_Object);
						order.SetName (listGoal [0].color_Goal_Object);
						order.SetSprite (listGoal [0].object_type_Goal_Object, listGoal [0].color_Goal_Object);

						SetGlassColor (order, listGoal [0].color_Goal_Object); 

					} else {
						Debug.Log ("_type");
						order.SetOValue (listGoal [0].value_Goal_Object);
						order.SetValue (listGoal [0].value_Goal_Object);
						order.SetName (listGoal [0].object_type_Goal_Object);
						order.SetSprite (listGoal [0].object_type_Goal_Object, listGoal [0].color_Goal_Object);

						SetObjectType (order, order._type_object);

					}

					if (i < 3) {
							order.Glass = Glasses [i];
						SetGlassFirst (order.Glass,order._type_object);
						order.InputOrder (pos.GetChild (i).position);

						listGoalCurrent.Add (order);
					} else {
						order.skeAnimation.GetComponent<Renderer> ().sortingOrder = 0;
						obj.transform.parent = tempGoal.transform;
					}
					txtgoal.text = listGoal.Count.ToString ();
					listGoal.RemoveAt (0);
	                
					yield return new WaitForSeconds (1f);
				} 
				else 
				{
					txtgoal.text = listGoal.Count.ToString ();
				}
                //if(i < 3)
                //    listGoalCurrent.Add(order);

            }
        }
    }


//==================================================================HandleEffectGoalBoss============================================================
    public void HandleEffectGoal(string mode)
    {

    }



    public void AnimationHitBoss(GameObject obj, string nameAnimation, bool loop)
    {
        if (!obj.GetComponent<SkeletonAnimation>().AnimationName.Equals( nameAnimation))
        {
            obj.GetComponent<SkeletonAnimation>().state.SetAnimation(0, nameAnimation, loop);
            obj.GetComponent<SkeletonAnimation>().state.AddAnimation(0, Contance.NGOI, true, 0f);
        }
    }

    public void AnimationWinOrLose(GameObject obj, string nameAnimation, bool loop)
    {
        if (!obj.GetComponent<SkeletonAnimation>().AnimationName.Equals(nameAnimation))
        {
            obj.GetComponent<SkeletonAnimation>().state.SetAnimation(0, nameAnimation, loop);
        }
    }

    public void ScaleItem(GameObject obj)
    {
        iTween.ScaleTo(obj, iTween.Hash("scale", new Vector3(1.3f, 1.3f, 1.3f), "time", 0.1f));
        iTween.ScaleTo(obj, iTween.Hash("scale", new Vector3(1f, 1f, 1f), "time", 0.1f, "delay", 0.1f));
    }



    // ====================================khi ăn các item như bánh quy or kem (mode: Clear_Juice and Popsicle)

    public void CountItemAte(int item, ref int totalItem)
    {
        iTween.ScaleTo(modeGame.GetChild(0).GetChild(0).gameObject, iTween.Hash("scale", new Vector3(0.6f, 0.6f, 0.6f), "time", 0.1f));
        iTween.ScaleTo(modeGame.GetChild(0).GetChild(0).gameObject, iTween.Hash("scale", new Vector3(0.4f, 0.4f, 0.4f), "time", 0.1f, "delay", 0.1f));
        AnimationHitBoss(modeGame.GetChild(0).gameObject, Contance.AN_BANH, false);
        totalItem -= item;
        modeGame.GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetComponent<TextMesh>().text = totalItem.ToString();

        if (totalItem == 0)
        {
            AnimationWinOrLose(modeGame.GetChild(0).gameObject,Contance.THANG,true);
        }
    }

	int count = -3;

    public void CheckHaveOrder(string type, Tile postile)
    {
        for (int i = 0; i < listGoalCurrent.Count; i++)
        {
            if (listGoalCurrent[i].gameObject.name.Contains(type))
            {
                if (listGoalCurrent[i]._value > 0 && listGoalCurrent[i].actived)
                {
                    switch (type)
                    {
                        case GameConstants.blue_tile_color:
						if(postile.tileData.tileType != TileData.TileType.coconut_1 && postile.tileData.tileType != TileData.TileType.coconut_2)
						{
							listGoalCurrent[i]._value--;
									//count = i;
								//effectCount[0] = i;
	//                            if(PoolingManager.poolingManager.List_Effect_Order_Blue.Count > 0)
	//                            {
	//                               GameObject effectblue = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Order_Blue, postile);
	//                               CollectFood(effectblue, postile.transform.position, listGoalCurrent[i].Bg_item, listGoalCurrent[i]);
	//                               LeanTween.delayedCall(1f, () =>
	//                               {
	//                                   PoolingManager.poolingManager.AddOrderColorToPool(effectblue, PoolingManager.poolingManager.List_Effect_Order_Blue);
	//                               });
	//                            }
	//                            else
	//                            {
	                                GameObject effectblue = Instantiate(effect_Move_Color[0], postile.transform.position, Quaternion.identity) as GameObject;
									listGoalCurrent[i].listEffect.Add(effectblue);
	                                CollectFood(effectblue, postile.transform.position, listGoalCurrent[i].Bg_item, listGoalCurrent[i]);
							//Destroy(effectblue,1.5f);
						}
//                                LeanTween.delayedCall(1f, () =>
//                                {
//                                    PoolingManager.poolingManager.AddOrderColorToPool(effectblue, PoolingManager.poolingManager.List_Effect_Order_Blue);
//                                });
//                            }
                            break;
                        case GameConstants.green_tile_color:
                                listGoalCurrent[i]._value--;
								//count = i;
							//effectCount[1] = i;
//                                if (PoolingManager.poolingManager.List_Effect_Order_Green.Count > 0)
//                                {
//                                    GameObject effectgreen = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Order_Green, postile);
//                                    CollectFood(effectgreen, postile.transform.position, listGoalCurrent[i].Bg_item, listGoalCurrent[i]);
//                                    LeanTween.delayedCall(1f, () =>
//                                    {
//                                        PoolingManager.poolingManager.AddOrderColorToPool(effectgreen, PoolingManager.poolingManager.List_Effect_Order_Green);
//                                    });
//                                }
//                                else
//                                {
                                    GameObject effectgreen = Instantiate(effect_Move_Color[1], postile.transform.position, Quaternion.identity) as GameObject;
									listGoalCurrent[i].listEffect.Add(effectgreen);
                                    CollectFood(effectgreen, postile.transform.position, listGoalCurrent[i].Bg_item, listGoalCurrent[i]);
						//Destroy(effectgreen,1.5f);
//                                    LeanTween.delayedCall(1f, () =>
//                                    {
//                                        PoolingManager.poolingManager.AddOrderColorToPool(effectgreen, PoolingManager.poolingManager.List_Effect_Order_Green);
//                                    });
//                                }
                            break;
                        case GameConstants.orange_tile_color:
                                listGoalCurrent[i]._value--;
								//count = i;
							//effectCount[2] = i;
//                                if (PoolingManager.poolingManager.List_Effect_Order_Orange.Count > 0)
//                                {
//                                    GameObject effectorange = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Order_Orange, postile);
//                                    CollectFood(effectorange, postile.transform.position, listGoalCurrent[i].Bg_item, listGoalCurrent[i]);
//                                    LeanTween.delayedCall(1f, () =>
//                                    {
//                                        PoolingManager.poolingManager.AddOrderColorToPool(effectorange, PoolingManager.poolingManager.List_Effect_Order_Orange);
//                                    });
//                                }
//                                else
//                                {
                                    GameObject effectorange = Instantiate(effect_Move_Color[2], postile.transform.position, Quaternion.identity) as GameObject;
									listGoalCurrent[i].listEffect.Add(effectorange);
                                    CollectFood(effectorange, postile.transform.position, listGoalCurrent[i].Bg_item, listGoalCurrent[i]);
						//Destroy(effectorange,1.5f);
//                                    LeanTween.delayedCall(1f, () =>
//                                    {
//                                        PoolingManager.poolingManager.AddOrderColorToPool(effectorange, PoolingManager.poolingManager.List_Effect_Order_Orange);
//                                    });
//                                }
                            break;
                        case GameConstants.purple_tile_color:
                                listGoalCurrent[i]._value--;
								//count = i;
							//effectCount[3] = i;
//                                if (PoolingManager.poolingManager.List_Effect_Order_Purple.Count > 0)
//                                {
//                                    GameObject effectpurple = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Order_Purple, postile);
//                                    CollectFood(effectpurple, postile.transform.position, listGoalCurrent[i].Bg_item, listGoalCurrent[i]);
//                                    LeanTween.delayedCall(1f, () =>
//                                    {
//                                        PoolingManager.poolingManager.AddOrderColorToPool(effectpurple, PoolingManager.poolingManager.List_Effect_Order_Purple);
//                                    });
//                                }
//                                else
//                                {
                                    GameObject effectpurple = Instantiate(effect_Move_Color[3], postile.transform.position, Quaternion.identity) as GameObject;
									listGoalCurrent[i].listEffect.Add(effectpurple);
                                    CollectFood(effectpurple, postile.transform.position, listGoalCurrent[i].Bg_item, listGoalCurrent[i]);
						//Destroy(effectpurple,1.5f);
//                                    LeanTween.delayedCall(1f, () =>
//                                    {
//                                        PoolingManager.poolingManager.AddOrderColorToPool(effectpurple, PoolingManager.poolingManager.List_Effect_Order_Purple);
//                                    });
//                                }
                            break;
                        case GameConstants.red_tile_color:
						//Debug.Log(listGoalCurrent[i].name);
						listGoalCurrent[i]._value--;//Debug.Log("redDestroy...");
								//count = i;
							//effectCount[4] = i;
//                                if (PoolingManager.poolingManager.List_Effect_Order_Red.Count > 0)
//                                {
//                                    GameObject effectred = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Order_Red, postile);
//                                    CollectFood(effectred, postile.transform.position, listGoalCurrent[i].Bg_item, listGoalCurrent[i]);
//                                    LeanTween.delayedCall(1f, () =>
//                                    {
//                                        PoolingManager.poolingManager.AddOrderColorToPool(effectred, PoolingManager.poolingManager.List_Effect_Order_Red);
//                                    });
//                                }
//                                else
//                                {
                                    GameObject effectred = Instantiate(effect_Move_Color[4], postile.transform.position, Quaternion.identity) as GameObject;
									listGoalCurrent[i].listEffect.Add(effectred);
                                    CollectFood(effectred, postile.transform.position, listGoalCurrent[i].Bg_item, listGoalCurrent[i]);
						//Destroy(effectred,1.5f);
//                                    LeanTween.delayedCall(1f, () =>
//                                    {
//                                        PoolingManager.poolingManager.AddOrderColorToPool(effectred, PoolingManager.poolingManager.List_Effect_Order_Red);
//                                    });
//                                }
                            break;
                        case GameConstants.yellow_tile_color:
                                listGoalCurrent[i]._value--;
								//count = i;
							//effectCount[5] = i;
//                                if (PoolingManager.poolingManager.List_Effect_Order_Yellow.Count > 0)
//                                {
//                                    GameObject effectryellow = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Order_Yellow, postile);
//                                    CollectFood(effectryellow, postile.transform.position, listGoalCurrent[i].Bg_item, listGoalCurrent[i]);
//                                    LeanTween.delayedCall(1f, () =>
//                                    {
//                                        PoolingManager.poolingManager.AddOrderColorToPool(effectryellow, PoolingManager.poolingManager.List_Effect_Order_Yellow);
//                                    });
//                                }
//                                else
//                                {
                                    GameObject effectryellow = Instantiate(effect_Move_Color[5], postile.transform.position, Quaternion.identity) as GameObject;
									listGoalCurrent[i].listEffect.Add(effectryellow);
									CollectFood(effectryellow, postile.transform.position, listGoalCurrent[i].Bg_item, listGoalCurrent[i]);
						//Destroy(effectryellow,1.5f);
//                                    LeanTween.delayedCall(1f, () =>
//                                    {
//                                        PoolingManager.poolingManager.AddOrderColorToPool(effectryellow, PoolingManager.poolingManager.List_Effect_Order_Yellow);
//                                    });
//                                }  
                            break;

                        case GameConstants.muffin_tile_type:
                                listGoalCurrent[i]._value--;
//                                if (PoolingManager.poolingManager.List_Effect_Order_Muffin.Count > 0)
//                                {
//                                    GameObject effectrmuffin = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Order_Muffin, postile);
//                                    CollectFood(effectrmuffin, postile.transform.position, listGoalCurrent[i].Bg_item, listGoalCurrent[i]);
//                                    LeanTween.delayedCall(1f, () =>
//                                    {
//                                        PoolingManager.poolingManager.AddOrderColorToPool(effectrmuffin, PoolingManager.poolingManager.List_Effect_Order_Muffin);
//                                    });
//                                }
//                                else
//                                {
                                    GameObject effectrmuffin = Instantiate(effect_Move_Color[6], postile.transform.position, Quaternion.identity) as GameObject;
									listGoalCurrent[i].listEffect.Add(effectrmuffin);
                                    CollectFood(effectrmuffin, postile.transform.position, listGoalCurrent[i].Bg_item, listGoalCurrent[i]);
									//Destroy(effectrmuffin,1.5f);
//                                    LeanTween.delayedCall(1f, () =>
//                                    {
//                                        PoolingManager.poolingManager.AddOrderColorToPool(effectrmuffin, PoolingManager.poolingManager.List_Effect_Order_Muffin);
//                                    });
//                                }  
                            break;

                        case GameConstants.honey_grid:
                                listGoalCurrent[i]._value--;
//                                if (PoolingManager.poolingManager.List_Effect_Order_Honey.Count > 0)
//                                {
//                                    GameObject effectrhoney = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Order_Honey, postile);
//                                    CollectFood(effectrhoney, postile.transform.position, listGoalCurrent[i].Bg_item, listGoalCurrent[i]);
//                                    LeanTween.delayedCall(1f, () =>
//                                    {
//                                        PoolingManager.poolingManager.AddOrderColorToPool(effectrhoney, PoolingManager.poolingManager.List_Effect_Order_Honey);
//                                    });
//                                }
//                                else
//                                {
                                    GameObject effectrhoney = Instantiate(effect_Move_Color[7], postile.transform.position, Quaternion.identity) as GameObject;
						listGoalCurrent[i].listEffect.Add(effectrhoney);
                                    CollectFood(effectrhoney, postile.transform.position, listGoalCurrent[i].Bg_item, listGoalCurrent[i]);
									//Destroy(effectrhoney,1.5f);
//                                    LeanTween.delayedCall(1f, () =>
//                                    {
//                                        PoolingManager.poolingManager.AddOrderColorToPool(effectrhoney, PoolingManager.poolingManager.List_Effect_Order_Honey);
//                                    });
//                                }
                            break;
					case GameConstants.cow_tile_type:
						listGoalCurrent [i]._value--;
//	                               if (PoolingManager.poolingManager.List_Effect_Order_milk.Count > 0)
//                                {
//                                    GameObject effectrmilk = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Order_milk, postile);
//                                    CollectFood(effectrmilk, postile.transform.position, listGoalCurrent[i].Bg_item, listGoalCurrent[i]);
//                                    LeanTween.delayedCall(1f, () =>
//                                    {
//                                        PoolingManager.poolingManager.AddOrderColorToPool(effectrmilk, PoolingManager.poolingManager.List_Effect_Order_milk);
//                                    });
//                                }
//                                else
//                                {
						GameObject effectrmilk = Instantiate (effect_Move_Color [8], postile.transform.position, Quaternion.identity) as GameObject;
						listGoalCurrent [i].listEffect.Add (effectrmilk);
						CollectFood (effectrmilk, postile.transform.position, listGoalCurrent [i].Bg_item, listGoalCurrent [i]);
									//Destroy(effectrmilk,1.5f);
//                                    LeanTween.delayedCall(1f, () =>
//                                    {
//                                        PoolingManager.poolingManager.AddOrderColorToPool(effectrmilk, PoolingManager.poolingManager.List_Effect_Order_milk);
//                                    });
//                                }
                            break;
                        case GameConstants.yogurt_tile_type:
                            listGoalCurrent[i]._value--;
//                            if (PoolingManager.poolingManager.List_Effect_Order_Purple.Count > 0)
//                            {
//								GameObject effectyogurt = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Order_Purple, postile);
//								CollectFood(effectyogurt, postile.transform.position, listGoalCurrent[i].Bg_item, listGoalCurrent[i]);
//                                LeanTween.delayedCall(1f, () =>
//                                {
//								PoolingManager.poolingManager.AddOrderColorToPool(effectyogurt, PoolingManager.poolingManager.List_Effect_Order_Purple);
//                                });
//                            }
//                            else
//                            {
								GameObject effectyogurt = Instantiate(effect_Move_Color[3], postile.transform.position, Quaternion.identity) as GameObject;
						listGoalCurrent[i].listEffect.Add(effectyogurt);
								CollectFood(effectyogurt, postile.transform.position, listGoalCurrent[i].Bg_item, listGoalCurrent[i]);
								//Destroy(effectyogurt,1.5f);
//                                LeanTween.delayedCall(1f, () =>
//                                {
//								PoolingManager.poolingManager.AddOrderColorToPool(effectyogurt, PoolingManager.poolingManager.List_Effect_Order_Purple);
//                                });
//                            }
                            break;
                        case GameConstants.coconut_2_tile_type:
                            listGoalCurrent[i]._value--;
//                            if (PoolingManager.poolingManager.List_Effect_Order_White.Count > 0)
//                            {
//                                GameObject effectcoconut = PoolingManager.poolingManager.GetEffectOrderColorFromPool(PoolingManager.poolingManager.List_Effect_Order_White, postile);
//                                CollectFood(effectcoconut, postile.transform.position, listGoalCurrent[i].Bg_item, listGoalCurrent[i]);
//                                LeanTween.delayedCall(1f, () =>
//                                {
//                                    PoolingManager.poolingManager.AddOrderColorToPool(effectcoconut, PoolingManager.poolingManager.List_Effect_Order_White);
//                                });
//                            }
//                            else
//                            {
                                GameObject effectcoconut = Instantiate(effect_Move_Color[9], postile.transform.position, Quaternion.identity) as GameObject;
						listGoalCurrent[i].listEffect.Add(effectcoconut);
                                CollectFood(effectcoconut, postile.transform.position, listGoalCurrent[i].Bg_item, listGoalCurrent[i]);
								//Destroy(effectcoconut,1.5f);
//                                LeanTween.delayedCall(1f, () =>
//                                {
//                                    PoolingManager.poolingManager.AddOrderColorToPool(effectcoconut, PoolingManager.poolingManager.List_Effect_Order_White);
//                                });
//                            }
                            break;
                        default:
                            break;
                    }
                    break;
                }
				else
                {
                    continue;
                }
            }
        }
    }

    public void CollectFood(GameObject obj, Vector3 start, GameObject end, CharacterScript order)
    {
		iTween.MoveTo(obj, iTween.Hash("path", new Vector3[] { start, new Vector3(Random.Range(-2.4f, 2.4f), Random.Range(start.y, order.Glass.transform.position.y), 0), new Vector3(order.Glass.transform.GetChild(0).transform.position.x, order.Glass.transform.GetChild(0).transform.position.y+0.25f)}, "time", 1f, "easetype", iTween.EaseType.linear, "oncomplete", "UpdateValueOrder", "oncompletetarget", gameObject, "oncompleteparams", order));
		//0.9f
		LeanTween.delayedCall (0.9f, () => {
			SoundController.sound.glassSoundPlay ();
		});
    }
		
    public void UpdateValueOrder(CharacterScript order)
    {
		order.SetValue(order._value);
		order.UpgradeGlass ();

		foreach (GameObject item in order.listEffect) 
		{
			Destroy(item);
		}

		if(order._value <= 0 && !order.finish)
        {
			order.finish = true;
            CreateItemGoal();
			order.OutPutOrder();
			if (tempGoal.childCount > 0) 
			{
				StartCoroutine (TimeWait (order, order.timeWin));
			} 
			else 
			{
				StartCoroutine (HideGlass(order));
			}
			listGoalCurrent.Remove(order);
            //order.OutPutOrder();
        }

        int orderExit = listGoalCurrent.Count + tempGoal.childCount + listGoal.Count;
        if (orderExit == 0)
            Contance.GAME_WIN = true;
    }

	IEnumerator HideGlass(CharacterScript order)
	{
		yield return new WaitForSeconds (1);
		order.Glass.SetActive (false);
	}

    IEnumerator TimeWait(CharacterScript order,float time)
    {
        CharacterScript orderNew = tempGoal.GetChild(0).GetComponent<CharacterScript>();
        orderNew.SetID(order._id);
		orderNew.Glass = order.Glass;
        orderNew.transform.parent = null;
        listGoalCurrent.Add(orderNew);
        yield return new WaitForSeconds(time);
		order.Glass.SetActive (false);
		order.Glass = null;
		if (orderNew._color != "") 
		{
			orderNew.Glass.transform.GetChild(0).GetComponent<Image> ().sprite = originGlass;
			SetTranfromGlass (orderNew.Glass);
		} 
		else 
		{
			SetGlassFirst (orderNew.Glass, orderNew._type_object);
		}
        orderNew.InputOrder(pos.GetChild(orderNew._id).position);
        //listGoalCurrent.Add(orderNew);
        txtgoal.text = (listGoal.Count + tempGoal.childCount).ToString();
    }

	void SetGlassFirst( GameObject glass, string type)
	{
		switch(type)
		{
			case "honey":
				glass.transform.GetChild (0).GetComponent<Image> ().sprite = honeyCup;
				SetTranfromGlass (glass);
				break;
			case "muffin":
				glass.transform.GetChild (0).GetComponent<Image> ().sprite = muffinPlate;
				glass.transform.GetChild (0).GetComponent<Image> ().SetNativeSize ();
				glass.transform.GetChild (0).localScale = Vector3.one * 0.75f;
				glass.transform.localPosition = new Vector3 (glass.transform.localPosition.x, -100);
				glass.transform.GetChild (1).gameObject.SetActive (false);
				break;
			case "yogurt":
				glass.transform.GetChild(0).GetComponent<Image> ().sprite = yogurtCup;
				SetTranfromGlass (glass);
				break;
			case "cow":
				glass.transform.GetChild(0).GetComponent<Image> ().sprite = originGlass;
				SetTranfromGlass (glass);
				break;
			case "coconut_2":
				glass.transform.GetChild(0).GetComponent<Image> ().sprite = originGlass;
				SetTranfromGlass (glass);
				break;
			default:
				break;
		}
	}

	void SetTranfromGlass(GameObject glass)
	{
		glass.transform.localPosition = new Vector3 (glass.transform.localPosition.x, -84.3f);
		glass.transform.GetChild (0).GetComponent<RectTransform> ().sizeDelta = new Vector3 (45,45);
		glass.transform.GetChild(0).localScale = Vector3.one;
		glass.transform.GetChild (1).gameObject.SetActive (false);
	}
}
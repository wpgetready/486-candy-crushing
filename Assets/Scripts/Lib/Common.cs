﻿/*
 * @Author: CuongNH
 * @Description: define commond method in project
 * */

using UnityEngine;
using System.Collections;

namespace vg.com
{
    namespace common
    {
        public class Common
        {
            #region Methods with Children of GameOjbect
            /// <summary>
            /// Tra ve list cac Transforms cua cac con cua mot GameObject
            /// </summary>
            /// <param name="fromGameObject">GameObject cha truyen vao</param>
            /// <returns> List of Transform</returns>
            public static Transform[] GetTransformsOfChilds(GameObject fromGameObject)
            {
                try
                {
                    Transform[] ts = fromGameObject.transform.GetComponentsInChildren<Transform>();

                    return ts;
                }
                catch
                {
                    return null;
                }
            }

            /// <summary>
            /// Lay mot GameObject con theo ten tu mot GameObject
            /// </summary>
            /// <param name="fromGameObject">GameObject cha truyen vao</param>
            /// <param name="withName">Ten cua GameObject con can lay</param>
            /// <returns>GameOjbect</returns>
            public static GameObject GetChildGameObjectByName(GameObject fromGameObject,
                string withName)
            {
                //Debug.Log("GetChildGameObjectByName");
                try
                {
                    Transform[] ts = fromGameObject.transform.GetComponentsInChildren<Transform>();
                    foreach (Transform t in ts)
                    {
                        if (t.gameObject.name == withName)
                        {
                            return t.gameObject;
                        }
                    }
                }
                catch
                {
                    return null;
                }

                return null;
            }

            /// <summary>
            /// Lay mot GameObject con theo tag tu mot GameObject
            /// </summary>
            /// <param name="fromGameObject">GameObject cha truyen va</param>
            /// <param name="withTagName">Tag Name cua GameOjbect con can lay</param>
            /// <returns>GameObject</returns>
            public static GameObject GetChildGameObjectByTagName(GameObject fromGameObject,
                string withTagName)
            {
                //Debug.Log("GetChildGameObjectByTagName");

                Transform[] ts = fromGameObject.transform.GetComponentsInChildren<Transform>();
                foreach (Transform t in ts)
                {
                    if (t.gameObject.tag == withTagName)
                    {
                        return t.gameObject;
                    }
                }

                return null;
            }
            #endregion

            #region Methods to move, scale GameObject
            /// <summary>
            /// Di chuyen mot GameObject bang Coroutine
            /// </summary>
            /// <param name="obj">GameObject can di chuyen</param>
            /// <param name="dest">Vi tri can di chuyen den</param>
            /// <param name="t">Thoi gian di chuyen</param>
            /// <param name="epsilon">Sai so vi tri khi lam tron</param>
            /// <returns></returns>
            public static IEnumerator MoveObject(GameObject obj,
                Vector3 dest, float t, float epsilon = 0.01f)
            {
                //Debug.Log("Advance MoveObject is started");

                if (obj != null)
                {
                    float i = 0.0f;
                    float rate = 1.0f / (t > 0 ? t : 1f);

                    float xDelta = Mathf.Abs(obj.transform.position.x - dest.x);
                    float yDelta = Mathf.Abs(obj.transform.position.y - dest.y);
                    float zDelta = Mathf.Abs(obj.transform.position.z - dest.z);

                    while (xDelta > epsilon || yDelta > epsilon || zDelta > epsilon)
                    {
                        try
                        {
                            i += Time.deltaTime * rate;

                            obj.transform.position = Vector3.Lerp(obj
                                .transform.position, dest, i);

                            xDelta = Mathf.Abs(obj.transform.position.x - dest.x);
                            yDelta = Mathf.Abs(obj.transform.position.y - dest.y);
                            zDelta = Mathf.Abs(obj.transform.position.z - dest.z);
                        }
                        catch
                        {
                            yield break;
                        }

                        yield return null;
                    }

                    if (obj != null)
                    {
                        obj.transform.position = dest;
                    }
                }

                yield return null;

                //Debug.Log("MoveObjectByCoroutine complete: " + Time.time);
            }

            /// <summary>
            /// Di chuyen Local GameObject voi toc do co dinh
            /// </summary>
            /// <param name="obj">GameObject can di chuyen</param>
            /// <param name="dest">Vi tri local can di chuyen toi</param>
            /// <param name="speed">Toc do di chuyen</param>
            /// <param name="epsilon">Sai so vi tri khi lam tron</param>
            /// <returns></returns>
            public static IEnumerator MoveLocalObjectBySpeed(GameObject obj,
                Vector3 dest, float speed, float epsilon = 0.01f)
            {
                //vị trí xuất phát
                //Vector3 endLocalPosition = targetLocalPosition;

                float xDelta = Mathf.Abs(obj.transform.localPosition.x - dest.x);
                float yDelta = Mathf.Abs(obj.transform.localPosition.y - dest.y);
                float zDelta = Mathf.Abs(obj.transform.localPosition.z - dest.z);

                while (xDelta > epsilon || yDelta > epsilon || zDelta > epsilon)
                {
                    try
                    {
                        obj.transform.localPosition = Vector3.MoveTowards(
                            obj.transform.localPosition, dest, Time.deltaTime * speed);

                        xDelta = Mathf.Abs(obj.transform.localPosition.x - dest.x);
                        yDelta = Mathf.Abs(obj.transform.localPosition.y - dest.y);
                        zDelta = Mathf.Abs(obj.transform.localPosition.z - dest.z);
                    }
                    catch (System.Exception e)
                    {
                        Debug.Log(e.Message);

                        yield break;
                    }

                    yield return null;
                }

                if (obj != null)
                {
                    obj.transform.localPosition = dest;
                }

                yield return null;

                //yield return new WaitForSeconds(0.2f);
            }

            /// <summary>
            /// Di chuyen Local GameObject theo thoi gian
            /// </summary>
            /// <param name="obj">GameObject can di chuyen</param>
            /// <param name="dest">Vi tri can di chuyen den</param>
            /// <param name="t">Thoi gian di chuyen</param>
            /// <param name="epsilon">Sai so vi tri khi lam tron</param>
            /// <returns></returns>
            public static IEnumerator MoveLocalObject(GameObject obj,
                Vector3 dest, float t, float epsilon = 0.01f)
            {
                //Debug.Log("Advance MoveLocalObject is started: " + t);

                float i = 0.0f;
                float rate = 1.0f / (t > 0 ? t : 1f);

                if (obj != null)
                {
                    float xDelta = Mathf.Abs(obj.transform.localPosition.x - dest.x);
                    float yDelta = Mathf.Abs(obj.transform.localPosition.y - dest.y);
                    float zDelta = Mathf.Abs(obj.transform.localPosition.z - dest.z);

                    while (xDelta > epsilon || yDelta > epsilon || zDelta > epsilon)
                    {
                        try
                        {
                            //Debug.Log("Time.deltaTime: " + Time.deltaTime);

                            i += Time.deltaTime * rate;
                            //i += 0.019898f * rate;

                            obj.transform.localPosition = Vector3.Lerp(obj
                                .transform.localPosition, dest, i);

                            xDelta = Mathf.Abs(obj.transform.localPosition.x - dest.x);
                            yDelta = Mathf.Abs(obj.transform.localPosition.y - dest.y);
                            zDelta = Mathf.Abs(obj.transform.localPosition.z - dest.z);
                        }
                        catch
                        {
                            yield break;
                        }

                        yield return null;
                    }

                    if (obj != null)
                    {
                        obj.transform.localPosition = dest;
                    }
                }

                //Debug.Log("MoveObjectByCoroutine complete: " + Time.time);
            }

            /// <summary>
            /// Scale GameObject
            /// </summary>
            /// <param name="obj">GameObject can scale</param>
            /// <param name="scale">Kich thuoc can scale den</param>
            /// <param name="t">Thoi gian thuc hien scale</param>
            /// <param name="epsilon">Sai so khi lam tron</param>
            /// <returns></returns>
            public static IEnumerator ScaleLocalObject(GameObject obj,
                Vector3 scale, float t, float epsilon = 0.01f)
            {
                //Debug.Log("ScaleObject is started");
                float i = 0.0f;
                float rate = 1.0f / (t > 0 ? t : 1f);

                while (obj != null && (Mathf.Abs(obj.transform.localScale.x - scale.x) > epsilon
                    || Mathf.Abs(obj.transform.localScale.y - scale.y) > epsilon))
                {
                    try
                    {
                        i += Time.deltaTime * rate;

                        obj.transform.localScale = Vector3.Lerp(
                            obj.transform.localScale, scale, i);
                    }
                    catch
                    {
                        yield break;
                    }

                    yield return null;
                }

                if (obj != null)
                {
                    obj.transform.localScale = scale;
                }

                yield return null;
            }
            #endregion

            /// <summary>
            /// So sanh 2 doi tuong co phai la 1 khong
            /// </summary>
            /// <param name="a">GameObject a</param>
            /// <param name="b">GameObject</param>
            /// <returns>bool; true when a is b else fale</returns>
            public static bool CompareTwoObject(GameObject a, GameObject b)
            {
                if (a != null && b != null && a.GetInstanceID() == b.GetInstanceID())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            /// <summary>
            /// Dung de lay vector giua cua 2 vector
            /// </summary>
            /// <param name="u">Vector u</param>
            /// <param name="v">Vector v</param>
            /// <returns>Vector3</returns>
            public static Vector3 MidPoint(Vector3 u, Vector3 v)
            {
                return new Vector3((u.x + v.x) / 2, (u.y + v.y) / 2, (u.z + v.z) / 2);
            }
        }
    }
}


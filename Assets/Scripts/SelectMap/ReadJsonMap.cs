﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using UnityEngine.UI;
using System;

public class ReadJsonMap : MonoBehaviour {

	public TextAsset level;

	public GameObject[] item;

	public Sprite it1;
	public Sprite it2;
	public Sprite it3;
	public Sprite it4;
	public Sprite it5;
	public Sprite it6;
	public Sprite it7;
	public Sprite it8;

	public GameObject bg_popup;
	public GameObject forground;

	public GameObject P_star1;
	public GameObject P_star2;
	public GameObject P_star3;

	public Transform flag;


	public GameObject bg;
	public GameObject loading;

    void Awake() 
    {
        //PlayerPrefs.SetString("Level0", "Finish");
            ReadDataJson(level.text);
    }


	// Use this for initialization
	void Start () {
       
//		show.sprite = show1;
//		PlayerPrefs.DeleteAll ();
		loading.SetActive (false);

		

		P_star1.SetActive (false);
		P_star2.SetActive (false);
		P_star3.SetActive (false);


		PlayerPrefs.SetInt ("LevelFinish", 10);

		if (PlayerPrefs.GetInt("LevelFinish") > 0)
			flag.position = GameObject.Find(PlayerPrefs.GetInt("LevelFinish").ToString()).transform.position;

		if(PlayerPrefs.GetInt ("LevelFinish") > 5)
			bg.GetComponent<RectTransform> ().anchoredPosition = new Vector3(0,-item [PlayerPrefs.GetInt ("LevelFinish")].GetComponent<RectTransform> ().anchoredPosition.y,0);
	}


	void ReadDataJson( string json)
	{
		var ST = JSON.Parse (json);
//		print (ST ["data"]);
		var N = JSONNode.Parse (ST ["data"].ToString ());
		for (int i = 0; i < N["level"].Count; i++) {
			item[i].name = (i+1).ToString();
			item[i].transform.GetChild(1).GetChild(0).gameObject.GetComponent<Text>().text = (i+1).ToString();
            ChangeMode(N["level"][i]["mode"].Value, i);
		}
	}

	void ChangeMode( string mode,int i)
	{
		if (mode.Equals ("order_fulfillment")) {
//			print (i +" : order_fulfillment");
			item[i].transform.GetChild(0).gameObject.GetComponent<Image>().sprite = it4;
			if(PlayerPrefs.GetString("Level"+i).Equals("Finish"))
				item[i].transform.GetChild(1).gameObject.GetComponent<Image>().sprite = it3;
			else
				item[i].transform.GetChild(1).gameObject.GetComponent<Image>().color = new Color(1,1,1,0);

			PlayerPrefs.SetString("ModeLevel"+(i+1).ToString(),mode);

		}else if (mode.Equals ("boss_battle")) {
//			print (i +" : boss_battle");
			item[i].transform.GetChild(0).gameObject.GetComponent<Image>().sprite = it6;
			if(PlayerPrefs.GetString("Level"+i).Equals("Finish"))
				item[i].transform.GetChild(1).gameObject.GetComponent<Image>().sprite = it5;
			else
				item[i].transform.GetChild(1).gameObject.GetComponent<Image>().color = new Color(1,1,1,0);

			PlayerPrefs.SetString("ModeLevel"+(i+1).ToString(),mode);

		}else if (mode.Equals ("clear_juice")) {
//			print (i +" : clear_juice");
			item[i].transform.GetChild(0).gameObject.GetComponent<Image>().sprite = it2;
			if(PlayerPrefs.GetString("Level"+i).Equals("Finish"))
				item[i].transform.GetChild(1).gameObject.GetComponent<Image>().sprite = it1;
			else
				item[i].transform.GetChild(1).gameObject.GetComponent<Image>().color = new Color(1,1,1,0);

			PlayerPrefs.SetString("ModeLevel"+(i+1).ToString(),mode);

		}else if (mode.Equals ("popsicle")) {
//			print (i +" : popsicle");
			item[i].transform.GetChild(0).gameObject.GetComponent<Image>().sprite = it8;
			if(PlayerPrefs.GetString("Level"+i).Equals("Finish"))
				item[i].transform.GetChild(1).gameObject.GetComponent<Image>().sprite = it7;
			else
				item[i].transform.GetChild(1).gameObject.GetComponent<Image>().color = new Color(1,1,1,0);

			PlayerPrefs.SetString("ModeLevel"+(i+1).ToString(),mode);

		}
	}

	public void Btn_Exit()
	{
		iTween.MoveTo(forground,iTween.Hash("position",new Vector3(4.8f,0,0),"time",1f,"easetype",iTween.EaseType.easeInBack,"oncomplete","SetActive", "oncompletetarget", gameObject));
		iTween.MoveTo(bg_popup,iTween.Hash("position",new Vector3(0,8,0),"time",1f,"delay",1f));
	}

	public void SetActive()
	{
		P_star1.SetActive (false);
		P_star2.SetActive (false);
		P_star3.SetActive (false);
	}

	public void Btn_Play()
	{
		iTween.MoveTo(forground,iTween.Hash("position",new Vector3(-4.8f,0,0),"time",0.5f,"easetype",iTween.EaseType.easeInBack,"oncomplete","Play", "oncompletetarget", gameObject));
		iTween.MoveTo(bg_popup,iTween.Hash("position",new Vector3(0,8,0),"time",1f,"delay",0.5f));
	}
	
	public void Play()
	{
		P_star1.SetActive (false);
		P_star2.SetActive (false);
		P_star3.SetActive (false);

		loading.SetActive (true);
		StartCoroutine(LoadingHandler.loading.HandleLoading(GameConstants.GAMEPLAY_SCENE_NAME));
	}
}

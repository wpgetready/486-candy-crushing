﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
public class ClickItem : MonoBehaviour 
{
	public Text solevel;

	public GameObject star1;
	public GameObject star2;
	public GameObject star3;

	public GameObject bg;

	public GameObject forground;

	public GameObject icon_mode;

	public Sprite order_fulfillment;
	public Sprite boss_battle;
	public Sprite clear_juice;
	public Sprite popsicle;

	public GameObject P_star1;
	public GameObject P_star2;
	public GameObject P_star3;

	public Text title;
	public Text goal;

	public GameObject badge;
	public Sprite effect;

	GameObject panel1;

	public RuntimeAnimatorController animEff;

	void Awake()
	{
		star1.SetActive (false);
		star2.SetActive (false);
		star3.SetActive (false);
//        for (int i = 0; i < 320; i++ )
//        {
//            PlayerPrefs.SetInt("NumberStar" + i, UnityEngine.Random.Range(0,3));
//        }

	}
	// Use this for initialization
	void Start () {
		int numberStar = PlayerPrefs.GetInt ("NumberStar" + solevel.text.ToString ());
		ShowStar (numberStar);
		panel1 = GameObject.Find ("Canvas/Panel");
	}


	public void Click()
	{
		if(PlayerPrefs.GetString("Level"+(Int32.Parse(solevel.text.ToString())-1).ToString()).Equals("Finish"))
		{
			SoundController.sound.Click ();
			//PlayerPrefs.SetString("Level"+solevel.text.ToString(),"Finish");
			//LeaderBoardController.oldNumberClick = Contance.NUMBER_LEVEL;
			Contance.NUMBER_LEVEL = Int32.Parse(solevel.text.ToString());
			//Debug.Log(Contance.NUMBER_LEVEL+":"+PlayerPrefs.GetString("ModeLevel"+solevel.text.ToString()).ToString());
			//Debug.Log("Contance.NUMBER_LEVEL: " + Contance.NUMBER_LEVEL);
			LeaderBoardController.numberClick = Contance.NUMBER_LEVEL;
			//Debug.Log("numberClick: " + LeaderBoardController.numberClick);

			//panel1.SetActive(true);

            bg.SetActive(true);
			bg.GetComponent<LeaderBoardController> ().ShowLeaderBoard ();
			int number = PlayerPrefs.GetInt ("NumberStar" + solevel.text.ToString ());
			ShowP_Star(number);
			bg.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text = solevel.text;
			int numberModelevel = int.Parse(solevel.text.ToString())+1;
			ChangeMode(PlayerPrefs.GetString("ModeLevel"+numberModelevel));
            
            //iTween.MoveTo(bg,iTween.Hash("position",new Vector3(0,0,0),"time",0.5f));
            //RectTransform rtForg = forground.GetComponent<RectTransform>();
            //print(rtForg.position);
            //var pos = new Vector3(Screen.width/2f,Screen.height/2f, 0);
			panel1.gameObject.SetActive(true);
			panel1.GetComponent<Image>().enabled = true;
			bg.transform.GetChild (0).GetChild (0).position = new Vector3 (0,11f);
			iTween.MoveTo(bg.transform.GetChild(0).GetChild(0).gameObject, iTween.Hash("position", Vector3.zero, "time", 0.25f, "easetype", iTween.EaseType.easeOutBack));

			//ShowP_Star (PlayerPrefs.GetInt("NumberStar"+solevel.text.ToString()));
		}
	}

	void ChangeMode( string mode)
	{
//		if(solevel.text == "1")
//		{
//			mode = "order_fulfillment";
//		}

		if (mode.Equals ("order_fulfillment")) {
            bg.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Image>().sprite = order_fulfillment;
            bg.transform.GetChild(0).GetChild(0).GetChild(3).GetComponent<Text>().text = "Complete Order!";
		}else if (mode.Equals ("boss_battle")) {
            bg.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Image>().sprite = boss_battle;
            bg.transform.GetChild(0).GetChild(0).GetChild(3).GetComponent<Text>().text = "Splat the Boss!";
		}else if (mode.Equals ("clear_juice")) {
            bg.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Image>().sprite = clear_juice;
            bg.transform.GetChild(0).GetChild(0).GetChild(3).GetComponent<Text>().text = "Clear all the Crackers!";
		}else if (mode.Equals ("popsicle")) {
            bg.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Image>().sprite = popsicle;
            bg.transform.GetChild(0).GetChild(0).GetChild(3).GetComponent<Text>().text = "Find all the popsicles!";
		}

		Contance.MODE = mode;
	}

	void ShowStar(int numberstar)
	{
		badge.SetActive (false);
		if (numberstar == 3) 
		{
			star1.SetActive (true);
			star2.SetActive (true);
			star3.SetActive (true);
			//badge.SetActive (false);
			badge.transform.parent.gameObject.GetComponent<Animator>().enabled = false;
			//badge.transform.parent.gameObject.transform.localScale = Vector3.one;
		} 
		else if (numberstar == 2) 
		{
			star1.SetActive (true);
			star2.SetActive (true);
			//badge.SetActive (false);
			badge.transform.parent.gameObject.GetComponent<Animator>().enabled = false;
			//badge.transform.parent.gameObject.transform.localScale = Vector3.one;
		} 
		else if (numberstar == 1) 
		{
			star1.SetActive (true);
			//badge.SetActive (false);
			badge.transform.parent.gameObject.GetComponent<Animator>().enabled = false;
			//badge.transform.parent.gameObject.transform.localScale = Vector3.one;
		} 
		else 
		{//if(transform.GetChild(2).GetComponent<Image>().color.a == 0 && effect != null)
			//{
			int hLevel = PlayerPrefs.GetInt ("hlevel") + 1;
			//Debug.Log ("hLevel..."+hLevel);
			//Debug.Log ("solevel...aaa"+solevel.text);
			if(Int32.Parse(solevel.text.ToString()) == hLevel)
				{
					showBadge ();
				}
				//badge.SetActive(false);
//				badge.GetComponent<Image>().sprite = effect;
//				badge.GetComponent<Image>().color = new Color(badge.GetComponent<Image>().color.r,badge.GetComponent<Image>().color.g,badge.GetComponent<Image>().color.b,0.5f);
//				badge.transform.parent.gameObject.GetComponent<Animator>().enabled = true;
		}
	}


	void showBadge()
	{
		badge.SetActive(true);
		badge.GetComponent<Image>().sprite = effect;
		badge.GetComponent<Image>().color = new Color(badge.GetComponent<Image>().color.r,badge.GetComponent<Image>().color.g,badge.GetComponent<Image>().color.b,0.5f);
		badge.transform.parent.gameObject.GetComponent<Animator>().enabled = true;
		badge.AddComponent<Animator> ();
		badge.GetComponent<Animator> ().runtimeAnimatorController = animEff;
	}

	void ShowP_Star(int numberstar)
	{

		if(numberstar == 3)
		{
            bg.transform.GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(1).gameObject.SetActive(true);
            bg.transform.GetChild(0).GetChild(0).GetChild(2).GetChild(1).GetChild(1).gameObject.SetActive(true);
            bg.transform.GetChild(0).GetChild(0).GetChild(2).GetChild(2).GetChild(1).gameObject.SetActive(true);

		}else if(numberstar == 2)
		{
            bg.transform.GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(1).gameObject.SetActive(true);
            bg.transform.GetChild(0).GetChild(0).GetChild(2).GetChild(1).GetChild(1).gameObject.SetActive(true);
		}else if(numberstar == 1)
		{
            bg.transform.GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(1).gameObject.SetActive(true);
		}
	}
}

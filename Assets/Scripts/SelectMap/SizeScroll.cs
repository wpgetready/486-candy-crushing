﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using SimpleJSON;
using System.Collections.Generic;
using System.IO;

public class SizeScroll : MonoBehaviour
{
	public GameObject[] itemBG;
	public GameObject itemBG1;
	public ScrollRect scrollRect;
	public RuntimeAnimatorController animButton;
	RectTransform rt;
	private bool load = false;
	//int i = 0;

	public GameObject[] item;
	public Sprite it1;
	public Sprite it2;
	public Sprite it3;
	public Sprite it4;
	public Sprite it5;
	public Sprite it6;
	public Sprite it7;
	public Sprite it8;
	public TextAsset json;
	private List<GameObject> ItemAdd;
	public int level;
	public static bool isload;
	public GameObject PopupLevel;
	public GameObject forground;
	public GameObject P_star1;
	public GameObject P_star2;
	public GameObject P_star3;
	public GameObject loading;
	public LoadingEffect loadingEffect;
	private JSONNode ST = "";
	private JSONNode N = "";
	public GameObject panel1;
	public Sprite[] badge;
	GameObject show_Goal;
	public int maxLevel;

	void Start ()
	{
		if(SoundController.sound.audioSource.isPlaying == true)
		{
			SoundController.sound.audioSource.Stop ();
		}

		MusicController.music.PlayOnMap ();

		if(PopupLevel != null)
		{
			PopupLevel.SetActive (true);
		}

		if(AdsControl.runAdFirstTime == 0)
		{
			AdsControl.instance.adcolonyController.Initialize ();
			AdsControl.instance.vungleControl.VungleInit ();
			AdsControl.runAdFirstTime = 1;
		}
		Contance.NUMBER_LEVEL = PlayerPrefs.GetInt ("hlevel");
		level = Contance.NUMBER_LEVEL+1;
		Debug.Log ("NUMBER_LEVEL"+level);
		if(level == 0)
		{
			level = 1;
		}
		ST = JSON.Parse (json.text);
		N = JSONNode.Parse (ST ["data"].ToString ());

		isload = false;
		rt = gameObject.GetComponent<RectTransform> ();

		// Add Coins
//		PlayerPrefs.SetInt("coins",1000);
		// Add Coins
        
		// UNLOCK LEVEL
//		for (int i = 0; i <= 200; i++)
//			PlayerPrefs.SetString ("Level"+i, "Finish");
		// UNLOCK LEVEL

				PlayerPrefs.SetString ("Level0", "Finish");

		PlayerPrefs.SetInt ("LevelStart", 3);	
		SortBG (level);
		scrollRect.verticalNormalizedPosition = 0.5f;

		float poschild = GameObject.Find (level.ToString ()).GetComponent<RectTransform> ().anchoredPosition.y;

		float posParent = GameObject.Find (level.ToString ()).transform.parent.gameObject.GetComponent<RectTransform> ().anchoredPosition.y;

		float posRT = poschild + posParent + 365;

		if (Contance.NUMBER_LEVEL >= 8) 
		{
			rt.anchoredPosition = new Vector3 (0, -posRT, 0);
			//rt.anchoredPosition = new Vector3 (0, -posParent, 0);
		}
		else 
		{
			rt.anchoredPosition = new Vector3 (0, 18391, 0);

		}

		if(PlayerPrefs.GetString("SOUND") == "")
		{
			PlayerPrefs.SetString ("SOUND", "ON");
			PlayerPrefs.SetString ("MUSIC", "ON");
			PlayerPrefs.Save ();
		}

		AdsControl.instance.addCoins.SetTag ();
	}

	int count = 0;

	void CreateBG (int i, bool value = false)
	{
		GameObject obj = Instantiate (itemBG [i]) as GameObject;
		obj.transform.SetParent (gameObject.transform, false);
		obj.transform.SetSiblingIndex (0);
		if(value == true)
		{
			show_Goal = obj;
		}
	}

	private void SortBG(int levelFinish)
	{
			GameObject obj = Instantiate(itemBG1) as GameObject;
			obj.transform.SetParent(gameObject.transform,false);
			PlayerPrefs.SetInt("LevelStart", 1);

		count = maxLevel/9+1;

			for(int i = 1; i < count; i++)
			{
				int j= i%7;
			if(i != count-1)
			{
				CreateBG(j);
			}
			else
			{
				CreateBG(j, true);
			}

			}

			//==================add data=====================
			int start = 1;
			ReadDataJson(start);
	}

	void LoadMoreData ()
	{
		rt.pivot = new Vector2 (0.5f, 0);
		scrollRect.verticalNormalizedPosition = 0.9999f;

		ItemAdd = new List<GameObject> ();
		string st = gameObject.transform.GetChild (0).GetChild (8).GetChild (1).GetChild (0).GetComponent<Text> ().text.ToString ();
		int sta = Int32.Parse (st);

		for (int i = 0; i < 6; i++) //4
		{
			string s = gameObject.transform.GetChild (0).GetChild (8).GetChild (1).GetChild (0).GetComponent<Text> ().text.ToString ();
			int levelMax = Int32.Parse (s);
			if (levelMax >= maxLevel) 
			{
				PlayerPrefs.SetInt ("LevelEnd", maxLevel);
				break;
			} 
			else 
			{
				GameObject obj = Instantiate (gameObject.transform.GetChild (6).transform.gameObject) as GameObject;//3
				obj.transform.SetParent (gameObject.transform, false);
				obj.transform.SetSiblingIndex (0);

				for (int j = 0; j < 9; j++) 
				{
					obj.transform.GetChild (j).name = (levelMax + j + 1).ToString ();
					obj.transform.GetChild (j).GetChild (1).GetChild (0).GetComponent<Text> ().text = (levelMax + j + 1).ToString ();
					if ((levelMax + j + 1) <= maxLevel) 
					{
						ItemAdd.Add (obj.transform.GetChild (j).gameObject);
					} 
					else 
					{
						PlayerPrefs.SetInt ("LevelEnd", maxLevel);
						//Finish (obj);
						//break;
					}
				}
			}
		}

		ReadDataNodeMoreJson (json.text, sta);

		StartCoroutine (timeHandle ());
	}

	void LoadPreData ()
	{
		rt.pivot = new Vector2 (0.5f, 1);
		scrollRect.verticalNormalizedPosition = 0.0001f;
		ItemAdd = new List<GameObject> ();
		string st = gameObject.transform.GetChild (gameObject.transform.childCount - 1).GetChild (0).GetChild (1).GetChild (0).GetComponent<Text> ().text.ToString ();
		int sta = Int32.Parse (st);

		for (int i = 0; i < 6; i++) //4
		{
			string s = gameObject.transform.GetChild (gameObject.transform.childCount - 1).GetChild (0).GetChild (1).GetChild (0).GetComponent<Text> ().text.ToString ();
			int levelMin = Int32.Parse (s);

			int end = gameObject.transform.childCount - 6;//4
			if (levelMin > 10) {
				GameObject obj = Instantiate (gameObject.transform.GetChild (end).gameObject) as GameObject;
				obj.transform.SetParent (gameObject.transform, false);
				for (int j = 0; j < 9; j++) {
					obj.transform.GetChild (8 - j).name = (levelMin - j - 1).ToString ();
					obj.transform.GetChild (8 - j).GetChild (1).GetChild (0).GetComponent<Text> ().text = (levelMin - j - 1).ToString ();
					ItemAdd.Add (obj.transform.GetChild (8 - j).gameObject);
				}
				if (obj.transform.childCount > 9)
					obj.transform.GetChild (9).gameObject.SetActive (false);
			} else {
				GameObject obj = Instantiate (itemBG1) as GameObject;
				obj.transform.SetParent (gameObject.transform, false);
				for (int j = 0; j < 9; j++) {
					obj.transform.GetChild (8 - j).name = (levelMin - j - 1).ToString ();
					obj.transform.GetChild (8 - j).GetChild (1).GetChild (0).GetComponent<Text> ().text = (levelMin - j - 1).ToString ();
					ItemAdd.Add (obj.transform.GetChild (8 - j).gameObject);

					if (levelMin - j - 1 == 1) {
						PlayerPrefs.SetInt ("LevelStart", 1);
						break;
					}
				}
				if (obj.transform.childCount > 9)
					obj.transform.GetChild (9).gameObject.SetActive (false);
			}
            
			if (PlayerPrefs.GetInt ("LevelStart") == 1)
				break;
		}


		ReadDataNodePreJson (json.text, sta);

		StartCoroutine (timeHandle ());
	}

	void ReadDataNodeMoreJson (string json, int start)
	{
		for (int i = 0; i < ItemAdd.Count; i++) 
		{
			ItemAdd [i].GetComponent<ClickItem> ().bg = PopupLevel;
			ChangeMode (N ["level"] [start + i] ["mode"].Value, ItemAdd [i], start + i);
		}
	}

	void ReadDataNodePreJson (string json, int start)
	{
		for (int i = 0; i < ItemAdd.Count; i++)
		{
			ItemAdd [i].GetComponent<ClickItem> ().bg = PopupLevel;
			ChangeMode (N ["level"] [start - i - 2] ["mode"].Value, ItemAdd [i], start - i - 2);
		}
	}

	void ReadDataJson (int start)
	{
		ItemInMap (start);
	}

	int countItem = -1;
	private void ItemInMap (int start)
	{
		ItemAdd = new List<GameObject> ();
		for (int i = gameObject.transform.childCount - 1; i >=0; i--)
		{
			for (int j = 0; j < 9; j++)
			{
				ItemAdd.Add (gameObject.transform.GetChild (i).GetChild (j).gameObject);
				countItem++;

				ItemAdd [countItem].name = N ["level"] [countItem + start - 1] ["numericId"].AsInt.ToString ();
				ItemAdd [countItem].transform.GetChild (2).GetChild (0).gameObject.GetComponent<Text> ().text = (countItem + start).ToString ();
				ItemAdd [countItem].GetComponent<ClickItem> ().bg = PopupLevel;
				ChangeMode (N ["level"] [countItem + start - 1] ["mode"].Value, ItemAdd [countItem], countItem+1);

				if (countItem == Contance.NUMBER_LEVEL) 
				{
					ItemAdd [countItem].transform.GetChild(2).transform.localScale = new Vector3(1.3f,1.3f);

					string modeGame = N ["level"] [countItem + start - 1] ["mode"].Value;
					if (modeGame.Equals ("order_fulfillment"))
					{
						ItemAdd [countItem].transform.GetChild (1).gameObject.GetComponent<Image> ().sprite = it3;
					}
					if (modeGame.Equals ("boss_battle"))
					{
						ItemAdd [countItem].transform.GetChild (1).gameObject.GetComponent<Image> ().sprite = it5;
					} 
					if (modeGame.Equals ("clear_juice"))
					{
						ItemAdd [countItem].transform.GetChild (1).gameObject.GetComponent<Image> ().sprite = it1;
					} 
					if (modeGame.Equals ("popsicle"))
					{
						ItemAdd [countItem].transform.GetChild (1).gameObject.GetComponent<Image> ().sprite = it7;
					}
				}

				if (countItem == PlayerPrefs.GetInt ("hlevel")) 
				{
					ItemAdd [countItem].GetComponent<Animator> ().runtimeAnimatorController = animButton;
					ItemAdd [countItem].GetComponent<Animator> ().enabled = true;
				}
//
				PlayerPrefs.SetInt ("LevelEnd", (count + start));
				if(countItem == maxLevel-1)
				{
					Finish (show_Goal);
				}

				if(countItem > maxLevel-1)
				{	
					ItemAdd [countItem].SetActive (false);
				}
			}
		}
	}

	void badgeButton(GameObject go, Sprite badge)
	{
		go.transform.GetChild (2).gameObject.GetComponent<Image> ().sprite = badge;
	}

	void ChangeMode (string mode, GameObject obj, int i)
	{
		int numberStar = PlayerPrefs.GetInt ("NumberStar" + i);
		if (mode.Equals ("order_fulfillment")) {
			//Debug.Log("order_fulfillment");
			obj.transform.GetChild (1).gameObject.GetComponent<Image> ().sprite = it4;
			if (PlayerPrefs.GetString ("Level" + i).Equals ("Finish")) {
				if (numberStar < 3) {
					obj.transform.GetChild (2).gameObject.GetComponent<Image> ().sprite = it3;
				} else {
					badgeButton (obj, badge [0]);
				}
			} else {
				obj.transform.GetChild (2).gameObject.GetComponent<Image> ().color = new Color (1, 1, 1, 0);
			}
			
			PlayerPrefs.SetString ("ModeLevel" + (i + 1).ToString (), mode);
			PlayerPrefs.Save ();
		} else {
			if (mode.Equals ("boss_battle")) {//Debug.Log("boss_battle");
				obj.transform.GetChild (1).gameObject.GetComponent<Image> ().sprite = it6;
				if (PlayerPrefs.GetString ("Level" + i).Equals ("Finish")) {
					if (numberStar < 3) {
						obj.transform.GetChild (2).gameObject.GetComponent<Image> ().sprite = it5;
					} else {
						badgeButton (obj, badge [1]);
					}
				} else {
					obj.transform.GetChild (2).gameObject.GetComponent<Image> ().color = new Color (1, 1, 1, 0);
				}
				
				PlayerPrefs.SetString ("ModeLevel" + (i + 1).ToString (), mode);
				PlayerPrefs.Save ();
			} else {
				if (mode.Equals ("clear_juice")) {//Debug.Log("clear_juice");
					obj.transform.GetChild (1).gameObject.GetComponent<Image> ().sprite = it2;
					if (PlayerPrefs.GetString ("Level" + i).Equals ("Finish")) {
						if (numberStar < 3) {
							obj.transform.GetChild (2).gameObject.GetComponent<Image> ().sprite = it1;
						} else {
							badgeButton (obj, badge [2]);
						}
					} else {
						obj.transform.GetChild (2).gameObject.GetComponent<Image> ().color = new Color (1, 1, 1, 0);
					}
					
					PlayerPrefs.SetString ("ModeLevel" + (i + 1).ToString (), mode);
					PlayerPrefs.Save ();
				} else if (mode.Equals ("popsicle")) {//Debug.Log("popsicle");
					obj.transform.GetChild (1).gameObject.GetComponent<Image> ().sprite = it8;
					if (PlayerPrefs.GetString ("Level" + i).Equals ("Finish")) {
						if (numberStar < 3) {
							obj.transform.GetChild (2).gameObject.GetComponent<Image> ().sprite = it7;
						} else {
							badgeButton (obj, badge [3]);
						}
					} else {
						obj.transform.GetChild (2).gameObject.GetComponent<Image> ().color = new Color (1, 1, 1, 0);
					}
					
					PlayerPrefs.SetString ("ModeLevel" + (i + 1).ToString (), mode);
					PlayerPrefs.Save ();
				}
			}
		}
	}

	public GameObject goal;

	private void Finish (GameObject obj)
	{
		goal.SetActive (true);
		goal.transform.SetParent (obj.transform, true);
		//goal.transform.localPosition = new Vector3 (0, -9, 0);
		goal.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
	}



	public void Btn_Exit ()
	{
		SoundController.sound.Click ();
		panel1.SetActive (true);
		panel1.GetComponent<Image>().enabled = false;
		iTween.MoveTo (PopupLevel.transform.GetChild (0).GetChild (0).gameObject, iTween.Hash ("position", new Vector3 (0, 11f, 0), "time", 0.35f, "easetype", iTween.EaseType.easeOutBack, "oncomplete", "SetActive", "oncompletetarget", gameObject));
	}

	public void SetActive ()
	{
		P_star1.SetActive (false);
		P_star2.SetActive (false);
		P_star3.SetActive (false);

		PopupLevel.SetActive (false);
	}

	public void Btn_Play ()
	{
		SoundController.sound.Click ();
		iTween.MoveTo (forground, iTween.Hash ("position", new Vector3 (0, -11f, 0), "time", 0.25f, "easetype", iTween.EaseType.easeInBack, "oncomplete", "Play", "oncompletetarget", gameObject));
	}

	public void Play ()
	{
		P_star1.SetActive (false);
		P_star2.SetActive (false);
		P_star3.SetActive (false);
		PopupLevel.SetActive (false);

		loading.SetActive (true);
		StartCoroutine (LoadingHandler.loading.HandleLoading (GameConstants.GAMEPLAY_SCENE_NAME));
	}

	IEnumerator timeHandle ()
	{
		yield return new WaitForSeconds (1);
		loadingEffect.loading = false;
		isload = false;
	}
}
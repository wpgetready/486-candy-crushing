﻿using UnityEngine;
using System.Collections;
using System;

public class NotifyRwd : MonoBehaviour 
{
	public GameObject notifyObj;
	public GameObject circle;
	int dateCount;
	DateTime oldDate;
	string newDates;

	void Awake()
	{
		dateCount = PlayerPrefs.GetInt ("datecount");
		notifyObj.SetActive (false);
	}

	IEnumerator Start()
	{
		while(true)
		{
			dayCheck ();
			yield return new WaitForSeconds (10);
		}
	}

	void dayCheck()
	{
		string stringDate = PlayerPrefs.GetString("PlayDate");
		//Debug.Log (stringDate);
		oldDate = new DateTime ();

		if (stringDate == "" || stringDate == null) 
		{
			oldDate =  DateTime.Now;
		} 
		else 
		{
			oldDate =  Convert.ToDateTime(stringDate);
		}

		DateTime newDate = DateTime.Now;
		newDates = Convert.ToString (newDate);

		TimeSpan difference = newDate.Subtract(oldDate);

		//Debug.Log ("difference"+difference.ToString());

		if (difference.Days >= 1 || stringDate == "") 
		{
			notifyObj.SetActive (true);
			circle.SetActive (true);
			//LeanTween.scale (circle, Vector3.one * 0.55f, 0.35f).setLoopClamp ();
			iTween.ScaleTo (circle, iTween.Hash ("scale",Vector3.one*0.55f,"time",0.25f,"looptype",iTween.LoopType.loop));
		}
		else 
		{
			notifyObj.SetActive (false);
			circle.SetActive (false);
		}
	}
}

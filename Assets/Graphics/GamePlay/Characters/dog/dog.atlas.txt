
dog.png
size: 898,317
format: RGBA8888
filter: Linear,Linear
repeat: none
L_ban tay
  rotate: false
  xy: 832, 88
  size: 64, 44
  orig: 64, 44
  offset: 0, 0
  index: -1
L_tay
  rotate: false
  xy: 323, 27
  size: 64, 81
  orig: 64, 81
  offset: 0, 0
  index: -1
L_tayao
  rotate: true
  xy: 612, 37
  size: 74, 55
  orig: 74, 55
  offset: 0, 0
  index: -1
R_ban tay
  rotate: false
  xy: 806, 12
  size: 64, 45
  orig: 64, 45
  offset: 0, 0
  index: -1
R_tay
  rotate: false
  xy: 549, 28
  size: 61, 81
  orig: 61, 81
  offset: 0, 0
  index: -1
R_tayao
  rotate: true
  xy: 669, 37
  size: 74, 54
  orig: 74, 54
  offset: 0, 0
  index: -1
body
  rotate: true
  xy: 2, 110
  size: 205, 374
  orig: 205, 374
  offset: 0, 0
  index: -1
face_idle
  rotate: true
  xy: 736, 134
  size: 181, 153
  orig: 181, 153
  offset: 0, 0
  index: -1
mat mo_idle
  rotate: true
  xy: 790, 59
  size: 73, 40
  orig: 73, 40
  offset: 0, 0
  index: -1
mat nham_idle
  rotate: false
  xy: 725, 6
  size: 79, 44
  orig: 79, 44
  offset: 0, 0
  index: -1
matnham_smile_01
  rotate: true
  xy: 736, 52
  size: 80, 52
  orig: 80, 52
  offset: 0, 0
  index: -1
mom_idle
  rotate: false
  xy: 162, 15
  size: 159, 93
  orig: 159, 93
  offset: 0, 0
  index: -1
mom_smile_01
  rotate: false
  xy: 2, 14
  size: 158, 94
  orig: 158, 94
  offset: 0, 0
  index: -1
mom_smile_02
  rotate: false
  xy: 389, 2
  size: 158, 107
  orig: 158, 107
  offset: 0, 0
  index: -1
mu
  rotate: true
  xy: 378, 111
  size: 204, 195
  orig: 204, 195
  offset: 0, 0
  index: -1
muao
  rotate: true
  xy: 575, 113
  size: 202, 159
  orig: 202, 159
  offset: 0, 0
  index: -1

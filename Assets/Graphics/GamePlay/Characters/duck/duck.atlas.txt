
duck.png
size: 513,527
format: RGBA8888
filter: Linear,Linear
repeat: none
L_bantay
  rotate: false
  xy: 455, 3
  size: 33, 50
  orig: 33, 50
  offset: 0, 0
  index: -1
L_bantay_idle
  rotate: false
  xy: 229, 2
  size: 45, 31
  orig: 45, 31
  offset: 0, 0
  index: -1
L_mat_smile
  rotate: true
  xy: 417, 55
  size: 52, 61
  orig: 52, 61
  offset: 0, 0
  index: -1
L_tay ao
  rotate: true
  xy: 306, 102
  size: 73, 95
  orig: 73, 95
  offset: 0, 0
  index: -1
L_tay_1
  rotate: true
  xy: 229, 35
  size: 54, 64
  orig: 54, 64
  offset: 0, 0
  index: -1
R_bantay
  rotate: false
  xy: 472, 111
  size: 35, 49
  orig: 35, 49
  offset: 0, 0
  index: -1
R_bantay_idle
  rotate: true
  xy: 482, 377
  size: 45, 29
  orig: 45, 29
  offset: 0, 0
  index: -1
R_mat_smile
  rotate: false
  xy: 428, 247
  size: 51, 63
  orig: 51, 63
  offset: 0, 0
  index: -1
R_tay ao
  rotate: false
  xy: 229, 91
  size: 75, 100
  orig: 75, 100
  offset: 0, 0
  index: -1
R_tay_1
  rotate: true
  xy: 403, 109
  size: 51, 67
  orig: 51, 67
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 2, 162
  size: 195, 363
  orig: 195, 363
  offset: 0, 0
  index: -1
dayno
  rotate: false
  xy: 424, 162
  size: 87, 81
  orig: 87, 81
  offset: 0, 0
  index: -1
face
  rotate: false
  xy: 199, 347
  size: 147, 178
  orig: 147, 178
  offset: 0, 0
  index: -1
face_smile
  rotate: true
  xy: 2, 6
  size: 154, 183
  orig: 154, 183
  offset: 0, 0
  index: -1
matmo
  rotate: false
  xy: 309, 177
  size: 113, 66
  orig: 113, 66
  offset: 0, 0
  index: -1
matnham
  rotate: false
  xy: 309, 245
  size: 117, 65
  orig: 117, 65
  offset: 0, 0
  index: -1
mom_1_smile
  rotate: true
  xy: 187, 32
  size: 128, 40
  orig: 128, 40
  offset: 0, 0
  index: -1
mom_2_smile
  rotate: false
  xy: 348, 312
  size: 132, 110
  orig: 132, 110
  offset: 0, 0
  index: -1
mom_idle_1
  rotate: false
  xy: 306, 60
  size: 109, 40
  orig: 109, 40
  offset: 0, 0
  index: -1
mom_idle_2
  rotate: false
  xy: 295, 15
  size: 109, 43
  orig: 109, 43
  offset: 0, 0
  index: -1
mu_idle
  rotate: true
  xy: 199, 193
  size: 152, 108
  orig: 152, 108
  offset: 0, 0
  index: -1
nomu
  rotate: false
  xy: 406, 9
  size: 47, 44
  orig: 47, 44
  offset: 0, 0
  index: -1
vanhmu
  rotate: false
  xy: 348, 424
  size: 150, 101
  orig: 150, 101
  offset: 0, 0
  index: -1

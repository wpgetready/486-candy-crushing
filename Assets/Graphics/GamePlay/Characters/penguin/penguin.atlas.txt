
penguin.png
size: 735,529
format: RGBA8888
filter: Linear,Linear
repeat: none
L_bantay
  rotate: false
  xy: 639, 112
  size: 75, 54
  orig: 75, 54
  offset: 0, 0
  index: -1
L_tay
  rotate: true
  xy: 156, 13
  size: 60, 90
  orig: 60, 90
  offset: 0, 0
  index: -1
L_tayao
  rotate: false
  xy: 2, 2
  size: 75, 71
  orig: 75, 71
  offset: 0, 0
  index: -1
R_bantay
  rotate: false
  xy: 653, 168
  size: 76, 55
  orig: 76, 55
  offset: 0, 0
  index: -1
R_tay
  rotate: false
  xy: 577, 76
  size: 60, 90
  orig: 60, 90
  offset: 0, 0
  index: -1
R_tayao
  rotate: false
  xy: 79, 2
  size: 75, 71
  orig: 75, 71
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 2, 164
  size: 232, 363
  orig: 232, 363
  offset: 0, 0
  index: -1
bonghong
  rotate: false
  xy: 361, 5
  size: 97, 88
  orig: 97, 88
  offset: 0, 0
  index: -1
bongtrang
  rotate: false
  xy: 460, 4
  size: 62, 43
  orig: 62, 43
  offset: 0, 0
  index: -1
duoimu
  rotate: false
  xy: 669, 349
  size: 64, 45
  orig: 64, 45
  offset: 0, 0
  index: -1
face_idle
  rotate: false
  xy: 465, 348
  size: 202, 179
  orig: 202, 179
  offset: 0, 0
  index: -1
face_smile_01
  rotate: true
  xy: 236, 95
  size: 178, 186
  orig: 178, 186
  offset: 0, 0
  index: -1
face_smile_02
  rotate: true
  xy: 465, 168
  size: 178, 186
  orig: 178, 186
  offset: 0, 0
  index: -1
khan
  rotate: false
  xy: 2, 75
  size: 218, 87
  orig: 218, 87
  offset: 0, 0
  index: -1
matcuoi_smile_02
  rotate: true
  xy: 424, 156
  size: 117, 32
  orig: 117, 32
  offset: 0, 0
  index: -1
matmo_idle
  rotate: false
  xy: 458, 111
  size: 117, 55
  orig: 117, 55
  offset: 0, 0
  index: -1
matnham_idle
  rotate: true
  xy: 653, 225
  size: 121, 56
  orig: 121, 56
  offset: 0, 0
  index: -1
matnham_smile_01
  rotate: false
  xy: 460, 49
  size: 115, 60
  orig: 115, 60
  offset: 0, 0
  index: -1
mom_idle
  rotate: false
  xy: 524, 14
  size: 48, 33
  orig: 48, 33
  offset: 0, 0
  index: -1
mom_smile_01
  rotate: false
  xy: 639, 59
  size: 68, 51
  orig: 68, 51
  offset: 0, 0
  index: -1
mom_smile_02
  rotate: false
  xy: 248, 6
  size: 111, 87
  orig: 111, 87
  offset: 0, 0
  index: -1
mu
  rotate: false
  xy: 236, 275
  size: 227, 252
  orig: 227, 252
  offset: 0, 0
  index: -1
vatkhan
  rotate: false
  xy: 669, 396
  size: 51, 131
  orig: 51, 131
  offset: 0, 0
  index: -1
